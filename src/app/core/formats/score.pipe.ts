import { ThrowStmt } from '@angular/compiler';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'score'
})
export class ScorePipe implements PipeTransform {

  transform(value = null): any {
    var grade = [
      {
        nama: 'A+',
        value: '95'
      },
      {
        nama: 'A',
        value: '90'
      },
      {
        nama: 'A-',
        value: '85'
      },
      {
        nama: 'B+',
        value: '80'
      },
      {
        nama: 'B',
        value: '75'
      },
      {
        nama: 'B-',
        value: '70'
      },
      {
        nama: 'C+',
        value: '67'
      },
      {
        nama: 'C',
        value: '65'
      },
      {
        nama: 'C-',
        value: '60'
      },
    ];

    if(value == null) {
      return "-";
    }

    // convert value to grade
    let gradeValue = "";
    // invert
    grade.reverse();
    grade.forEach(element => {
      if (value >= element.value) {
        gradeValue = element.nama;
      }
    }
    );
    return gradeValue;
  }
}
