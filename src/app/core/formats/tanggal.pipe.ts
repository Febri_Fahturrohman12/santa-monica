import { Pipe, PipeTransform } from '@angular/core';
import { formatFullDate, formatTime, formatDay, formatMonth, formatDate } from 'node-format-date';

@Pipe({
  name: 'tanggal'
})
export class TanggalPipe implements PipeTransform {

  transform(value: string, is_time: boolean = false, is_second: boolean = false, is_format = false, is_hour: boolean = false): any {
    // return formatDate(new Date(value));
    let d = new Date(value);
    let ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
    let mo = new Intl.DateTimeFormat('en', { month: 'short' }).format(d);
    let moNumeric = new Intl.DateTimeFormat('en', { month: 'numeric' }).format(d);
    let da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);
    let times = Intl.DateTimeFormat('en-US', {
      hour: '2-digit',
      minute: '2-digit',
      hour12: false
    }).format(d);

    let times_det = Intl.DateTimeFormat('en-US', {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit',
      hour12: false
    }).format(d);

    if (is_format == false) {
      if (is_time) {
        return `${da} ${mo} ${ye}` + ' ' + `${times}`;
      }

      if (is_second) {
        return `${da} ${mo} ${ye}` + ' ' + `${times_det}`;
      }
      return `${da} ${mo} ${ye}`;
    } else {
      if (is_hour == false) {
        return `${da}-${moNumeric}-${ye}`;
      } else {
        return `${da}-${moNumeric}-${ye} ${times}`;
      }
    }
  }

}
