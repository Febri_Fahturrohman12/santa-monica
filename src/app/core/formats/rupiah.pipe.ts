import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rupiah'
})
export class RupiahPipe implements PipeTransform {

  transform(value: number, is_rp: boolean = false, is_min: boolean = false): any {
    if (value == null) return null
    if (value == null) return null
    var separator = ''
    var fixRupiah = ''
    var number_string = value.toString().replace(/[^,\d]/g, '').toString(),
      split = number_string.split(','),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);
    if (ribuan) {
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }
    rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
    if (rupiah) {
      if (is_min) {
        return is_rp ? `-Rp${rupiah}` : `-${rupiah}`;
      } else {
        return is_rp ? `Rp${rupiah}` : `${rupiah}`;
      }
    }
    return '';
  }
}
