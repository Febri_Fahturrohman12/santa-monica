import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'periode'
})
export class PeriodePipe implements PipeTransform {


  transform(value: string, ...args: any[]): any {
    return value != '' ? moment(value).format('YY/MM/DD') : '';
  }


}
