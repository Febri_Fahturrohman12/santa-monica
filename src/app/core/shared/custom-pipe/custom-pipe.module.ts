import { NgModule } from '@angular/core';
import { RupiahPipe } from '../../formats/rupiah.pipe';

@NgModule({
  declarations: [RupiahPipe],
  exports: [RupiahPipe],
  imports: []
})
export class CustomPipeModule { }
