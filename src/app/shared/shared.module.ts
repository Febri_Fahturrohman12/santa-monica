import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MonthDatePickerComponent } from './month-date-picker/month-date-picker.component';

@NgModule({
    declarations: [
        MonthDatePickerComponent,
    ],
    exports: [
        MonthDatePickerComponent,
    ],
    imports: [
        CommonModule
    ]
})
export class SharedModule { }
