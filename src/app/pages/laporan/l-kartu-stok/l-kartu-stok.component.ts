import {Component, OnInit} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {LandaService} from "../../../core/services/landa.service";
import {NgbCalendar, NgbModal, NgbTimepickerConfig} from "@ng-bootstrap/ng-bootstrap";
import * as moment from 'moment';


@Component({
    selector: 'app-l-kartu-stok',
    templateUrl: './l-kartu-stok.component.html',
    styleUrls: ['./l-kartu-stok.component.scss']
})
export class LKartuStokComponent implements OnInit {
    breadCrumbItems: Array<{}>;
    pageTitle: string;
    is_tampilkan: boolean;
    listData: any = [];
    listGetBarang: any = [];
    daterange: any = {};

    apiUrl = environment.apiURL;
    model: any = {};

    constructor(private landaService: LandaService, private modalService: NgbModal) {
    }

    ngOnInit() {
        this.pageTitle = 'Kartu Stok';
        this.is_tampilkan = false;

        this.getBarang();
        this.empty();
        this.defaultDateRange();
        // this.model.periode = moment().subtract(1, 'days').format('YYYY-MM-DD') + '-' + moment().format('YYYY-MM-DD');

    }

    defaultDateRange() {
        const end = moment();
        const start = moment().subtract(1, 'days');
        this.daterange.start = start.format('YYYY-MM-DD');
        this.daterange.end = end.format('YYYY-MM-DD');
        this.model.periode = moment().subtract(1, 'days').format('DD-MM-YYYY') + '-' + moment().format('DD-MM-YYYY');
    }

    getBarang() {
        this.landaService.DataGet('/t_kasir/getBarang', {}).subscribe((res: any) => {
            this.listGetBarang = res.data.list;
        })
    }

    empty() {
        this.listData = [];
        this.model = {
            barang_id : null
        };
    }

    selectedDate(value: any, datepicker?: any) {
        datepicker.start = value.start;
        datepicker.end = value.end;
        this.daterange.start = value.start;
        this.daterange.end = value.end;
        this.daterange.label = value.label;
    }

    options: any = {
        locale: {
            format: "DD-MM-YYYY",
        },
        alwaysShowCalendars: true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'day'), moment()],
            'Last 7 Days': [moment().subtract(7, 'day'), moment()],
            'Last 30 Days': [moment().subtract(30, 'day'), moment()],
            'This Month': [moment().startOf("month"), moment().endOf("month")],
            'Last Month': [moment().subtract(1, 'month'), moment()],
            'Last 3 Months': [moment().subtract(4, 'month'), moment()]
        }
        // startDate: moment().startOf("month").format("DD-MM-YYYY"),
        // endDate: moment(this.future_date).endOf("month").format("DD-MM-YYYY"),
    };


    tampilkan() {
        if (this.model.m_barang_id !== undefined && this.model.m_barang_id !== null) {
            const data = {
                periode_mulai: 'null',
                periode_selesai: 'null',
                m_barang_id: this.model.m_barang_id
            };


            if (this.daterange.start !== undefined && this.daterange.end !== undefined) {
                data.periode_mulai = moment(this.daterange.start).format('YYYY-MM-DD');
                data.periode_selesai = moment(this.daterange.end).format('YYYY-MM-DD');
            }

            this.landaService.DataGet('/l_kartu_stok/index', data).subscribe((res: any) => {
                if (res.status_code === 200) {
                    this.listData = res.data.data.detail;
                    this.is_tampilkan = true;
                } else {
                    this.landaService.alertError('Sorry', 'Empty data');
                    this.is_tampilkan = false;
                }
            });
        } else {
            this.landaService.alertError('Sorry', 'Select the filter first');
        }
    }


    export(val) {
        //window.open(this.apiUrl + '/l_leaderboard_team/export?id=' + val, '_blank'); endpoint lama
        window.open(this.apiUrl + '/l_leaderboard_team/index?m_event_id=' + this.model.m_event_id + "&short_by=" + this.model.short_by + "&m_event_race_id=" + this.model.m_event_race_id + "&is_export=1", '_blank');
    }

    reset_tampilan() {
        this.is_tampilkan = false;
    }
}
