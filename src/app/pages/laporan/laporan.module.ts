// CORE
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomPipeModule } from 'src/app/core/shared/custom-pipe/custom-pipe.module';

// LIBRARY
import { NgbDropdownModule, NgbNavModule, NgbDatepickerModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { DataTablesModule } from 'angular-datatables';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { ArchwizardModule } from 'angular-archwizard';
import { UiSwitchModule } from 'ngx-ui-switch';
import { Daterangepicker } from 'ng2-daterangepicker';

// APP
import { UIModule } from '../../layouts/shared/ui/ui.module';
import { SharedModule } from '../../shared/shared.module';
import { LaporanRoutingModule } from './laporan-routing.module';
import { ChartsModule } from 'ng2-charts';
import { AddonComponent } from './addon/addon.component';
import { LPenjualanComponent } from './l-penjualan/l-penjualan.component';
import { LKomisiComponent } from './l-komisi/l-komisi.component';
import { LCashflowPendapatanComponent } from './l-cashflow-pendapatan/l-cashflow-pendapatan.component';
import { LCashflowPengambilanKasComponent } from './l-cashflow-pengambilan-kas/l-cashflow-pengambilan-kas.component';
import { LRekapCashflowComponent } from './l-rekap-cashflow/l-rekap-cashflow.component';
import { LKartuStokComponent } from './l-kartu-stok/l-kartu-stok.component';
import { LProgressSetoranComponent } from './l-progress-setoran/l-progress-setoran.component';

export const options: Partial<IConfig> = {
  thousandSeparator: '.',
};

@NgModule({
  declarations: [AddonComponent, LPenjualanComponent, LKomisiComponent, LCashflowPendapatanComponent, LCashflowPengambilanKasComponent, LRekapCashflowComponent, LKartuStokComponent, LProgressSetoranComponent],
  imports: [
    CustomPipeModule,
    SharedModule,
    CommonModule,
    LaporanRoutingModule,
    LaporanRoutingModule,
    FormsModule,
    Daterangepicker,
    UIModule,
    NgbDropdownModule,
    NgbDatepickerModule,
    NgbNavModule,
    NgSelectModule,
    NgbModule,
    UiSwitchModule,
    ReactiveFormsModule,
    ArchwizardModule,
    DataTablesModule,
    ChartsModule,
    NgxMaskModule.forRoot(options)
  ],
  providers: []
})
export class LaporanModule { }
