import {Component, OnInit} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {LandaService} from "../../../core/services/landa.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import * as moment from 'moment';
import Swal, {SweetAlertResult} from "sweetalert2";

@Component({
    selector: 'app-l-penjualan',
    templateUrl: './l-penjualan.component.html',
    styleUrls: ['./l-penjualan.component.scss']
})
export class LPenjualanComponent implements OnInit {
    breadCrumbItems: Array<{}>;
    pageTitle: string;
    alasan: string;
    is_tampilkan: boolean;
    dtOptions: any;
    img: string;

    listData: any = [];
    totalData: any = {};
    list: any = [];
    label: any = [];
    labelsub: any = [];
    listEvent: any;
    listAddon: any = {};
    listTipe: any;
    header: null;
    is_export: number;
    model: any = {};
    formModal: any = {};
    daterange: any = {};
    // options: any = {
    //     locale: {format: 'YYYY-MM-DD'},
    //     alwaysShowCalendars: false,
    // };
    apiUrl = environment.apiURL;
    total: any;

    options: any = {
        locale: {
            format: "DD-MM-YYYY",
        },
        alwaysShowCalendars: true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'day'), moment()],
            'Last 7 Days': [moment().subtract(7, 'day'), moment()],
            'Last 30 Days': [moment().subtract(30, 'day'), moment()],
            'This Month': [moment().startOf("month"), moment().endOf("month")],
            'Last Month': [moment().subtract(1, 'month'), moment()],
            'Last 3 Months': [moment().subtract(4, 'month'), moment()]
        }
        // startDate: moment().startOf("month").format("DD-MM-YYYY"),
        // endDate: moment(this.future_date).endOf("month").format("DD-MM-YYYY"),
    };


    constructor(private landaService: LandaService, private modalService: NgbModal) {
    }



    ngOnInit() {
        this.pageTitle = 'Laporan Penjualan';
        this.is_tampilkan = false;

        this.empty();
        this.model.periode = moment().format('DD-MM-YYYY') + ' - ' + moment().format('DD-MM-YYYY');
    }

    empty() {
        this.listEvent = [];
        this.list = [];
        this.listData = [];
        this.header = null;
        this.model = {};
        this.is_export = 0;
        this.daterange.start = moment().format('YYYY-MM-DD');
        this.daterange.end = moment().format('YYYY-MM-DD');
        this.model.periode = moment().format('DD-MM-YYYY') + ' - ' + moment().format('DD-MM-YYYY');
        // this.tampilkan();
    }

    selectedDate(value: any, datepicker?: any) {
        console.log(datepicker);
        datepicker.start = value.start;
        datepicker.end = value.end;
        this.daterange.start = value.start;
        this.daterange.end = value.end;
        this.daterange.label = value.label;
    }

    tampilkan(is_export = 0) {
        console.log(this.daterange);
        if (this.daterange.start !== undefined && this.daterange.end !== undefined) {
            const data = {
                periode_mulai: 'null',
                periode_selesai: 'null'
            }

            if (this.daterange.start !== undefined && this.daterange.end !== undefined) {
                data.periode_mulai = moment(this.daterange.start).format('YYYY-MM-DD');
                data.periode_selesai = moment(this.daterange.end).format('YYYY-MM-DD');
            }

            if (is_export == 1) {
                window.open(this.apiUrl + '/l_penjualan/index?' +
                    '&periode_mulai=' + data.periode_mulai +
                    '&periode_selesai=' + data.periode_selesai +
                    '&is_export=' + is_export,
                    '_blank');
            } else {
                this.landaService.DataGet('/l_penjualan/index', data).subscribe((res: any) => {
                    if (res.status_code === 200 && res.data.data.length > 0) {
                        this.listData = res.data.data;
                        this.totalData = res.data.total;
                        this.is_tampilkan = true;
                    } else {
                        this.landaService.alertError('Mohon Maaf', 'Data Kosong');
                        this.is_tampilkan = false;
                        this.listData = [];
                    }
                });
            }

        } else {
            this.landaService.alertError('Sorry', 'Select the filter first');
        }
    }

    export(val) {
        window.open(this.apiUrl + '/l_addon_ordered/export?id=' + val, '_blank');
    }

    detail(val) {
        window.open(window.location.origin + '/transaksi/transaksi-kasir/' + val.id, '_blank');
    }

    reset_tampilan() {
        this.is_tampilkan = false;
    }

    hapus(val) {
        Swal.fire({
            title: 'Peringatan!',
            text: 'Apakah anda yakin menghapus data penjualan dengan kode nota ' + val.kode_nota + ' ini ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            confirmButtonText: 'Ya, Hapus data ini !'
        }).then(result => {
            if (result.value) {

                Swal.fire({
                    title: 'Masukkan Alasan',
                    // input: 'textarea',
                    html: '<textarea rows="5" class=" form-control form-control-sm" id="custom-textarea" style="font-size: 16px;"></textarea>',

                    // inputPlaceholder: 'Type something...',
                    showCancelButton: true,
                    confirmButtonText: 'Submit',
                    cancelButtonText: 'Cancel',
                    preConfirm: () => {
                        const inputValue = (document.getElementById('custom-textarea') as HTMLTextAreaElement).value;
                        if (!inputValue) {
                            Swal.showValidationMessage('Alasan tidak boleh kosong');
                        }
                        return inputValue;
                    }

                }).then((result: SweetAlertResult) => {
                    if (result.isConfirmed && result.value) {
                        const inputValue: string = result.value;
                        val.alasan = inputValue;
                        this.landaService.DataPost('/l_penjualan/hapus', val).subscribe((res: any) => {
                            if (res.status_code === 200) {
                                this.landaService.alertSuccess('Berhasil', 'Data penjualan berhasil dihapus !');
                                this.tampilkan();
                            } else {
                                this.landaService.alertError('Sorry', res.errors);
                            }
                        });
                    }
                });
            }
        });
    }

}
