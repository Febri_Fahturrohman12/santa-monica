import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {AddonComponent} from './addon/addon.component';
import { LPenjualanComponent } from './l-penjualan/l-penjualan.component';
import { LKomisiComponent } from './l-komisi/l-komisi.component';
import { LCashflowPendapatanComponent } from './l-cashflow-pendapatan/l-cashflow-pendapatan.component';
import { LCashflowPengambilanKasComponent } from './l-cashflow-pengambilan-kas/l-cashflow-pengambilan-kas.component';
import { LRekapCashflowComponent } from './l-rekap-cashflow/l-rekap-cashflow.component';
import { LKartuStokComponent } from './l-kartu-stok/l-kartu-stok.component';
import { LProgressSetoranComponent } from './l-progress-setoran/l-progress-setoran.component';

const routes: Routes = [

    {
        path: 'laporan-kartu-stok',
        component: LKartuStokComponent,
        data: {
            breadcrumb: 'Laporan Kartu Stok'
        }
    },
    {
        path: 'laporan-penjualan',
        component: LPenjualanComponent,
        data: {
            breadcrumb: 'Laporan Penjualan'
        }
    },
    {
        path: 'laporan-komisi',
        component: LKomisiComponent,
        data: {
            breadcrumb: 'Laporan Komisi'
        }
    },
    {
        path: 'laporan-cashflow-pendapatan',
        component: LCashflowPendapatanComponent,
        data: {
            breadcrumb: 'Laporan Cashflow Pendapatan'
        }
    },
    {
        path: 'laporan-cashflow-pengambilan-kas',
        component: LCashflowPengambilanKasComponent,
        data: {
            breadcrumb: 'Laporan Pengambilan Kas'
        }
    },
    {
        path: 'laporan-rekap-cashflow',
        component: LRekapCashflowComponent,
        data: {
            breadcrumb: 'Laporan Rekap Cashflow'
        }
    },
    {
        path: 'laporan-progress',
        component: LProgressSetoranComponent,
        data: {
            breadcrumb: 'Laporan Progress Setoran'
        }
    }

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LaporanRoutingModule {
}
