import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LCashflowPendapatanComponent } from './l-cashflow-pendapatan.component';

describe('LCashflowPendapatanComponent', () => {
  let component: LCashflowPendapatanComponent;
  let fixture: ComponentFixture<LCashflowPendapatanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LCashflowPendapatanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LCashflowPendapatanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
