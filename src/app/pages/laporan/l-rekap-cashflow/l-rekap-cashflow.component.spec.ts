import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LRekapCashflowComponent } from './l-rekap-cashflow.component';

describe('LRekapCashflowComponent', () => {
  let component: LRekapCashflowComponent;
  let fixture: ComponentFixture<LRekapCashflowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LRekapCashflowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LRekapCashflowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
