import {Component, OnInit} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {LandaService} from "../../../core/services/landa.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import * as moment from 'moment';

@Component({
  selector: 'app-addon',
  templateUrl: './addon.component.html',
  styleUrls: ['./addon.component.scss']
})
export class AddonComponent implements OnInit {
  breadCrumbItems: Array<{}>;
  pageTitle: string;
  is_tampilkan: boolean;
  dtOptions: any;
  img: string;

  listData: any = [];
  list: any = [];
  label: any = [];
  labelsub: any = [];
  listEvent: any;
  listAddon: any = {};
  listTipe: any;
  header: null;
  is_export: number;
  model: any = {};
  formModal: any = {};
  daterange: any = {};
  options: any = {
      locale: {format: 'YYYY-MM-DD'},
      alwaysShowCalendars: false,
  };
  apiUrl = environment.apiURL;
  total: any;


  constructor(private landaService: LandaService, private modalService: NgbModal) {
  }

  ngOnInit() {
      this.pageTitle = 'Leaderboard';
      this.is_tampilkan = false;

      this.empty();
      this.getEvent();
      // this.model.periode = moment().subtract(1, 'days').format('YYYY-MM-DD') + '-' + moment().format('YYYY-MM-DD');
  }

  empty() {
      this.listEvent = [];
      this.list = [];
      this.listData = [];
      this.header = null;
      this.model = {};
      this.is_export = 0;
      // this.daterange.start = moment().subtract(1, 'days').format('YYYY-MM-DD');
      // this.daterange.end = moment().format('YYYY-MM-DD');
      // this.model.periode = moment().subtract(1, 'days').format('YYYY-MM-DD') + '-' + moment().format('YYYY-MM-DD');

  }

  selectedDate(value: any, datepicker?: any) {
      datepicker.start = value.start;
      datepicker.end = value.end;
      this.daterange.start = value.start;
      this.daterange.end = value.end;
      this.daterange.label = value.label;
  }

  getEvent() {
      this.landaService.DataGet('/get_data/event', {}).subscribe((res: any) => {
          this.listEvent = res.data;
      });
  }

  tampilkan(is_export) {
      if (this.model.m_event_id != null || this.model.m_event_id != undefined) {

          const data = {
              m_event_id: this.model.m_event_id,
              is_export: is_export
          };

          if (is_export == 1) {
              window.open(this.apiUrl + '/l_addon_ordered/index?' +
                  '&m_event_id=' + this.model.m_event_id +
                  '&is_export=' + is_export,
                  '_blank');
          } else {
              this.landaService.DataGet('/l_addon_ordered/index', data).subscribe((res: any) => {
                  if (res.status_code === 200 && res.data.list.length > 0) {
                      this.listData = res.data.list;
                      this.total = res.data.total;
                      this.is_tampilkan = true;
                  } else {
                      this.landaService.alertError('Sorry', 'Empty data');
                      this.is_tampilkan = false;
                  }
              });
          }

      } else {
          this.landaService.alertError('Sorry', 'Select the filter first');
      }
  }

  export(val) {
      window.open(this.apiUrl + '/l_addon_ordered/export?id=' + val, '_blank');
  }

  reset_tampilan() {
      this.is_tampilkan = false;
  }
}
