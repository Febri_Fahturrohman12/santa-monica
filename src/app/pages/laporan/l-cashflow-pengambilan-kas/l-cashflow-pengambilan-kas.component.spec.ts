import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LCashflowPengambilanKasComponent } from './l-cashflow-pengambilan-kas.component';

describe('LCashflowPengambilanKasComponent', () => {
  let component: LCashflowPengambilanKasComponent;
  let fixture: ComponentFixture<LCashflowPengambilanKasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LCashflowPengambilanKasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LCashflowPengambilanKasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
