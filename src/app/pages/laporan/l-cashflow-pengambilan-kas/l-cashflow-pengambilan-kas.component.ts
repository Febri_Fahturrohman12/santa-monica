import {Component, OnInit} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {LandaService} from "../../../core/services/landa.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import * as moment from 'moment';
import Swal, {SweetAlertResult} from "sweetalert2";

@Component({
  selector: 'app-l-cashflow-pengambilan-kas',
  templateUrl: './l-cashflow-pengambilan-kas.component.html',
  styleUrls: ['./l-cashflow-pengambilan-kas.component.scss']
})
export class LCashflowPengambilanKasComponent implements OnInit {
  breadCrumbItems: Array<{}>;
  pageTitle: string;
  alasan: string;
  is_tampilkan: boolean;
  dtOptions: any;
  img: string;

  listData: any = [];
  listGetTherapis: any = [];
  totalData: any = {};
  list: any = [];
  label: any = [];
  labelsub: any = [];
  listEvent: any;
  listAddon: any = {};
  listTipe: any;
  header: null;
  is_export: number;
  model: any = {};
  formModal: any = {};
  daterange: any = {};
  
  apiUrl = environment.apiURL;
  total: any;

  options: any = {
      locale: {
          format: "DD-MM-YYYY",
      },
      alwaysShowCalendars: true,
      ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'day'), moment()],
          'Last 7 Days': [moment().subtract(7, 'day'), moment()],
          'Last 30 Days': [moment().subtract(30, 'day'), moment()],
          'This Month': [moment().startOf("month"), moment().endOf("month")],
          'Last Month': [moment().subtract(1, 'month'), moment()],
          'Last 3 Months': [moment().subtract(4, 'month'), moment()]
      }
      // startDate: moment().startOf("month").format("DD-MM-YYYY"),
      // endDate: moment(this.future_date).endOf("month").format("DD-MM-YYYY"),
  };


  constructor(private landaService: LandaService, private modalService: NgbModal) {
  }



  ngOnInit() {
      this.pageTitle = 'Laporan Pengambilan Kas';
      this.is_tampilkan = false;

      this.getTherapis();
      this.empty();
      this.model.periode = moment().format('DD-MM-YYYY') + ' - ' + moment().format('DD-MM-YYYY');
  }

  getTherapis() {
    this.landaService.DataGet('/get_data/user', {}).subscribe((res: any) => {
      this.listGetTherapis = res.data;
  });
}

  empty() {
      this.listEvent = [];
      this.list = [];
      this.listData = [];
      this.header = null;
      this.model = {};
      this.is_export = 0;
      this.daterange.start = moment().format('YYYY-MM-DD');
      this.daterange.end = moment().format('YYYY-MM-DD');
      this.model.periode = moment().format('DD-MM-YYYY') + ' - ' + moment().format('DD-MM-YYYY');
      // this.tampilkan();
  }

  selectedDate(value: any, datepicker?: any) {
      datepicker.start = value.start;
      datepicker.end = value.end;
      this.daterange.start = value.start;
      this.daterange.end = value.end;
      this.daterange.label = value.label;
  }

  tampilkan(is_export = 0) {
      if (this.daterange.start !== undefined && this.daterange.end !== undefined) {
          const data = {
              periode_mulai: 'null',
              periode_selesai: 'null',
              user_id: this.model.user_id
          }

          if (this.daterange.start !== undefined && this.daterange.end !== undefined) {
              data.periode_mulai = moment(this.daterange.start).format('YYYY-MM-DD');
              data.periode_selesai = moment(this.daterange.end).format('YYYY-MM-DD');
          }

          if (is_export == 1) {
              window.open(this.apiUrl + '/l_pengambilankas/index?' +
                  '&periode_mulai=' + data.periode_mulai +
                  '&periode_selesai=' + data.periode_selesai +
                  '&is_export=' + is_export,
                  '_blank');
          } else {
              this.landaService.DataGet('/l_pengambilankas/index', data).subscribe((res: any) => {
                  if (res.status_code === 200 && res.data.data.length > 0) {
                      this.listData = res.data.data;
                      this.totalData = res.data.total;
                      this.is_tampilkan = true;
                  } else {
                      this.landaService.alertError('Mohon Maaf', 'Data Kosong');
                      this.is_tampilkan = false;
                      this.listData = [];
                  }
              });
          }

      } else {
          this.landaService.alertError('Sorry', 'Select the filter first');
      }
  }

  export(val) {
      window.open(this.apiUrl + '/l_addon_ordered/export?id=' + val, '_blank');
  }

  detail(val) {
      window.open(window.location.origin + '/transaksi/transaksi-kasir/' + val.id, '_blank');
  }

  reset_tampilan() {
      this.is_tampilkan = false;
  }
}
