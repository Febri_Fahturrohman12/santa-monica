import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LKomisiComponent } from './l-komisi.component';

describe('LKomisiComponent', () => {
  let component: LKomisiComponent;
  let fixture: ComponentFixture<LKomisiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LKomisiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LKomisiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
