import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LProgressSetoranComponent } from './l-progress-setoran.component';

describe('LProgressSetoranComponent', () => {
  let component: LProgressSetoranComponent;
  let fixture: ComponentFixture<LProgressSetoranComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LProgressSetoranComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LProgressSetoranComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
