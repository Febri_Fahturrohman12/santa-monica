import { TemplateRef,Component, OnInit, ViewEncapsulation, ViewChild, Input, Output, HostListener } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LandaService } from '../../../core/services/landa.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OrgData } from 'angular-org-chart/src/app/modules/org-chart/orgData';
import Swal from 'sweetalert2';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { analytics } from 'firebase';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import * as moment from 'moment';

@Component({
  selector: 'app-l-progress-setoran',
  templateUrl: './l-progress-setoran.component.html',
  styleUrls: ['./l-progress-setoran.component.scss']
})
export class LProgressSetoranComponent implements OnInit {

  breadCrumbItems: Array<{}>;
  pageTitle: string;
  alasan: string;
  is_tampilkan: boolean;
  dtOptions: any;
  img: string;

  listData: any = [];
  listJudul: any;
  listAnggota: any;
  totalData: any = {};
  list: any = [];
  label: any = [];
  labelsub: any = [];
  listEvent: any;
  listAddon: any = {};
  listTipe: any;
  header: null;
  periode: string;
  is_export: number;
  model: any = {};
  formModal: any = {};
  daterange: any = {};
  total: any;

  
  listBulan: any;
  listTahun: any;

  options: any = {
      locale: {
          format: "DD-MM-YYYY",
      },
      alwaysShowCalendars: true,
      ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'day'), moment()],
          'Last 7 Days': [moment().subtract(7, 'day'), moment()],
          'Last 30 Days': [moment().subtract(30, 'day'), moment()],
          'This Month': [moment().startOf("month"), moment().endOf("month")],
          'Last Month': [moment().subtract(1, 'month'), moment()],
          'Last 3 Months': [moment().subtract(4, 'month'), moment()]
      }
  };

  constructor(private landaService: LandaService, private modalService: NgbModal) {
  }

  ngOnInit() {
    this.pageTitle = 'Laporan Setoran';
    this.is_tampilkan = false;

    
    this.getDataAnggota();
    this.empty();
    this.model.periode = moment().format('DD-MM-YYYY') + ' - ' + moment().format('DD-MM-YYYY');
    this.listBulan = [{
        id: 1,
        nama: "Januari"
    },{
        id: 2,
        nama: "Februari"
    },{
        id: 3,
        nama: "Maret"
    },{
        id: 4,
        nama: "April"
    },{
        id: 5,
        nama: "Mei"
    },{
        id: 6,
        nama: "Juni"
    },{
        id: 7,
        nama: "Juli"
    },{
        id: 8,
        nama: "Agustus"
    },{
        id: 9,
        nama: "September"
    },{
        id: 10,
        nama: "Oktober"
    },{
        id: 11,
        nama: "November"
    },{
        id: 12,
        nama: "Desember"
    }];
    let dt = new Date();
    let thisYear = dt.getFullYear();
    let tahunSebelum = thisYear - 10;
    let tahunSesudah = thisYear + 10;
    let tahun = [];
    let i;
    for (i = tahunSebelum; i <= tahunSesudah; i++) {
        tahun.push(i);
    }
    this.listTahun = tahun;
    this.model.bulan = dt.getMonth()+1;
    this.model.tahun = thisYear;
  }

  empty() {
      this.listEvent = [];
      this.list = [];
      this.listData = [];
      this.header = null;
      this.model = {};
      this.is_export = 0;
  }

  selectedDate(value: any, datepicker?: any) {
      console.log(datepicker);
      datepicker.start = value.start;
      datepicker.end = value.end;
      this.daterange.start = value.start;
      this.daterange.end = value.end;
      this.daterange.label = value.label;
  }

    getDataAnggota() {
        this.landaService.DataGet('/l_progress_setoran/getAnggota', this.model).subscribe((res: any) => {
            this.listAnggota = res.data.list;
        });
    }

    reset_tampilan() {
        this.is_tampilkan = false;
    }

    tampilkan(is_export){
        var date = new Date();
        var firstDay = new Date(this.model.tahun, this.model.bulan - 1 , 1);
        var lastDay = new Date(this.model.tahun, (this.model.bulan -1) + 1, 0);
  
        const data = {
          periode_mulai: moment(firstDay).format('YYYY-MM-DD'),
          periode_selesai: moment(lastDay).format('YYYY-MM-DD'),
          m_user_id: this.model.m_user_id,
          bulan: this.model.bulan,
          tahun: this.model.tahun,
        };

        if (this.model.tahun !== null && this.model.bulan !== null) {
            const data = {
              bulan: this.model.bulan,
              tahun: this.model.tahun,
              m_user_id: this.model.m_user_id,
              listJudul: null,
              is_export
            };
    
            if (is_export === 1) {
                data.listJudul = this.listJudul
                // window.open(this.apiURL + '/l_jadwal/getAll?' + $.param(data), '_blank');
            } else {
                this.landaService.DataGet('/l_progress_setoran/getAll', data).subscribe((res: any) => {
                    if (res.status_code === 200) {
                        this.listData = res.data.list;
                        this.listJudul = res.data.listJudul;
                        this.periode = res.data.periode;

                        this.is_tampilkan = true;
                    } else {
                        this.is_tampilkan = false;
                    }
                });
            }
        } else {
            this.landaService.alertError('Mohon Maaf', 'Bulan dan Tahun harus diisi !');
        }
    }
}
