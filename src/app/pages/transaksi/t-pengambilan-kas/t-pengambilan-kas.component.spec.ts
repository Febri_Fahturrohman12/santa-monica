import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TPengambilanKasComponent } from './t-pengambilan-kas.component';

describe('TPengambilanKasComponent', () => {
  let component: TPengambilanKasComponent;
  let fixture: ComponentFixture<TPengambilanKasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TPengambilanKasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TPengambilanKasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
