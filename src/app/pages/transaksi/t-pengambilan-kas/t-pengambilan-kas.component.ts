import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {DataTableDirective} from "angular-datatables";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {LandaService} from "../../../core/services/landa.service";
import {Router} from "@angular/router";
import * as moment from "moment";
import Swal from "sweetalert2";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
  selector: 'app-t-pengambilan-kas',
  templateUrl: './t-pengambilan-kas.component.html',
  styleUrls: ['./t-pengambilan-kas.component.scss']
})
export class TPengambilanKasComponent implements OnInit {
    
    @ViewChild('modalGantiFoto') modalGantiFoto : TemplateRef<any>; // Note: TemplateRef

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtInstance: Promise<DataTables.Api>;
    dtOptions: any;
    
    daterange: any;
    options: any = {
        locale: {format: 'YYYY-MM-DD'},
        alwaysShowCalendars: false,
    };

    myForm = new FormGroup({
        foto: new FormControl('', [Validators.required]),
    });
    breadCrumbItems: Array<{}>;
    pageTitle: string;
    isView: boolean;
    isEdit: boolean;
    model: any = {};
    form: any = {};
    modelParam: any = {};
    modelCheck: any = {};
    listData: any;
    listAkses: any;
    showForm: boolean;
    listHakAkses: any = [];
    listDetailHistoriUser: any = [];
    listDesa: any = [];
    listHistori: any = [];
    tabContent: string;
    dtOptionsHistori: any;
    control_link: any = '/t_pengambilan_kas';
    imageChangedEvent: any = '';
    imageSrc: string;
    listKaryawan: any;
    

    constructor(private landaService: LandaService, private router: Router, 
        private modalService: NgbModal) {
    }

    ngOnInit(): void {

        this.pageTitle = 'Pengambilan Kas';
        this.breadCrumbItems = [{
            label: 'Master'
        }, {
            label: 'Pengambilan Kas',
            active: true
        }];
        this.modelParam = {};

        this.getData();
        this.empty();
        this.getKaryawan();
    }

    reloadDataTable(): void {
        if (this.daterange.start !== undefined && this.daterange.end !== undefined) {
            this.modelParam.periode_mulai = moment(this.daterange.start).format('YYYY-MM-DD');
            this.modelParam.periode_selesai = moment(this.daterange.end).format('YYYY-MM-DD');
        }
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.draw();
        });
    }

    getData() {
        this.dtOptions = {
            serverSide: true,
            processing: true,
            ordering: false,
            pagingType: 'full_numbers',
            ajax: (dataTablesParameters: any, callback) => {
                const params = {
                    filter: JSON.stringify(this.modelParam),
                    offset: dataTablesParameters.start,
                    limit: dataTablesParameters.length,
                };

                this.landaService
                    .DataGet('/t_pengambilan_kas/index', params)
                    .subscribe((res: any) => {
                        this.listData = res.data.list.map((i) => {
                            i.tgl_lahir = i.tgl_lahir == null || i.tgl_lahir == '' || i.tgl_lahir == 'null' ? 0 : moment(i.tanggal).format("YYYY-MM-DD");
                            i.tgl_lahir_view = i.tgl_lahir == null || i.tgl_lahir == '' || i.tgl_lahir == 'null' ? 0 : moment(i.tanggal).format("DD-MM-YYYY");

                            return i;
                        });
                        callback({
                            recordsTotal: res.data.totalItems,
                            recordsFiltered: res.data.totalItems,
                            data: [],
                        });
                    });
            },
        };
    }

    getKaryawan() {
      this.landaService.DataGet('/t_pengambilan_kas/getKaryawan', {}).subscribe((res: any) => {
        this.listKaryawan = res.data.list;
      });
    }
    toDate(dob) {
        if (dob) {
            const [year, month, day] = dob.split('-');
            const obj = {
                year: parseInt(year),
                month: parseInt(month),
                day: parseInt(day.split(' ')[0].trim()),
            };
            return obj;
        }
    }

    setTabContent(content) {
        this.tabContent = content;
    }

    getTabContent() {
        return this.tabContent;
    }

    empty() {
        this.modelCheck = {
            DataMaster: false,
            Tabel: false,
            Form: false

        };
        this.daterange = {};
        this.model = {};
        this.modelParam = {};
        this.modelParam.status = 0;
        this.listHakAkses = [];
        this.getData();
        
    }

    selectedDate(value: any, datepicker?: any) {
        datepicker.start = value.start;
        datepicker.end = value.end;
        this.daterange.start = value.start;
        this.daterange.end = value.end;
        this.daterange.label = value.label;
    }

    resetTgl() {
        this.daterange = {};
        this.modelParam = {};
        this.modelParam.periode_mulai = '';
        this.modelParam.periode_selesai = '';
        this.modelParam.status = 0;
        this.reloadDataTable();
    }

    index() {
        this.showForm = !this.showForm;
        this.pageTitle = 'Data Pengambilan Kas';
        this.getData();
    }

    create() {
        this.empty();
        this.getKaryawan();
        this.showForm = !this.showForm;
        this.pageTitle = 'Tambah Data Pengambilan Kas';
        this.isView = false;
        this.isEdit = false;
        this.tabContent = 'pengambilankas';
        this.getTabContent();
    }

    edit(val) {
        
        this.getKaryawan();
        this.showForm = !this.showForm;
        this.model = val;
        this.model.tanggal = this.toDate(val.tanggal);
        this.tabContent = 'pengambilankas';
        this.pageTitle = 'Pengambilan Kas : ' + val.nama;
        this.isView = false;
        this.isEdit = true;
        this.getTabContent();
        // this.getDetailHistoriUser(val.id);
    }

    view(val) {
      
        this.getKaryawan();
        this.showForm = !this.showForm;
        this.model = val;
        this.model.tanggal = this.toDate(val.tanggal);
        this.tabContent = 'pengambilankas';
        this.pageTitle = 'Pengambilan Kas : ' + val.nama;
        
        this.isView = true;
        this.getTabContent();
        // this.getDetailHistoriUser(val.id);
    }

    onFileChange(event) {
        const reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.model.foto = {
                    base64: reader.result as string
                };
                this.myForm.patchValue({
                    foto: reader.result,
                });
            };
        }
    }

    save() {
        const final = Object.assign(this.model);

        this.landaService.DataPost('/t_pengambilan_kas/save', final).subscribe((res: any) => {
            if (res.status_code === 200) {
                this.landaService.alertSuccess('Berhasil', 'Data Pengambilan Kas telah disimpan!');
                this.index();
            } else {
                this.landaService.alertError('Sorry', res.errors);
            }
        });

    }

    delete(val) {
        const data = {
            id: val != null ? val.id : null,
            is_deleted: 1,
        };
        Swal.fire({
            title: 'Apakah anda yakin ?',
            text: 'Menghapus data pengambilankas akan berpengaruh terhadap data lainnya',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            confirmButtonText: 'Ya, Hapus data ini !'
        }).then(result => {
            if (result.value) {
                this.landaService.DataPost('/t_pengambilan_kas/delete', data).subscribe((res: any) => {
                    if (res.status_code === 200) {
                        this.landaService.alertSuccess('Berhasil', 'Data Pengambilan Kas telah dihapus !');
                        this.reloadDataTable();

                    } else {
                        this.landaService.alertError('Sorry', res.errors);
                    }
                });
            }
        });
    }

    restore(val) {
        const data = {
            id: val != null ? val.id : null,
            is_deleted: 0,
        };
        Swal.fire({
            title: 'Apakah anda yakin ?',
            text: 'Merestore data pengambilankas ini',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            confirmButtonText: 'Ya, Restore data ini !'
        }).then(result => {
            if (result.value) {
                this.landaService.DataPost('/t_pengambilan_kas/restore', data).subscribe((res: any) => {
                    if (res.status_code === 200) {
                        this.landaService.alertSuccess('Berhasil', 'Data Pengambilan Kas telah direstore !');
                        this.reloadDataTable();

                    } else {
                        this.landaService.alertError('Sorry', res.errors);
                    }
                });
            }
        });
    }

    getDetailHistoriUser(val) {
        this.landaService.DataGet(this.control_link + '/getHistori', {t_pengambilan_kas_id: val}).subscribe((res: any) => {
            this.listDetailHistoriUser = res.data;
        })
    }

    hapusfoto(){
        this.imageChangedEvent = this.landaService.getImage('', '../img/default.png');
        this.imageSrc = this.landaService.getImage('', '../img/default.png');
    }
    fileChangeEvent(event: any): void {
        this.modalService.open(this.modalGantiFoto, { size: 'md', backdrop: 'static' });
        this.imageChangedEvent = event;
    }
    imageCropped(event: ImageCroppedEvent) {
        this.imageSrc = event.base64;
        this.myForm.patchValue({
            foto: event.base64,
        });
    }
    imageLoaded(image: HTMLImageElement) {
        // show cropper
    }
    cropperReady() {
        // cropper ready
    }
    loadImageFailed() {
        // show message
    }
}