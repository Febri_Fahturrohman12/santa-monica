import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TSetoranComponent } from './t-setoran.component';

describe('TSetoranComponent', () => {
  let component: TSetoranComponent;
  let fixture: ComponentFixture<TSetoranComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TSetoranComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TSetoranComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
