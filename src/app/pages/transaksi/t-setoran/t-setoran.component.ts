import {Component, OnInit, ViewChild} from '@angular/core';
import {LandaService} from "../../../core/services/landa.service";
import {DataTableDirective} from "angular-datatables";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import * as moment from "moment/moment";
import Swal, {SweetAlertResult} from 'sweetalert2';
import {NgbCalendar} from "@ng-bootstrap/ng-bootstrap";


@Component({
  selector: 'app-t-setoran',
  templateUrl: './t-setoran.component.html',
  styleUrls: ['./t-setoran.component.scss']
})
export class TSetoranComponent implements OnInit {
    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtInstance: Promise<DataTables.Api>;
    dtOptions: any;
    myForm = new FormGroup({
        fileUpload: new FormControl('', [Validators.required]),
    });
    breadCrumbItems: Array<{}>;
    pageTitle: string;
    isView: boolean;
    isEdit: boolean;
    model: any = {};
    form: any = {};
    modelParam: any = {};
    showForm: boolean;
    listData: any;
    listDetail: any = [];
    listBarang: any = [];

    tabContent: string;
    control_link: any = '/t_setoran';

    constructor(private landaService: LandaService, private router: Router, private calendar: NgbCalendar) {
    }

    ngOnInit(): void {

        this.pageTitle = 'Setoran';
        this.breadCrumbItems = [{
            label: 'Transaksi'
        }, {
            label: 'Setoran',
            active: true
        }];
        this.modelParam = {};

        this.getData();
        this.empty();
    }

    reloadDataTable(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.draw();
        });
    }

    getData() {
        this.dtOptions = {
            serverSide: true,
            processing: true,
            ordering: false,
            pagingType: 'full_numbers',
            ajax: (dataTablesParameters: any, callback) => {
                const params = {
                    filter: JSON.stringify(this.modelParam),
                    offset: dataTablesParameters.start,
                    limit: dataTablesParameters.length,
                };

                this.landaService
                    .DataGet('/t_setoran/index', params)
                    .subscribe((res: any) => {
                        this.listData = res.data.list;
                        callback({
                            recordsTotal: res.data.totalItems,
                            recordsFiltered: res.data.totalItems,
                            data: [],
                        });
                    });
            },
        };
    }

    toDate(dob) {
        if (dob) {
            const [year, month, day] = dob.split('-');
            const obj = {
                year: parseInt(year),
                month: parseInt(month),
                day: parseInt(day.split(' ')[0].trim()),
            };
            return obj;
        }
    }

    empty() {
        this.model = {};
        this.modelParam = {};
        this.listDetail = [];
        this.modelParam.is_deleted = 0;
        this.getData();
    }

    index() {
        this.showForm = !this.showForm;
        this.pageTitle = 'Data Setoran';
        this.getData();
    }

    create() {
        this.empty();
        this.showForm = !this.showForm;
        this.pageTitle = 'Tambah Data Setoran';
        this.isView = false;
        this.isEdit = false;
        this.model.tanggal = this.calendar.getToday();
        this.listDetail = [];
        this.getBarang();

    }

    edit(val) {
        this.showForm = !this.showForm;
        this.model = val;
        this.model.tanggal = this.toDate(val.tanggal);
        this.isView = false;
        this.isEdit = true;
        this.getBarang();
        this.getDetail(val.id);
    }

    view(val) {
        this.showForm = !this.showForm;
        this.model = val;
        this.model = val;
        this.model.tanggal = this.toDate(val.tanggal);
        this.isView = true;
        this.getBarang();
        this.getDetail(val.id);
    }

    save(model) {
        const data = {
            data: model,
            detail: this.listDetail
        }
        this.landaService.DataPost('/t_setoran/save', data).subscribe((res: any) => {
            if (res.status_code === 200) {
                this.landaService.alertSuccess('Berhasil', 'Data Setoran telah disimpan!');
                this.index();
            } else {
                this.landaService.alertError('Sorry', res.errors);
            }
        });
    }
    getDetail(id){
        const data = {
            id: id
        }
        this.landaService.DataGet('/t_setoran/getDetail', data).subscribe((res: any) => {
            this.listDetail= res.data.list;
        });
    }

    saveStatus(val) {
        Swal.fire({
            title: 'Masukkan Alasan',
            html: '<textarea rows="5" class=" form-control form-control-sm" id="custom-textarea" style="font-size: 16px;"></textarea>',
            icon: 'info',

            showCancelButton: true,
            confirmButtonText: 'Submit',
            cancelButtonText: 'Cancel',
            preConfirm: () => {
                const inputValue = (document.getElementById('custom-textarea') as HTMLTextAreaElement).value;
                if (!inputValue) {
                    Swal.showValidationMessage('Alasan tidak boleh kosong');
                }
                return inputValue;
            }

        }).then((result: SweetAlertResult) => {
            if (result.isConfirmed && result.value) {
                const inputValue: string = result.value;
                val.alasan = inputValue;
                this.landaService.DataPost('/t_setoran/saveStatus', val).subscribe((res: any) => {
                    if (res.status_code === 200) {
                        this.landaService.alertSuccess('Berhasil', 'Data Setoran telah dihapus!');
                        this.index();
                        // this.reloadDataTable();

                    } else {
                        this.landaService.alertError('Sorry', res.errors);
                    }
                });
            }
        });


    }

    delete(val) {
        const data = {
            id: val != null ? val.id : null,
            is_deleted: 1,
        };
        Swal.fire({
            title: 'Apakah anda yakin ?',
            text: 'Menghapus data Kebutuhan Setoran akan berpengaruh terhadap data lainnya',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            confirmButtonText: 'Ya, Hapus data ini !'
        }).then(result => {
            if (result.value) {
                this.landaService.DataPost('/t_setoran/delete', data).subscribe((res: any) => {
                    if (res.status_code === 200) {
                        this.landaService.alertSuccess('Berhasil', 'Data Kebutuhan Setoran telah dihapus !');
                        this.reloadDataTable();

                    } else {
                        this.landaService.alertError('Sorry', res.errors);
                    }
                });
            }
        });
    }

    restore(val) {
        const data = {
            id: val != null ? val.id : null,
            is_deleted: 0,
        };
        Swal.fire({
            title: 'Apakah anda yakin ?',
            text: 'Merestore data Kebutuhan Setoran ini',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            confirmButtonText: 'Ya, Restore data ini !'
        }).then(result => {
            if (result.value) {
                this.landaService.DataPost('/t_setoran/restore', data).subscribe((res: any) => {
                    if (res.status_code === 200) {
                        this.landaService.alertSuccess('Berhasil', 'Data Kebutuhan Setoran telah direstore !');
                        this.reloadDataTable();

                    } else {
                        this.landaService.alertError('Sorry', res.errors);
                    }
                });
            }
        });
    }

    approve(val) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan approve Setoran ini?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            // confirmButtonText: 'Ya, Restore data ini !'
        }).then(result => {
            if (result.value) {
                this.showForm = !this.showForm;
                this.save(val);
            }
        });
    }


    /**
     * DETAIL Setoran
     */

    addDetail(val) {
        const newDet = {};
        val.push(newDet);
    }

    removeDetail(val, paramindex) {
        val.splice(paramindex, 1);
    }

    getBarang() {
        this.landaService.DataGet('/t_setoran/getBarang', {}).subscribe((res: any) => {
            this.listBarang = res.data.list;
        });
    }

    checkAkses(hakAkses) {
        return this.landaService.checkAkses(hakAkses);
    }
}

