import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransaksiOrderComponent } from './transaksi-order/transaksi-order.component';
import { TKasirComponent } from './t-kasir/t-kasir.component';
import { TPengambilanKasComponent } from './t-pengambilan-kas/t-pengambilan-kas.component';
import { TPenerimaanComponent } from './t-penerimaan/t-penerimaan.component';
import { TSetoranComponent } from './t-setoran/t-setoran.component';
import { TSetoranAnggotaComponent } from './t-setoran-anggota/t-setoran-anggota.component';


const routes: Routes = [
	{
		path: 'transaksi-penerimaan',
		component: TPenerimaanComponent,
		data: {
			breadcrumb: 'Penerimaan'
		}
	},
	{
		path: 'transaksi-kasir',
		component: TKasirComponent,
		data: {
			breadcrumb: 'Kasir'
		}
	},
	{
		path: 'transaksi-kasir/:id',
		component: TKasirComponent,
		data: {
			breadcrumb: 'Kasir'
		}
	},
	{
		path: 'transaksi-order',
		component: TransaksiOrderComponent,
		data: {
			breadcrumb: 'Order'
		}
	},
	{
		path: 'transaksi-order/:id/:tipe',
		component: TransaksiOrderComponent,
		data: {
			breadcrumb: 'Transaction Order'
		}
	},
	{
		path: 'transaksi-pengambilan-kas',
		component: TPengambilanKasComponent,
		data: {
			breadcrumb: 'Pengambilan Kas'
		}
	},
	{
		path: 'kebutuhan-setoran',
		component: TSetoranComponent,
		data: {
			breadcrumb: 'Kebutuhan Setoran'
		}
	},
	{
		path: 'setoran-anggota',
		component: TSetoranAnggotaComponent,
		data: {
			breadcrumb: 'Setoran Anggota'
		}
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class TransaksiRoutingModule { }
