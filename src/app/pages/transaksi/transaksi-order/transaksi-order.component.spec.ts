import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransaksiOrderComponent } from './transaksi-order.component';

describe('TransaksiOrderComponent', () => {
  let component: TransaksiOrderComponent;
  let fixture: ComponentFixture<TransaksiOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransaksiOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransaksiOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
