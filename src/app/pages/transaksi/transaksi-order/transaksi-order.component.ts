import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {DataTableDirective} from "angular-datatables";
import {LandaService} from "../../../core/services/landa.service";
import Swal from "sweetalert2";
import {NgbModal, NgbTimepickerConfig} from "@ng-bootstrap/ng-bootstrap";
import {environment} from "../../../../environments/environment";
import * as moment from 'moment';

@Component({
    selector: 'app-transaksi-order',
    templateUrl: './transaksi-order.component.html',
    styleUrls: ['./transaksi-order.component.scss']
})
export class TransaksiOrderComponent implements OnInit {
    breadCrumbItems: Array<{}>;
    pageTitle: string;
    showForm: boolean;
    isView: boolean;

    model: any = {};
    modelParam: any = {};

    apiUrl = environment.apiURL;
    listData: any;
    listEvent: any = [];
    listAddon: any = [];
    listGetAddon: any = [];
    listExport: any = [];
    listUserAddress: any = [];
    listStatus: any = [
        {
            label   : "All",
            val     : ""
        },
        {
            label   : "Success Join",
            val     : "join"
        },
        {
            label   : "Waiting For Payment",
            val     : "waitpay"
        },
        {
            label   : "Waiting For Confirmation",
            val     : "waitcon"
        },
        {
            label   : "Declined",
            val     : "declined"
        },
        {
            label   : "Expired",
            val     : "expired"
        }
    ]

    formModal: any = {};
    formModalAddress: any = {};
    formModalExport: any = {};


    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtInstance: Promise<DataTables.Api>;
    dtOptions: any;
    daterange: any;
    options: any = {
        locale: {format: 'YYYY-MM-DD'},
        alwaysShowCalendars: false,
    };

    constructor(private landaService: LandaService,
                private route: ActivatedRoute,
                timepicker: NgbTimepickerConfig,
                private router: Router,
                private modalService: NgbModal) {
        timepicker.spinners = false;
        timepicker.size = 'small';
    }


    ngOnInit() {

        if (this.route.snapshot.paramMap.get("id") !== null) {
            this.pageTitle = 'Order';
            this.breadCrumbItems = [{
                label: 'Transaksi'
            }, {
                label: 'Order',
                active: true
            }];

            this.empty();
            this.view(null, this.route.snapshot.paramMap.get("id"));
        } else {
            this.pageTitle = 'Order';
            this.breadCrumbItems = [{
                label: 'Transaksi'
            }, {
                label: 'Order',
                active: true
            }];

            this.getData();
            this.empty();
        }
    }

    empty() {
        this.model = {};
        this.modelParam = {};
        this.modelParam.status = "";
        this.daterange = {};
        this.getEvent();
    }

    selectedDate(value: any, datepicker?: any) {
        datepicker.start = value.start;
        datepicker.end = value.end;
        this.daterange.start = value.start;
        this.daterange.end = value.end;
        this.daterange.label = value.label;
    }

    resetTgl() {
        this.daterange = {};
        this.modelParam = {};
        this.modelParam.periode_mulai = '';
        this.modelParam.periode_selesai = '';
    }

    reloadDataTable(): void {
        if (this.daterange.start !== undefined && this.daterange.end !== undefined) {
            this.modelParam.periode_mulai = moment(this.daterange.start).format('YYYY-MM-DD');
            this.modelParam.periode_selesai = moment(this.daterange.end).format('YYYY-MM-DD');
        }
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.draw();
        });
    }

    getData() {
        this.dtOptions = {
            serverSide: true,
            processing: true,
            ordering: false,
            pagingType: 'full_numbers',
            ajax: (dataTablesParameters: any, callback) => {
                const params = {
                    filter: JSON.stringify(this.modelParam),
                    offset: dataTablesParameters.start,
                    limit: dataTablesParameters.length
                };

                this.landaService.DataGet('/t_order/index', params).subscribe((res: any) => {
                    this.listData = res.data.list;
                    callback({
                        recordsTotal: res.data.totalItems,
                        recordsFiltered: res.data.totalItems,
                        data: [],
                    });
                });
            },
        };
    }

    index() {
        if (this.route.snapshot.paramMap.get("id") !== null && this.route.snapshot.paramMap.get("tipe") == 'true') {
            this.router.navigate(['/transaksi/transaksi-transaction']);
        } else if (this.route.snapshot.paramMap.get("id") !== null && this.route.snapshot.paramMap.get("tipe") == 'order') {
            this.router.navigate(['/transaksi/transaksi-order']);
        } else {
            this.showForm = !this.showForm;
            this.pageTitle = 'Transaksi Order';
            this.getData();
        }
    }

    create() {
        this.empty();
        this.showForm = !this.showForm;
        this.pageTitle = 'Create Transaksi Order';
        this.isView = false;
    }

    edit(no_order) {
        this.landaService.DataGet('/t_order/data_detail', {no_order: no_order}).subscribe((res: any) => {
            this.model = res.data;
            this.showForm = !this.showForm;
            this.pageTitle = 'Order No : ' + this.model.no_order;
            this.getData();
            this.isView = false;
            this.getAddon(this.model);
            this.getdetail(this.model);
        });

    }

    getdetail(val: any) {
        throw new Error('Method not implemented.');
    }

    view(val, no_order) {
        this.landaService.DataGet('/t_order/data_detail', {no_order: no_order}).subscribe((res: any) => {
            this.model = res.data;
            this.showForm = !this.showForm;
            this.pageTitle = 'Order No : ' + this.model.no_order;
            this.getData();
            this.isView = true;
            this.getAddon(this.model);
            this.getdetail(this.model);
        });
    }

    delete(val) {
        // const data = {
        //     id: val.id,
        //     is_deleted: 1,
        //     is_midtrans: val.is_midtrans,
        //     no_order: val.no_order
        // };

        Swal.fire({
            title: 'Apakah anda yakin ?',
            text: 'Menghapus Transaksi Order akan berpengaruh terhadap data lainnya',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            confirmButtonText: 'Ya, Hapus data ini !'
        }).then(result => {
            if (result.value) {
                this.landaService.DataPost('/t_order/delete', val).subscribe((res: any) => {
                    if (res.status_code === 200) {
                        this.reloadDataTable();
                        this.landaService.alertSuccess('Berhasil', 'Transaksi Order telah dihapus !');
                    } else {
                        this.landaService.alertError('Sorry', res.errors);
                    }
                });
            }
        });
    }

    save() {
        this.landaService.DataPost('/t_order/save', this.model).subscribe((res: any) => {
            if (res.status_code === 200) {
                this.landaService.alertSuccess('Berhasil', 'Transaksi Order telah disimpan!');
                this.index();
            } else {
                this.landaService.alertError('Sorry', res.errors);
            }
        });
    }

    export(filter,params) {
        window.open(this.apiUrl + '/t_order/export?filter=' + JSON.stringify(filter) + '&limit=' + params.limit + '&offset=' + params.offset, '_blank');
    }

    exportNew(filter) {
        Swal.fire({
            title: 'Exporting..',
            text: 'Generating data. Do not close this page. Please Wait....',
            icon: 'warning',
            showCancelButton: false,
            showConfirmButton: false,
            cancelButtonColor: '#f46a6a',
            allowOutsideClick: false,
        })
        console.log(JSON.stringify(filter));
        this.getDataExport(filter);
        //window.open(this.apiUrl + '/t_order/export?filter=' + JSON.stringify(filter), '_blank');
    }

    getDataExport(filter) {
         const params = {
            filter: JSON.stringify(filter),
        }
        this.landaService.DataGet('/t_order/pre_export', params).subscribe((res: any) => {
            if(res.status_code == 200){
                this.downloadExport(filter);
            }else{
                this.landaService.alertError('Generating Data Failed', "Something went wrong on the server");
            }
        });

    }

    downloadExport(filter) {
        Swal.fire({
            title: 'Download Report Order',
            text: 'Your data is ready, click download button bellow to start download.',
            icon: 'success',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            confirmButtonText: 'Download'
        }).then(result => {
            if (result.value) {
                window.open(this.apiUrl + '/t_order/exportAll?filter=' + JSON.stringify(filter), '_blank');
            }
        });
    }

    getEvent() {
        this.landaService.DataGet('/get_data/event', {}).subscribe((res: any) => {
            this.listEvent = res.data;
            // if (this.modelParam.event == undefined) {
            //     this.modelParam.event = this.listEvent[0].id;
            //     this.reloadDataTable();
            // }
        })

    }

    getAddon(val) {
        const param = {
            t_order_id: val.id,
            m_user_address_id: val.m_user_address_id
        }

        this.landaService.DataGet('/t_order/getAddon', param).subscribe((res: any) => {
            this.listAddon = res.data;
        });
    }

    getAddonList(val) {
        const param = {
            m_event_id: val.m_event_id,
        }

        this.landaService.DataGet('/t_order/getAddonList', param).subscribe((res: any) => {
            this.listGetAddon = res.data;
        });
    }

    getAddress(val) {
        const param = {
            m_user_id: val.m_user_id,
        }

        this.landaService.DataGet('/t_order/user_address', param).subscribe((res: any) => {
            this.listUserAddress = res.data;
        });
    }

    updateAddon(modal, val) {
        console.log(val)
        this.listGetAddon = [];
        this.formModal = {};
        this.formModal.id = val.id;
        this.modalService.open(modal, {size: 'lg', backdrop: 'static'});
        this.getAddon(val);
        this.getAddonList(val);

    }

    updateAddress(modal, val) {
        this.formModalAddress = {};
        this.formModalAddress.id = val.id;
        this.formModalAddress.m_user_address_id = val.m_user_address_id;

        this.modalService.open(modal, {size: 'md', backdrop: 'static'});

        this.getAddress(val);
    }

    closeModal() {
        this.formModal = undefined;
        this.modalService.dismissAll();
    }

    saveModal() {
        const param = {
            t_order_id: this.formModal.id,
            list: this.listAddon
        }
        this.landaService.DataPost('/t_order/updateAddon', param).subscribe((res: any) => {
            if (res.status_code === 200) {
                this.landaService.alertSuccess('Success', 'Addon berhasil di update!');
                this.modalService.dismissAll();
                this.reloadDataTable();
            } else {
                this.landaService.alertError('Sorry', res.errors);
            }
        });
    }

    saveModalAddress() {
        this.landaService.DataPost('/t_order/updateAddress', this.formModalAddress).subscribe((res: any) => {
            if (res.status_code === 200) {
                this.landaService.alertSuccess('Success', 'Address berhasil di update!');
                this.modalService.dismissAll();
                this.reloadDataTable();
            } else {
                this.landaService.alertError('Sorry', res.errors);
            }
        });
    }

    addDetailAddon() {
        const newDet = {
            m_event_addon_id: '',
            qty: 0
        };

        this.listAddon.push(newDet); 
    }

    removeDetailAddon(val, paramindex) {
        val.splice(paramindex, 1);
    }

    checkAkses(hakAkses) {
        return this.landaService.checkAkses(hakAkses);
        return true;
    }


    listExportExcel(modalExport, val) {
        console.log(val);
        const params = {
            filter: JSON.stringify(val),
        }
        this.landaService.DataGet('/t_order/exportModal', params).subscribe((res: any) => {
            this.listExport = res.data;
        });

        this.formModalExport = {};
        this.formModalExport = val;
        this.modalService.open(modalExport, {size: 'lg', backdrop: 'static'});
    }
}
