import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TPenerimaanComponent } from './t-penerimaan.component';

describe('TPenerimaanComponent', () => {
  let component: TPenerimaanComponent;
  let fixture: ComponentFixture<TPenerimaanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TPenerimaanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TPenerimaanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
