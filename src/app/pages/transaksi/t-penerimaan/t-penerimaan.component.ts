import {Component, OnInit, ViewChild} from '@angular/core';
import {LandaService} from "../../../core/services/landa.service";
import {DataTableDirective} from "angular-datatables";
import {FormControl, FormGroup, Validators} from "@angular/forms";
// import {LandaService} from "../../../core/services/landa.service";
import {Router} from "@angular/router";
import * as moment from "moment/moment";
import Swal, {SweetAlertResult} from 'sweetalert2';
import {NgbCalendar} from "@ng-bootstrap/ng-bootstrap";


@Component({
    selector: 'app-t-penerimaan',
    templateUrl: './t-penerimaan.component.html',
    styleUrls: ['./t-penerimaan.component.scss']
})
export class TPenerimaanComponent implements OnInit {
    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtInstance: Promise<DataTables.Api>;
    dtOptions: any;
    myForm = new FormGroup({
        fileUpload: new FormControl('', [Validators.required]),
    });
    breadCrumbItems: Array<{}>;
    pageTitle: string;
    isView: boolean;
    isEdit: boolean;
    model: any = {};
    form: any = {};
    modelParam: any = {};
    showForm: boolean;
    listData: any;
    listDetail: any = [];
    listBarang: any = [];

    tabContent: string;
    control_link: any = '/t_penerimaan';

    constructor(private landaService: LandaService, private router: Router, private calendar: NgbCalendar) {
    }

    ngOnInit(): void {

        this.pageTitle = 'Penerimaan';
        this.breadCrumbItems = [{
            label: 'Transaksi'
        }, {
            label: 'Penerimaan',
            active: true
        }];
        this.modelParam = {};

        this.getData();
        this.empty();
    }

    reloadDataTable(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.draw();
        });
    }

    getData() {
        this.dtOptions = {
            serverSide: true,
            processing: true,
            ordering: false,
            pagingType: 'full_numbers',
            ajax: (dataTablesParameters: any, callback) => {
                const params = {
                    filter: JSON.stringify(this.modelParam),
                    offset: dataTablesParameters.start,
                    limit: dataTablesParameters.length,
                };

                this.landaService
                    .DataGet('/t_penerimaan/index', params)
                    .subscribe((res: any) => {
                        this.listData = res.data.list;
                        callback({
                            recordsTotal: res.data.totalItems,
                            recordsFiltered: res.data.totalItems,
                            data: [],
                        });
                    });
            },
        };
    }

    toDate(dob) {
        if (dob) {
            const [year, month, day] = dob.split('-');
            const obj = {
                year: parseInt(year),
                month: parseInt(month),
                day: parseInt(day.split(' ')[0].trim()),
            };
            return obj;
        }
    }

    empty() {
        this.model = {};
        this.model.jenis_kelamin = 0;
        this.modelParam = {};
        this.listDetail = [];
        this.getData();
    }

    index() {
        this.showForm = !this.showForm;
        this.pageTitle = 'Data Penerimaan';
        this.getData();
        console.log(this.showForm);
    }

    create() {
        this.empty();
        this.showForm = !this.showForm;
        this.pageTitle = 'Tambah Data Penerimaan';
        this.isView = false;
        this.isEdit = false;
        this.model.tanggal = this.calendar.getToday();
        this.listDetail = [];
        this.getBarang();

    }

    edit(val) {
        this.showForm = !this.showForm;
        this.model = val;
        this.pageTitle = 'Kode : ' + val.kode;
        this.model.tanggal = this.toDate(val.tanggal);
        this.isView = false;
        this.isEdit = true;
        this.getBarang();
    }

    view(val) {
        this.showForm = !this.showForm;
        this.model = val;
        this.tabContent = 'user';
        this.pageTitle = 'User : ' + val.nama;
        this.isView = true;
    }

    save(model, status) {
        const data = {
            status: status,
            data: model,
            detail: this.listDetail
        }
        this.landaService.DataPost('/t_penerimaan/save', data).subscribe((res: any) => {
            if (res.status_code === 200) {
                this.landaService.alertSuccess('Berhasil', 'Data penerimaan telah disimpan!');
                this.index();
                // this.reloadDataTable();

            } else {
                this.landaService.alertError('Sorry', res.errors);
            }
        });
    }

    saveStatus(val) {
        Swal.fire({
            title: 'Masukkan Alasan',
            html: '<textarea rows="5" class=" form-control form-control-sm" id="custom-textarea" style="font-size: 16px;"></textarea>',
            icon: 'info',

            showCancelButton: true,
            confirmButtonText: 'Submit',
            cancelButtonText: 'Cancel',
            preConfirm: () => {
                const inputValue = (document.getElementById('custom-textarea') as HTMLTextAreaElement).value;
                if (!inputValue) {
                    Swal.showValidationMessage('Alasan tidak boleh kosong');
                }
                return inputValue;
            }

        }).then((result: SweetAlertResult) => {
            if (result.isConfirmed && result.value) {
                const inputValue: string = result.value;
                val.alasan = inputValue;
                this.landaService.DataPost('/t_penerimaan/saveStatus', val).subscribe((res: any) => {
                    if (res.status_code === 200) {
                        this.landaService.alertSuccess('Berhasil', 'Data penerimaan telah dihapus!');
                        this.index();
                        // this.reloadDataTable();

                    } else {
                        this.landaService.alertError('Sorry', res.errors);
                    }
                });
            }
        });


    }

    delete(val) {
        const data = {
            id: val != null ? val.id : null,
            is_deleted: 1,
        };
        Swal.fire({
            title: 'Apakah anda yakin ?',
            text: 'Menghapus data user akan berpengaruh terhadap data lainnya',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            confirmButtonText: 'Ya, Hapus data ini !'
        }).then(result => {
            if (result.value) {
                this.landaService.DataPost('/m_user/delete', data).subscribe((res: any) => {
                    if (res.status_code === 200) {
                        this.landaService.alertSuccess('Berhasil', 'Data User telah dihapus !');
                        this.reloadDataTable();

                    } else {
                        this.landaService.alertError('Sorry', res.errors);
                    }
                });
            }
        });
    }

    restore(val) {
        const data = {
            id: val != null ? val.id : null,
            is_deleted: 0,
        };
        Swal.fire({
            title: 'Apakah anda yakin ?',
            text: 'Merestore data user ini',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            confirmButtonText: 'Ya, Restore data ini !'
        }).then(result => {
            if (result.value) {
                this.landaService.DataPost('/m_user/restore', data).subscribe((res: any) => {
                    if (res.status_code === 200) {
                        this.landaService.alertSuccess('Berhasil', 'Data User telah direstore !');
                        this.reloadDataTable();

                    } else {
                        this.landaService.alertError('Sorry', res.errors);
                    }
                });
            }
        });
    }

    approve(val,status) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan approve penerimaan ini?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            // confirmButtonText: 'Ya, Restore data ini !'
        }).then(result => {
            if (result.value) {
                this.showForm = !this.showForm;
                this.save(val, status);
            }
        });
    }


    /**
     * DETAIL PENERIMAAN
     */

    addDetail(val) {
        const newDet = {};
        val.push(newDet);
    }

    removeDetail(val, paramindex) {
        val.splice(paramindex, 1);
    }

    getBarang() {
        this.landaService.DataGet('/t_kasir/getBarang', {}).subscribe((res: any) => {
            this.listBarang = res.data.list;
        });
    }

    kalkulasi() {
        var sub_total = 0;
        var alert = 0;
        this.listDetail.forEach(function (value, key) {
            if (value.m_barang_id !== undefined && value.jumlah !== undefined && value.jumlah != '') {
                var jumlah = parseInt(value.jumlah);

                var number = Number.isInteger(jumlah);

                if (number) {
                    if (jumlah !== undefined && jumlah != null && jumlah > 0) {
                        value.harga = (value.harga === "") ? 0 : value.harga;
                        value.sub_total = parseInt(value.harga) * value.jumlah;
                        sub_total += parseInt(value.sub_total);
                    } else {
                        alert = 1;
                        value.jumlah = undefined;
                        value.sub_total = undefined;
                    }
                } else {
                    alert = 2;
                    value.jumlah = undefined;
                    value.sub_total = undefined;
                }
            }
        });

        if (alert == 1) {
            this.landaService.alertError("Terjadi Kesalahan", "Jumlah barang harus lebih dari 0");
        } else if (alert == 2) {
            this.landaService.alertError("Terjadi Kesalahan", "Jumlah barang harus berisi angka");
        }

        this.model.total = sub_total;
    };
}
