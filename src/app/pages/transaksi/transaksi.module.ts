import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UiSwitchModule } from 'ngx-ui-switch';
import { ArchwizardModule } from 'angular-archwizard';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { Daterangepicker } from 'ng2-daterangepicker';
import { CustomPipeModule } from 'src/app/core/shared/custom-pipe/custom-pipe.module';

import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { PERFECT_SCROLLBAR_CONFIG } from "ngx-perfect-scrollbar";
import { PerfectScrollbarConfigInterface } from "ngx-perfect-scrollbar";

import {
  NgbAlertModule,
  NgbCarouselModule,
  NgbDropdownModule,
  NgbModalModule,
  NgbProgressbarModule,
  NgbTooltipModule,
  NgbPopoverModule,
  NgbPaginationModule,
  NgbNavModule,
  NgbAccordionModule,
  NgbCollapseModule,
  NgbDatepickerModule,
  NgbTimepickerModule,
  NgbModule,
} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { UIModule } from '../../layouts/shared/ui/ui.module';
import { TransaksiRoutingModule } from './transaksi-routing.module';
import { TransaksiOrderComponent } from './transaksi-order/transaksi-order.component';
import { TKasirComponent } from './t-kasir/t-kasir.component';
import { TPengambilanKasComponent } from './t-pengambilan-kas/t-pengambilan-kas.component';
import { TPenerimaanComponent } from './t-penerimaan/t-penerimaan.component';
import { TSetoranComponent } from './t-setoran/t-setoran.component';
import { TSetoranAnggotaComponent } from './t-setoran-anggota/t-setoran-anggota.component';

export const options: Partial<IConfig> = {
  thousandSeparator: '.',
};

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelSpeed: 0.3,
};

@NgModule({
  declarations: [TransaksiOrderComponent, TKasirComponent, TPengambilanKasComponent, TPenerimaanComponent, TSetoranComponent, TSetoranAnggotaComponent],
  imports: [
    CustomPipeModule,
    CommonModule,
    TransaksiRoutingModule,
    PerfectScrollbarModule,
    FormsModule,
    UIModule,
    NgbAlertModule,
    NgbCarouselModule,
    NgbDropdownModule,
    NgbDatepickerModule,
    NgbTimepickerModule,
    NgbModalModule,
    NgbProgressbarModule,
    NgbTooltipModule,
    NgbPopoverModule,
    NgbPaginationModule,
    NgbNavModule,
    NgbAccordionModule,
    NgSelectModule,
    UiSwitchModule,
    NgbCollapseModule,
    NgbModule,
    ReactiveFormsModule,
    ArchwizardModule,
    DataTablesModule,
    Daterangepicker,
    NgxMaskModule.forRoot(options)
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    }
  ]
})
export class TransaksiModule { }
