import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TKasirComponent } from './t-kasir.component';

describe('KasirComponent', () => {
  let component: TKasirComponent;
  let fixture: ComponentFixture<TKasirComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TKasirComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TKasirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
