import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {NgbCalendar, NgbModal, NgbTimepickerConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../../../environments/environment';
import * as moment from 'moment';
import {LandaService} from '../../../core/services/landa.service';
import {ActivatedRoute, Router} from '@angular/router';
import Swal from 'sweetalert2';
import {log} from 'util';


@Component({
    selector: 'app-kasir',
    templateUrl: './t-kasir.component.html',
    styleUrls: ['./t-kasir.component.scss']
})
export class TKasirComponent implements OnInit {
    control_link: any = '/t_kasir';
    @ViewChild(DataTableDirective)

    dtElement: DataTableDirective;
    dtInstance: Promise<DataTables.Api>;
    dtOptions: any;
    daterange: any;
    options: any = {
        locale: {format: 'YYYY-MM-DD'},
        alwaysShowCalendars: false,
    };

    apiUrl = environment.apiURL;
    breadCrumbItems: Array<{}>;
    model: any = {};
    modelCustomer: any = {};
    formModal: any = {};
    is: any = {};
    isView: boolean;
    isFocusInput: boolean;
    loading_save: boolean;
    pageTitle: string;
    modelParam: any = {};
    listCaraPembayaran: any = [];
    listLayanan: any = [];
    listBarang: any = [];
    listGetLayanan: any = [];
    listGetBarang: any = [];
    listGetDiskon: any = [];
    listCustomer: any = [];
    listTerapis: any = [];
    checkout: any = {};
    imageChangedEvent: any = '';
    imageSrc: string;
    listDataProvinsi: any;
    listDataKota: any;
    listDataKecamatan: any;
    listDataDesa: any;

    constructor(
        private landaService: LandaService,
        private route: ActivatedRoute,
        timepicker: NgbTimepickerConfig,
        private router: Router,
        private modalService: NgbModal,
        private calendar: NgbCalendar
    ) {
        timepicker.spinners = false;
        timepicker.size = 'small';
    }

    ngOnInit(): void {
        this.pageTitle = 'Order';
        this.breadCrumbItems = [{
            label: 'Transaksi'
        }, {
            label: 'Order',
            active: true
        }];

        if (this.route.snapshot.paramMap.get('id') !== null) {
            this.getTransaksi(this.route.snapshot.paramMap.get('id'));
        } else {
            this.empty();
        }
    }

    toTime(val) {
        if (val) {
            const [hour, minute, second] = val.split(':');
            const model = {
                hour: parseInt(hour),
                minute: parseInt(minute),
                second: parseInt(second)
            };
            return model;
        }
    }

    toDate(dob) {
        if (dob) {
            const [year, month, day] = dob.split('-');
            const obj = {
                year: parseInt(year),
                month: parseInt(month),
                day: parseInt(day.split(' ')[0].trim()),
            };
            return obj;
        }
    }

    getTransaksi(id) {
        this.landaService.DataGet('/t_kasir/getTransaksi', {id: id}).subscribe((res: any) => {
            this.checkout = res.data.checkout;
            this.getTerapis();
            this.checkout.is_bayar = 1;
            this.checkout.tanggal = this.toDate(moment(res.data.checkout.tanggal).format('YYYY-MM-DD'));
            this.checkout.waktu = this.toTime(res.data.checkout.waktu);

            this.listLayanan = res.data.listLayanan;
            this.listBarang = res.data.listBarang;

            /**
             * START SET TOTAL
             */
            this.checkout.total_layanan = res.data.total.layanan.harga;
            this.checkout.total_layanan_jumlah = res.data.total.layanan.jumlah;
            this.checkout.total_layanan_subtotal = res.data.total.layanan.subtotal;

            this.checkout.total_barang = res.data.total.barang.harga;
            this.checkout.total_barang_jumlah = res.data.total.barang.jumlah;
            this.checkout.total_barang_subtotal = res.data.total.barang.subtotal;

            this.checkout.total_diskon_layanan = res.data.checkout.total_diskon_layanan;
            this.checkout.total_diskon_barang = res.data.checkout.total_diskon_barang;

            this.checkout.jumlah_bayar = parseInt(this.checkout.total_layanan) + parseInt(this.checkout.total_barang) + parseInt(this.checkout.service_layanan) - parseInt(this.checkout.total_diskon);

            /**
             * END SET TOTAL
             */

            /**
             * START SET DATA KANAN
             */
            this.listCaraPembayaran = res.data.caraPembayaran;
            if (this.listCaraPembayaran !== undefined) {
                for (var i = 0; i < this.listCaraPembayaran.length; i++) {
                    this.listCaraPembayaran[i]['jumlah_bayar'] = parseInt(this.listCaraPembayaran[i]['jumlah_bayar']);
                }
            }

            /**
             * END SET DATA KANAN
             */

            this.checkout.cara_pembayaran = res.data.total.pembayaran;
        });
    }


    empty() {

        this.model = {};
        this.modelParam = {};
        this.modelParam.status = '';
        this.daterange = {};
        this.checkout = {};
        this.checkout = {
            is_bayar: 0,
            bayar: false,
            total_layanan_jumlah: 0,
            total_layanan: 0,
            total_layanan_subtotal: 0,
            total_barang_jumlah: 0,
            total_barang: 0,
            total_barang_subtotal: 0,
            total_diskon_layanan: 0,
            total_diskon_barang: 0,
            service_layanan: 0,
            cara_pembayaran: 0,
            jumlah_bayar: 0,
            kekurangan: 0,
            kembalian: 0,
            tanggal: this.calendar.getToday()
        };

        this.getCaraPembayaran();
        this.getDiskon();
        this.getTerapis();

        if (this.route.snapshot.paramMap.get('id') !== null) {
            this.router.navigate(['/transaksi/transaksi-kasir/' + this.route.snapshot.paramMap.get('id')]);
        }
    }

    /**
     * GET TRANSAKSI SETELAH DINOTAKAN
     */


    /**
     * GET GLOBAL
     */

    getTerapis() {
        this.landaService.DataGet('/get_data/user', {}).subscribe((res: any) => {
            this.listTerapis = res.data;
        });
    }

    getCustomerSearch(val) {
        if (val.length >= 3) {
            this.landaService.DataGet('/get_data/getCustomerSearch', {nama: val}).subscribe((res: any) => {
                this.listCustomer = res.data;

                if (this.listCustomer.length == 0) {
                    // const newDet = {
                    //     id: 0,
                    //     nama: val,
                    //     nama_view: val + ' - Baru'
                    // };
                    // this.listCustomer.push(newDet);

                    this.landaService.alertError('Mohon Maaf', ['Nama yang anda masukkan, tidak ditemukan']);
                }
            });
        }

    }


    getCaraPembayaran() {
        this.landaService.DataGet('/t_kasir/getCaraPembayaran', {}).subscribe((res: any) => {
            this.listCaraPembayaran = res.data.list;

            if (this.listCaraPembayaran.length > 0) {
                this.listCaraPembayaran.forEach(function(vals, key) {
                    vals.jumlah_bayar = '';
                });
            }
        });
    }

    /**
     * DETAIL
     */

    removeDetail(val, paramindex) {
        val.splice(paramindex, 1);
        this.getTotalLayanan();
        this.getTotalBarang();
    }

    removeDiskon(val, paramindex, tipe) {
        val.diskon = 0;

        val.jenis_diskon = {
            id: '',
            nama: 'PILIH DISKON',
            nominal: 0
        };

        if (tipe == 'layanan') {
            this.getTotalLayanan();
        } else {
            this.getTotalBarang();
        }
    }

    /**
     * MODAL GLOBAL
     */

    openModal(namaModal, val) {
        if ((this.checkout.m_customer_id === undefined || this.checkout.m_customer_id == null) && val != 'modalCustomer') {
            this.modalService.dismissAll();
            this.landaService.alertError('Peringatan', ['Customer tidak boleh kosong']);
            return;
        }
        if (val == 'modalLayanan') {
            this.getLayanan();
        } else if (val == 'modalLayananPaket') {
            this.getLayanan(2);
        } else if (val == 'modalBarang') {
            this.getBarang();
        } else if (val == 'modalCustomer') {
            this.getProvinsi();
        }

        this.modalService.open(namaModal, {size: 'md', backdrop: 'static'});
    }

    /**
     * MODAL PELAYANAN
     * @param openModalPelayanan
     * @param val
     */

    getDiskon() {
        this.landaService.DataGet('/t_kasir/getDiskon', {}).subscribe((res: any) => {
            this.listGetDiskon = res.data.list;

            if (this.listGetDiskon.length > 0) {
                this.listGetDiskon.unshift({
                    id: '',
                    nama: 'PILIH DISKON',
                    nominal: 0
                });
            }
        });
    }

    getLayanan(tipe = 1) {
        this.landaService.DataGet('/t_kasir/getLayanan', {tipe: tipe}).subscribe((res: any) => {
            this.listGetLayanan = res.data.list;
            this.listGetLayanan.forEach((val: any, key: any) => {
                if (val.tipe == 2) {
                    val.isi_paket_array = val.isi_paket.split(',');
                }
            });

        });
    }

    closeModal() {
        this.formModal = undefined;
        this.modalService.dismissAll();
    }

    choiceLayanan(val) {
        this.modalService.dismissAll();
        val.jumlah = 1;
        val.jenis_diskon = {
            id: '',
            nama: 'PILIH DISKON',
            nominal: 0
        };
        val.diskon = 0;
        val.subtotal = val.harga * val.jumlah;
        this.listLayanan.push(val);
        this.getTotalLayanan();
    }

    /**
     * MODAL BARANG
     * @param openModalBarang
     * @param val
     */

    getBarang() {
        this.landaService.DataGet('/t_kasir/getBarang', {}).subscribe((res: any) => {
            this.listGetBarang = res.data.list;
        });
    }

    choiceBarang(val) {
        this.modalService.dismissAll();

        val.jumlah = 1;
        val.jenis_diskon = {
            id: '',
            nama: 'PILIH DISKON',
            nominal: 0
        };
        val.diskon = 0;
        val.subtotal = val.harga * val.jumlah;
        this.listBarang.push(val);
        this.getTotalBarang();
    }


    /**
     * PERHITUNGAN TOTAL==========================================================
     */

    /**
     * SET DISKON
     */

    setDiskon = function(val) {
        this.checkout.total_diskon = val.nominal !== undefined && val.nominal != null ? val.nominal : 0;

        this.totalPembelian();
    };

    /**
     * GET TOTAL LAYANAN
     */
    getTotalLayanan = function() {
        this.checkout.total_layanan_jumlah = 0;
        this.checkout.total_layanan_subtotal = 0;
        this.checkout.total_layanan = 0;
        this.checkout.total_diskon_layanan = 0;
        console.log(this.listLayanan);
        for (var i = 0; i < this.listLayanan.length; i++) {
            var layanan = this.listLayanan[i];

            layanan.jumlah = layanan.jumlah === undefined || layanan.jumlah === '' ? 0 : layanan.jumlah;

            /**
             * SET TOTAL JUMLAH, DISKON, SUBTOTAL PRODUK
             */

            if (layanan.jenis_diskon !== undefined && layanan.jenis_diskon !== null && this.checkout.is_bayar !== 1) {
                if (layanan.jenis_diskon.id !== 5) {
                    this.listLayanan[i].diskon = parseInt(layanan.jumlah) * parseInt(layanan.jenis_diskon.nominal);
                }
            }

            if (this.listLayanan[i].diskon !== undefined) {
                this.checkout.total_diskon_layanan += this.listLayanan[i].diskon;
            }

            if (((layanan.jumlah * parseInt(layanan.harga)) - parseInt(layanan.diskon)) < 0) {
                this.listLayanan[i].subtotal = (layanan.jumlah * parseInt(layanan.harga));
                this.checkout.total_diskon_layanan = this.checkout.total_diskon_layanan - this.listLayanan[i].diskon;

                layanan.diskon = 0;
                layanan.jenis_diskon = {
                    id: '',
                    nama: 'PILIH DISKON'
                };
                this.landaService.alertError('Peringatan', ['Diskon tidak boleh melebihi harga jual']);
            } else {

                this.listLayanan[i].subtotal = (layanan.jumlah * parseInt(layanan.harga)) - parseInt(layanan.diskon);
            }

            this.checkout.total_layanan_jumlah += parseInt(layanan.jumlah);
            this.checkout.total_layanan += (layanan.jumlah * parseInt(layanan.harga));
            this.checkout.total_layanan_subtotal += this.listLayanan[i].subtotal;
        }

        this.totalPembelian(this.listLayanan[i]);

    };


    /**
     * GET TOTAL BARANG
     */
    getTotalBarang = function() {
        this.checkout.total_barang_jumlah = 0;
        this.checkout.total_barang_subtotal = 0;
        this.checkout.total_barang = 0;
        this.checkout.total_diskon_barang = 0;

        for (var i = 0; i < this.listBarang.length; i++) {
            var barang = this.listBarang[i];

            barang.jumlah = barang.jumlah === undefined || barang.jumlah === '' ? 0 : barang.jumlah;

            /**
             * SET TOTAL JUMLAH, DISKON, SUBTOTAL PRODUK
             */

            if (barang.jenis_diskon !== undefined && this.checkout.is_bayar !== 1) {
                if (barang.jenis_diskon.id !== 5) {
                    this.listBarang[i].diskon = parseInt(barang.jumlah) * parseInt(barang.jenis_diskon.nominal);
                }
            }

            if (this.listBarang[i].diskon !== undefined) {
                this.checkout.total_diskon_barang += this.listBarang[i].diskon;
            }
            if (((barang.jumlah * parseInt(barang.harga)) - parseInt(barang.diskon)) < 0) {
                this.listBarang[i].subtotal = (barang.jumlah * parseInt(barang.harga));
                this.checkout.total_diskon_barang = this.checkout.total_diskon_barang - this.listBarang[i].diskon;

                barang.diskon = 0;
                barang.jenis_diskon = {
                    id: '',
                    nama: 'PILIH DISKON'
                };
                this.landaService.alertError('Peringatan', ['Diskon tidak boleh melebihi harga jual']);
            } else {
                this.listBarang[i].subtotal = (barang.jumlah * parseInt(barang.harga)) - parseInt(barang.diskon);
            }

            this.checkout.total_barang_jumlah += parseInt(barang.jumlah);
            this.checkout.total_barang += (barang.jumlah * parseInt(barang.harga));
            this.checkout.total_barang_subtotal += this.listBarang[i].subtotal;
        }

        this.totalPembelian();
    };

    totalPembelian = function() {
        if (Number.isNaN(parseInt(this.checkout.total_barang)) || this.checkout.total_barang == null) {
            this.checkout.total_barang = 0;
        }

        if (Number.isNaN(parseInt(this.checkout.total_layanan)) || this.checkout.total_layanan == null) {
            this.checkout.total_layanan = 0;
        }

        if (Number.isNaN(parseInt(this.checkout.total_diskon_layanan)) || this.checkout.total_diskon_layanan == null) {
            this.checkout.total_diskon_layanan = 0;
        }

        if (Number.isNaN(parseInt(this.checkout.service_layanan)) || this.checkout.service_layanan == null) {
            this.checkout.service_layanan = 0;
        }


        this.checkout.total_pembelian_subtotal = parseInt(this.checkout.total_layanan) + parseInt(this.checkout.total_barang) + parseInt(this.checkout.service_layanan) - parseInt(this.checkout.total_diskon_layanan) - parseInt(this.checkout.total_diskon_barang);

        this.jumlah_bayar();
    };

    jumlah_bayar = function() {
        this.checkout.jumlah_bayar = parseInt(this.checkout.total_layanan) + parseInt(this.checkout.total_barang) + parseInt(this.checkout.service_layanan) - parseInt(this.checkout.total_diskon_layanan) - parseInt(this.checkout.total_diskon_barang);
        this.checkout.kekurangan = this.checkout.jumlah_bayar - this.checkout.cara_pembayaran;
        this.checkout.kembalian = this.checkout.cara_pembayaran - this.checkout.jumlah_bayar;
    };

    getTotalPembayaran = function() {
        var total = 0;
        for (var i = 0; i < this.listCaraPembayaran.length; i++) {
            var bayar = this.listCaraPembayaran[i];
            if (bayar.jumlah_bayar != undefined && bayar.jumlah_bayar !== '') {
                total += (parseInt(bayar.jumlah_bayar));
            }

        }
        this.checkout.cara_pembayaran = total;
        this.jumlah_bayar();
    };


    /**
     * CHECKOUT
     */

    save_checkout() {
        this.loading_save = true;
        if (this.checkout.m_customer_id === undefined) {
            this.landaService.alertError('Peringatan', ['Customer Kosong']);
            this.loading_save = false;
            return;
        }
        /**
         * pengecekan jika layanan wajib isi terapis
         */

        if (this.listLayanan.length != 0 && this.checkout.terapis_id === undefined) {
            this.landaService.alertError('Peringatan', ['Anda belum memilih terapis!']);

            this.loading_save = false;
            return;
        }

        /**
         * pengecekan jika belum memilih item sama sekali
         */
        if (this.listLayanan.length == 0 && this.listBarang.length == 0) {
            this.landaService.alertError('Peringatan', ['Anda belum menginputkan Item!']);

            this.loading_save = false;
            return;
        }

        var total_bayar: any = 0;
        for (var i = 0; i < this.listCaraPembayaran.length; i++) {
            if (Number.isNaN(parseInt(this.listCaraPembayaran[i]['jumlah_bayar'])) || this.listCaraPembayaran[i]['jumlah_bayar'] == '') {
                var jumlah_bayar: any = 0;
            } else {
                var jumlah_bayar: any = parseInt(this.listCaraPembayaran[i]['jumlah_bayar']);
            }
            if (jumlah_bayar !== undefined) {
                total_bayar += parseInt(jumlah_bayar);
            }
        }

        if (total_bayar < this.checkout.jumlah_bayar) {
            this.landaService.alertError('Peringatan', ['Pembayaran Anda Kurang']);
            this.loading_save = false;
            return;
        }

        var param = {
            'checkout': this.checkout,
            'listLayanan': this.listLayanan,
            'listBarang': this.listBarang,
            'cara_pembayaran': this.listCaraPembayaran
        };
        this.landaService.DataPost('/t_kasir/checkout', param).subscribe((res: any) => {
            if (res.status_code === 200) {
                this.landaService.alertSuccess('Berhasil', 'Transaksi berhasil di checkout');
                this.loading_save = false;
                this.checkout.is_bayar = 1;
                this.checkout.bayar = true;
                this.router.navigate(['/transaksi/transaksi-kasir/' + res.data.id]);
                setTimeout(() => {
                    this.print_nota(res.data.id);
                }, 1000);

            } else {
                this.loading_save = false;
                this.landaService.alertError('Sorry', res.errors);
            }
        });
    }


    /**
     * MMODAL CUSTOMER
     */

    getProvinsi() {
        this.landaService.DataGet('/appwilayah/getProvinsi', {}).subscribe((res: any) => {
            this.listDataProvinsi = res.data.list;
        });
    }

    getKota(param = null) {
        let id;
        if (this.modelCustomer.w_provinsi_id != '' || this.modelCustomer.w_provinsi_id != 'undefined') {
            id = this.modelCustomer.w_provinsi_id;
        } else {
            id = '0';
        }
        this.landaService
            .DataGet('/appwilayah/getKota/' + id, {})
            .subscribe((res: any) => {
                this.listDataKota = res.data.list;
                if (param == 'change') {
                    this.modelCustomer.w_kota_id = null;
                    this.modelCustomer.w_kecamatan_id = null;
                    this.modelCustomer.w_desa_id = null;

                }
            });
    }

    getKecamatan(param = null) {
        let id;
        if (this.modelCustomer.w_kota_id != '') {
            id = this.modelCustomer.w_kota_id;
        } else {
            id = '0';
        }
        this.landaService
            .DataGet('/appwilayah/getKecamatan/' + id, {})
            .subscribe((res: any) => {
                this.listDataKecamatan = res.data.list;
                if (param == 'change') {
                    this.modelCustomer.w_kecamatan_id = null;
                    this.modelCustomer.w_desa_id = null;

                }
            });
    }

    getDesa(param = null) {
        let id;
        if (this.modelCustomer.w_kecamatan_id != '') {
            id = this.modelCustomer.w_kecamatan_id;
        } else {
            id = '0';
        }
        this.landaService
            .DataGet('/appwilayah/getDesa/' + id, {})
            .subscribe((res: any) => {
                this.listDataDesa = res.data.list;
                if (param == 'change') {
                    this.modelCustomer.w_desa_id = null;

                }
            });
    }

    saveCustomer() {
        const final = Object.assign(this.modelCustomer);

        this.landaService.DataPost('/m_customer/save', final).subscribe((res: any) => {
            if (res.status_code === 200) {
                this.landaService.alertSuccess('Berhasil', 'Data Customer telah disimpan!');
                this.getCustomerSearch('a');
                this.closeModal();
            } else {
                this.landaService.alertError('Sorry', res.errors);
            }
        });

    }

    print_nota = function(id) {
        const data = {
            id: id,
        };
        window.open(this.apiUrl + '/nota/print?' + $.param(data), '_blank');
    };
}
