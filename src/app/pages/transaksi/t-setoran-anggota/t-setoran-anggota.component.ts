import {Component, OnInit, ViewChild} from '@angular/core';
import {LandaService} from "../../../core/services/landa.service";
import {DataTableDirective} from "angular-datatables";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import * as moment from "moment/moment";
import Swal, {SweetAlertResult} from 'sweetalert2';
import {NgbCalendar} from "@ng-bootstrap/ng-bootstrap";


@Component({
  selector: 'app-t-setoran-anggota',
  templateUrl: './t-setoran-anggota.component.html',
  styleUrls: ['./t-setoran-anggota.component.scss']
})
export class TSetoranAnggotaComponent implements OnInit {
    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtInstance: Promise<DataTables.Api>;
    dtOptions: any;
    myForm = new FormGroup({
        fileUpload: new FormControl('', [Validators.required]),
    });
    breadCrumbItems: Array<{}>;
    pageTitle: string;
    isView: boolean;
    isEdit: boolean;
    model: any = {};
    form: any = {};
    modelParam: any = {};
    showForm: boolean;
    listData: any;
    listDetail: any = [];
    listBarang: any = [];
    listPJ: any = [];
    listSetoran: any = [];

    tabContent: string;
    control_link: any = '/t_setorananggota';

    constructor(private landaService: LandaService, private router: Router, private calendar: NgbCalendar) {
    }

    ngOnInit(): void {

        this.pageTitle = 'Setoran';
        this.breadCrumbItems = [{
            label: 'Transaksi'
        }, {
            label: 'Setoran',
            active: true
        }];
        this.modelParam = {};

        this.getData();
        this.empty();
    }

    reloadDataTable(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.draw();
        });
    }

    getData() {
        this.dtOptions = {
            serverSide: true,
            processing: true,
            ordering: false,
            pagingType: 'full_numbers',
            ajax: (dataTablesParameters: any, callback) => {
                const params = {
                    filter: JSON.stringify(this.modelParam),
                    offset: dataTablesParameters.start,
                    limit: dataTablesParameters.length,
                };

                this.landaService
                    .DataGet('/t_setorananggota/index', params)
                    .subscribe((res: any) => {
                        this.listData = res.data.list;
                        callback({
                            recordsTotal: res.data.totalItems,
                            recordsFiltered: res.data.totalItems,
                            data: [],
                        });
                    });
            },
        };
    }

    toDate(dob) {
        if (dob) {
            const [year, month, day] = dob.split('-');
            const obj = {
                year: parseInt(year),
                month: parseInt(month),
                day: parseInt(day.split(' ')[0].trim()),
            };
            return obj;
        }
    }

    empty() {
        this.model = {};
        this.modelParam = {};
        this.listDetail = [];
        this.modelParam.status = null;
        this.modelParam.is_deleted = 0;
        this.getData();
        this.getSetoran();
        this.getPJ();
    }

    index() {
        this.showForm = !this.showForm;
        this.pageTitle = 'Data Setoran';
        this.getData();
    }

    create() {
        this.empty();
        this.showForm = !this.showForm;
        this.pageTitle = 'Tambah Data Setoran';
        this.isView = false;
        this.isEdit = false;
        this.model.tanggal = this.calendar.getToday();
        this.listDetail = [];
        this.getBarang();

    }

    edit(val) {
        this.showForm = !this.showForm;
        this.model = val;
        this.model.tanggal = this.toDate(val.tanggal);
        this.isView = false;
        this.isEdit = true;
        this.getBarang();
        this.getDetail(val.t_setoran_id);
    }

    view(val) {
        this.showForm = !this.showForm;
        this.model = val;
        this.model = val;
        this.model.tanggal = this.toDate(val.tanggal);
        this.isView = true;
        this.getBarang();
        this.getDetail(val.t_setoran_id);
    }

    save(model) {
        const data = {
            data: model
        }
        this.landaService.DataPost('/t_setorananggota/save', data).subscribe((res: any) => {
            if (res.status_code === 200) {
                this.landaService.alertSuccess('Berhasil', 'Data Setoran telah disimpan!');
                this.index();
            } else {
                this.landaService.alertError('Sorry', res.errors);
            }
        });
    }
  
    delete(val) {
        const data = {
            id: val != null ? val.id : null,
            is_deleted: 1,
        };
        Swal.fire({
            title: 'Apakah anda yakin ?',
            text: 'Menghapus data Kebutuhan Setoran akan berpengaruh terhadap data lainnya',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            confirmButtonText: 'Ya, Hapus data ini !'
        }).then(result => {
            if (result.value) {
                this.landaService.DataPost('/t_setorananggota/delete', data).subscribe((res: any) => {
                    if (res.status_code === 200) {
                        this.landaService.alertSuccess('Berhasil', 'Data Kebutuhan Setoran telah dihapus !');
                        this.reloadDataTable();

                    } else {
                        this.landaService.alertError('Sorry', res.errors);
                    }
                });
            }
        });
    }

    restore(val) {
        const data = {
            id: val != null ? val.id : null,
            is_deleted: 0,
        };
        Swal.fire({
            title: 'Apakah anda yakin ?',
            text: 'Merestore data Kebutuhan Setoran ini',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            confirmButtonText: 'Ya, Restore data ini !'
        }).then(result => {
            if (result.value) {
                this.landaService.DataPost('/t_setorananggota/restore', data).subscribe((res: any) => {
                    if (res.status_code === 200) {
                        this.landaService.alertSuccess('Berhasil', 'Data Kebutuhan Setoran telah direstore !');
                        this.reloadDataTable();

                    } else {
                        this.landaService.alertError('Sorry', res.errors);
                    }
                });
            }
        });
    }

    approve(val) {
        const data = {
            id: val != null ? val.id : null,
            status: 0,
        };
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan approve Setoran ini?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            // confirmButtonText: 'Ya, Restore data ini !'
        }).then(result => {
            if (result.value) {
                this.landaService.DataPost('/t_setorananggota/approve', data).subscribe((res: any) => {
                    if (res.status_code === 200) {
                        this.landaService.alertSuccess('Berhasil', 'Data Setoran telah disimpan!');
                        this.reloadDataTable();
                    } else {
                        this.landaService.alertError('Sorry', res.errors);
                    }
                });
            }
        });
    }
    
    getBarang() {
      this.landaService.DataGet('/t_setorananggota/getBarang', {}).subscribe((res: any) => {
          this.listBarang = res.data.list;
      });
    }
    getDetail(id){
      const data = {
        id: id
    }
      this.landaService.DataGet('/t_setorananggota/getDetail', data).subscribe((res: any) => {
        this.listDetail= res.data.list;
    });
    }
    getSetoran(){
      this.landaService.DataGet('/t_setorananggota/getSetoran', {}).subscribe((res: any) => {
          this.listSetoran= res.data.list;
      });
    }
    getPJ(){
        this.landaService.DataGet('/t_setorananggota/getPJ', {}).subscribe((res: any) => {
            this.listPJ= res.data.list;
        });
    }

    checkAkses(hakAkses) {
        return this.landaService.checkAkses(hakAkses);
    }
}

