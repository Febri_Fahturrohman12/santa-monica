import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TSetoranAnggotaComponent } from './t-setoran-anggota.component';

describe('TSetoranAnggotaComponent', () => {
  let component: TSetoranAnggotaComponent;
  let fixture: ComponentFixture<TSetoranAnggotaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TSetoranAnggotaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TSetoranAnggotaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
