import {NgModule} from '@angular/core';
import {Component, OnInit, ViewChild} from "@angular/core";
import {environment} from "../../../environments/environment";
import {DataTableDirective} from "angular-datatables";
import {LandaService} from "../../core/services/landa.service";
import {ChartDataSets, ChartOptions} from 'chart.js';
import {MultiDataSet, Color, Label} from 'ng2-charts';
import * as moment from 'moment';
import {ChartType} from 'chart.js';
import {Title} from '@angular/platform-browser';
import { AuthenticationService } from "../../core/services/auth.service";

@Component({
    selector: "app-dashboard",
    templateUrl: "./dashboard.component.html",
    styleUrls: ["./dashboard.component.scss"],
})

export class DashboardComponent implements OnInit {
    apiURL = environment.apiURL;
    apiUrl = environment.apiURL;
    breadCrumbItems: Array<{}>;
    pageTitle: string;
    model: any = [];
    y: any = [];
    listPenjualan:any = [];
    
    listData: any = [];
    TotalPenjualan:any;
    
    modelParam: any = {};

    totalData: any = {};
    user: any;
    datenow: {
		day,
		date,
		month,
		year
	};
    imagedefault: any;
    datauser: {
        nama,
        tempat_lahir,
        tgl_lahir,
        m_roles_id,
        nama_roles
    };
    
    listAnggota: any = [];
    TotalAnggota: any;

    
    listBarang: any = [];
    listSetoran: any = [];
    
    listBarangSetoran: any = [];
    keterangansetor: any;
    availablesetor: any;
    tampilkancek: any;

    daterange: any = {};
    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtInstance: Promise<DataTables.Api>;
    dtOptions: any;

    options: any = {
        locale: {
            format: "DD-MM-YYYY",
        },
        alwaysShowCalendars: true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'day'), moment()],
            'Last 7 Days': [moment().subtract(7, 'day'), moment()],
            'Last 30 Days': [moment().subtract(30, 'day'), moment()],
            'This Month': [moment().startOf("month"), moment().endOf("month")],
            'Last Month': [moment().subtract(1, 'month'), moment()],
            'Last 3 Months': [moment().subtract(4, 'month'), moment()]
        }
    };
    constructor(private landaService: LandaService, private titleService: Title, 
		private authenticationService: AuthenticationService,) {}

    ngOnInit() {
        this.pageTitle = "Dashboard";
        this.breadCrumbItems = [
            {
                label: "Santa Monica 17th",
                active: true,
            },
        ];
        this.datenow = {
			day : '',
			date: '',
			month: '',
			year: '',
		}
        this.datauser = {
			nama : '',
			tempat_lahir: '',
			tgl_lahir: '',
            m_roles_id: '',
            nama_roles: ''
		}
        
        this.listData = [];
		this.user = this.authenticationService.getDetailUser();
        this.TotalPenjualan = "";
        this.TotalAnggota = "";
        this.tampilkancek = 0;
        this.keterangansetor = "Cek dulu setorannya disini";
        this.availablesetor = 0;

        this.getIndex();
        this.getAnggota();
        this.getBarang();
        this.getSetoran();
    }
    selectedDate(value: any, datepicker?: any) {
        datepicker.start = value.start;
        datepicker.end = value.end;
        this.daterange.start = value.start;
        this.daterange.end = value.end;
        this.daterange.label = value.label;
    }

    getIndex() {
		this.landaService.DataGet('/dashboard/index', {}).subscribe((res: any) => {
			this.datenow = res.data.datenow;
            this.imagedefault = res.data.imagedefault;
            this.datauser = res.data.datauser;
		});
	}

    getAnggota(){
        const params = {
            filter: JSON.stringify(this.modelParam),
        };
        this.landaService.DataGet('/dashboard/getAnggota', params).subscribe((res: any) => {
			this.listAnggota = res.data.list;
			this.TotalAnggota = res.data.total;
		});
    }

    getBarang(){
        const params = {
            filter: JSON.stringify(this.modelParam),
        };
        this.landaService.DataGet('/dashboard/getBarang', params).subscribe((res: any) => {
			this.listBarang = res.data.list;
		});
    }
    getSetoran(){
        this.landaService.DataGet('/t_setorananggota/getSetoran', {}).subscribe((res: any) => {
            this.listSetoran= res.data.list;
        });
    }

    resettampilancek(){
        this.tampilkancek = 0;
        this.keterangansetor = "Cek dulu setorannya disini";
        this.availablesetor = 0;
    }

    cekSetoran(){
        const params = {
            filter: JSON.stringify(this.modelParam),
        };
        this.landaService.DataGet('/dashboard/cekSetoran', params).subscribe((res: any) => {
            this.tampilkancek = 1;
            this.keterangansetor = res.data.keterangan;
            this.availablesetor = res.data.available;
			this.listBarangSetoran = res.data.barangsetoran;
		});
    }
}


