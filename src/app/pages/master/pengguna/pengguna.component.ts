import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { LandaService } from '../../../core/services/landa.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-pengguna',
  templateUrl: './pengguna.component.html',
  styleUrls: ['./pengguna.component.scss']
})
export class PenggunaComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtInstance: Promise<DataTables.Api>;
  dtOptions: any;
  breadCrumbItems: Array<{}>;
  pageTitle: string;
  isView: boolean;
  isEdit: boolean;
  model: {
    id,
    nama,
    email,
    alamat,
    telepon,
    username,
    password,
    m_jabatan_id,
    akses_wilayah,
    is_super_admin,

    akses: {
      // hakakses,
      pengguna,
      jabatan,
      petani,
      supplier,
      bahan_penunjang,
      pandega,
      hatchery,
      harga,
      size,
      spesies,
      transaksi,
      laporan,
      pembenihan,
      budidaya,
      tambak,
      pernyataan,
      kode_form,
      kesepakatan,
      tes_rasa,
      setting_aplikasi,
      grade,
      rm,
      status

      form1: {
        copy,
        print,
        input,
        lihat,
        approval

      },
      form2: {
        copy,
        print,
        input,
        lihat,
        approval

      },
      form3: {
        copy,
        print,
        input,
        lihat,
        approval

      },
      form4: {
        copy,
        print,
        input,
        lihat,
        approval

      },
      form5: {
        copy,
        print,
        input,
        lihat,
        approval

      },
      form6: {
        copy,
        print,
        input,
        lihat,
        approval

      },
      form7: {
        copy,
        print,
        input,
        lihat,
        approval

      },
      form8: {
        copy,
        print,
        input,
        lihat,
        approval

      },
      form9: {
        copy,
        print,
        input,
        lihat,
        approval

      },
      form10: {
        copy,
        print,
        input,
        lihat,
        approval

      },
      form11: {
        copy,
        print,
        input,
        lihat,
        approval

      },
      form12: {
        copy,
        print,
        input,
        lihat,
        approval

      },
      master_list_tambak: {
        copy,
        print,
        lihat,
        approval

      },
      daftar_tambak_panen: {
        copy,
        print,
        lihat,
        approval

      },
      pengajuan_tes_rasa: {
        copy,
        print,
        lihat,
        approval

      },
      kunjungan_ics: {
        copy,
        print,
        lihat,
        approval

      },
      list_pembenihan: {
        copy,
        print,
        lihat,
        approval

      },
      list_tambak_echoshrimps: {
        copy,
        print,
        lihat,
        approval

      },
      profile_tambak_echosrimps: {
        copy,
        print,
        lihat,
        approval

      },
      list_petani_echoshrimps: {
        copy,
        print,
        lihat,
        approval

      },
      biodata_petani: {
        copy,
        print,
        lihat,
        approval

      },
      biodata_penjaga_tambak: {
        copy,
        print,
        lihat,
        approval

      },
      rekap_data_benur: {
        copy,
        print,
        lihat,
        approval

      },
      pembelian_insentif: {
        copy,
        print,
        lihat,
        approval

      },
      rekap_pengiriman_udang: {
        copy,
        print,
        lihat,
        approval

      },
      produktifitas_budidaya: {
        copy,
        print,
        lihat,
        approval

      },
      komparansi_penlu: {
        copy,
        print,
        lihat,
        approval

      },
      pengajuan_raw_material: {
        copy,
        print,
        lihat,
        approval

      },
      realisasi_raw_material: {
        copy,
        print,
        lihat,
        approval

      },
      balance_report: {
        copy,
        print,
        lihat,
        approval

      }
    }


  }
  modelParam: {
    nama,
    alamat,

  }

  modelCheck: {
    DataMaster,
    Tabel,
    Form
  }
  listData: any;
  listJabatan: any;
  listAkses: any;
  showForm: boolean;
  tabContent: string;
  listWilayah: any;

  constructor(private landaService: LandaService, private router: Router) { }

  ngOnInit(): void {
    this.getJabatan();
    this.getHakakses();
    this.getWilayah();

    this.pageTitle = 'Pengguna';
    this.breadCrumbItems = [{
      label: 'Master'
    }, {
      label: 'Pengguna',
      active: true
    }];
    this.modelParam = {
      nama: '',
      alamat: '',
      // jabatan: '',
    }

    this.getData();
    this.empty();
  }

  reloadDataTable(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.draw();
    });
  }
  getJabatan() {
    this.landaService.DataGet('/m_jabatan/index', {}).subscribe((res: any) => {
      this.listJabatan = res.data.list;

    });
  }
  getHakakses() {
    this.landaService.DataGet('/m_hakakses/index', {}).subscribe((res: any) => {
      this.listAkses = res.data.list;


    });
  }

  checkAllKolom(val, arr) {
    arr.forEach((value: any, key: any) => {
      Object.keys(value).forEach(key => {
        if (val) {
          this.model.akses[value] = true;
        } else {
          this.model.akses[value] = false;

        }
      });

    });
  }
  checkAllKolom1(val, arr) {
    arr.forEach((value: any, key: any) => {
      Object.keys(value).forEach(key => {
        if (val) {
          this.model.akses[key][value[key]] = true;
        } else {
          this.model.akses[key][value[key]] = false;

        }
      });

    });
  }
  checkAllKolom2(val, arr) {
    arr.forEach((value: any, key: any) => {
      Object.keys(value).forEach(key => {
        if (val) {
          this.model.akses[key][value[key]] = true;
        } else {
          this.model.akses[key][value[key]] = false;

        }
      });

    });
  }


  getData() {
    this.dtOptions = {
      serverSide: true,
      processing: true,
      ordering: false,
      pagingType: 'full_numbers',
      ajax: (dataTablesParameters: any, callback) => {
        const params = {
          filter: JSON.stringify(this.modelParam),
          offset: dataTablesParameters.start,
          limit: dataTablesParameters.length,
        };

        this.landaService
          .DataGet('/m_pengguna/index', params)
          .subscribe((res: any) => {
            this.listData = res.data.list;
            callback({
              recordsTotal: res.data.totalItems,
              recordsFiltered: res.data.totalItems,
              data: [],
            });
          });
      },
    };
  }

  empty() {
    this.modelCheck = {
      DataMaster: false,
      Tabel: false,
      Form: false

    }
    this.model = {
      id: null,
      nama: '',
      email: '',
      alamat: '',
      telepon: '',
      username: '',
      password: '',
      m_jabatan_id: '',
      is_super_admin: false,
      akses_wilayah:'',

      akses: {
        laporan: false,
        transaksi: false,
        pengguna: false,
        // hakakses: false,
        jabatan: false,
        petani: false,
        supplier: false,
        bahan_penunjang: false,
        pandega: false,
        hatchery: false,
        harga: false,
        size: false,
        spesies: false,
        pembenihan: false,
        budidaya: false,
        tambak: false,
        pernyataan: false,
        kode_form: false,
        kesepakatan: false,
        tes_rasa: false,
        setting_aplikasi: false,
        grade: false,
        rm: false,
        status: false,


        form1: {
          copy: false,
          print: false,
          input: false,
          lihat: false,
          approval: false

        },
        form2: {
          copy: false,
          print: false,
          input: false,
          lihat: false,
          approval: false

        },
        form3: {
          copy: false,
          print: false,
          input: false,
          lihat: false,
          approval: false

        },
        form4: {
          copy: false,
          print: false,
          input: false,
          lihat: false,
          approval: false

        },
        form5: {
          copy: false,
          print: false,
          input: false,
          lihat: false,
          approval: false

        },
        form6: {
          copy: false,
          print: false,
          input: false,
          lihat: false,
          approval: false

        },
        form7: {
          copy: false,
          print: false,
          input: false,
          lihat: false,
          approval: false

        },
        form8: {
          copy: false,
          print: false,
          input: false,
          lihat: false,
          approval: false

        },
        form9: {
          copy: false,
          print: false,
          input: false,
          lihat: false,
          approval: false

        },
        form10: {
          copy: false,
          print: false,
          input: false,
          lihat: false,
          approval: false

        },
        form11: {
          copy: false,
          print: false,
          input: false,
          lihat: false,
          approval: false

        },
        form12: {
          copy: false,
          print: false,
          input: false,
          lihat: false,
          approval: false

        },
        master_list_tambak: {
          copy: false,
          print: false,
          lihat: false,
          approval: false

        },
        daftar_tambak_panen: {
          copy: false,
          print: false,
          lihat: false,
          approval: false

        },
        pengajuan_tes_rasa: {
          copy: false,
          print: false,
          lihat: false,
          approval: false

        },
        kunjungan_ics: {
          copy: false,
          print: false,
          lihat: false,
          approval: false

        },
        list_pembenihan: {
          copy: false,
          print: false,
          lihat: false,
          approval: false

        },
        list_tambak_echoshrimps: {
          copy: false,
          print: false,
          lihat: false,
          approval: false

        },
        profile_tambak_echosrimps: {
          copy: false,
          print: false,
          lihat: false,
          approval: false

        },
        list_petani_echoshrimps: {
          copy: false,
          print: false,
          lihat: false,
          approval: false

        },
        biodata_petani: {
          copy: false,
          print: false,
          lihat: false,
          approval: false

        },
        biodata_penjaga_tambak: {
          copy: false,
          print: false,
          lihat: false,
          approval: false

        },
        rekap_data_benur: {
          copy: false,
          print: false,
          lihat: false,
          approval: false

        },
        pembelian_insentif: {
          copy: false,
          print: false,
          lihat: false,
          approval: false

        },
        rekap_pengiriman_udang: {
          copy: false,
          print: false,
          lihat: false,
          approval: false

        },
        produktifitas_budidaya: {
          copy: false,
          print: false,
          lihat: false,
          approval: false

        },
        komparansi_penlu: {
          copy: false,
          print: false,
          lihat: false,
          approval: false

        },
        pengajuan_raw_material: {
          copy: false,
          print: false,
          lihat: false,
          approval: false

        },
        realisasi_raw_material: {
          copy: false,
          print: false,
          lihat: false,
          approval: false

        },
        balance_report: {
          copy: false,
          print: false,
          lihat: false,
          approval: false

        }
      }



    }
    this.getData();
  }

  index() {
    this.showForm = !this.showForm;
    this.pageTitle = 'Data Pengguna';
    this.getData();
  }

  create() {
    this.getTabContent();
    this.empty();
    this.tabContent = 'pengguna';
    this.showForm = !this.showForm;
    this.pageTitle = 'Tambah Data Pengguna';
    this.isView = false;
    this.isEdit = false;
    
  }

  edit(val) {
    this.getTabContent();
    this.showForm = !this.showForm;
    this.tabContent = 'pengguna';
    this.model = val;
    this.model.password = "";
    this.pageTitle = 'Pengguna : ' + val.nama;
    this.getData();
    this.isView = false;
    this.isEdit = true;
   
  }
  view(val) {
    this.getTabContent();
    this.showForm = !this.showForm;
    this.model = val;
    this.tabContent = 'pengguna';
    this.pageTitle = 'Pengguna : ' + val.nama;
    this.getData();
    this.isView = true;
  }


  setTabContent(content) {
    this.tabContent = content;
  }

  getTabContent() {
    return this.tabContent;
  }
getWilayah(){
  this.landaService.DataGet('/L_produktifitas_budidaya/wilayah',{}).subscribe((res: any) =>{
    this.listWilayah = res.data;
  })
}
  save() {
    const final = Object.assign(this.model);
   
    this.landaService.DataPost('/m_pengguna/save', final).subscribe((res: any) => {
      if (res.status_code === 200) {
        this.landaService.alertSuccess('Berhasil', 'Data Pengguna telah disimpan!');
        this.index();
      } else {
        this.landaService.alertError('Sorry', res.errors);
      }
    });

  }



  delete(val) {
    const data = {
      id: val != null ? val.id : null,
      is_deleted: 1,
    };
    Swal.fire({
      title: 'Apakah anda yakin ?',
      text: 'Menghapus data pengguna akan berpengaruh terhadap data lainnya',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Ya, Hapus data ini !'
    }).then(result => {
      if (result.value) {
        this.landaService.DataPost('/m_pengguna/delete', data).subscribe((res: any) => {
          if (res.status_code === 200) {
            this.landaService.alertSuccess('Berhasil', 'Data Pengguna telah dihapus !');
            this.reloadDataTable();

          } else {
            this.landaService.alertError('Sorry', res.errors);
          }
        });
      }
    });
  }



}
