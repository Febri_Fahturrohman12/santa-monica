import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {LandaService} from '../../../core/services/landa.service';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import { AuthenticationService } from '../../../core/services/auth.service';
import * as moment from 'moment';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-m-akses',
  templateUrl: './m-akses.component.html',
  styleUrls: ['./m-akses.component.scss']
})
export class MAksesComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtInstance: Promise<DataTables.Api>;
  dtOptions: any;
  breadCrumbItems: Array<{}>;
  pageTitle: string;
  isView: boolean;
  model: any = {};
  modelCheck: any = {};

  // modelCheck: {
  //   event,
  //   badge,
  //   bank,
  //   banner,
  //   kategori,
  //   faq,
  //   font,
  //   level,
  //   mobile_page,
  //   occupation,
  //   slider,
  //   race_kategori,
  //   tropi,
  //   trivia,
  //   order,
  //   shipper,
  //   shipping,
  //   transaction,
  //   voucher,
  //   free_confirm,
  //   paid_confirm,
  //   blog_post,
  //   blog_komen,
  //   blog_like,
  //   product_redeem,
  //   library,
  //   Tabel,
  //   Form
  // }

  modelParam: {
    nama
  }

  user : any;
  listData: any;
  showForm: boolean;


  constructor(private landaService: LandaService, private router: Router, private authService: AuthenticationService) { }

  ngOnInit() {
    this.pageTitle = 'Hak Akses';
    this.breadCrumbItems = [{
      label: 'Master'
    }, {
      label: 'Hak Akses',
      active: true
    }];
    this.modelParam = {
      nama: '',
    }
    this.getData();
    this.empty();

    // console.log(this.router.url);
  }


  reloadDataTable(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.draw();
    });
  }



  getData() {
    this.dtOptions = {
      serverSide: true,
      processing: true,
      ordering: false,
      pagingType: 'full_numbers',
      ajax: (dataTablesParameters: any, callback) => {
        const params = {
          filter: JSON.stringify(this.modelParam),
          offset: dataTablesParameters.start,
          limit: dataTablesParameters.length,
        };
        this.landaService
            .DataGet('/m_akses/index', params)
            .subscribe((res: any) => {
              this.listData = res.data.list;
              callback({
                recordsTotal: res.data.totalItems,
                recordsFiltered: res.data.totalItems,
                data: [],
              });
            });
      },
    };
  }

  empty() {
    this.modelCheck = {
      event: false,
      manual_data_strava: false,
      manual_data_entry: false,
      event_comment: false,
      event_like: false,
      history_booking: false,
      order_team: false,
      leaderboard: false,
      leaderboard_community: false,
      pickup_request: false,
      badge: false,
      bank: false,
      banner: false,
      kategori: false,
      faq: false,
      font: false,
      level: false,
      mobile_page: false,
      occupation: false,
      slider: false,
      race_kategori: false,
      tropi: false,
      trivia: false,
      order: false,
      shipper: false,
      shipping: false,
      transaction: false,
      voucher: false,
      free_confirm: false,
      paid_confirm: false,
      blog_post: false,
      blog_komen: false,
      blog_like: false,
      product_redeem: false,
      library: false,
      Tabel: false,
      Form: false

    }

    this.model = {
      id: null,
      nama: '',
      akses: {}

    }
    this.getData();

    this.user = this.authService.getDetailUser();
  }

  index() {
    this.showForm = !this.showForm;
    this.pageTitle = 'Data Hak Akses';
    this.getData();
  }

  create() {
    this.empty();
    this.showForm = !this.showForm;
    this.pageTitle = 'Tambah Hak Akses';
    this.isView = false;
  }

  edit(val) {

    this.showForm = !this.showForm;
    this.model = val;
    this.model.akses = val.akses == null || val.akses == '' ? {} : val.akses;
    this.pageTitle = 'Edit : ' + val.nama;
    this.getData();
    this.isView = false;

  }
  view(val) {
    this.showForm = !this.showForm;
    this.model = val;
    this.model.akses = val.akses == null || val.akses == '' ? {} : val.akses;
    this.pageTitle = 'View : ' + val.nama;
    this.getData();
    this.isView = true;
  }

  save() {
    const final = Object.assign(this.model);
    // console.log(final); return;
    this.landaService.DataPost('/m_akses/save', final).subscribe((res: any) => {
      if (res.status_code === 200) {
        this.landaService.alertSuccess('Berhasil', 'Data Hak Akses telah disimpan!');
        this.index();
      } else {
        this.landaService.alertError('Sorry', res.errors);
      }
    });

  }



  delete(val) {
    const data = {
      id: val != null ? val.id : null,
      is_deleted: 1,
    };
    Swal.fire({
      title: 'Apakah anda yakin ?',
      text: 'Menghapus data Hak Akses akan berpengaruh terhadap data lainnya',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Ya, Hapus data ini !'
    }).then(result => {
      if (result.value) {
        this.landaService.DataPost('/m_akses/delete', data).subscribe((res: any) => {
          if (res.status_code === 200) {
            this.landaService.alertSuccess('Berhasil', 'Data Hak Akses telah dihapus !');
            this.reloadDataTable();

          } else {
            this.landaService.alertError('Sorry', res.errors);
          }
        });
      }
    });
  }

  checkAllKolom(val, arr) {
    arr.forEach((value: any, key: any) => {
      Object.keys(value).forEach(key => {
        if (val) {
          this.model.akses[value] = true;
        } else {
          this.model.akses[value] = false;

        }
      });

    });
  }

  checkAkses(hakAkses) {
    return this.landaService.checkAkses(hakAkses);
}
}
