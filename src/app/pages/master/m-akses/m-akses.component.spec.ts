import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MAksesComponent } from './m-akses.component';

describe('MAksesComponent', () => {
  let component: MAksesComponent;
  let fixture: ComponentFixture<MAksesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MAksesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MAksesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
