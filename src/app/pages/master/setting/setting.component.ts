import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { LandaService } from '../../../core/services/landa.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtInstance: Promise<DataTables.Api>;
  dtOptions: any;
  myForm = new FormGroup({
    fileUpload: new FormControl('', [Validators.required]),
  });
  breadCrumbItems: Array<{}>;
  pageTitle: string;
  isView: boolean;
  isEdit: boolean;
  model: any = {};
  listData: any;
  listDetail: any;
  showForm: boolean;

  constructor(private landaService: LandaService, private router: Router) {}

  ngOnInit(): void {
    this.pageTitle = 'Setting Aplikasi';
    this.breadCrumbItems = [{
      label: 'Master'
    }, {
      label: 'Setting Aplikasi',
      active: true
    }];
    this.getData();
    this.empty();
  }


  getData() {
    this.landaService.DataGet('/m_settingAplikasi/index', {}).subscribe((res: any) => {
      this.listData = res.data.list;
      this.edit(this.listData);

    });
  }

  onFileChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.model.logo_login = {
          base64: reader.result as string
        };
        this.myForm.patchValue({
          fileUpload: reader.result,
        });
      };
    }
  }

  onFileChange1(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.model.logo_menu = {
          base64: reader.result as string
        };
        this.myForm.patchValue({
          fileUpload: reader.result,
        });
      };
    }
  }

  onFileChange2(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.model.logo_laporan = {
          base64: reader.result as string
        };
        this.myForm.patchValue({
          fileUpload: reader.result,
        });
      };
    }
  }

  empty() {
    this.listDetail = [];
    this.model = {};
    this.getData();
  }


  edit(val) {
    this.pageTitle = 'Setting Aplikasi ';
    this.model = val;
    this.model.logo_login = this.landaService.getImage('setting_aplikasi', this.model.logo_login);
    this.model.logo_menu = this.landaService.getImage('setting_aplikasi', this.model.logo_menu);
    this.model.logo_laporan = this.landaService.getImage('setting_aplikasi', this.model.logo_laporan);
    this.isView = false;
  }

  save() {
    const data = {
      model: Object.assign({}, this.model),
    }

    this.landaService.DataPost('/m_settingAplikasi/save', data).subscribe((res: any) => {
      if (res.status_code === 200) {
        this.landaService.alertSuccess('Berhasil', 'Data Setting Aplikasi telah disimpan!');
        this.listData = res.data;
        window.location.reload();
        this.edit(this.listData);
      } else {
        this.landaService.alertError('Sorry', res.errors);
      }
    });
  }

  reloadDataTable(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.draw();
    });
  }

  delete(val) {
    const data = {
      id: val != null ? val.id : null,
      is_deleted: 1,
    };

    Swal.fire({
      title: 'Apakah anda yakin ?',
      text: 'Menghapus data perusahaan akan berpengaruh terhadap data lainnya',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Ya, Hapus data ini !'
    }).then(result => {
      if (result.value) {
        this.landaService.DataPost('/m_settingAplikasi/delete', data).subscribe((res: any) => {
          if (res.status_code === 200) {
            this.landaService.alertSuccess('Berhasil', 'Data Petani telah dihapus !');
            this.reloadDataTable();
          } else {
            this.landaService.alertError('Sorry', res.errors);
          }
        });
      }
    });
  }
}
