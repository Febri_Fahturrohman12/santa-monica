import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MCarapembayaranComponent } from './m-carapembayaran.component';

describe('MCarapembayaranComponent', () => {
  let component: MCarapembayaranComponent;
  let fixture: ComponentFixture<MCarapembayaranComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MCarapembayaranComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MCarapembayaranComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
