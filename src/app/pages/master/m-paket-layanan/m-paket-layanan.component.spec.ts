import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MPaketLayananComponent } from './m-paket-layanan.component';

describe('MPaketLayananComponent', () => {
  let component: MPaketLayananComponent;
  let fixture: ComponentFixture<MPaketLayananComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MPaketLayananComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MPaketLayananComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
