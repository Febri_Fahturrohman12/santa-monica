import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UiSwitchModule } from 'ngx-ui-switch';
import { ArchwizardModule } from 'angular-archwizard';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { Daterangepicker } from 'ng2-daterangepicker';
import { CKEditorModule } from 'ckeditor4-angular';
import { ColorPickerModule } from 'ngx-color-picker';
import { QRCodeModule } from 'angularx-qrcode';
import { ImageCropperModule } from 'ngx-image-cropper';

import {
  NgbAlertModule,
  NgbCarouselModule,
  NgbDropdownModule,
  NgbModalModule,
  NgbProgressbarModule,
  NgbTooltipModule,
  NgbPopoverModule,
  NgbPaginationModule,
  NgbNavModule,
  NgbAccordionModule,
  NgbCollapseModule,
  NgbDatepickerModule,
  NgbModule,
} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { UIModule } from '../../layouts/shared/ui/ui.module';

import { MasterRoutingModule } from './master-routing.module';
import { PenggunaComponent } from './pengguna/pengguna.component';
import { SettingComponent } from './setting/setting.component';
import { MAksesComponent } from './m-akses/m-akses.component';

import { MPenggunaComponent } from './m-pengguna/m-pengguna.component';
import { CategoryComponent } from './category/category.component';
import { MUserComponent } from './m-user/m-user.component';
import { MBarangComponent } from './m-barang/m-barang.component';
import { MKategoriComponent } from './m-kategori/m-kategori.component';
import { MLayananComponent } from './m-layanan/m-layanan.component';
import { MCustomerComponent } from './m-customer/m-customer.component';
import { MCarapembayaranComponent } from './m-carapembayaran/m-carapembayaran.component';
import { MPaketLayananComponent } from './m-paket-layanan/m-paket-layanan.component';
import { MDiskonComponent } from './m-diskon/m-diskon.component';



export const options: Partial<IConfig> = {
  thousandSeparator: '.',
};


@NgModule({
    declarations: [PenggunaComponent, SettingComponent, MAksesComponent, MPenggunaComponent, CategoryComponent, MUserComponent, MBarangComponent, MKategoriComponent, MLayananComponent, MCustomerComponent, MCarapembayaranComponent, MPaketLayananComponent, MDiskonComponent],
  imports: [
    CKEditorModule,
    CommonModule,
    MasterRoutingModule,
    FormsModule,
    UIModule,
    NgbAlertModule,
    NgbCarouselModule,
    NgbDropdownModule,
    NgbDatepickerModule,
    NgbModalModule,
    NgbProgressbarModule,
    NgbTooltipModule,
    NgbPopoverModule,
    NgbPaginationModule,
    NgbNavModule,
    NgbAccordionModule,
    NgSelectModule,
    UiSwitchModule,
    NgbCollapseModule,
    NgbModule,
    ReactiveFormsModule,
    ArchwizardModule,
    DataTablesModule,
    Daterangepicker,
    ImageCropperModule,
    NgxMaskModule.forRoot(options),
    ColorPickerModule,
    QRCodeModule
  ]
})
export class MasterModule { }
