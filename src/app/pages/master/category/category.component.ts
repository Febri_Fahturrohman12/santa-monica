import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { LandaService } from '../../../core/services/landa.service';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
    breadCrumbItems: Array<{}>;
    pageTitle: string;
    showForm: boolean;
    isView: boolean;
    isCreate: boolean;

    listData: any;
    listParent: any = [];
    listKategoriStrava : any = [];
    listKategoriGarmin : any = [];
    modelParam: any = {};
    model: any = {};

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtInstance: Promise<DataTables.Api>;
    dtOptions: any;

    constructor(private landaService: LandaService) { }

    ngOnInit() {
        this.pageTitle = 'Category';
        this.breadCrumbItems = [{
          label: 'Master'
        }, {
          label: 'Category',
          active: true
        }];

        this.getData();
        this.empty();
    }

    empty() {

        this.modelParam= {};
        this.model= {};
        this.listParent = [];
        this.modelParam.status = 0;
        this.model.is_parent = 1;
    }

    reloadDataTable(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.draw();
        });
    }

    getData() {
        this.dtOptions = {
            serverSide: true,
            processing: true,
            ordering: false,
            pagingType: 'full_numbers',
            ajax: (dataTablesParameters: any, callback) => {
                const params = {
                    filter: JSON.stringify(this.modelParam),
                    offset: dataTablesParameters.start,
                    limit: dataTablesParameters.length,
                };

                this.landaService.DataGet('/m_category/index', params).subscribe((res: any) => {
                    this.listData = res.data.list;
                    callback({
                        recordsTotal: res.data.totalItems,
                        recordsFiltered: res.data.totalItems,
                        data: [],
                    });
                });
            },
        };
    }

    index() {
        this.showForm = !this.showForm;
        this.pageTitle = 'Data Category';
        this.getData();
    }

    create() {
        this.empty();
        this.showForm = !this.showForm;
        this.pageTitle = 'Create Category';
        this.isView = false;
        this.isCreate = true;
        this.getCategoriStrava();
        this.getCategoriGarmin();
        this.getParent();
        this.model.status = 'active';

    }

    edit(val) {
        this.showForm = !this.showForm;
        this.model = val;
        this.pageTitle = 'Category : ' + val.nama;
        this.getCategoriStrava();
        this.getCategoriGarmin();
        this.getData();
        this.isView = false;
        this.isCreate = false;
        this.getParent();
    }
    
    view(val) {
        this.showForm = !this.showForm;
        this.model = val;
        this.pageTitle = 'Category : ' + val.nama;
        this.getCategoriStrava();
        this.getCategoriGarmin();
        this.getData();
        this.isView = true;
        this.isCreate = false;
    }

    delete(val) {
        const data = {
            id : val.id
        };

        Swal.fire({
            title: 'Apakah anda yakin ?',
            text: 'Menghapus Data Category akan berpengaruh terhadap data lainnya',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            confirmButtonText: 'Ya, Hapus data ini !'
        }).then(result => {
            if (result.value) {
                this.landaService.DataPost('/m_category/delete', data).subscribe((res: any) => {
                    if (res.status_code === 200) {
                        this.reloadDataTable();
                        this.landaService.alertSuccess('Berhasil', 'Data Category telah dihapus !');
                    } else {
                        this.landaService.alertError('Sorry', res.errors);
                    }
                });
            }
        });
    }

    restore(val) {
        const data = {
            id : val.id
        };

        Swal.fire({
            title: 'Apakah anda yakin ?',
            text: 'Merestore data ini',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            confirmButtonText: 'Ya, Restore data ini !'
        }).then(result => {
            if (result.value) {
                this.landaService.DataPost('/m_category/restore', data).subscribe((res: any) => {
                    if (res.status_code === 200) {
                        this.reloadDataTable();
                        this.landaService.alertSuccess('Berhasil', 'Data Category telah direstore !');
                    } else {
                        this.landaService.alertError('Sorry', res.errors);
                    }
                });
            }
        });
    }

    save() {
        this.landaService.DataPost('/m_category/save', this.model).subscribe((res: any) => {
            if (res.status_code === 200) {
                this.landaService.alertSuccess('Berhasil', 'Data Category telah disimpan!');
                this.index();
            } else {
                this.landaService.alertError('Sorry', res.errors);
            }
        });
    }

    getParent() {
        this.landaService.DataGet('/m_category/getParent', {}).subscribe((res: any) => {
            this.listParent = res.data;

        });
    }
    getCategoriStrava() {
        this.landaService.DataGet('/m_category/getCategoriStrava', {}).subscribe((res: any) => {
            this.listKategoriStrava = res.data;

        });
    }

    getCategoriGarmin() {
        this.landaService.DataGet('/m_category/getCategoriGarmin', {}).subscribe((res: any) => {
            this.listKategoriGarmin = res.data;

        });
    }

    checkAkses(hakAkses) {
        return this.landaService.checkAkses(hakAkses);
        return true;
    }
}
