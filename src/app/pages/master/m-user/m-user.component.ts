import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTableDirective} from "angular-datatables";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {LandaService} from "../../../core/services/landa.service";
import {Router} from "@angular/router";
import * as moment from "moment";
import Swal from "sweetalert2";

@Component({
    selector: 'app-m-user',
    templateUrl: './m-user.component.html',
    styleUrls: ['./m-user.component.scss']
})
export class MUserComponent implements OnInit {

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtInstance: Promise<DataTables.Api>;
    dtOptions: any;
    myForm = new FormGroup({
        fileUpload: new FormControl('', [Validators.required]),
    });
    breadCrumbItems: Array<{}>;
    pageTitle: string;
    isView: boolean;
    isEdit: boolean;
    model: any = {};
    form: any = {};
    modelParam: any = {};
    modelCheck: any = {};
    listData: any;
    listAkses: any;
    showForm: boolean;
    listHakAkses: any = [];
    listProvinsi: any = [];
    listKota: any = [];
    listKecamatan: any = [];
    listDetailHistoriUser: any = [];
    listDesa: any = [];
    listHistori: any = [];
    tabContent: string;
    dtOptionsHistori: any;
    control_link: any = '/m_user';

    constructor(private landaService: LandaService, private router: Router) {
    }

    ngOnInit(): void {

        this.pageTitle = 'User';
        this.breadCrumbItems = [{
            label: 'Master'
        }, {
            label: 'User',
            active: true
        }];
        this.modelParam = {};

        this.getData();
        this.empty();
    }

    reloadDataTable(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.draw();
        });
    }

    getData() {
        this.dtOptions = {
            serverSide: true,
            processing: true,
            ordering: false,
            pagingType: 'full_numbers',
            ajax: (dataTablesParameters: any, callback) => {
                const params = {
                    filter: JSON.stringify(this.modelParam),
                    offset: dataTablesParameters.start,
                    limit: dataTablesParameters.length,
                };

                this.landaService
                    .DataGet('/m_user/index', params)
                    .subscribe((res: any) => {
                        this.listData = res.data.list.map((i) => {
                            i.tgl_lahir = i.tgl_lahir == null || i.tgl_lahir == '' || i.tgl_lahir == 'null' ? 0 : moment(i.tanggal).format("YYYY-MM-DD");
                            i.tgl_lahir_view = i.tgl_lahir == null || i.tgl_lahir == '' || i.tgl_lahir == 'null' ? 0 : moment(i.tanggal).format("DD-MM-YYYY");

                            return i;
                        });
                        callback({
                            recordsTotal: res.data.totalItems,
                            recordsFiltered: res.data.totalItems,
                            data: [],
                        });
                    });
            },
        };
    }

    toDate(dob) {
        if (dob) {
            const [year, month, day] = dob.split('-');
            const obj = {
                year: parseInt(year),
                month: parseInt(month),
                day: parseInt(day.split(' ')[0].trim()),
            };
            return obj;
        }
    }

    setTabContent(content) {
        this.tabContent = content;
    }

    getTabContent() {
        return this.tabContent;
    }

    empty() {
        this.modelCheck = {
            DataMaster: false,
            Tabel: false,
            Form: false

        };
        this.model = {};
        this.model.jenis_kelamin = 0;
        this.modelParam = {};
        this.modelParam.status = 0;
        this.listHakAkses = [];
        this.getData();
        this.getHakAkses();
    }

    index() {
        this.showForm = !this.showForm;
        this.pageTitle = 'Data User';
        this.getData();
    }

    create() {
        this.empty();
        this.showForm = !this.showForm;
        this.pageTitle = 'Tambah Data User';
        this.isView = false;
        this.isEdit = false;
        this.tabContent = 'user';
        this.model.agama = "islam";
        this.getHakAkses();
        this.getProvinsi();
        this.getTabContent();
    }

    edit(val) {
        this.getProvinsi();
        this.getKabupaten(val.provinsi_id);
        this.getKecamatan(val.kota_id);
        this.getDesa(val.kecamatan_id);
        this.getHakAkses();
        this.showForm = !this.showForm;
        this.model = val;
        this.tabContent = 'user';
        this.model.password = "";
        this.pageTitle = 'User : ' + val.nama;
        this.model.tgl_lahir = this.toDate(val.tgl_lahir);
        this.isView = false;
        this.isEdit = true;
        this.getTabContent();
        this.getDetailHistoriUser(val.id);
    }

    view(val) {
        this.showForm = !this.showForm;
        this.model = val;
        this.tabContent = 'user';
        this.pageTitle = 'User : ' + val.nama;
        this.getHakAkses();
        this.isView = true;
        this.getTabContent();
        this.getDetailHistoriUser(val.id);
    }

    onFileChange(event) {
        const reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.model.foto = {
                    base64: reader.result as string
                };
                this.myForm.patchValue({
                    fileUpload: reader.result,
                });
            };
        }
    }

    getHakAkses() {
        this.landaService.DataGet('/get_data/hak_akses', {}).subscribe((res: any) => {
            this.listHakAkses = res.data;

        })
    }

    getProvinsi() {
        this.landaService.DataGet('/get_data/provinsi', {}).subscribe((res: any) => {
            this.listProvinsi = res.data;
        })
    }

    getKabupaten(val) {
        console.log(val);
        this.landaService.DataGet('/get_data/kota', {provinsi_id: val}).subscribe((res: any) => {
            this.listKota = res.data;
        })
    }

    getKecamatan(val) {
        this.landaService.DataGet('/get_data/kecamatan', {kota_id: val}).subscribe((res: any) => {
            this.listKecamatan = res.data;
        })
    }

    getDesa(val) {
        this.landaService.DataGet('/get_data/desa', {kecamatan_id: val}).subscribe((res: any) => {
            this.listDesa = res.data;
        })
    }

    save() {
        const final = Object.assign(this.model);

        this.landaService.DataPost('/m_user/save', final).subscribe((res: any) => {
            if (res.status_code === 200) {
                this.landaService.alertSuccess('Berhasil', 'Data User telah disimpan!');
                this.index();
            } else {
                this.landaService.alertError('Sorry', res.errors);
            }
        });

    }

    delete(val) {
        const data = {
            id: val != null ? val.id : null,
            is_deleted: 1,
        };
        Swal.fire({
            title: 'Apakah anda yakin ?',
            text: 'Menghapus data user akan berpengaruh terhadap data lainnya',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            confirmButtonText: 'Ya, Hapus data ini !'
        }).then(result => {
            if (result.value) {
                this.landaService.DataPost('/m_user/delete', data).subscribe((res: any) => {
                    if (res.status_code === 200) {
                        this.landaService.alertSuccess('Berhasil', 'Data User telah dihapus !');
                        this.reloadDataTable();

                    } else {
                        this.landaService.alertError('Sorry', res.errors);
                    }
                });
            }
        });
    }

    restore(val) {
        const data = {
            id: val != null ? val.id : null,
            is_deleted: 0,
        };
        Swal.fire({
            title: 'Apakah anda yakin ?',
            text: 'Merestore data user ini',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            confirmButtonText: 'Ya, Restore data ini !'
        }).then(result => {
            if (result.value) {
                this.landaService.DataPost('/m_user/restore', data).subscribe((res: any) => {
                    if (res.status_code === 200) {
                        this.landaService.alertSuccess('Berhasil', 'Data User telah direstore !');
                        this.reloadDataTable();

                    } else {
                        this.landaService.alertError('Sorry', res.errors);
                    }
                });
            }
        });
    }

    getDetailHistoriUser(val) {
        this.landaService.DataGet(this.control_link + '/getHistori', {m_user_id: val}).subscribe((res: any) => {
            this.listDetailHistoriUser = res.data;
        })
    }

    checkAkses(hakAkses) {
        return this.landaService.checkAkses(hakAkses);
    }

}
