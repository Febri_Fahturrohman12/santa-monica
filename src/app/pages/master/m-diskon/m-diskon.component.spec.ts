import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MDiskonComponent } from './m-diskon.component';

describe('MDiskonComponent', () => {
  let component: MDiskonComponent;
  let fixture: ComponentFixture<MDiskonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MDiskonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MDiskonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
