import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {LandaService} from '../../../core/services/landa.service';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-m-pengguna',
    templateUrl: './m-pengguna.component.html',
    styleUrls: ['./m-pengguna.component.scss']
})

export class MPenggunaComponent implements OnInit {

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtInstance: Promise<DataTables.Api>;
    dtOptions: any;
    breadCrumbItems: Array<{}>;
    pageTitle: string;
    isView: boolean;
    isEdit: boolean;
    model: any = {};
    form: any = {};
    modelParam: any = {};
    modelCheck: any = {};
    listData: any;
    listJabatan: any;
    listAkses: any;
    showForm: boolean;
    listHakAkses: any = [];
    filterStatus: any;

    constructor(private landaService: LandaService, private router: Router) {
    }

    ngOnInit(): void {
        // this.getJabatan();
        // this.getHakakses();
        // this.getWilayah();

        this.empty();
        this.pageTitle = 'Data User';
        this.breadCrumbItems = [{
            label: 'Master'
        }, {
            label: 'User',
            active: true
        }];

        this.filterStatus = [{
            id: 0,
            nama: 'Aktif'
        },
            {
                id: 1,
                nama: 'Non Aktif'
            }];

        this.modelParam.is_deleted = 0;

    }

    reloadDataTable(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.draw();
        });
    }

    getData() {
        this.dtOptions = {
            serverSide: true,
            processing: true,
            ordering: false,
            pagingType: 'full_numbers',
            ajax: (dataTablesParameters: any, callback) => {
                const params = {
                    filter: JSON.stringify(this.modelParam),
                    offset: dataTablesParameters.start,
                    limit: dataTablesParameters.length,
                };

                this.landaService
                    .DataGet('/m_pengguna/index', params)
                    .subscribe((res: any) => {
                        this.listData = res.data.list;
                        callback({
                            recordsTotal: res.data.totalItems,
                            recordsFiltered: res.data.totalItems,
                            data: [],
                        });
                    });
            },
        };
    }

    empty() {
        this.modelParam = {};
        this.modelCheck = {};
        this.model = {};
        this.getData();
    }

    index() {
        this.showForm = !this.showForm;
        this.pageTitle = 'Data User';
        this.getData();
    }

    create() {
        this.empty();
        this.showForm = !this.showForm;
        this.pageTitle = 'Tambah Data User';
        this.isView = false;
        this.isEdit = false;

    }

    edit(val) {
        this.showForm = !this.showForm;
        this.model = val;
        this.model.password = "";
        this.pageTitle = 'User : ' + val.nama;
        this.getData();
        this.isView = false;
        this.isEdit = true;


    }

    view(val) {
        this.showForm = !this.showForm;
        this.model = val;
        this.pageTitle = 'User : ' + val.nama;
        this.getData();

        this.isView = true;
    }

    save() {
        const final = Object.assign(this.model);

        this.landaService.DataPost('/m_pengguna/save', final).subscribe((res: any) => {
            if (res.status_code === 200) {
                this.landaService.alertSuccess('Berhasil', 'Data Pengguna telah disimpan!');
                this.index();
            } else {
                this.landaService.alertError('Sorry', res.errors);
            }
        });

    }


    delete(val) {
        const data = {
            id: val != null ? val.id : null,
            is_deleted: 1,
        };
        Swal.fire({
            title: 'Apakah anda yakin ?',
            text: 'Menghapus data user akan berpengaruh terhadap data lainnya',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            confirmButtonText: 'Ya, Hapus data ini !'
        }).then(result => {
            if (result.value) {
                this.landaService.DataPost('/m_pengguna/delete', data).subscribe((res: any) => {
                    if (res.status_code === 200) {
                        this.landaService.alertSuccess('Berhasil', 'Data Pengguna telah dihapus !');
                        this.reloadDataTable();

                    } else {
                        this.landaService.alertError('Sorry', res.errors);
                    }
                });
            }
        });
    }
}

