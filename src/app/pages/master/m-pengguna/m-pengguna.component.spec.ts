import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MPenggunaComponent } from './m-pengguna.component';

describe('MPenggunaComponent', () => {
  let component: MPenggunaComponent;
  let fixture: ComponentFixture<MPenggunaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MPenggunaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MPenggunaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
