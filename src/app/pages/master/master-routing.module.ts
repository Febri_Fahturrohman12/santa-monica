import {NgModule, Component} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';


import {SettingComponent} from './setting/setting.component';
import {MAksesComponent} from './m-akses/m-akses.component';
import {CategoryComponent} from './category/category.component';
import {MPenggunaComponent} from './m-pengguna/m-pengguna.component';
import {MUserComponent} from './m-user/m-user.component';
import {MBarangComponent} from './m-barang/m-barang.component';
import {MLayananComponent} from './m-layanan/m-layanan.component';
import {MCustomerComponent} from './m-customer/m-customer.component';
import {MCarapembayaranComponent} from './m-carapembayaran/m-carapembayaran.component';
import {MPaketLayananComponent} from './m-paket-layanan/m-paket-layanan.component';
import {MDiskonComponent} from './m-diskon/m-diskon.component';


const routes: Routes = [
    {
        path: 'setting-aplikasi',
        component: SettingComponent,
        data: {
            breadcrumb: 'Application Setting'
        }
    },

    {
        path: 'master-akses',
        component: MAksesComponent,
        data: {
            breadcrumb: 'Roles'
        }
    },
    {
        path: 'category',
        component: CategoryComponent,
        data: {
            breadcrumb: 'Category'
        }
    },
    {
        path: 'master-pengguna',
        component: MPenggunaComponent
    },
    {
        path: 'master-user',
        component: MUserComponent,
        data: {
            breadcrumb: 'User'
        }
    },
    {
        path: 'master-barang',
        component: MBarangComponent,
        data: {
            breadcrumb: 'Barang'
        }
    },
    {
        path: 'master-layanan',
        component: MLayananComponent,
        data: {
            breadcrumb: 'Layanan'
        }
    },
    {
        path: 'master-paket-layanan',
        component: MPaketLayananComponent,
        data: {
            breadcrumb: 'Paket Layanan'
        }
    },
    {
        path: 'master-anggota',
        component: MCustomerComponent,
        data: {
            breadcrumb: 'Anggota'
        }
    }, {
        path: 'master-pembayaran',
        component: MCarapembayaranComponent,
        data: {
            breadcrumb: 'Cara Pembayaran'
        }
    }, {
        path: 'master-diskon',
        component: MDiskonComponent,
        data: {
            breadcrumb: 'Diskon'
        }
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MasterRoutingModule {
}
