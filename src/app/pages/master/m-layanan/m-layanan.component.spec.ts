import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MLayananComponent } from './m-layanan.component';

describe('MLayananComponent', () => {
  let component: MLayananComponent;
  let fixture: ComponentFixture<MLayananComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MLayananComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MLayananComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
