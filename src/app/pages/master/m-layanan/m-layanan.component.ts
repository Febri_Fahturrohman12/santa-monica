import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {DataTableDirective} from "angular-datatables";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {LandaService} from "../../../core/services/landa.service";
import {Router} from "@angular/router";
import * as moment from "moment";
import Swal from "sweetalert2";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
    selector: 'app-m-layanan',
    templateUrl: './m-layanan.component.html',
    styleUrls: ['./m-layanan.component.scss']
    })
export class MLayananComponent implements OnInit {
    
    @ViewChild('modalGantiFoto') modalGantiFoto : TemplateRef<any>; // Note: TemplateRef

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtInstance: Promise<DataTables.Api>;
    dtOptions: any;
    myForm = new FormGroup({
        foto: new FormControl('', [Validators.required]),
    });
    breadCrumbItems: Array<{}>;
    pageTitle: string;
    isView: boolean;
    isEdit: boolean;
    model: any = {};
    form: any = {};
    modelParam: any = {};
    modelCheck: any = {};
    listData: any;
    listAkses: any;
    showForm: boolean;
    listHakAkses: any = [];
    listDetailHistoriUser: any = [];
    listDesa: any = [];
    listHistori: any = [];
    tabContent: string;
    dtOptionsHistori: any;
    control_link: any = '/m_layanan';
    imageChangedEvent: any = '';
    imageSrc: string;

    constructor(private landaService: LandaService, private router: Router, 
        private modalService: NgbModal) {
    }

    ngOnInit(): void {

        this.pageTitle = 'Layanan';
        this.breadCrumbItems = [{
            label: 'Master'
        }, {
            label: 'Layanan',
            active: true
        }];
        this.modelParam = {};

        this.getData();
        this.empty();
    }

    reloadDataTable(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.draw();
        });
    }

    getData() {
        this.dtOptions = {
            serverSide: true,
            processing: true,
            ordering: false,
            pagingType: 'full_numbers',
            ajax: (dataTablesParameters: any, callback) => {
                const params = {
                    filter: JSON.stringify(this.modelParam),
                    offset: dataTablesParameters.start,
                    limit: dataTablesParameters.length,
                };

                this.landaService
                    .DataGet('/m_layanan/index', params)
                    .subscribe((res: any) => {
                        this.listData = res.data.list.map((i) => {
                            i.tgl_lahir = i.tgl_lahir == null || i.tgl_lahir == '' || i.tgl_lahir == 'null' ? 0 : moment(i.tanggal).format("YYYY-MM-DD");
                            i.tgl_lahir_view = i.tgl_lahir == null || i.tgl_lahir == '' || i.tgl_lahir == 'null' ? 0 : moment(i.tanggal).format("DD-MM-YYYY");

                            return i;
                        });
                        callback({
                            recordsTotal: res.data.totalItems,
                            recordsFiltered: res.data.totalItems,
                            data: [],
                        });
                    });
            },
        };
    }

    toDate(dob) {
        if (dob) {
            const [year, month, day] = dob.split('-');
            const obj = {
                year: parseInt(year),
                month: parseInt(month),
                day: parseInt(day.split(' ')[0].trim()),
            };
            return obj;
        }
    }

    setTabContent(content) {
        this.tabContent = content;
    }

    getTabContent() {
        return this.tabContent;
    }

    empty() {
        this.modelCheck = {
            DataMaster: false,
            Tabel: false,
            Form: false

        };
        this.model = {};
        this.modelParam = {};
        this.modelParam.status = 0;
        this.listHakAkses = [];
        this.getData();
        
    }

    index() {
        this.showForm = !this.showForm;
        this.pageTitle = 'Data Layanan';
        this.getData();
    }

    create() {
        this.empty();
        this.showForm = !this.showForm;
        this.pageTitle = 'Tambah Data Layanan';
        this.isView = false;
        this.isEdit = false;
        this.tabContent = 'layanan';
        this.getTabContent();
    }

    edit(val) {
        
        this.showForm = !this.showForm;
        this.model = val;
        this.tabContent = 'layanan';
        this.pageTitle = 'Layanan : ' + val.nama;
        this.isView = false;
        this.isEdit = true;
        this.getTabContent();
        // this.getDetailHistoriUser(val.id);
    }

    view(val) {
        this.showForm = !this.showForm;
        this.model = val;
        this.tabContent = 'layanan';
        this.pageTitle = 'Layanan : ' + val.nama;
        
        this.isView = true;
        this.getTabContent();
        // this.getDetailHistoriUser(val.id);
    }

    onFileChange(event) {
        const reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.model.foto = {
                    base64: reader.result as string
                };
                this.myForm.patchValue({
                    foto: reader.result,
                });
            };
        }
    }

    save() {
        const final = Object.assign(this.model);

        this.landaService.DataPost('/m_layanan/save', final).subscribe((res: any) => {
            if (res.status_code === 200) {
                this.landaService.alertSuccess('Berhasil', 'Data Layanan telah disimpan!');
                this.index();
            } else {
                this.landaService.alertError('Sorry', res.errors);
            }
        });

    }

    delete(val) {
        const data = {
            id: val != null ? val.id : null,
            is_deleted: 1,
        };
        Swal.fire({
            title: 'Apakah anda yakin ?',
            text: 'Menghapus data layanan akan berpengaruh terhadap data lainnya',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            confirmButtonText: 'Ya, Hapus data ini !'
        }).then(result => {
            if (result.value) {
                this.landaService.DataPost('/m_layanan/delete', data).subscribe((res: any) => {
                    if (res.status_code === 200) {
                        this.landaService.alertSuccess('Berhasil', 'Data Layanan telah dihapus !');
                        this.reloadDataTable();

                    } else {
                        this.landaService.alertError('Sorry', res.errors);
                    }
                });
            }
        });
    }

    restore(val) {
        const data = {
            id: val != null ? val.id : null,
            is_deleted: 0,
        };
        Swal.fire({
            title: 'Apakah anda yakin ?',
            text: 'Merestore data layanan ini',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#34c38f',
            cancelButtonColor: '#f46a6a',
            confirmButtonText: 'Ya, Restore data ini !'
        }).then(result => {
            if (result.value) {
                this.landaService.DataPost('/m_layanan/restore', data).subscribe((res: any) => {
                    if (res.status_code === 200) {
                        this.landaService.alertSuccess('Berhasil', 'Data Layanan telah direstore !');
                        this.reloadDataTable();

                    } else {
                        this.landaService.alertError('Sorry', res.errors);
                    }
                });
            }
        });
    }

    getDetailHistoriUser(val) {
        this.landaService.DataGet(this.control_link + '/getHistori', {m_layanan_id: val}).subscribe((res: any) => {
            this.listDetailHistoriUser = res.data;
        })
    }

    hapusfoto(){
        this.imageChangedEvent = this.landaService.getImage('', '../img/default.png');
        this.imageSrc = this.landaService.getImage('', '../img/default.png');
    }
    fileChangeEvent(event: any): void {
        this.modalService.open(this.modalGantiFoto, { size: 'md', backdrop: 'static' });
        this.imageChangedEvent = event;
    }
    imageCropped(event: ImageCroppedEvent) {
        this.imageSrc = event.base64;
        this.myForm.patchValue({
            foto: event.base64,
        });
    }
    imageLoaded(image: HTMLImageElement) {
        // show cropper
    }
    cropperReady() {
        // cropper ready
    }
    loadImageFailed() {
        // show message
    }

    checkAkses(hakAkses) {
        return this.landaService.checkAkses(hakAkses);
    }
}
