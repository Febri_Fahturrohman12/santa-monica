import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MKategoriComponent } from './m-kategori.component';

describe('MKategoriComponent', () => {
  let component: MKategoriComponent;
  let fixture: ComponentFixture<MKategoriComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MKategoriComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MKategoriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
