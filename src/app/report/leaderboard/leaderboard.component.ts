import {Component, OnInit} from '@angular/core';
import {LandaService} from "../../core/services/landa.service";
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {environment} from "../../../environments/environment";
@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.scss']
})
export class LeaderboardComponent implements OnInit {

  breadCrumbItems: Array<{}>;
  pageTitle: string;

  is_tampilkan: boolean;

  listData: any = [];
  listDataRunning: any = [];
  listDataCycling: any = [];
  list: any = [];
  listEvent: any;
  listTipe: any;
  is_team: null;
  formModal: any = {};

  apiUrl = environment.apiURL;


  model: {
      m_event_id;
  }
    kategori_race: any;
    event: any;
    id: any;

  constructor(private landaService: LandaService, private modalService: NgbModal) {
  }

  ngOnInit() {
      this.pageTitle = 'Leaderboard';
      this.is_tampilkan = false;

      this.empty();
      this.getEvent();
      this.tampilkan();
      this.id = setInterval(() => {
        this.tampilkan(); 
      }, 7500);
  }

  empty() {
      this.listEvent = [];
      this.list = [];
      this.listData = [];
      this.is_team = null;
      this.model = {
          m_event_id: null
      };
  }

  getEvent() {
      this.landaService.DataGet('/report_global/event', {}).subscribe((res: any) => {
          this.listEvent = res.data;
      });
  }

  tampilkan() {
          this.landaService.DataGet('/report/index', this.model).subscribe((res: any) => {
            this.listDataRunning = res.data.listRunning;
            this.listDataCycling = res.data.listCycling;
            this.kategori_race = res.data.kategori_race;
            this.event = res.data.event;
            this.is_team = res.data.is_team;
            this.is_tampilkan = true;
          });
  }
  
  export(val) {
      window.open(this.apiUrl + '/report/export?id=' + val, '_blank');
  }

  reset_tampilan() {
      this.is_tampilkan = false;
  }

  modal(modal, val) {
      console.log(val);
      this.formModal = val;
      this.modalService.open(modal, {size: 'lg', backdrop: 'static'});
      this.landaService.DataGet('/report/getUser', {id: val.id}).subscribe((res: any) => {
          this.list = res.data.list;
      });
  }
}

