import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ArchwizardModule } from 'angular-archwizard';
import { UiSwitchModule } from 'ngx-ui-switch';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { DataTablesModule } from 'angular-datatables';
import { Daterangepicker } from 'ng2-daterangepicker';

export const options: Partial<IConfig> = {
  thousandSeparator: '.',
};
// import form bootstrap
import {
  NgbDropdownModule,
  NgbNavModule,
  NgbDatepickerModule,
  NgbModule
} from '@ng-bootstrap/ng-bootstrap';

import { UIModule } from '../layouts/shared/ui/ui.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { ReportRoutingModule } from './report-routing.module';
import { ChartsModule } from 'ng2-charts';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';
import { LeaderboardFinisherComponent } from './leaderboard-finisher/leaderboard-finisher.component';
import { LeaderboardKeklComponent } from './leaderboard-kekl/leaderboard-kekl.component';

@NgModule({
  declarations: [LeaderboardComponent, LeaderboardFinisherComponent, LeaderboardKeklComponent,],
  imports: [
    CommonModule,
    ReportRoutingModule,
    FormsModule,
    Daterangepicker,
    UIModule,
    NgbDropdownModule,
    NgbDatepickerModule,
    NgbNavModule,
    NgSelectModule,
    NgbModule,
    UiSwitchModule,
    ReactiveFormsModule,
    ArchwizardModule,
    DataTablesModule,
    ChartsModule,
    NgxMaskModule.forRoot(options)
  ]
})
export class ReportModule { }
