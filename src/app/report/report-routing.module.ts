import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';
import { LeaderboardFinisherComponent } from './leaderboard-finisher/leaderboard-finisher.component';
import { LeaderboardKeklComponent } from './leaderboard-kekl/leaderboard-kekl.component';

const routes: Routes = [
  {
    path: 'leaderboards',
    component: LeaderboardComponent,
    data: {
			breadcrumb: 'Leaderboard'
		}
  },
  {
    path: 'leaderboard-finisher',
    component: LeaderboardFinisherComponent,
    data: {
			breadcrumb: 'Leaderboard-finisher'
		}
  },
  {
    path: 'leaderboard-kekl',
    component: LeaderboardKeklComponent,
    data: {
			breadcrumb: 'Leaderboard-finisher'
		}
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule { }
