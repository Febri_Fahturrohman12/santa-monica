import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaderboardKeklComponent } from './leaderboard-kekl.component';

describe('LeaderboardKeklComponent', () => {
  let component: LeaderboardKeklComponent;
  let fixture: ComponentFixture<LeaderboardKeklComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaderboardKeklComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaderboardKeklComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
