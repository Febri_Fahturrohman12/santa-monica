import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaderboardFinisherComponent } from './leaderboard-finisher.component';

describe('LeaderboardFinisherComponent', () => {
  let component: LeaderboardFinisherComponent;
  let fixture: ComponentFixture<LeaderboardFinisherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaderboardFinisherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaderboardFinisherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
