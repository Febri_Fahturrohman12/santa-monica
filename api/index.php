<?php
$ip = $_SERVER['REMOTE_ADDR'];
$domain = $_SERVER['HTTP_HOST'];
$subDomain = explode('.', $_SERVER['HTTP_HOST']);

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
#ini_set('max_execution_time', 300)

if (isset($_SERVER['HTTP_ORIGIN'])) {
    $http_origin = $_SERVER['HTTP_ORIGIN'];
    header("Access-Control-Allow-Origin: " . $http_origin);
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
    header("Access-Control-Allow-Headers: X-Requested-With, Content-Type, Accept, Origin, Authorization");
}


if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    }
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
        header("Access-Control-Allow-Headers: X-Requested-With, Content-Type, Accept, Origin, Authorization");
    }
}

//include_once 'aliyun-php-sdk-core/Config.php';
//include_once 'aliyun-php-sdk-dm/Dm/Request/V20151123/SingleSendMailRequest.php';

# Session lifetime of 20 hours
ini_set('session.gc_maxlifetime', 20 * 60 * 60);
ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);
ini_set('session.use_cookies', 1);
date_default_timezone_set("Asia/Jakarta");

if (!file_exists(__DIR__ . '/sessions')) {
    mkdir(__DIR__ . '/sessions', 0777, true);
}

session_save_path(__DIR__ . '/sessions');
session_name('santamonica');
session_start();
// $_SESSION['s'] = 'aa';
// print_r($_SESSION); exit;

/* --- System --- */
require 'src/systems/gump-validation/gump.class.php';
require 'src/systems/domain.php';
require 'src/systems/systems.php';
require 'src/systems/functions.php';
require 'src/systems/functionKasir.php';

// TELEGRAM
// $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
//     $text = "<pre>";
//     $text .= date("d-m-Y H:i:s") . " \n \n";
//     $text .= 'User : '. (isset($_SESSION['user']['nama']) ? $_SESSION['user']['nama'] : '') . (isset($_SESSION['user']['id']) ? ' == '. $_SESSION['user']['id'] : '') ." \n \n";
//     $text .= "Url : " . $link . "\n \n";
// 	$text .= "IP : " . $ip . "\n \n";
// $text .= $error . " </pre>";

require 'vendor/autoload.php';


if (file_exists('vendor/cahkampung/landa-php/src/LandaPhp.php')) {
    require 'vendor/cahkampung/landa-php/src/LandaPhp.php';
}

if (file_exists('vendor/cahkampung/landa-acc/functions.php')) {
    require 'vendor/cahkampung/landa-acc/functions.php';
}

if (config('TELEGRAM_LOG') == true) {
    set_error_handler("errorHandler");
    register_shutdown_function("shutdownHandler");
    ini_set("display_errors", 0);
}
$display = true;
$config = [
    'displayErrorDetails' => $display,
    'determineRouteBeforeAppMiddleware' => true,
];

$container = new \Slim\Container(["settings" => $config]);

$app = new \Slim\App($container);
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

require 'src/systems/dependencies.php';
require 'src/systems/middleware.php';

/** route to php file */
$file = getUrlFile();
$mobile = explode("/", $file);

if (
    ($subDomain[0] == 'apinewtest' && $domain == "apinewtest.santamonica.com")
    || ($subDomain[0] == 'test' && $domain == "test.santamonica.com")
) {
    if ($mobile[2] == "mobile") {
        require 'src/systems/jwtAuth.php';
    }
}

// $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
// sendMessage($ip." - ".$_SERVER['HTTP_USER_AGENT']." - ".$actual_link);
// print_r($file);exit();
require $file;
$app->run();


