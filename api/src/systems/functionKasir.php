<?php
function generateNota($tanggal = null)
{
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);


    if (!empty($tanggal)) {
        $bulan = date("m", strtotime($tanggal));
        $tahun = date("Y", strtotime($tanggal));
    } else {
        $bulan = date("m");
        $tahun = date("Y");
    }


    $db->select("t_penjualan.kode_nota")
        ->from("t_penjualan")
        ->where("MONTH(FROM_UNIXTIME(t_penjualan.created_at))", "=", $bulan)
        ->where("YEAR(FROM_UNIXTIME(t_penjualan.created_at))", "=", $tahun)
        ->orderBy("t_penjualan.kode_nota DESC");

    $db->limit(1);
    $cekKode = $db->find();
    if ($cekKode) {
        $kode_terakhir = $cekKode->kode_nota;
    } else {
        $kode_terakhir = 0;
    }
    $kode_item = (substr($kode_terakhir, -6) + 1);
    $kode = substr('000000' . $kode_item, strlen($kode_item));
    $kode = date("mY") . $kode;

    return $kode;
}

function getTotalStok($m_barang_id, $tanggal = null)
{
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);

    $db->select("sum(jumlah_masuk) as jumlah_masuk, sum(jumlah_keluar) as jumlah_keluar,m_barang.nama")
        ->from("inv_kartu_stok")
        ->leftJoin("m_barang", "m_barang.id = inv_kartu_stok.m_barang_id")
        ->where("m_barang.id", "=", $m_barang_id);
    if (!empty($tanggal)) {
        $db->andWhere("inv_kartu_stok.tanggal", "<=", $tanggal);
    }

    $model = $db->find();

    $masuk = isset($model->jumlah_masuk) ? $model->jumlah_masuk : 0;
    $keluar = isset($model->jumlah_keluar) ? $model->jumlah_keluar : 0;
    return $masuk - $keluar;
}

function checkStokKasir($barang)
{
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);

    $status = [
        "status" => false,
        "m_item_id" => [],
        "nama_item" => [],
        "erroMessage" => [],
    ];
    if (!empty($barang)) {
        foreach ($barang as $key => $value) {
            $barangId = isset($value["id"]) ? $value["id"] : "";
            $jumlah = isset($value["jumlah"]) ? $value["jumlah"] : 0;
            $namaItem = isset($value["nama"]) ? $value["nama"] : '';

            $stok = getTotalStok($barangId, date("Y-m-d"));
            if ($jumlah > $stok) {
                $status["status"] = true;
                $status["m_item_id"][] = isset($value["id"]) ? $value["id"] : "";
                $status["nama_item"][] = $namaItem;
                $status["erroMessage"][] = "Stok " . $namaItem . " saat ini (" . $stok . ")";
            }
        }
    } else {
        $status = [
            "status" => false,
            "m_item_id" => [],
            "nama_item" => [],
            "erroMessage" => []
        ];
    }

    return $status;
}
