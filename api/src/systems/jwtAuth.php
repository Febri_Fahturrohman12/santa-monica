<?php
use \Firebase\JWT\JWT;
$app->add(new \Tuupola\Middleware\JwtAuthentication([
    "algorithm" => ["HS256"],
    "secret"    => "venturoproindonesia",
    "ignore"    => [
        "/auth/forgotPassword",
        "/auth/getPreferences",
        "/auth/login",
        "/auth/loginOtp",
        "/auth/loginSosmed",
        "/auth/receiveAfterRegister",
        "/auth/receiveRegister",
        "/auth/sendAfterRegister",
        "/auth/sendRegister",

        "/cron/*",

        "/event/sendJoinEvent",
        "/event/receiveJoinEvent",

        "/global/getSliderBeforeLogin",

        "/login",
        "/login/otp",
        "/login/sosmed",

        // midtrans callback ignore
        "/midtrans/finish",
        "/midtrans/handling",
        "/midtrans/unfinish",

        "/profil/changePassword",

        // strava callback ignore
        "/strava/antrianRedis",
        "/strava/callback",
        "/strava/cronjob",
        "/strava/getAthleteId",
        "/strava/resetAntrianRedis",
        "/strava/setAntrianManual",
        "/strava/testTimeout",
    ],
    "secure"    => false,
    "error"     => function ($response, $arguments) {
        $data["status"] = "error";
        $data["message"]= $arguments["message"];
        return $response
            ->withHeader("Content-Type", "application/json")
            ->getBody()->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }
]));