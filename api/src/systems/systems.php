<?php

use Service\Db;
use Service\Landa;

function config($key)
{
    include getConfig();

    return isset($config[$key]) ? $config[$key] : '';
}

function site_url()
{
    return rtrim(config('SITE_URL'), '/') . '/';
}
function site_api_url()
{
    return rtrim(config('SITE_API_URL'), '/') . '/';
}
// Function Print_r Die
function pd($params)
{
    print_r($params);

    exit;
}

// Function Echo JSON ENCODE DIE
function ej($params)
{
    // $subDomain = explode('.', $_SERVER['HTTP_HOST']);
    // if ( $subDomain[0] == 'app' && $domain == "app.santamonica.tech") {
    //     $path = 'config/santamonica.tech/config.php';
    // }
    echo json_encode($params);

    exit;
}
function ejt($params)
{
    $subDomain = explode('.', $_SERVER['HTTP_HOST']);
    if ( $subDomain[0] == 'test') {
        echo json_encode($params);
        exit;
    } else if ( $subDomain[0] == 'apis') {
        echo json_encode($params);
        exit;
    }
}

function print_die($params)
{
    echo json_encode($params);

    exit;
}
function site_path()
{
    static $_path;
    if (!$_path) {
        $_path = rtrim(parse_url(config('SITE_URL'), PHP_URL_PATH), '/');
    }

    return $_path;
}

function img_url()
{
    return rtrim(config('SITE_URL'), '/') . '/assets/img/';
}

function img_path()
{
    return rtrim(config('IMG_PATH'), '/') . '/';
}

function dispatch()
{
    $path = $_SERVER['REQUEST_URI'];
    if (null !== config('SITE_URL')) {
        $path = preg_replace('@^' . preg_quote(site_path()) . '@', '', $path);
    }
    $parts = preg_split('/\?/', $path, -1, PREG_SPLIT_NO_EMPTY);
    $uri = trim($parts[0], '/');
    if ('index.php' == $uri || '' == $uri) {
        $uri = 'site';
    }

    return $uri;
}

function get_client_ip()
{
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } elseif (isset($_SERVER['HTTP_X_FORWARDED'])) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    } elseif (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    } elseif (isset($_SERVER['HTTP_FORWARDED'])) {
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    } elseif (isset($_SERVER['REMOTE_ADDR'])) {
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    } else {
        $ipaddress = 'UNKNOWN';
    }

    return $ipaddress;
}

function sendMessage($messaggio)
{
    $token = '2025122374:AAFlvb7xuNlf10LlWBmwCUt-jWUdy0Jt7tE';
    $chat_id = config('TELEGRAM_CHANNEL');
    $result = '';
    if (!empty($chat_id)) {
        $url = 'https://api.telegram.org/bot' . $token . '/sendMessage?chat_id=' . $chat_id . '&text=' . urlencode($messaggio) . '&parse_mode=HTML';
        $ch = curl_init();
        $optArray = [CURLOPT_URL => $url, CURLOPT_RETURNTRANSFER => true];
        curl_setopt_array($ch, $optArray);
        $result = curl_exec($ch);
        curl_close($ch);
    }

    return $result;
}

function errorHandler($error_level, $error_message, $error_file, $error_line, $error_context)
{
    $error = 'lvl: ' . $error_level . ' | msg:' . $error_message . ' | file:' . $error_file . ' | ln:' . $error_line;

    if (16384 == $error_level) {
        return false;
    }

    switch ($error_level) {
        case E_ERROR:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_PARSE:
            mylog($error, 'fatal');

            break;

        case E_USER_ERROR:
        case E_RECOVERABLE_ERROR:
            mylog($error, 'error');

            break;

        case E_WARNING:
        case E_CORE_WARNING:
        case E_COMPILE_WARNING:
        case E_USER_WARNING:
            mylog($error, 'warn');

            break;

        case E_NOTICE:
        case E_USER_NOTICE:
            mylog($error, 'info');

            break;

        case E_STRICT:
            mylog($error, 'debug');

            break;

        default:
            mylog($error, 'warn');
    }
}

function shutdownHandler()
{
    $lasterror = error_get_last();

    switch ($lasterror['type']) {
        case E_ERROR:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_USER_ERROR:
        case E_RECOVERABLE_ERROR:
        case E_CORE_WARNING:
        case E_COMPILE_WARNING:
        case E_PARSE:
            $error = '[SHUTDOWN] lvl:' . $lasterror['type'] . " \nMessage:" . $lasterror['message'] . " \nFile:" . $lasterror['file'] . ' line:' . $lasterror['line'];
            mylog($error, 'fatal');
    }
}

function mylog($error, $errlvl)
{
    $username = isset($_SESSION['user']['username']) ? $_SESSION['user']['username'] : 'Not found';
    $link = (isset($_SERVER['HTTPS']) && 'on' === $_SERVER['HTTPS'] ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $text = '<pre>';
    $text .= date('d-m-Y H:i:s') . " \n \n";
    $text .= 'Url : ' . $link . "\n \n";
    $text .= 'IP : ' . get_client_ip() . ' | User : ' . $username . " \n \n";
    $text .= $error . ' </pre>';
    sendMessage($text);
}

function getUrlFile()
{
    $uri = dispatch();
    $getUri = explode('/', $uri);

    $path = isset($getUri[0]) ? $getUri[0] : '';
    $action = isset($getUri[1]) ? $getUri[1] : '';
    $action2 = isset($getUri[2]) ? $getUri[2] : '';
    // ej($getUri);
    if ('api' == $path) {
        $file = 'src/routes/' . (isset($action) ? $action : 'sites') . '.php';
        if (file_exists($file)) {
            return $file;
        }
    } elseif ('acc' == $path) {
        $file = 'vendor/cahkampung/landa-acc/routes/' . $action . '.php';
        if (file_exists($file)) {
            return $file;
        }
    } else if (file_exists('src/routes/' . (isset($action2) ? $action2 : 'sites') . '.php')) {
        return 'src/routes/' . (isset($action2) ? $action2 : 'sites') . '.php';
    } else if (file_exists('src/routes/mobile/' . (isset($action2) ? $action2 : 'sites') . '.php')) {
        return 'src/routes/mobile/' . (isset($action2) ? $action2 : 'sites') . '.php';
    } else if (file_exists('src/routes/mobile/' . (isset($path) ? $path : 'sites') . '.php')) {
        return 'src/routes/mobile/' . (isset($path) ? $path : 'sites') . '.php';
    }else if (file_exists('src/routes/report/' . (isset($action2) ? $action2 : 'sites') . '.php')) {
        return 'src/routes/report/' . (isset($action2) ? $action2 : 'sites') . '.php';
    } else if (file_exists('src/routes/report/' . (isset($path) ? $path : 'sites') . '.php')) {
        return 'src/routes/report/' . (isset($path) ? $path : 'sites') . '.php';
    } else {
        $file = 'src/routes/' . $path . '.php';
        if (file_exists($file)) {
            return $file;
        }
    }

    return 'src/routes/sites.php';
}

function getPathMobile()
{
    $uri = dispatch();
    $getUri = explode('/', $uri);

    $path = isset($getUri[0]) ? $getUri[0] : '';
    $action = isset($getUri[1]) ? $getUri[1] : '';
    $action2 = isset($getUri[2]) ? $getUri[2] : '';
    $is_mobile = 0;
    if (file_exists('src/routes/mobile/' . (isset($action2) ? $action2 : 'sites') . '.php')) {
        $is_mobile = 1;
    } else if (file_exists('src/routes/mobile/' . (isset($path) ? $path : 'sites') . '.php')) {
        $is_mobile = 1;
    } else if (file_exists('src/routes/report/' . (isset($action2) ? $action2 : 'sites') . '.php')) {
        $is_mobile = 1;
    } else if (file_exists('src/routes/report/' . (isset($path) ? $path : 'sites') . '.php')) {
        $is_mobile = 1;
    }

    return $is_mobile;
}

function successResponse($response, $message)
{
    return $response->write(json_encode([
        'status_code' => 200,
        'data' => $message,
    ],JSON_INVALID_UTF8_IGNORE))->withStatus(200);
}

function unprocessResponse($response, $message)
{
    return $response->write(json_encode([
        'status_code' => 422,
        'errors' => $message,
    ]))->withStatus(200);
}

function unauthorizedResponse($response, $message)
{
    return $response->write(json_encode([
        'status_code' => 403,
        'errors' => $message,
    ]))->withStatus(403);
}

function validate($data, $validasi, $custom = [])
{
    if (!empty($custom)) {
        $validasiData = array_merge($validasi, $custom);
    } else {
        $validasiData = $validasi;
    }
    $lang = 'en';
    $gump = new GUMP($lang);
    $validate = $gump->is_valid($data, $validasiData);

    if (true === $validate) {
        return true;
    }

    return $validate;
}

function get_string_between($string, $start, $end)
{
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if (0 == $ini) {
        return '';
    }
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;

    return substr($string, $ini, $len);
}

function format_base64($text)
{
    return "data:image/png;base64," . $text;
}

function weeks_in_month($month, $year)
{
    // Start of month
    $start = mktime(0, 0, 0, $month, 1, $year);
    // End of month
    $end = mktime(0, 0, 0, $month, date('t', $start), $year);
    // Start week
    $start_week = date('W', $start);
    // End week
    $end_week = date('W', $end);

    if ($end_week < $start_week) { // Month wraps
        //year has 52 weeks
        $weeksInYear = 52;
        //but if leap year, it has 53 weeks
        if ($year % 4 == 0) {
            $weeksInYear = 53;
        }
        return (($weeksInYear + $end_week) - $start_week) + 1;
    }

    return ($end_week - $start_week) + 1;
}

function getWeeks($date, $rollover = 'Sunday')
{
    $cut = substr($date, 0, 8);
    $daylen = 86400;

    $timestamp = strtotime($date);
    $first = strtotime($cut . "00");
    $elapsed = ($timestamp - $first) / $daylen;

    $weeks = 1;

    for ($i = 1; $i <= $elapsed; $i++) {
        $dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
        $daytimestamp = strtotime($dayfind);

        $day = strtolower(date("l", $daytimestamp));

        if ($day == strtolower($rollover))
            $weeks++;
    }

    return $weeks;
}

function whereNotIn($data, $where, $index = '')
{
    $result = '';
    if (!empty($data)) {
        foreach ($data as $k => $value) {
            if (!empty($index)) {
                $val = isset($value[$index]) ? $value[$index] : 0;
            } else {
                $val = !empty($value) ? $value : 0;
            }


            if ($val > 0) {
                if ($k == 0) {
                    $result .= '( ' . $where . ' != ' . $val . ' )';
                } else {
                    $result .= ' AND ( ' . $where . ' != ' . $val . ' )';
                }
            }
        }
    }

    return $result;
}

function arrayToString($array, $index = null, $sparator = ',', $default = 0)
{
    $data = $default;
    if (!empty($array)) {
        $data = [];
        foreach ($array as $key => $val) {
            if (!empty($index)) {
                if (isset($val[$index]) && !empty($val[$index])) {
                    $data[] = isset($val[$index]) ? $val[$index] : '';
                }
            } else {
                if (!empty($val)) {
                    $data[] = $val;
                }
            }
        }

        $data = implode($sparator, $data);
    }

    return $data;
}

function stringToArray($string, $index = null, $sparator = ',')
{
    $data = [];
    if (!empty($string)) {
        $string = str_replace(' ', '', $string);
        $arr = explode($sparator, $string);

        if (!empty($arr)) {
            foreach ($arr as $key => $val) {
                if (!empty($index)) {
                    if (!empty($val)) {
                        $data[$key][$index] = $val;
                    }
                } else {
                    if (!empty($val)) {
                        $data[] = $val;
                    }
                }
            }
        }
    }

    return $data;
}

function http_request($url)
{
    // persiapkan curl
    $ch = curl_init();
    $config['useragent'] = 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0';

    curl_setopt($ch, CURLOPT_USERAGENT, $config['useragent']);
    // set url
    curl_setopt($ch, CURLOPT_URL, $url);

    // return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // $output contains the output string
    $output = curl_exec($ch);

    // tutup curl
    curl_close($ch);

    // mengembalikan hasil curl
    return $output;
}

function http_request_put($url)
{
    // persiapkan curl
    $ch = curl_init();
    $config['useragent'] = 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0';

    curl_setopt($ch, CURLOPT_USERAGENT, $config['useragent']);
    // set url
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

    // return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // $output contains the output string
    $output = curl_exec($ch);

    // tutup curl
    curl_close($ch);

    // mengembalikan hasil curl
    return $output;
}

function getApiKey()
{
    $subDomain = explode('.', $_SERVER['HTTP_HOST']);
    if (isset($subDomain[0]) && 'tes' == $subDomain[0]) {
        // sandbox
        $apikey = 'd9b52c03e870ccb9c94aae623168c67e';
    } else if (isset($subDomain[0]) && 'apites' == $subDomain[0]) {
        // sandbox
        $apikey = 'd9b52c03e870ccb9c94aae623168c67e';
    } else if (isset($subDomain[0]) && 'localhost' == $subDomain[0]) {
        $apikey = 'd9b52c03e870ccb9c94aae623168c67e';
    } else {
        // prod
        $apikey = '423486db37dc379c73ab771cb4c06cb3';
    }

    return $apikey;
}

function getUrlShipper()
{
    $subDomain = explode('.', $_SERVER['HTTP_HOST']);

    if (isset($subDomain[0]) && 'tes' == $subDomain[0]) {
        // sandbox
        $urlShipper = 'https://api.sandbox.shipper.id/public/v1';
//        $urlShipper = 'https://api.shipper.id/prod/public/v1';

    } else if (isset($subDomain[0]) && 'apites' == $subDomain[0]) {
        // sandbox
        $urlShipper = 'https://api.sandbox.shipper.id/public/v1';
//        $urlShipper = 'https://api.shipper.id/prod/public/v1';

    } else if (isset($subDomain[0]) && 'localhost' == $subDomain[0]) {
        $urlShipper = 'https://api.sandbox.shipper.id/public/v1';

    } else {
        // prod
        $urlShipper = 'https://api.shipper.id/prod/public/v1';
    }
    return $urlShipper;

}


function configurationStrava()
{
    $data = [
        "client_id" => "62611",
        "client_secret" => "930ae7c4932d0e945ba253c808826e51bb080e41",
        "access_token" => "014cbe6438046db133040dc38f13bc32da983549",
        "refresh_token" => "1e732247fc1f8a0e3e0ddfe46f829c02cdb54850"
    ];

    return $data;
}


function getConfigMidtrans()
{
//    sulip
    // $key = 'SB-Mid-server-J8KlPAstcr8Go7THr-_biXOe';
    // sandbox
    // $key = 'SB-Mid-server-_xsXBpMqwdRsVnZbkSgd8CQQ';
    // prod
    $key = 'Mid-server-desv-CLssjTQtvoT3_zEmt4X';

    return $key;

}


function getMethodBayar($other = 1, $id_event = 0, $spesial = 0)
{
    $db = Db::db();
    $model = $db->select("payment_method_voucher_global")
        ->from("m_event")
        ->where("id","=", $id_event)
        ->find();

    if ($spesial == 0) {
        $arr = [
            [
                'id' => 1,
                'nama' => 'Bank Transfer',
//            'tipe' => 'manual',
//            'is_midtrans' => 0
                'tipe' => 'bank_transfer2',
                'is_midtrans' => 1
            ],
            [
                'id' => 2,
                'nama' => 'Virtual Account',
                'tipe' => 'bank_transfer',
                'is_midtrans' => 1
            ],
            [
                'id' => 3,
                'nama' => 'Credit Card',
                'tipe' => 'credit_card',
                'is_midtrans' => 1
            ],
            [
                'id' => 4,
                'nama' => 'Gopay',
                'tipe' => 'gopay_qris',
                'is_midtrans' => 1
            ]
        ];

        // $a = true;
        if ($other == 1) {
            $b = [
                'id' => 5,
                'nama' => 'Other Methode',
                'tipe' => 'other_methode',
                'is_midtrans' => 0
            ];

            array_push($arr, $b);
        }
    }else{
        $arr = [
            [
                'id' => 1,
                'nama' => isset($model->payment_method_voucher_global) && !empty($model->payment_method_voucher_global) ? $model->payment_method_voucher_global : 'Payment Manual',
//                'nama' => 'Voucher Spesial',
                'tipe' => 'other_methode',
                'is_midtrans' => 0
            ]
        ];
    }

    return $arr;
}


function getMethodBayarCustom($other = 1)
{
    $arr = [
        [
            'id' => 1,
            'nama' => 'Bank Transfer',
            'tipe' => 'manual',
            'is_midtrans' => 0
        ],
        [
            'id' => 2,
            'nama' => 'Virtual Account',
            'tipe' => 'bank_transfer',
            'is_midtrans' => 1
        ],
        [
            'id' => 3,
            'nama' => 'Credit Card',
            'tipe' => 'credit_card',
            'is_midtrans' => 1
        ],
        [
            'id' => 4,
            'nama' => 'Gopay',
            'tipe' => 'gopay_qris',
            'is_midtrans' => 1
        ],
        [
            'id' => 5,
            'nama' => 'Bank Transfer',
            'tipe' => 'manual_transfer',
            'is_midtrans' => 0
        ],
    ];

    // $a = true;
    if ($other == 1) {
        $b = [
            'id' => 5,
            'nama' => 'Other Methode',
            'tipe' => 'other_methode',
            'is_midtrans' => 0
        ];

        array_push($arr, $b);
    }

    return $arr;
}


function getMethodPayment($other = 1)
{
    $arr = [
        [
            'id' => 1,
            'nama' => 'Bank Transfer',
//            'tipe' => 'manual',
//            'is_midtrans' => 0
            'tipe' => 'bank_transfer',
            'is_midtrans' => 1
        ],
        [
            'id' => 2,
            'nama' => 'Virtual Account',
            'tipe' => 'bank_transfer',
            'is_midtrans' => 1
        ],
        [
            'id' => 3,
            'nama' => 'Credit Card',
            'tipe' => 'credit_card',
            'is_midtrans' => 1
        ],
        [
            'id' => 4,
            'nama' => 'Gopay',
            'tipe' => 'gopay',
            'is_midtrans' => 1
        ],
        [
            'id' => 5,
            'nama' => 'Bank Transfer',
//            'tipe' => 'manual_transfer',
//            'is_midtrans' => 0
            'tipe' => 'bank_transfer',
            'is_midtrans' => 1
        ],
        [
            'id' => 6,
            'nama' => 'Free Without Addon',
            'tipe' => 'free_addon',
            'is_midtrans' => 0
        ]
    ];

    // $a = true;
    if ($other == 1) {
        $b = [
            'id' => 5,
            'nama' => 'Other Methode',
            'tipe' => 'other_methode',
            'is_midtrans' => 0
        ];

        array_push($arr, $b);
    }

    return $arr;
}


function getMethodBayarEvent($cek = 1, $id_event = 0, $voucher_spesial)
{
    $db = Db::db();
    $model = $db->select("payment_method")
        ->from("m_event")
        ->where("id","=", $id_event)
        ->find();

    if ($cek == 1 && $voucher_spesial == 0) {
        $arr = [
            [
                'id' => 1,
                'nama' => 'Virtual Account',
                'tipe' => 'bank_transfer',
                'is_midtrans' => 1
            ],
            [
                'id' => 2,
                'nama' => 'Credit Card',
                'tipe' => 'credit_card',
                'is_midtrans' => 1
            ],
            [
                'id' => 3,
                'nama' => 'Gopay',
                'tipe' => 'gopay_qris',
                'is_midtrans' => 1
            ]
        ];
    } elseif ($voucher_spesial == 1) {
        $arr = [
            [
                'id' => 1,
//                'nama' => isset($model->payment_method) && !empty($model->payment_method) ? $model->payment_method : 'Free Without Addon',
                'nama' => 'voucher spesial',
                'tipe' => 'free_addon',
                'is_midtrans' => 0
            ]
        ];
    }else {
        $arr = [
            [
                'id' => 1,
                'nama' => isset($model->payment_method) && !empty($model->payment_method) ? $model->payment_method : 'Free Without Addon',
                'tipe' => 'free_addon',
                'is_midtrans' => 0
            ]
        ];
    }

    return $arr;
}
function normalizeChars() {
    return array(
        'ï¿½' => 'S', 'ï¿½' => 's', 'ï¿½' => 'Dj', 'ï¿½' => 'Z', 'ï¿½' => 'z', 'ï¿½' => 'A', 'ï¿½' => 'A', 'ï¿½' => 'A', 'ï¿½' => 'A', 'ï¿½' => 'A',
        'ï¿½' => 'A', 'ï¿½' => 'A', 'ï¿½' => 'C', 'ï¿½' => 'E', 'ï¿½' => 'E', 'ï¿½' => 'E', 'ï¿½' => 'E', 'ï¿½' => 'I', 'ï¿½' => 'I', 'ï¿½' => 'I',
        'ï¿½' => 'I', 'ï¿½' => 'N', 'ï¿½' => 'O', 'ï¿½' => 'O', 'ï¿½' => 'O', 'ï¿½' => 'O', 'ï¿½' => 'O', 'ï¿½' => 'O', 'ï¿½' => 'U', 'ï¿½' => 'U',
        'ï¿½' => 'U', 'ï¿½' => 'U', 'ï¿½' => 'Y', 'ï¿½' => 'B', 'ï¿½' => 'Ss', 'ï¿½' => 'a', 'ï¿½' => 'a', 'ï¿½' => 'a', 'ï¿½' => 'a', 'ï¿½' => 'a',
        'ï¿½' => 'a', 'ï¿½' => 'a', 'ï¿½' => 'c', 'ï¿½' => 'e', 'ï¿½' => 'e', 'ï¿½' => 'e', 'ï¿½' => 'e', 'ï¿½' => 'i', 'ï¿½' => 'i', 'ï¿½' => 'i',
        'ï¿½' => 'i', 'ï¿½' => 'o', 'ï¿½' => 'n', 'ï¿½' => 'o', 'ï¿½' => 'o', 'ï¿½' => 'o', 'ï¿½' => 'o', 'ï¿½' => 'o', 'ï¿½' => 'o', 'ï¿½' => 'u',
        'ï¿½' => 'u', 'ï¿½' => 'u', 'ï¿½' => 'u', 'ï¿½' => 'y', 'ï¿½' => 'y', 'ï¿½' => 'b', 'ï¿½' => 'y', 'ï¿½' => 'f',
    );
}
function urlParsing($string) {
    $arrDash = array("--", "---", "----", "-----");
    $string = strtolower(trim($string));
    $string = strtr($string, normalizeChars());
    $string = preg_replace('/[^a-zA-Z0-9 -.]/', '', $string);
    $string = str_replace(" ", "-", $string);
    $string = str_replace("&", "", $string);
    $string = str_replace(array("'", "\"", "&quot;"), "", $string);
    $string = str_replace($arrDash, "-", $string);
    return str_replace($arrDash, "-", $string);
}
function time_ago($timestamp)
{
    $time_ago = strtotime($timestamp);
    $current_time = time();
    $time_difference = $current_time - $time_ago;
    $seconds = $time_difference;
    $minutes = round($seconds / 60);        // value 60 is seconds
    $hours = round($seconds / 3600);       //value 3600 is 60 minutes * 60 sec
    $days = round($seconds / 86400);      //86400 = 24 * 60 * 60;
    $weeks = round($seconds / 604800);     // 7*24*60*60;
    $months = round($seconds / 2629440);    //((365+365+365+365+366)/5/12)*24*60*60
    $years = round($seconds / 31553280);   //(365+365+365+365+366)/5 * 24 * 60 * 60
    if ($seconds <= 60) {
        return "Just Now";
    } else if ($minutes <= 60) {
        if ($minutes == 1) {
            return "one minute ago";
        } else {
            return "$minutes minutes ago";
        }
    } else if ($hours <= 24) {
        if ($hours == 1) {
            return "an hour ago";
        } else {
            return "$hours hrs ago";
        }
    } else if ($days <= 7) {
        if ($days == 1) {
            return "yesterday";
        } else {
            return "$days days ago";
        }
    } else if ($weeks <= 4.3) {  //4.3 == 52/12
        if ($weeks == 1) {
            return "a week ago";
        } else {
            return "$weeks weeks ago";
        }
    }
    // } else if($months <=12){
    //  if($months==1){
    //    return "a month ago";
    //  }else{
    //    return "$months months ago";
    //  }
    // }else {
    //  if($years==1){
    //    return "one year ago";
    //  }else {
    //    return "$years years ago";
    //  }
    // }
    else {
        $tgl = date('d M Y H:i', strtotime($timestamp));
        return $tgl;
    }
}


