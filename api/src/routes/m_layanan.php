<?php

use Service\Db;
use Model\Layanan;
use Service\Landa;

$app->get('/m_layanan/index', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $layanan = new Layanan();

    $getData = $layanan->getAll($params);

    return successResponse($response, [
        'list' => $getData['data'],
        'totalItems' => $getData['totalItem']
    ]);
});

$app->post("/m_layanan/save", function ($request, $response) {
    $params = $request->getParams();
    $landa = new Landa();
    $layanan = new Layanan();

    $validasi = $layanan->validasi($params);

    if (true === $validasi) {

        $save = $layanan->save($params);

        if ($save['status']) {
            return successResponse($response, $save['data']);
        }

        return unprocessResponse($response, [$save['error']]);
    }

    return unprocessResponse($response, $validasi);

});

$app->post("/m_layanan/delete", function ($request, $response) {
    $params = $request->getParams();
    $layanan = new Layanan();

    $model = $layanan->delete($params);

    if ($model['status']) {
        return successResponse($response, $model['data']);
    }

    return unprocessResponse($response, [$model['error']]);
});

$app->post("/m_layanan/restore", function ($request, $response) {
    $params = $request->getParams();
    $layanan = new Layanan();

    $model = $layanan->restore($params);

    if ($model['status']) {
        return successResponse($response, $model['data']);
    }

    return unprocessResponse($response, [$model['error']]);
});


$app->get("/m_layanan/getHistori", function ($request, $response) {
    $data = $request->getParams();
    $db = Db::db();

    $val = getHistori('m_barang_id', 0, 'nama', $data['m_barang_id']);
    $model = $val['data'];
    return successResponse($response, $model);
});

?>