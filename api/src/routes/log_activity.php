<?php

use Service\Db;
use Service\Firebase;
use Service\Landa;


function validasi($data, $custom = array())
{
    $validasi = array(
        "judul" => "required",
        "deskripsi" => "required",
        "sort_order" => "required"
    );

    GUMP::set_field_name("judul", "Title");
    GUMP::set_field_name("deskripsi", "Excerpt");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}


$app->get('/log_activity/index', function ($request, $response) {
    $params = $request->getParams();
    $landa = new Landa();
    $db = Db::db();


    $db->select("
    m_event_data_histori.*,
    m_user.nama nama_admin,
    m_event.nama,
    user.email,
    user.nama nama_user
    
    ")
        ->from("m_event_data_histori")
        ->leftJoin("m_user", "m_user.id = m_event_data_histori.created_by")
        ->leftJoin("m_user as user", "user.id = m_event_data_histori.m_user_id")
        ->leftJoin("m_event", "m_event.id = m_event_data_histori.m_event_id");


    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == 'tgl') {
                $convert = (array)$val;
                $tanggal = $landa->arrayToDateCustom($convert);
                $db->customWhere("FROM_UNIXTIME(m_event_data_histori.created_at, '%Y-%m-%d') = '" . $tanggal . "'", "AND");
            } elseif ($key == "event") {
                $db->where('m_event_data_histori.m_event_id', '=', $val);
            } elseif ($key == 'nama_user') {
                $db->where('user.nama', "LIKE", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }


    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    $db->orderBy("m_event_data_histori.created_at DESC");
    $model = $db->findAll();
    $totalItem = $db->count();
//print_die($model);
    if (!empty($model)) {
        foreach ($model as $value) {
            $value->created_at = date('d F Y H:i:s', $value->created_at);
        }
    }

    return successResponse($response, [
        'list' => $model,
        'totalItems' => $totalItem

    ]);
});


$app->post("/m_banner/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = Db::db();
    $model = $db->update("m_banner", ['is_deleted' => 1], ['id' => $data['id']]);
    if (isset($model)) {
        return successResponse($response, [$model]);
    }

    return unprocessResponse($response, ['terjadi masalah pada server']);
});


?>