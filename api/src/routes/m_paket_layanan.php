<?php

use Service\Db;
use Model\PaketLayanan;
use Service\Landa;

$app->get('/m_paket_layanan/index', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $paketlayanan = new PaketLayanan();

    $getData = $paketlayanan->getAll($params);

    return successResponse($response, [
        'list' => $getData['data'],
        'totalItems' => $getData['totalItem']
    ]);
});

$app->post("/m_paket_layanan/save", function ($request, $response) {
    $params = $request->getParams();
    $landa = new Landa();
    $paketlayanan = new PaketLayanan();

    $validasi = $paketlayanan->validasi($params);

    if (true === $validasi) {

        $save = $paketlayanan->save($params);

        if ($save['status']) {
            return successResponse($response, $save['data']);
        }

        return unprocessResponse($response, [$save['error']]);
    }

    return unprocessResponse($response, $validasi);

});

$app->post("/m_paket_layanan/delete", function ($request, $response) {
    $params = $request->getParams();
    $paketlayanan = new PaketLayanan();

    $model = $paketlayanan->delete($params);

    if ($model['status']) {
        return successResponse($response, $model['data']);
    }

    return unprocessResponse($response, [$model['error']]);
});

$app->post("/m_paket_layanan/restore", function ($request, $response) {
    $params = $request->getParams();
    $paketlayanan = new PaketLayanan();

    $model = $paketlayanan->restore($params);

    if ($model['status']) {
        return successResponse($response, $model['data']);
    }

    return unprocessResponse($response, [$model['error']]);
});


$app->get("/m_paket_layanan/getHistori", function ($request, $response) {
    $data = $request->getParams();
    $db = Db::db();

    $val = getHistori('m_barang_id', 0, 'nama', $data['m_barang_id']);
    $model = $val['data'];
    return successResponse($response, $model);
});

?>