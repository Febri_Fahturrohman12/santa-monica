<?php

use Service\Db;
use Model\Barang;
use Service\Landa;

$app->get('/m_barang/index', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $barang = new Barang();

    $getData = $barang->getAll($params);

    return successResponse($response, [
        'list' => $getData['data'],
        'totalItems' => $getData['totalItem']
    ]);
});

$app->post("/m_barang/save", function ($request, $response) {
    $params = $request->getParams();
    $landa = new Landa();
    $barang = new Barang();

    $validasi = $barang->validasi($params);

    if (true === $validasi) {
        // Upload foto barang
        if (isset($params['foto']) && !empty($params['foto'])) {
            $path = 'assets/img/barang/';
            $uploadFile = $landa->base64ToImage($path, $params['foto']);
            if ($uploadFile['status']) {
                $params['foto'] = $uploadFile['data'];
            } else {
                return unprocessResponse($response, [$uploadFile['error']]);
            }
        }

        $save = $barang->save($params);

        if ($save['status']) {
            return successResponse($response, $save['data']);
        }

        return unprocessResponse($response, [$save['error']]);
    }

    return unprocessResponse($response, $validasi);

});

$app->post("/m_barang/delete", function ($request, $response) {
    $params = $request->getParams();
    $barang = new Barang();

    $model = $barang->delete($params);

    if ($model['status']) {
        return successResponse($response, $model['data']);
    }

    return unprocessResponse($response, [$model['error']]);
});

$app->post("/m_barang/restore", function ($request, $response) {
    $params = $request->getParams();
    $barang = new Barang();

    $model = $barang->restore($params);

    if ($model['status']) {
        return successResponse($response, $model['data']);
    }

    return unprocessResponse($response, [$model['error']]);
});


$app->get("/m_barang/getHistori", function ($request, $response) {
    $data = $request->getParams();
    $db = Db::db();

    $val = getHistori('m_barang_id', 0, 'nama', $data['m_barang_id']);
    $model = $val['data'];
    return successResponse($response, $model);
});

?>