<?php

use Service\Db;
use Model\LaporanPenjualan;

$app->get('/l_penjualan/index', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $penjualan = new LaporanPenjualan();

    $getData = $penjualan->getAll($params);
    if (!isset($params['is_export']) || empty($params['is_export'])) {
        return successResponse($response, $getData);
    }
});

$app->post('/l_penjualan/hapus', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $penjualan = new LaporanPenjualan();

    $getData = $penjualan->hapus($params);

    return successResponse($response, $getData);
});


?>
