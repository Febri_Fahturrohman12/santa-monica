<?php

use Service\Db;
use Model\LaporanPengambilanKas;

$app->get('/l_pengambilankas/index', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $komisi = new LaporanPengambilanKas();

    $getData = $komisi->getAll($params);
    if (!isset($params['is_export']) || empty($params['is_export'])) {
        return successResponse($response, $getData);
    }
});

$app->post('/l_pengambilankas/hapus', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $komisi = new LaporanPengambilanKas();

    $getData = $komisi->hapus($params);

    return successResponse($response, $getData);
});


?>
