<?php

use Service\Db;
use Service\Firebase;
use Service\Landa;


function validasi($data, $custom = array())
{
    $validasi = array(//  "jenis" => "required"

    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get('/t_order/index', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $landa = new Landa();
    $data = $db->select('
    t_order.id,
    t_order.m_event_id,
    t_order.m_event_race_id,
    t_order.m_user_address_id,
    t_order.m_user_id,
    t_order.is_join,
    t_order.is_payment,
    t_order.payment_method,
    t_order.is_paid,
    t_order.is_declined,
    t_order.is_expired,
    t_order.no_invoice,
    t_order.no_order,
    t_order.tanggal,
    IFNULL(t_order.is_wellness_program, 0) as is_wellness_program,
    IFNULL(t_order.lingkar_perut, 0) lingkar_perut,
    IFNULL(t_order.berat_badan, 0) as berat_badan,
    IFNULL(t_order.tinggi_badan, 0) as tinggi_badan,
    IFNULL(t_order.is_shipper,0) as is_shipper,
    IFNULL(t_order.m_user_address_id, 0) id_address,
    t_confirm_order.id id_confirm_order,
    t_confirm_order.status status_order,
    IFNULL(t_confirm_order.is_midtrans, 0) is_midtrans,
    m_user.nama,
    m_user.is_deleted is_deleted_user,
    m_event.nama as nama_event,
    m_event_race.jarak,
    m_event_race.is_terjauh,
            m_club.nama organisasi,
    m_race_kategori.nama as nama_ketagori
    ')
        ->from('t_order')
        ->leftJoin("t_confirm_order", "t_confirm_order.t_order_id = t_order.id")
        ->leftJoin("m_user", "m_user.id = t_order.m_user_id")
        ->leftJoin("m_club_member", "t_order.m_user_id = m_club_member.m_user_id")
        ->leftJoin("m_club", "m_club.id = m_club_member.m_club_id")
        ->leftJoin("m_event", "m_event.id = t_order.m_event_id")
        ->leftJoin("m_event_race", "t_order.m_event_race_id = m_event_race.id")
        ->leftJoin("m_race_kategori", "m_race_kategori.id = m_event_race.m_race_kategori_id")
        // ->where("t_order.id", "=", 3267)
        ->where("t_order.is_deleted", "=", 0)
//        ->andwhere("m_user.is_deleted", "=", 0)
        ->andwhere("m_event.is_deleted", "=", 0);

    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        $periode_mulai = '';
        $periode_selesai = '';
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $data->where('m_user.nama', "LIKE", $val);
            } elseif ($key == "email") {
                $data->where('m_user.email', "LIKE", $val);
            } elseif ($key == "order_no") {
                $data->where('t_order.no_order', "LIKE", $val);

            } elseif ($key == "event") {
                if (!empty($val)) {
                    $data->where('t_order.m_event_id', "=", $val);
                }
            } elseif ($key == "no_invoice") {
                $data->where('t_order.no_invoice', "LIKE", $val);
            } elseif ($key == "club") {
                $data->where('m_club.nama', "LIKE", $val);
            } elseif ($key == "email") {
                $data->where('m_user.email', "LIKE", $val);
            }
            // elseif ($key == "tgl") {
            //     $val = json_decode(json_encode($val), true);
            //     $val = $landa->arrayToDate($val);
            //     $data->where("DATE(t_order.tanggal)", "=", $val);
            // }
            // elseif ($key == "tgl") {

            //     $val = date('Y-m-d', strtotime($val));
            //     $data->where("DATE(t_order.tanggal)", "=", $val);
            // }
            else if ($key == 'status' && $val == 'expired') {
                $data->customwhere("t_order.is_expired = 1", "AND");
            } else if ($key == 'status' && $val == 'declined') {
                $data->customwhere("t_order.is_declined = 1", "AND");
            } else if ($key == 'status' && $val == 'waitpay') {
                $data->customwhere("t_confirm_order.status = 'pending' AND t_order.is_join = 0 AND t_order.is_payment = 0", "AND");
            } else if ($key == 'status' && $val == 'waitcon') {
                $data->customwhere("t_confirm_order.status = 'pending' AND t_order.is_join = 0 AND t_order.is_payment = 1", "AND");
            } else if ($key == 'status' && $val == 'join') {
                $data->customwhere("t_confirm_order.status = 'approved' AND t_order.is_join = 1 AND t_order.is_expired = 0 AND t_order.is_declined = 0", "AND");
            } elseif ($key == "periode_mulai") {
                $periode_mulai = $val;
            } elseif ($key == "periode_selesai") {
                $periode_selesai = $val;
            }
        }
    }

    if (isset($params['no_order']) && !empty($params['no_order'])) {
        $data->where("t_order.no_order", "=", $params['no_order']);
    }


    if (!empty($periode_mulai) && !empty($periode_selesai)) {
        $db->customWhere('DATE(t_order.tanggal) >= "' . $periode_mulai . '" AND DATE(t_order.tanggal) <= "' . $periode_selesai . '"', "AND");
    }

    if (isset($params["limit"]) && !empty($params["limit"])) {
        $data->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $data->offset($params["offset"]);
    }

    $models = $data->orderBy("FIELD(t_confirm_order.status, 'pending', 'approved', 'decline','declined', 'expire'), t_order.tanggal DESC")->findAll();
    $models = $data->groupBy("t_order.id, t_order.m_event_id")->findAll();
    $totalItem = $data->count();

    $data = $db->select('t_order_addon.*, m_event_addon.nama nama_produk')
        ->from('t_order_addon')
        ->leftJoin('m_event_addon', 't_order_addon.m_event_addon_id = m_event_addon.id')
        ->findAll();
    $arrAddon = [];
    if (!empty($data)) {
        foreach ($data as $key => $value) {
            $arrAddon[$value->t_order_id][] = $value;
        }
    }

    foreach ($models as $key => $value) {
        $value->is_addon = isset($arrAddon[$value->id]) && !empty($arrAddon[$value->id]) ? 1 : 0;

        $arr = getMethodBayarCustom();
        foreach ($arr as $k => $v) {
            if ($value->payment_method == $v['tipe']) {
                $value->payment_method = $v['nama'];
                break;
            }
        }
        if ($value->payment_method == 'free_addon') {
            $value->payment_method = ucwords(str_replace("_", " ", $value->payment_method));
        }

        if ($value->is_paid == 1) {

            if (empty($value->is_payment) && empty($value->is_join)) {
                $value->status_pesan = 'Waiting for Payment';
                if (empty($value->status_order)) {
                    $value->status_order = 'pending';
                }
            } elseif ($value->is_payment == 1 && empty($value->is_join)) {
                $value->status_pesan = 'Waiting for Confirmation';
                if (empty($value->status_order)) {
                    $value->status_order = 'pending';
                }
            } elseif ($value->is_payment == 1 && $value->is_join == 1) {
                $value->status_order = 'approved';
                $value->status_pesan = 'Success Join';
            } elseif (empty($value->id_confirm_order)) {
                $value->status_pesan = 'Waiting for Payment';
                if (empty($value->status_order)) {
                    $value->status_order = 'pending';
                }
            }

            if ($value->is_declined == 1) {
                $value->status_pesan = 'Declined';
            }

            if ($value->is_expired == 1) {
                $value->status_order = 'expire';
                $value->status_pesan = 'Expired';
            }
        } else {

            if ($value->status_order == 'approved') {
                $value->status_pesan = 'Success Join';
            } elseif ($value->is_declined == 1) {
                $value->status_pesan = 'Declined';
            } else {
                $value->status_pesan = 'Pending';
            }
        }

    }

    return successResponse($response, [
        'list' => $models,
        'totalItems' => $totalItem

    ]);
});

$app->get('/t_order/data_detail', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $landa = new Landa();
    $db->select('t_order.*,
    IFNULL(t_order.is_wellness_program, 0) as is_wellness_program,
    IFNULL(t_order.lingkar_perut, 0) lingkar_perut,
    IFNULL(t_order.berat_badan, 0) as berat_badan,
    IFNULL(t_order.tinggi_badan, 0) as tinggi_badan,
    IFNULL(t_order.is_shipper,0) as is_shipper,
    IFNULL(t_order.m_user_address_id, 0) id_address,
    t_confirm_order.id id_confirm_order,
    t_confirm_order.status status_order,
    IFNULL(t_confirm_order.is_midtrans, 0) is_midtrans,
    m_user.id as m_user_id,
    m_user.nama,
    m_user.jenis_kelamin,
    m_user.no_id,
    m_user.no_hp,
    m_user.gol_darah,
    m_club.nama organisasi,
    m_user.status as status_user,
    m_user.tanggal_lahir,
    m_event.nama as nama_event,
    m_event_race.jarak,
    m_event_race.harga,
    m_event_race.is_terjauh,
    m_race_kategori.nama as nama_ketagori,
    wilayah_negara.nama as nama_negara,
    wilayah_provinsi.nama as nama_provinsi,
    m_occupation.nama_idn,
    m_user_receiver.nama as nama_receiver,
    m_user_receiver.no_hp as phone_receiver,
    m_user_receiver.alamat as alamat_receiver,
    m_user_receiver.kode_pos as kode_pos_receiver,
    wilayah_negara_receiver.nama as negara_receiver,
    wilayah_provinsi_receiver.nama as provinsi_receiver,
    wilayah_kabupaten_receiver.nama as kabupaten_receiver,
    wilayah_kecamatan_receiver.nama as kecamatan_receiver,
    m_user_address.ket,
    m_user_address.district_id,
    m_user_address.subdistrict_id,
    m_voucher_det.kode_voucher
    ')
        ->from('t_order')
        ->leftJoin("t_confirm_order", "t_confirm_order.t_order_id = t_order.id")
        ->leftJoin("m_voucher_det", "m_voucher_det.id = t_order.m_voucher_id")
        ->leftJoin("m_user", "m_user.id = t_order.m_user_id")
        ->leftJoin("m_club_member", "m_user.id = m_club_member.m_user_id")
        ->leftJoin("m_club", "m_club.id = m_club_member.m_club_id")
        ->leftJoin("m_event", "m_event.id = t_order.m_event_id")
        ->leftJoin("m_event_race", "m_event_race.id = t_order.m_event_race_id")
        ->leftJoin("m_race_kategori", "m_race_kategori.id = m_event_race.m_race_kategori_id")
        ->leftJoin("wilayah_negara", "wilayah_negara.id = m_user.negara_id")
        ->leftJoin("wilayah_provinsi", "wilayah_provinsi.id = m_user.provinsi_id")
        ->leftJoin("m_occupation", "m_occupation.id = m_user.m_occupation_id")
        // ->leftJoin('m_event_data', 'm_event_data.m_event_id = t_order.m_event_id AND m_event_data.m_user_id = t_order.m_user_id AND m_event_data.status = "approve"')


        //        receiver
        ->leftJoin("m_user_address", "m_user_address.id = t_order.m_user_address_id")
        ->leftJoin("m_user as m_user_receiver", "m_user_receiver.id = m_user_address.m_user_id")
        ->leftJoin("wilayah_negara as wilayah_negara_receiver", "wilayah_negara_receiver.id = m_user_address.country_id")
        ->leftJoin("wilayah_provinsi as wilayah_provinsi_receiver", "wilayah_provinsi_receiver.id = m_user_address.province_id")
        ->leftJoin("wilayah_kabupaten as wilayah_kabupaten_receiver", "wilayah_kabupaten_receiver.id = m_user_address.regency_id")
        ->leftJoin("wilayah_kecamatan as wilayah_kecamatan_receiver", "wilayah_kecamatan_receiver.id = m_user_address.district_id")
        ->where("t_order.no_order", "=", $params['no_order']);

    $models = $db->find();

    if (!empty($models)) {
//       
        if (!empty($models->district_id) && !empty($models->subdistrict_id)) {
            $desa = getDesa($models->district_id, $models->subdistrict_id);
        }
        $models->desa_receiver = isset($desa['nama']) ? $desa['nama'] : null;
        $models->ttl = date("d-m-Y", strtotime($models->tanggal_lahir));

    }

    return successResponse($response, $models);
});


$app->post("/t_order/save", function ($request, $response) {
    $data = $request->getParams();
    $db = Db::db();
    $landa = new Landa();
//    ej($data);
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            if (isset($data['tanggal'])) {
                $data['tanggal'] = $landa->arrayToDate($data['tanggal']);
            }

            if (isset($data['tanggal_confirm'])) {
                $data['tanggal_confirm'] = $landa->arrayToDate($data['tanggal_confirm']);
            }

            if (isset($data['image']['base64']) && !empty($data['image']['base64'])) {
                $path = 'pembayaran_free/';

                if (isset($data["id"])) {
                    $fotoLama = $db->select("id, image, path")->from("t_confirm_order")
                        ->where("id", "=", $data["id"])
                        ->andWhere("is_paid", "=", 0)
                        ->find();

                    if (!empty($fotoLama->image)) {
                        if ($landa->cekImage(str_replace('assets/img/', '', $path) . $fotoLama->image)) {
                            $landa->deleteImage(str_replace('assets/img/', '', $path) . $fotoLama->image);
                        }
//                        if (file_exists($path . $fotoLama->image)) {
//                            unlink($path . $fotoLama->image);
//                        }
                    }
                }

                /**
                 * upload image
                 */
                //        penamaan file = module-user-md5(userid)-strtotime
                $fileName = "pembayaran-free-" . strtotime(date('Y-m-d H:i:s')); //string
                $foto = $landa->base64_to_jpeg($data['image']['base64'], $fileName); //file
                $upload = $landa->uploadImage($foto, "pembayaran_free", $foto);
                $data['image'] = $foto;
                $data['path'] = 'pembayaran_free/';
                /**
                 * end upload image
                 */

//                $data['path'] = 'pembayaran_free/';
//                $uploadFile = $landa->base64ToImage($path, $data['image']['base64']);
//
//                if ($uploadFile) {
//                    $data['image'] = $uploadFile['data'];
//                } else {
//                    return unprocessResponse($response, [$uploadFile['error']]);
//                }
            } else {
                unset($data['image']);
            }


            if (isset($data["id"])) {
                $model = $db->update("t_confirm_order", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("t_confirm_order", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, [$e]);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->post("/t_order/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = Db::db();
    try {
        if ($data['is_midtrans'] == 1 && $data['status_order'] != 'expire') {
            if ($data['status_order'] == 'approved') {
                return unprocessResponse($response, ["Paid data cannot be deleted"]);
            }
            $cancel = cancelPayment($data['no_order']);
        }

        $data['t_order_id'] = $data['id'];
        unset($data['id']);

        $model = $db->insert("t_order_histori_delete", $data);
        $db->run("DELETE FROM m_event_data WHERE m_user_id IN (" . $data['m_user_id'] . ") AND m_event_id =" . $data['m_event_id']);
        $db->run('DELETE FROM t_confirm_order WHERE t_order_id IN (' . $data['t_order_id'] . ')');
        $db->run('DELETE FROM t_order_addon WHERE t_order_id IN (' . $data['t_order_id'] . ')');
        $db->run('DELETE FROM t_order_entitlement WHERE t_order_id IN (' . $data['t_order_id'] . ')');
        $db->run('DELETE FROM t_order WHERE id IN (' . $data['t_order_id'] . ')');

//    $db->delete("t_confirm_order", ['t_order_id' => $data['id']]);
//    $db->delete("t_order", ['id' => $data['id']]);


        if (isset($model)) {
            return successResponse($response, [$model]);
        }
    } catch (Exception $e) {
        return unprocessResponse($response, [$e]);
    }

});


$app->get('/t_order/export', function ($request, $response) {
    ini_set('max_execution_time', 0);
    ini_set('memory_limit', '256M');

    $params = $request->getParams();
    $db = Db::db();

    $data = $db->select('
        t_order.id,
        t_order.m_event_id,
        t_order.m_event_race_id,
        t_order.m_user_address_id,
        t_order.m_user_id,
        t_order.is_join,
        t_order.is_payment,
        t_order.payment_method,
        t_order.is_paid,
        t_order.is_declined,
        t_order.is_expired,
        t_order.no_invoice,
        t_order.no_order,
        t_order.ebib_no,
        t_order.ebib_name,
        t_order.tanggal,
        t_order.track_id,
        IFNULL(t_order.is_wellness_program, 0) as is_wellness_program,
        IFNULL(t_order.lingkar_perut, 0) lingkar_perut,
        IFNULL(t_order.berat_badan, 0) as berat_badan,
        IFNULL(t_order.tinggi_badan, 0) as tinggi_badan,
        IFNULL(t_order.is_shipper,0) as is_shipper,
        IFNULL(t_order.m_user_address_id, 0) id_address,
        t_confirm_order.id id_confirm_order,
        t_confirm_order.status status_order,
        IFNULL(t_confirm_order.is_midtrans, 0) is_midtrans,
        m_user.id as m_user_id,
        m_user.nama,
        m_user.no_hp,
        m_user.email,
        m_user.kecamatan_id,
        m_user.desa_id,
        m_user.kode_pos,
        m_user.alamat,
        m_user.jenis_kelamin,
        m_club.nama organisasi,
        wilayah_provinsi.nama provinsi,
        wilayah_kabupaten.nama kabupaten,
        wilayah_kecamatan.nama kecamatan,
        m_event.nama as nama_event,
        m_event_race.jarak jarak_race,
        IFNULL(m_event_race.is_terjauh,0) is_terjauh,
        m_race_kategori.nama race_kategori,
        IFNULL(SUM(m_event_data.calories), 0) as kalori,
        IFNULL(SUM(m_event_data.jarak), 0) as total_jarak,
        IFNULL( SEC_TO_TIME(SUM(TIME_TO_SEC(m_event_data.waktu))), 0) as waktu_total,
        m_user_address.country_id,
        m_user_address.province_id,
        m_user_address.regency_id,
        m_user_address.district_id,
        m_user_address.subdistrict_id,
        m_user_address.postcode,
        m_user_address.address,
        m_user_address.pic,
        m_user_address.phone,
        m_user_address.is_default,
        m_user_address.ket,
        provinsi_user.nama provinsi_user,
        kabupaten_user.nama kabupaten_user,
        kecamatan_user.nama kecamatan_user
    ')
        ->from('t_order')
        ->leftJoin("t_confirm_order", "t_confirm_order.t_order_id = t_order.id")
        ->leftJoin("m_event_race", "m_event_race.id = t_order.m_event_race_id")
        ->leftJoin("m_race_kategori", "m_race_kategori.id = m_event_race.m_race_kategori_id")
        ->leftJoin("m_event", "m_event.id = t_order.m_event_id")
        ->leftJoin("m_user", "m_user.id = t_order.m_user_id")
        ->leftJoin("m_user_address", "m_user_address.id = t_order.m_user_address_id")
        ->leftJoin("wilayah_provinsi", "wilayah_provinsi.id = m_user_address.province_id")
        ->leftJoin("wilayah_kabupaten", "wilayah_kabupaten.id = m_user_address.regency_id")
        ->leftJoin("wilayah_kecamatan", "wilayah_kecamatan.id = m_user_address.district_id")
        ->leftJoin("wilayah_provinsi provinsi_user", "provinsi_user.id = m_user.provinsi_id")
        ->leftJoin("wilayah_kabupaten kabupaten_user", "kabupaten_user.id = m_user.kabupaten_id")
        ->leftJoin("wilayah_kecamatan kecamatan_user", "kecamatan_user.id = m_user.kecamatan_id")
        ->leftJoin("m_club_member", "m_user.id = m_club_member.m_user_id")
        ->leftJoin("m_club", "m_club.id = m_club_member.m_club_id")
        ->leftJoin('m_event_data', 'm_event_data.m_event_id = t_order.m_event_id AND m_event_data.m_user_id = t_order.m_user_id AND m_event_data.status = "approve"')
        ->where("t_order.is_deleted", "=", 0)
        ->andwhere("m_event.is_deleted", "=", 0);
//        ->andwhere("m_user.is_deleted", "=", 0);

    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        $periode_mulai = '';
        $periode_selesai = '';
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $data->where('m_user.nama', "LIKE", $val);
            } elseif ($key == "order_no") {
                $data->where('t_order.no_order', "LIKE", $val);

            } elseif ($key == "event") {
                if (!empty($val)) {
                    $data->where('t_order.m_event_id', "=", $val);
                }
            } elseif ($key == "no_invoice") {
                $data->where('t_order.no_invoice', "LIKE", $val);
            } elseif ($key == "club") {
                $data->where('m_club.nama', "LIKE", $val);
            }
            // elseif ($key == "tgl") {
            //     $val = date('Y-m-d', strtotime($val));
            //     $data->where("DATE(t_order.tanggal)", "=", $val);
            // }
            else if ($key == 'status' && $val == 'expired') {
                $data->customwhere("t_order.is_expired = 1", "AND");
            } else if ($key == 'status' && $val == 'declined') {
                $data->customwhere("t_order.is_declined = 1", "AND");
            } else if ($key == 'status' && $val == 'waitpay') {
                $data->customwhere("t_confirm_order.status = 'pending' AND t_order.is_join = 0 AND t_order.is_payment = 0", "AND");
            } else if ($key == 'status' && $val == 'waitcon') {
                $data->customwhere("t_confirm_order.status = 'pending' AND t_order.is_join = 0 AND t_order.is_payment = 1", "AND");
            } else if ($key == 'status' && $val == 'join') {
                $data->customwhere("t_confirm_order.status = 'approved' AND t_order.is_join = 1 AND t_order.is_expired = 0 AND t_order.is_declined = 0", "AND");
            } elseif ($key == "periode_mulai") {
                $periode_mulai = $val;
            } elseif ($key == "periode_selesai") {
                $periode_selesai = $val;
            }
        }
    }

    if (isset($params['no_order']) && !empty($params['no_order'])) {
        $data->where("t_order.no_order", "=", $params['no_order']);
    }

    if (!empty($periode_mulai) && !empty($periode_selesai)) {
        $db->customWhere('DATE(t_order.tanggal) >= "' . $periode_mulai . '" AND DATE(t_order.tanggal) <= "' . $periode_selesai . '"', "AND");
    }

    if (isset($params["limit"]) && !empty($params["limit"])) {
        $data->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $data->offset($params["offset"]);
    }
    // $db->limit(1);
    $data->orderBy("FIELD(t_confirm_order.status, 'pending', 'approved', 'decline','declined', 'expire'), t_order.id DESC");
    $models = $data->groupBy("t_order.id, t_order.m_event_id")->findAll();

    $idToArr = json_decode(json_encode($models, JSON_INVALID_UTF8_IGNORE), true);
    $order_id = arrayToString($idToArr, 'id');
//    echo json_encode($order_id,JSON_INVALID_UTF8_IGNORE);exit();
    if (!empty($models)) {
        /**
         * addon
         */

        $addon = $db->select('t_order_addon.*, m_event_addon.harga, m_event_addon.nama nama_produk, m_event_addon.is_donasi')
            ->from('t_order_addon')
            ->leftJoin('m_event_addon', 't_order_addon.m_event_addon_id = m_event_addon.id')
            ->customWhere('t_order_addon.t_order_id IN (' . $order_id . ')')
            ->orderBy("m_event_addon.is_donasi ASC")
            ->findAll();

        $arrAddon = [];
        $panjang = 0;
        $a = [];
        if (!empty($addon)) {
            foreach ($addon as $key => $value) {
                $arrAddon[$value->t_order_id][] = $value;
                if ($value->is_donasi != 1) {
                    $a[$value->t_order_id][] = $value;
                }
                if (isset($a[$value->t_order_id]) && $panjang < count($a[$value->t_order_id])) {
                    $panjang = count($a[$value->t_order_id]);
                }
            }
        }

        /**
         * entitlement
         */

        $entitlement = $db->select('t_order_entitlement.*, m_event_entitlement.nama nama_produk, m_event_entitlement_det.variant, 1 as qty')
            ->from('t_order_entitlement')
            ->leftJoin('m_event_entitlement_det', 't_order_entitlement.m_event_entitlement_det_id = m_event_entitlement_det.id')
            ->leftJoin('m_event_entitlement', 'm_event_entitlement_det.m_event_entitlement_id = m_event_entitlement.id')
            ->customWhere('t_order_entitlement.t_order_id IN (' . $order_id . ')')
            ->findAll();

        $arrEntitlement = [];
        $panjangEntitlement = 0;
        $b = [];
        if (!empty($entitlement)) {
            foreach ($entitlement as $key => $value) {
                $arrEntitlement[$value->t_order_id][] = $value;
                if (isset($arrEntitlement[$value->t_order_id]) && $panjangEntitlement < count($arrEntitlement[$value->t_order_id])) {
                    $panjangEntitlement = count($arrEntitlement[$value->t_order_id]);
                }
            }
        }


        foreach ($models as $key => $value) {
//            if (isset($value->m_user_address_id) && !empty($value->m_user_address_id)) {
//                $desa = getDesa($value->district_id, $value->subdistrict_id);
//                $value->alamat = isset($value->address) ? preg_replace('/[^\p{L}\p{N}\s]/u', '', $value->address) : '';
//            } else {
//                $desa = getDesa($value->kecamatan_id, $value->desa_id);
//                $value->alamat = isset($value->alamat) ? preg_replace('/[^\p{L}\p{N}\s]/u', '', $value->alamat) : '';
//            }
//            $value->desa = isset($desa['nama']) ? $desa['nama'] : null;
//            $alamat = getAlamat($value->m_user_id);

            /**
             * masukkan addon
             */

            $value->addonTemp = isset($arrAddon[$value->id]) ? $arrAddon[$value->id] : [];

            if (!empty($value->addonTemp)) {
                foreach ($value->addonTemp as $keys => $vals) {
                    if ($vals->is_donasi == 1) {
                        if (!isset($value->donasi)) {
                            $value->donasi = $vals->qty * $vals->harga;
                        } else {
                            $value->donasi += $vals->qty * $vals->harga;
                        }


                        if (!isset($value->addon)) {
                            $value->addon[] = '';
                        }
                    } else {
                        if (!isset($value->donasi)) {
                            $value->donasi = null;
                        }
                        $value->addon[] = !empty($vals->nama_produk) ? $vals->nama_produk : 'Medali Bulungan Run 2021 : variant Medal (click here)' . ' (' . $vals->qty . ')';
                    }
                }
            } else {
                $value->donasi = null;
                $value->addon[] = '';
            }
            $value->total_addon = count($value->addon);

            /**
             * masukkan entitlement
             */

            $value->entitlementTemp = isset($arrEntitlement[$value->id]) ? $arrEntitlement[$value->id] : [];

            if (!empty($value->entitlementTemp)) {
                foreach ($value->entitlementTemp as $keys => $vals) {
                    $value->entitlement[] = $vals->nama_produk . ' : variant ' . $vals->variant . ' (' . $vals->qty . ')';
                }
            } else {
                $value->entitlement[] = '';
            }
            $value->total_addon = count($value->addon);
            $value->total_entitlement = count($value->entitlement);

            $arr = getMethodBayarCustom();
            foreach ($arr as $k => $v) {
                if ($value->payment_method == $v['tipe']) {
                    $value->payment_method = $v['nama'];
                    break;
                }
            }
            if ($value->payment_method == 'free_addon') {
                $value->payment_method = ucwords(str_replace("_", " ", $value->payment_method));
            }


            if ($value->is_paid == 1) {
                if (empty($value->is_payment) && empty($value->is_join)) {
                    $value->status_pesan = 'Waiting for Payment';
                    if (empty($value->status_order)) {
                        $value->status_order = 'pending';
                    }
                } elseif ($value->is_payment == 1 && empty($value->is_join)) {
                    $value->status_pesan = 'Waiting for Confirmation';
                    if (empty($value->status_order)) {
                        $value->status_order = 'pending';
                    }
                } elseif ($value->is_payment == 1 && $value->is_join == 1) {
                    $value->status_pesan = 'Success Join';
                } elseif (empty($value->id_confirm_order)) {
                    $value->status_pesan = 'Waiting for Payment';
                    if (empty($value->status_order)) {
                        $value->status_order = 'pending';
                    }
                }

                if ($value->is_declined == 1) {
                    $value->status_pesan = 'Declined';
                }

                if ($value->is_expired == 1) {
                    $value->status_order = 'expire';
                    $value->status_pesan = 'Expired';
                }

                if (!isset($value->status_pesan) && empty($value->status_pesan)) {
                    $value->status_pesan = null;
                }
            } else {
                if ($value->status_order == 'approved') {
                    $value->status_pesan = 'Success Join';
                } elseif ($value->is_declined == 1) {
                    $value->status_pesan = 'Declined';
                } else {
                    $value->status_pesan = 'Pending';
                }

                if (!isset($value->status_pesan) && empty($value->status_pesan)) {
                    $value->status_pesan = null;
                }
            }


            if ($value->is_terjauh == 1 && !empty($value->total_jarak)) {
                $value->status_finish = 'Finished';
            } elseif (empty($value->is_terjauh) && $value->total_jarak >= $value->jarak_race) {
                $value->status_finish = 'Finished';
            } else {
                $value->status_finish = 'Not Finish';
            }
        }
    }

    foreach ($models as $key => $value) {
        for ($i = 0; count($value->addon) < $panjang; $i++) {
            $value->addon[] = '';
        }

        for ($i = 0; count($value->entitlement) < $panjangEntitlement; $i++) {
            $value->entitlement[] = '';
        }
    }

    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();
    // Set properties
    $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
        ->setLastModifiedBy("Maarten Balliauw")
        ->setTitle("Office 2007 XLSX Test Document")
        ->setSubject("Office 2007 XLSX Test Document")
        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("Test result file");
    $sheet = $objPHPExcel->getActiveSheet();

    $style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        ),
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            )
        )
    );

    $header_left = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
        ),
        'font' => array(
            'bold' => true,
        )
    );

    $header_center = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        ),
        'font' => array(
            'bold' => true,
        )
    );

    $style_left = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
        ),
    );

    $style_center = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        ),
    );
    // Add some data

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', "NO")
        ->setCellValue('B1', "CREATED AT")
        ->setCellValue('C1', "EVENT")
        ->setCellValue('D1', "ORDER NO")
        ->setCellValue('E1', "NAME")
        ->setCellValue('F1', "GENDER")
        ->setCellValue('G1', "ADDRESS")
        ->setCellValue('H1', "SUBDISTRICT")
        ->setCellValue('I1', "DISTRICT")
        ->setCellValue('J1', "REGENCY")
        ->setCellValue('K1', "PROVINCE")
        ->setCellValue('L1', "POSTCODE")
        ->setCellValue('M1', "NO. HANDPHONE")
        ->setCellValue('N1', "EMAIL")
        ->setCellValue('O1', "COMMUNITY");


//    $objPHPExcel->getActiveSheet()->getStyle("A1:M1")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle("A1:A1")->applyFromArray($header_center);
    $objPHPExcel->getActiveSheet()->getStyle("B1:B1")->applyFromArray($header_left);
    $objPHPExcel->getActiveSheet()->getStyle("C1:D1")->applyFromArray($header_center);
    $objPHPExcel->getActiveSheet()->getStyle("E1:E1")->applyFromArray($header_left);
    $objPHPExcel->getActiveSheet()->getStyle("G1:G1")->applyFromArray($header_center);
    $objPHPExcel->getActiveSheet()->getStyle("F1:F1")->applyFromArray($header_left);


    $no = 2;
    foreach ($models as $key => $value) {
        if ($value->organisasi == "🤔") {
            $value->organisasi = "";
        }

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . $no, $key + 1)
            ->setCellValue('B' . $no, !empty($value->tanggal) ? date('d-m-Y H:i', strtotime($value->tanggal)) : null)
            ->setCellValue('C' . $no, strtoupper($value->nama_event))
            ->setCellValue('D' . $no, ' ' . $value->no_order)
            ->setCellValue('E' . $no, strtoupper(removeEmoji($value->nama)))
            ->setCellValue('F' . $no, $value->jenis_kelamin)
            ->setCellValue('G' . $no, strtoupper(removeEmoji($value->alamat)));

        if (isset($value->m_user_address_id) && !empty($value->m_user_address_id)) {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H' . $no, isset($value->desa) ? strtoupper($value->desa) : '')
                ->setCellValue('I' . $no, strtoupper($value->kecamatan))
                ->setCellValue('J' . $no, strtoupper($value->kabupaten))
                ->setCellValue('K' . $no, strtoupper($value->provinsi))
                ->setCellValue('L' . $no, strtoupper($value->postcode));
        }else{
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H' . $no, isset($value->desa) ? strtoupper($value->desa) : '')
                ->setCellValue('I' . $no, strtoupper($value->kecamatan_user))
                ->setCellValue('J' . $no, strtoupper($value->kabupaten_user))
                ->setCellValue('K' . $no, strtoupper($value->provinsi_user))
                ->setCellValue('L' . $no, $value->kode_pos);
        }

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('M' . $no, ' ' . cekNoHp($value->no_hp))
            ->setCellValue('N' . $no, strtoupper($value->email))
            ->setCellValue('O' . $no, strtoupper(removeEmoji($value->organisasi)));


        /**
         * loop entitlement
         */

        $j = 'P';
        if (!empty($value->entitlement)) {
            foreach ($value->entitlement as $k => $v) {
                $sheet->setCellValue($j . $no, strtoupper($v));
                $j++;
            }
        }

        $sheet->setCellValue('P1', "ENTITLEMENT");
        $highestColumn = $sheet->getHighestColumn(2);
        $sheet->mergeCells('P1:' . $highestColumn . '1');
        $sheet->getStyle("O" . 1 . ":" . $highestColumn . 1)->applyFromArray($style);

        /**
         * loop addon
         */

        $m = $j;
        if (!empty($value->addon)) {
            foreach ($value->addon as $k => $v) {
                $sheet->setCellValue($m . $no, strtoupper($v));
                $m++;
            }
        }

        $sheet->setCellValue($j . '1', "ADDON");
        $highestColumn = $sheet->getHighestColumn(2);
        $sheet->mergeCells($j . '1:' . $highestColumn . '1');


//        echo $m."<br/>";
        $sheet->setCellValue($m . '1', "DONASI");
        $sheet->setCellValue($m . $no, ' ' . $value->donasi);
        $m++;

        $sheet->setCellValue($m . '1', "PAYMENT METHOD");
        $sheet->setCellValue($m . $no, ' ' . strtoupper($value->payment_method));
        $m++;

        $sheet->setCellValue($m . '1', "STATUS PAYMENT");
        $sheet->setCellValue($m . $no, ' ' . strtoupper($value->status_pesan));
        $m++;

        $sheet->setCellValue($m . '1', "RACE CATEGORY");
        $sheet->setCellValue($m . $no, ' ' . strtoupper($value->race_kategori));
        $m++;

        $sheet->setCellValue($m . '1', "TRACK ID");
        $sheet->setCellValue($m . $no, ' ' . $value->track_id);
        $m++;

        $sheet->setCellValue($m . '1', "EBIB NO");
        $sheet->setCellValue($m . $no, ' ' . strtoupper($value->ebib_no));
        $m++;

        $sheet->setCellValue($m . '1', "DISTANCE");
        $sheet->setCellValue($m . $no, ' ' . str_replace('.',',',$value->total_jarak));
        $m++;

        $sheet->setCellValue($m . '1', "TIME");
        $sheet->setCellValue($m . $no, ' ' . $value->waktu_total);
        $m++;

        $sheet->setCellValue($m . '1', "CALORIES");
        $sheet->setCellValue($m . $no, ' ' . $value->kalori);
        $m++;

        $sheet->setCellValue($m . '1', "STATUS");
        $sheet->setCellValue($m . $no, ' ' . strtoupper($value->status_finish));

        $objPHPExcel->getActiveSheet()->getStyle("A" . $no . ":A" . $no)->applyFromArray($style_center);
        $objPHPExcel->getActiveSheet()->getStyle("B" . $no . ":B" . $no)->applyFromArray($style_left);
        $objPHPExcel->getActiveSheet()->getStyle("C" . $no . ":D" . $no)->applyFromArray($style_center);
        $objPHPExcel->getActiveSheet()->getStyle("E" . $no . ":E" . $no)->applyFromArray($style_left);
        $objPHPExcel->getActiveSheet()->getStyle("F" . $no . ":F" . $no)->applyFromArray($style_center);
        $objPHPExcel->getActiveSheet()->getStyle("G" . $no . ":G" . $no)->applyFromArray($style_left);
        $objPHPExcel->getActiveSheet()->getStyle("H" . $no . ":" . $m . $no)->applyFromArray($style_center);
        $objPHPExcel->getActiveSheet()->getStyle("H1:" . $m . "1")->applyFromArray($header_center);


        $sheet->getStyle("A" . 1 . ":" . $m . $no)->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    )
                )
            )
        );

        $no++;
    }
//print_die($models);
    date_default_timezone_set("Asia/Jakarta");
    $date = date("dmY");
    $hour = date("Hi");

    // Miscellaneous glyphs, UTF-8
    // Rename sheet
    $objPHPExcel->getActiveSheet()->setTitle('Report-User-Order');
    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="User_Order_'.$date.'_'.$hour.'.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
});

// get Data before export and save it to Redis
$app->get('/t_order/pre_export', function ($request, $response) {
    ini_set('max_execution_time', 0);
    ini_set('memory_limit', '256M');

    $params = $request->getParams();
    $db = Db::db();

    $data = $db->select('
        t_order.id,
        t_order.m_event_id,
        t_order.m_event_race_id,
        t_order.m_user_address_id,
        t_order.m_user_id,
        t_order.is_join,
        t_order.is_payment,
        t_order.payment_method,
        t_order.is_paid,
        t_order.is_declined,
        t_order.is_expired,
        t_order.no_invoice,
        t_order.no_order,
        t_order.ebib_no,
        t_order.ebib_name,
        t_order.tanggal,
        t_order.track_id,
        IFNULL(t_order.is_wellness_program, 0) as is_wellness_program,
        IFNULL(t_order.lingkar_perut, 0) lingkar_perut,
        IFNULL(t_order.berat_badan, 0) as berat_badan,
        IFNULL(t_order.tinggi_badan, 0) as tinggi_badan,
        IFNULL(t_order.is_shipper,0) as is_shipper,
        IFNULL(t_order.m_user_address_id, 0) id_address,
        t_confirm_order.id id_confirm_order,
        t_confirm_order.status status_order,
        IFNULL(t_confirm_order.is_midtrans, 0) is_midtrans,
        m_user.id as m_user_id,
        m_user.nama,
        m_user.no_hp,
        m_user.email,
        m_user.kecamatan_id,
        m_user.desa_id,
        m_user.kode_pos,
        m_user.alamat,
        m_user.jenis_kelamin,
        m_club.nama organisasi,
        wilayah_provinsi.nama provinsi,
        wilayah_kabupaten.nama kabupaten,
        wilayah_kecamatan.nama kecamatan,
        m_event.nama as nama_event,
        m_event_race.jarak jarak_race,
        IFNULL(m_event_race.is_terjauh,0) is_terjauh,
        m_race_kategori.nama race_kategori,
        IFNULL(SUM(m_event_data.calories), 0) as kalori,
        IFNULL(SUM(m_event_data.jarak), 0) as total_jarak,
        IFNULL( SEC_TO_TIME(SUM(TIME_TO_SEC(m_event_data.waktu))), 0) as waktu_total,
        m_user_address.country_id,
        m_user_address.province_id,
        m_user_address.regency_id,
        m_user_address.district_id,
        m_user_address.subdistrict_id,
        m_user_address.postcode,
        m_user_address.address,
        m_user_address.pic,
        m_user_address.phone,
        m_user_address.is_default,
        m_user_address.ket,
        provinsi_user.nama provinsi_user,
        kabupaten_user.nama kabupaten_user,
        kecamatan_user.nama kecamatan_user
    ')
        ->from('t_order')
        ->leftJoin("t_confirm_order", "t_confirm_order.t_order_id = t_order.id")
        ->leftJoin("m_event_race", "m_event_race.id = t_order.m_event_race_id")
        ->leftJoin("m_race_kategori", "m_race_kategori.id = m_event_race.m_race_kategori_id")
        ->leftJoin("m_event", "m_event.id = t_order.m_event_id")
        ->leftJoin("m_user", "m_user.id = t_order.m_user_id")
        ->leftJoin("m_user_address", "m_user_address.id = t_order.m_user_address_id")
        ->leftJoin("wilayah_provinsi", "wilayah_provinsi.id = m_user_address.province_id")
        ->leftJoin("wilayah_kabupaten", "wilayah_kabupaten.id = m_user_address.regency_id")
        ->leftJoin("wilayah_kecamatan", "wilayah_kecamatan.id = m_user_address.district_id")
        ->leftJoin("wilayah_provinsi provinsi_user", "provinsi_user.id = m_user.provinsi_id")
        ->leftJoin("wilayah_kabupaten kabupaten_user", "kabupaten_user.id = m_user.kabupaten_id")
        ->leftJoin("wilayah_kecamatan kecamatan_user", "kecamatan_user.id = m_user.kecamatan_id")
        ->leftJoin("m_club_member", "m_user.id = m_club_member.m_user_id")
        ->leftJoin("m_club", "m_club.id = m_club_member.m_club_id")
        ->leftJoin('m_event_data', 'm_event_data.m_event_id = t_order.m_event_id AND m_event_data.m_user_id = t_order.m_user_id AND m_event_data.status = "approve"')
        ->where("t_order.is_deleted", "=", 0)
        ->andwhere("m_event.is_deleted", "=", 0);
//        ->andwhere("m_user.is_deleted", "=", 0);

    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        $periode_mulai = '';
        $periode_selesai = '';
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $data->where('m_user.nama', "LIKE", $val);
            } elseif ($key == "order_no") {
                $data->where('t_order.no_order', "LIKE", $val);

            } elseif ($key == "event") {
                if (!empty($val)) {
                    $data->where('t_order.m_event_id', "=", $val);
                }
            } elseif ($key == "no_invoice") {
                $data->where('t_order.no_invoice', "LIKE", $val);
            } elseif ($key == "club") {
                $data->where('m_club.nama', "LIKE", $val);
            }
            // elseif ($key == "tgl") {
            //     $val = date('Y-m-d', strtotime($val));
            //     $data->where("DATE(t_order.tanggal)", "=", $val);
            // }
            else if ($key == 'status' && $val == 'expired') {
                $data->customwhere("t_order.is_expired = 1", "AND");
            } else if ($key == 'status' && $val == 'declined') {
                $data->customwhere("t_order.is_declined = 1", "AND");
            } else if ($key == 'status' && $val == 'waitpay') {
                $data->customwhere("t_confirm_order.status = 'pending' AND t_order.is_join = 0 AND t_order.is_payment = 0", "AND");
            } else if ($key == 'status' && $val == 'waitcon') {
                $data->customwhere("t_confirm_order.status = 'pending' AND t_order.is_join = 0 AND t_order.is_payment = 1", "AND");
            } else if ($key == 'status' && $val == 'join') {
                $data->customwhere("t_confirm_order.status = 'approved' AND t_order.is_join = 1 AND t_order.is_expired = 0 AND t_order.is_declined = 0", "AND");
            } elseif ($key == "periode_mulai") {
                $periode_mulai = $val;
            } elseif ($key == "periode_selesai") {
                $periode_selesai = $val;
            }
        }
    }

    if (isset($params['no_order']) && !empty($params['no_order'])) {
        $data->where("t_order.no_order", "=", $params['no_order']);
    }

    if (!empty($periode_mulai) && !empty($periode_selesai)) {
        $db->customWhere('DATE(t_order.tanggal) >= "' . $periode_mulai . '" AND DATE(t_order.tanggal) <= "' . $periode_selesai . '"', "AND");
    }

    if (isset($params["limit"]) && !empty($params["limit"])) {
        $data->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $data->offset($params["offset"]);
    }
    // $db->limit(1);
    $data->orderBy("FIELD(t_confirm_order.status, 'pending', 'approved', 'decline','declined', 'expire'), t_order.id DESC");
    $models = $data->groupBy("t_order.id, t_order.m_event_id")->findAll();

    $idToArr = json_decode(json_encode($models, JSON_INVALID_UTF8_IGNORE), true);
    $order_id = arrayToString($idToArr, 'id');
//    echo json_encode($order_id,JSON_INVALID_UTF8_IGNORE);exit();
    if (!empty($models)) {
        /**
         * addon
         */

        $addon = $db->select('t_order_addon.*, m_event_addon.harga, m_event_addon.nama nama_produk, m_event_addon.is_donasi')
            ->from('t_order_addon')
            ->leftJoin('m_event_addon', 't_order_addon.m_event_addon_id = m_event_addon.id')
            ->customWhere('t_order_addon.t_order_id IN (' . $order_id . ')')
            ->orderBy("m_event_addon.is_donasi ASC")
            ->findAll();

        $arrAddon = [];
        $panjang = 0;
        $a = [];
        if (!empty($addon)) {
            foreach ($addon as $key => $value) {
                $arrAddon[$value->t_order_id][] = $value;
                if ($value->is_donasi != 1) {
                    $a[$value->t_order_id][] = $value;
                }
                if (isset($a[$value->t_order_id]) && $panjang < count($a[$value->t_order_id])) {
                    $panjang = count($a[$value->t_order_id]);
                }
            }
        }

        /**
         * entitlement
         */

        $entitlement = $db->select('t_order_entitlement.*, m_event_entitlement.nama nama_produk, m_event_entitlement_det.variant, 1 as qty')
            ->from('t_order_entitlement')
            ->leftJoin('m_event_entitlement_det', 't_order_entitlement.m_event_entitlement_det_id = m_event_entitlement_det.id')
            ->leftJoin('m_event_entitlement', 'm_event_entitlement_det.m_event_entitlement_id = m_event_entitlement.id')
            ->customWhere('t_order_entitlement.t_order_id IN (' . $order_id . ')')
            ->findAll();

        $arrEntitlement = [];
        $panjangEntitlement = 0;
        $b = [];
        if (!empty($entitlement)) {
            foreach ($entitlement as $key => $value) {
                $arrEntitlement[$value->t_order_id][] = $value;
                if (isset($arrEntitlement[$value->t_order_id]) && $panjangEntitlement < count($arrEntitlement[$value->t_order_id])) {
                    $panjangEntitlement = count($arrEntitlement[$value->t_order_id]);
                }
            }
        }


        foreach ($models as $key => $value) {
//            if (isset($value->m_user_address_id) && !empty($value->m_user_address_id)) {
//                $desa = getDesa($value->district_id, $value->subdistrict_id);
//                $value->alamat = isset($value->address) ? preg_replace('/[^\p{L}\p{N}\s]/u', '', $value->address) : '';
//            } else {
//                $desa = getDesa($value->kecamatan_id, $value->desa_id);
//                $value->alamat = isset($value->alamat) ? preg_replace('/[^\p{L}\p{N}\s]/u', '', $value->alamat) : '';
//            }
//            $value->desa = isset($desa['nama']) ? $desa['nama'] : null;
//            $alamat = getAlamat($value->m_user_id);

            /**
             * masukkan addon
             */

            $value->addonTemp = isset($arrAddon[$value->id]) ? $arrAddon[$value->id] : [];

            if (!empty($value->addonTemp)) {
                foreach ($value->addonTemp as $keys => $vals) {
                    if ($vals->is_donasi == 1) {
                        if (!isset($value->donasi)) {
                            $value->donasi = $vals->qty * $vals->harga;
                        } else {
                            $value->donasi += $vals->qty * $vals->harga;
                        }


                        if (!isset($value->addon)) {
                            $value->addon[] = '';
                        }
                    } else {
                        if (!isset($value->donasi)) {
                            $value->donasi = null;
                        }
                        $value->addon[] = !empty($vals->nama_produk) ? $vals->nama_produk : 'Medali Bulungan Run 2021 : variant Medal (click here)' . ' (' . $vals->qty . ')';
                    }
                }
            } else {
                $value->donasi = null;
                $value->addon[] = '';
            }
            $value->total_addon = count($value->addon);

            /**
             * masukkan entitlement
             */

            $value->entitlementTemp = isset($arrEntitlement[$value->id]) ? $arrEntitlement[$value->id] : [];

            if (!empty($value->entitlementTemp)) {
                foreach ($value->entitlementTemp as $keys => $vals) {
                    $value->entitlement[] = $vals->nama_produk . ' : variant ' . $vals->variant . ' (' . $vals->qty . ')';
                }
            } else {
                $value->entitlement[] = '';
            }
            $value->total_addon = count($value->addon);
            $value->total_entitlement = count($value->entitlement);

            $arr = getMethodBayarCustom();
            foreach ($arr as $k => $v) {
                if ($value->payment_method == $v['tipe']) {
                    $value->payment_method = $v['nama'];
                    break;
                }
            }
            if ($value->payment_method == 'free_addon') {
                $value->payment_method = ucwords(str_replace("_", " ", $value->payment_method));
            }


            if ($value->is_paid == 1) {
                if (empty($value->is_payment) && empty($value->is_join)) {
                    $value->status_pesan = 'Waiting for Payment';
                    if (empty($value->status_order)) {
                        $value->status_order = 'pending';
                    }
                } elseif ($value->is_payment == 1 && empty($value->is_join)) {
                    $value->status_pesan = 'Waiting for Confirmation';
                    if (empty($value->status_order)) {
                        $value->status_order = 'pending';
                    }
                } elseif ($value->is_payment == 1 && $value->is_join == 1) {
                    $value->status_pesan = 'Success Join';
                } elseif (empty($value->id_confirm_order)) {
                    $value->status_pesan = 'Waiting for Payment';
                    if (empty($value->status_order)) {
                        $value->status_order = 'pending';
                    }
                }

                if ($value->is_declined == 1) {
                    $value->status_pesan = 'Declined';
                }

                if ($value->is_expired == 1) {
                    $value->status_order = 'expire';
                    $value->status_pesan = 'Expired';
                }

                if (!isset($value->status_pesan) && empty($value->status_pesan)) {
                    $value->status_pesan = null;
                }
            } else {
                if ($value->status_order == 'approved') {
                    $value->status_pesan = 'Success Join';
                } elseif ($value->is_declined == 1) {
                    $value->status_pesan = 'Declined';
                } else {
                    $value->status_pesan = 'Pending';
                }

                if (!isset($value->status_pesan) && empty($value->status_pesan)) {
                    $value->status_pesan = null;
                }
            }


            if ($value->is_terjauh == 1 && !empty($value->total_jarak)) {
                $value->status_finish = 'Finished';
            } elseif (empty($value->is_terjauh) && $value->total_jarak >= $value->jarak_race) {
                $value->status_finish = 'Finished';
            } else {
                $value->status_finish = 'Not Finish';
            }
        }
    }

    foreach ($models as $key => $value) {
        for ($i = 0; count($value->addon) < $panjang; $i++) {
            $value->addon[] = '';
        }

        for ($i = 0; count($value->entitlement) < $panjangEntitlement; $i++) {
            $value->entitlement[] = '';
        }
    }

//    print_die($models);
    try{
        $redist = redisConnect();
        $redist->set('export', json_encode($models));
        return successResponse($response, ['Get data sukses']);
    } catch (Exception $e) {
        return unprocessResponse($response, $e);
    }
});


/**
 * export excel
 */

$app->get('/t_order/exportAll', function ($request, $response) {
    $redist = redisConnect();
    $newModels = $redist->get('export');
    ini_set('max_execution_time', 0);

    $models = json_decode($newModels);
//print_die($models);
    if(!isset($models) && empty($models)){
        return unprocessResponse($response, ['Tidak ada data ditemukan pada local storage']);
    }

//    print_die($models);
    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();
    // Set properties
    $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
        ->setLastModifiedBy("Maarten Balliauw")
        ->setTitle("Office 2007 XLSX Test Document")
        ->setSubject("Office 2007 XLSX Test Document")
        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("Test result file");
    $sheet = $objPHPExcel->getActiveSheet();

    $style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        ),
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            )
        )
    );

    $header_left = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
        ),
        'font' => array(
            'bold' => true,
        )
    );

    $header_center = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        ),
        'font' => array(
            'bold' => true,
        )
    );

    $style_left = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
        ),
    );

    $style_center = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        ),
    );
    // Add some data

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', "NO")
        ->setCellValue('B1', "CREATED AT")
        ->setCellValue('C1', "EVENT")
        ->setCellValue('D1', "ORDER NO")
        ->setCellValue('E1', "NAME")
        ->setCellValue('F1', "GENDER")
        ->setCellValue('G1', "ADDRESS")
        ->setCellValue('H1', "SUBDISTRICT")
        ->setCellValue('I1', "DISTRICT")
        ->setCellValue('J1', "REGENCY")
        ->setCellValue('K1', "PROVINCE")
        ->setCellValue('L1', "POSTCODE")
        ->setCellValue('M1', "NO. HANDPHONE")
        ->setCellValue('N1', "EMAIL")
        ->setCellValue('O1', "COMMUNITY");


//    $objPHPExcel->getActiveSheet()->getStyle("A1:M1")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle("A1:A1")->applyFromArray($header_center);
    $objPHPExcel->getActiveSheet()->getStyle("B1:B1")->applyFromArray($header_left);
    $objPHPExcel->getActiveSheet()->getStyle("C1:D1")->applyFromArray($header_center);
    $objPHPExcel->getActiveSheet()->getStyle("E1:E1")->applyFromArray($header_left);
    $objPHPExcel->getActiveSheet()->getStyle("G1:G1")->applyFromArray($header_center);
    $objPHPExcel->getActiveSheet()->getStyle("F1:F1")->applyFromArray($header_left);


    $no = 2;
    foreach ($models as $key => $value) {
        if ($value->organisasi == "🤔") {
            $value->organisasi = "";
        }

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . $no, $key + 1)
            ->setCellValue('B' . $no, !empty($value->tanggal) ? date('d-m-Y H:i', strtotime($value->tanggal)) : null)
            ->setCellValue('C' . $no, strtoupper($value->nama_event))
            ->setCellValue('D' . $no, ' ' . $value->no_order)
            ->setCellValue('E' . $no, strtoupper(removeEmoji($value->nama)))
            ->setCellValue('F' . $no, $value->jenis_kelamin)
            ->setCellValue('G' . $no, strtoupper(removeEmoji($value->alamat)));

        if (isset($value->m_user_address_id) && !empty($value->m_user_address_id)) {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H' . $no, isset($value->desa) ? strtoupper($value->desa) : '')
                ->setCellValue('I' . $no, strtoupper($value->kecamatan))
                ->setCellValue('J' . $no, strtoupper($value->kabupaten))
                ->setCellValue('K' . $no, strtoupper($value->provinsi))
                ->setCellValue('L' . $no, strtoupper($value->postcode));
        }else{
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H' . $no, isset($value->desa) ? strtoupper($value->desa) : '')
                ->setCellValue('I' . $no, strtoupper($value->kecamatan_user))
                ->setCellValue('J' . $no, strtoupper($value->kabupaten_user))
                ->setCellValue('K' . $no, strtoupper($value->provinsi_user))
                ->setCellValue('L' . $no, $value->kode_pos);
        }

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('M' . $no, ' ' . cekNoHp($value->no_hp))
            ->setCellValue('N' . $no, strtoupper($value->email))
            ->setCellValue('O' . $no, strtoupper(removeEmoji($value->organisasi)));


        /**
         * loop entitlement
         */

        $j = 'P';
        if (!empty($value->entitlement)) {
            foreach ($value->entitlement as $k => $v) {
                $sheet->setCellValue($j . $no, strtoupper($v));
                $j++;
            }
        }

        $sheet->setCellValue('P1', "ENTITLEMENT");
        $highestColumn = $sheet->getHighestColumn(2);
        $sheet->mergeCells('P1:' . $highestColumn . '1');
        $sheet->getStyle("O" . 1 . ":" . $highestColumn . 1)->applyFromArray($style);

        /**
         * loop addon
         */

        $m = $j;
        if (!empty($value->addon)) {
            foreach ($value->addon as $k => $v) {
                $sheet->setCellValue($m . $no, strtoupper($v));
                $m++;
            }
        }

        $sheet->setCellValue($j . '1', "ADDON");
        $highestColumn = $sheet->getHighestColumn(2);
        $sheet->mergeCells($j . '1:' . $highestColumn . '1');


//        echo $m."<br/>";
        $sheet->setCellValue($m . '1', "DONASI");
        $sheet->setCellValue($m . $no, ' ' . $value->donasi);
        $m++;

        $sheet->setCellValue($m . '1', "PAYMENT METHOD");
        $sheet->setCellValue($m . $no, ' ' . strtoupper($value->payment_method));
        $m++;

        $sheet->setCellValue($m . '1', "STATUS PAYMENT");
        $sheet->setCellValue($m . $no, ' ' . strtoupper($value->status_pesan));
        $m++;

        $sheet->setCellValue($m . '1', "RACE CATEGORY");
        $sheet->setCellValue($m . $no, ' ' . strtoupper($value->race_kategori));
        $m++;

        $sheet->setCellValue($m . '1', "TRACK ID");
        $sheet->setCellValue($m . $no, ' ' . $value->track_id);
        $m++;

        $sheet->setCellValue($m . '1', "EBIB NO");
        $sheet->setCellValue($m . $no, ' ' . strtoupper($value->ebib_no));
        $m++;

        $sheet->setCellValue($m . '1', "DISTANCE");
        $sheet->setCellValue($m . $no, ' ' . str_replace('.',',',$value->total_jarak));
        $m++;

        $sheet->setCellValue($m . '1', "TIME");
        $sheet->setCellValue($m . $no, ' ' . $value->waktu_total);
        $m++;

        $sheet->setCellValue($m . '1', "CALORIES");
        $sheet->setCellValue($m . $no, ' ' . $value->kalori);
        $m++;

        $sheet->setCellValue($m . '1', "STATUS");
        $sheet->setCellValue($m . $no, ' ' . strtoupper($value->status_finish));

        $objPHPExcel->getActiveSheet()->getStyle("A" . $no . ":A" . $no)->applyFromArray($style_center);
        $objPHPExcel->getActiveSheet()->getStyle("B" . $no . ":B" . $no)->applyFromArray($style_left);
        $objPHPExcel->getActiveSheet()->getStyle("C" . $no . ":D" . $no)->applyFromArray($style_center);
        $objPHPExcel->getActiveSheet()->getStyle("E" . $no . ":E" . $no)->applyFromArray($style_left);
        $objPHPExcel->getActiveSheet()->getStyle("F" . $no . ":F" . $no)->applyFromArray($style_center);
        $objPHPExcel->getActiveSheet()->getStyle("G" . $no . ":G" . $no)->applyFromArray($style_left);
        $objPHPExcel->getActiveSheet()->getStyle("H" . $no . ":" . $m . $no)->applyFromArray($style_center);
        $objPHPExcel->getActiveSheet()->getStyle("H1:" . $m . "1")->applyFromArray($header_center);


        $sheet->getStyle("A" . 1 . ":" . $m . $no)->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    )
                )
            )
        );

        $no++;
    }
//print_die($models);
    date_default_timezone_set("Asia/Jakarta");
    $date = date("dmY");
    $hour = date("Hi");

    // Miscellaneous glyphs, UTF-8
    // Rename sheet
    $objPHPExcel->getActiveSheet()->setTitle('Report-User-Order');
    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="User_Order_'.$date.'_'.$hour.'.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
});



$app->get('/t_order/getAddon', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();

    $user_address = $db->select('m_user_address.*,
    wilayah_negara.nama as nama_negara,
    wilayah_provinsi.nama as nama_provinsi,
    wilayah_kabupaten.nama as nama_kabupaten,
    wilayah_kecamatan.nama as nama_kecamatan
    ')
        ->from('m_user_address')
        ->leftJoin("wilayah_negara", "wilayah_negara.id = m_user_address.country_id")
        ->leftJoin("wilayah_provinsi", "wilayah_provinsi.id = m_user_address.province_id")
        ->leftJoin("wilayah_kabupaten", "wilayah_kabupaten.id = m_user_address.regency_id")
        ->leftJoin("wilayah_kecamatan", "wilayah_kecamatan.id = m_user_address.district_id")
        ->where("m_user_address.id", "=", $params['m_user_address_id'])
        ->andWhere("is_default", "=", 1)
        ->find();

    if (!empty($user_address)) {
        $desa = getDesa($user_address->district_id, $user_address->subdistrict_id);
        $user_address->nama_desa = isset($desa['nama']) ? $desa['nama'] : null;
        $user_address->alamat_lengkap = $user_address->address . ', ' . $user_address->nama_desa . ', ' . $user_address->nama_kecamatan . ', ' . $user_address->nama_kabupaten . ', ' . $user_address->nama_provinsi . ', ' . ucfirst(strtolower($user_address->nama_negara));
    }

    $data = $db->select('t_order_addon.*, m_event_addon.nama nama_produk')
        ->from('t_order_addon')
        ->leftJoin('m_event_addon', 't_order_addon.m_event_addon_id = m_event_addon.id')
        ->where('t_order_addon.t_order_id', '=', $params['t_order_id'])
        ->findAll();

    if (!empty($data)) {
        foreach ($data as $key => $value) {
            $value->alamat_lengkap = $user_address->alamat_lengkap;
        }
    }

    return successResponse($response, $data);
});


$app->get('/t_order/getAddonList', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();

    $params['m_event_id'] = isset($params['m_event_id']) && !empty($params['m_event_id']) ? $params['m_event_id'] : 0;
    $data = $db->select("m_event_addon.*")
        ->from("m_event_addon")
        ->where("m_event_addon.m_event_id", "=", $params['m_event_id'])
        ->findAll();

    return successResponse($response, $data);
});

$app->post('/t_order/updateAddon', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    if (!empty($params)) {
        $temp = $db->delete("t_order_addon", ["t_order_id" => $params['t_order_id']]);
        foreach ($params['list'] as $key => $value) {
            if (isset($value['id'])) {
                unset($value['id']);
            }
            $value['t_order_id'] = $params['t_order_id'];
            $db->insert("t_order_addon", $value);
        }
    }

    return successResponse($response, $params);
});

$app->post('/t_order/updateAddress', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();

    if (!empty($params)) {
        $temp = $db->update("t_order", ["m_user_address_id" => $params['m_user_address_id']], ["id" => $params['id']]);
    }

    return successResponse($response, $params);
});


/**
 * cek export
 */

$app->get('/t_order/exportModal', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
//    print_die($params);

    $limit = 500;
    $offset = 0;
    $arr = [];
    while ($offset >= 0) {
        $data = $db->select('
        t_order.id,
        t_order.m_event_id,
        t_order.m_event_race_id,
        t_order.m_user_address_id,
        t_order.m_user_id,
        t_order.is_join,
        t_order.is_payment,
        t_order.payment_method,
        t_order.is_paid,
        t_order.is_declined,
        t_order.is_expired,
        t_order.no_invoice,
        t_order.no_order,
        t_order.ebib_no,
        t_order.ebib_name,
        t_order.tanggal,
        t_order.track_id,
        IFNULL(t_order.is_wellness_program, 0) as is_wellness_program,
        IFNULL(t_order.lingkar_perut, 0) lingkar_perut,
        IFNULL(t_order.berat_badan, 0) as berat_badan,
        IFNULL(t_order.tinggi_badan, 0) as tinggi_badan,
        IFNULL(t_order.is_shipper,0) as is_shipper,
        IFNULL(t_order.m_user_address_id, 0) id_address,
        t_confirm_order.id id_confirm_order,
        t_confirm_order.status status_order,
        IFNULL(t_confirm_order.is_midtrans, 0) is_midtrans,
        m_user.id as m_user_id,
        m_user.nama,
        m_user.no_hp,
        m_user.email,
        m_user.kecamatan_id,
        m_user.desa_id,
        m_user.kode_pos,
        m_club.nama organisasi,
        wilayah_provinsi.nama provinsi,
        wilayah_kabupaten.nama kabupaten,
        wilayah_kecamatan.nama kecamatan,
        m_event.nama as nama_event,
        m_event_race.jarak jarak_race,
        IFNULL(m_event_race.is_terjauh,0) is_terjauh,
        m_race_kategori.nama race_kategori,
        IFNULL(SUM(m_event_data.calories), 0) as kalori,
        IFNULL(SUM(m_event_data.jarak), 0) as total_jarak,
        IFNULL( SEC_TO_TIME(SUM(TIME_TO_SEC(m_event_data.waktu))), 0) as waktu_total
    ')
            ->from('t_order')
            ->leftJoin("t_confirm_order", "t_confirm_order.t_order_id = t_order.id")
            ->leftJoin("m_event_race", "m_event_race.id = t_order.m_event_race_id")
            ->leftJoin("m_race_kategori", "m_race_kategori.id = m_event_race.m_race_kategori_id")
            ->leftJoin("m_event", "m_event.id = t_order.m_event_id")
            ->leftJoin("m_user", "m_user.id = t_order.m_user_id")
            ->leftJoin("wilayah_provinsi", "wilayah_provinsi.id = m_user.provinsi_id")
            ->leftJoin("wilayah_kabupaten", "wilayah_kabupaten.id = m_user.kabupaten_id")
            ->leftJoin("wilayah_kecamatan", "wilayah_kecamatan.id = m_user.kecamatan_id")
            ->leftJoin("m_club_member", "m_user.id = m_club_member.m_user_id")
            ->leftJoin("m_club", "m_club.id = m_club_member.m_club_id")
            ->leftJoin('m_event_data', 'm_event_data.m_event_id = t_order.m_event_id AND m_event_data.m_user_id = t_order.m_user_id AND m_event_data.status = "approve"')
            ->where("t_order.is_deleted", "=", 0)
            ->andwhere("m_event.is_deleted", "=", 0);
//            ->andwhere("m_user.is_deleted", "=", 0);

        if (isset($params["filter"])) {
            $filter = (array)json_decode($params["filter"]);
            $periode_mulai = '';
            $periode_selesai = '';
//            print_die($filter);
            foreach ($filter as $key => $val) {
                if ($key == "nama") {
                    $data->where('m_user.nama', "LIKE", $val);
                } elseif ($key == "order_no") {
                    $data->where('t_order.no_order', "LIKE", $val);

                } elseif ($key == "event") {
                    if (!empty($val)) {
                        $data->where('t_order.m_event_id', "=", $val);
                    }
                } elseif ($key == "no_invoice") {
                    $data->where('t_order.no_invoice', "LIKE", $val);
                } elseif ($key == "club") {
                    $data->where('m_club.nama', "LIKE", $val);
                }
                // elseif ($key == "tgl") {
                //     $val = date('Y-m-d', strtotime($val));
                //     $data->where("DATE(t_order.tanggal)", "=", $val);
                // }
                else if ($key == 'status' && $val == 'expired') {
                    $data->customwhere("t_order.is_expired = 1", "AND");
                } else if ($key == 'status' && $val == 'declined') {
                    $data->customwhere("t_order.is_declined = 1", "AND");
                } else if ($key == 'status' && $val == 'waitpay') {
                    $data->customwhere("t_confirm_order.status = 'pending' AND t_order.is_join = 0 AND t_order.is_payment = 0", "AND");
                } else if ($key == 'status' && $val == 'waitcon') {
                    $data->customwhere("t_confirm_order.status = 'pending' AND t_order.is_join = 0 AND t_order.is_payment = 1", "AND");
                } else if ($key == 'status' && $val == 'join') {
                    $data->customwhere("t_confirm_order.status = 'approved' AND t_order.is_join = 1 AND t_order.is_expired = 0 AND t_order.is_declined = 0", "AND");
                } elseif ($key == "periode_mulai") {
                    $periode_mulai = $val;
                } elseif ($key == "periode_selesai") {
                    $periode_selesai = $val;
                }
            }
        }

        if (isset($params['no_order']) && !empty($params['no_order'])) {
            $data->where("t_order.no_order", "=", $params['no_order']);
        }

        if (!empty($periode_mulai) && !empty($periode_selesai)) {
            $db->customWhere('DATE(t_order.tanggal) >= "' . $periode_mulai . '" AND DATE(t_order.tanggal) <= "' . $periode_selesai . '"', "AND");
        }

        if (isset($limit) && !empty($limit)) {
            $data->limit($limit);
        }
        if (isset($offset) && !empty($offset)) {
            $data->offset($offset);
        }
        // $db->limit(1);
        $data->orderBy("FIELD(t_confirm_order.status, 'pending', 'approved', 'decline','declined', 'expire'), t_order.id DESC");
        $models = $data->groupBy("t_order.id, t_order.m_event_id")->findAll();

        if (!empty($models)) {
            $arr[] = [
                "label" => $offset . ' - ' . ($offset + 500),
                "offset" => $offset,
                "limit" => 500
            ];
        } else {
            break;
        }

        $offset += 500;
    }

    return successResponse($response, $arr);
});

$app->get('/t_order/user_address', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();

    $data = $db->select("id, address")
        ->from("m_user_address")
        ->where("m_user_id", "=", $params['m_user_id'])
        ->findAll();

    return successResponse($response, $data);
});

$app->get('/t_order/test', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $redist = redisConnect();
    $newModels = $redist->get('export');
    ini_set('max_execution_time', 0);
    ini_set('memory_limit', '256M');

    $models = json_decode($newModels);

       $view = $this->view->fetch('laporan/laporan-user-order.html', [
                "model" => $models
            ]);
            header("Content-type: application/vnd.ms-excel");
            header("Content-Disposition: attachment;Filename=laporan-iter.xls");
            echo $view;
});
?>