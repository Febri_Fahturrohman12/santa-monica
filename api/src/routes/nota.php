<?php

use Model\Kasir;
use Service\Db;

$app->get('/nota/print', function ($request, $response) {
    $data = $request->getParams();
    $id = $data['id'];
    $db = Db::db();
    $layanan = new Kasir();

    // GET TRANSAKSI
    $models = $db->select('*')->from('t_penjualan')->where('id', '=', $id)->find();

    // GET DETAIL LAYANAN
    $orderTransaksi = $layanan->getTransaksiRecord($id);
    $orderLayanan = $layanan->getLayananRecord($id);
    $orderBarang = $layanan->getBarangRecord($id);

    $perusahaan = $db->select('*')->from('m_setting')->find();
    $arr = [];
    $arr['transaksi'] = $orderTransaksi['data'];
    $arr['layanan'] = $orderLayanan['data'];
    $arr['barang'] = $orderBarang['data'];
    $arr['logo'] = $perusahaan->logo_menu;
    
    // KALKULASI
    $subtotallayanan = 0;
    foreach ($arr['layanan'] as $key => $value) {
        $subtotallayanan += $value->subtotal;
        $arr['subtotal_layanan'] = $subtotallayanan;
    }

    
    $subtotalbarang = 0;
    $arr['subtotal_barang'] = 0;
    foreach ($arr['barang'] as $key => $value) {
        $subtotalbarang += $value->subtotal;
        $arr['subtotal_barang'] = $subtotalbarang;
    }
    $arr['subtotal_servicelayanan'] = (Int) $arr['transaksi']->service_layanan;
    $arr['subtotal_diskon'] = (Int) $arr['transaksi']->diskon;
    $arr['total_amount'] = $arr['subtotal_layanan'] + $arr['subtotal_barang'];
    $arr['total_all'] = ($arr['total_amount'] + $arr['subtotal_servicelayanan']) - $arr['subtotal_diskon'];

    // Tipe 1 download, 2 Whatsapp, 3 Cetak Struk

    $arr['urlDownload'] = site_api_url().'nota/printPDF?model%5Bid%5D='.$arr['transaksi']->id.'&tipe=1';
    $arr['urlWhatsApp'] = site_api_url().'nota/sendWA?model%5Bid%5D='.$arr['transaksi']->id.'&tipe=2';
    $arr['urlCetak'] = site_api_url().'nota/cetakStruk?model%5Bid%5D='.$arr['transaksi']->id.'&tipe=3';
    
    $view = twigView();
    $content = $view->fetch('print_nota.html', [
        "data" => $arr
    ]);
    
    return $content;
});


$app->get('/nota/printPDF', function ($request, $response) {
    $data = $request->getParams();
    $id = $data['model']['id'];
    $db = Db::db();
    $layanan = new Kasir();
    // GET TRANSAKSI
    $models = $db->select('*')->from('t_penjualan')->where('id', '=', $id)->find();

    // GET DETAIL LAYANAN
    $orderTransaksi = $layanan->getTransaksiRecord($id);
    $orderLayanan = $layanan->getLayananRecord($id);
    $orderBarang = $layanan->getBarangRecord($id);

    $arr = [];
    $arr['transaksi'] = $orderTransaksi['data'];
    $arr['layanan'] = $orderLayanan['data'];
    $arr['barang'] = $orderBarang['data'];

    
    // KALKULASI
    $subtotallayanan = 0;
    foreach ($arr['layanan'] as $key => $value) {
        $subtotallayanan += $value->subtotal;
        $arr['subtotal_layanan'] = $subtotallayanan;
    }

    
    $subtotalbarang = 0;
    $arr['subtotal_barang'] = 0;
    foreach ($arr['barang'] as $key => $value) {
        $subtotalbarang += $value->subtotal;
        $arr['subtotal_barang'] = $subtotalbarang;
    }
    $arr['subtotal_servicelayanan'] = (Int) $arr['transaksi']->service_layanan;
    $arr['subtotal_diskon'] = (Int) $arr['transaksi']->diskon;
    $arr['total_amount'] = $arr['subtotal_layanan'] + $arr['subtotal_barang'];
    $arr['total_all'] = ($arr['total_amount'] + $arr['subtotal_servicelayanan']) - $arr['subtotal_diskon'];

    $arr['urlDownload'] = site_api_url().'nota/printPDF?model%5Bid%5D='.$arr['transaksi']->id.'&tipe=1';
    $arr['urlWhatsApp'] = site_api_url().'nota/sendWA?model%5Bid%5D='.$arr['transaksi']->id.'&tipe=2';
    $arr['urlCetak'] = site_api_url().'nota/cetakStruk?model%5Bid%5D='.$arr['transaksi']->id.'&tipe=3';
    
    $view = twigView();
    $content = $view->fetch('pdf_nota.html', [
        "data" => $arr
    ]);
    
    //DOMPDF
    /** set options */
    $options = new Dompdf\Options();
    $options->setIsRemoteEnabled(true);

    $contxt = stream_context_create([
        'ssl' => [
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true,
        ],
    ]);

    $filename = 'INVOICE'.$arr['transaksi']->kode_nota.'.pdf';
    $url = 'assets/filepdf/'.$filename;
    $link = config('SITE_API_URL').$url;
    /** set dompdf */
    $dompdf = new Dompdf\Dompdf();
    $dompdf->setOptions($options);
    $dompdf->setHttpContext($contxt);
    $dompdf->loadHtml($content);
    $dompdf->setPaper('A5', 'potrait');
    $dompdf->render();
    $output = $dompdf->output();
    
    $dompdf->stream($filename, ['Attachment' => false]);
    exit;
    return $dompdf;
});

$app->get('/nota/sendWA', function ($request, $response) {
    $data = $request->getParams();
    $id = $data['model']['id'];
    $db = Db::db();
    $layanan = new Kasir();
    
    // GET TRANSAKSI
    $models = $db->select('*')->from('t_penjualan')->where('id', '=', $id)->find();

    // GET DETAIL LAYANAN
    $orderTransaksi = $layanan->getTransaksiRecord($id);
    $orderLayanan = $layanan->getLayananRecord($id);
    $orderBarang = $layanan->getBarangRecord($id);

    $arr = [];
    $arr['transaksi'] = $orderTransaksi['data'];
    $arr['layanan'] = $orderLayanan['data'];
    $arr['barang'] = $orderBarang['data'];

    
    // KALKULASI
    $subtotallayanan = 0;
    foreach ($arr['layanan'] as $key => $value) {
        $subtotallayanan += $value->subtotal;
        $arr['subtotal_layanan'] = $subtotallayanan;
    }

    
    $subtotalbarang = 0;
    $arr['subtotal_barang'] = 0;
    foreach ($arr['barang'] as $key => $value) {
        $subtotalbarang += $value->subtotal;
        $arr['subtotal_barang'] = $subtotalbarang;
    }
    $arr['subtotal_servicelayanan'] = (Int) $arr['transaksi']->service_layanan;
    $arr['subtotal_diskon'] = (Int) $arr['transaksi']->diskon;
    $arr['total_amount'] = $arr['subtotal_layanan'] + $arr['subtotal_barang'];
    $arr['total_all'] = ($arr['total_amount'] + $arr['subtotal_servicelayanan']) - $arr['subtotal_diskon'];

    $arr['urlDownload'] = site_api_url().'nota/printPDF?model%5Bid%5D='.$arr['transaksi']->id.'&tipe=1';
    $arr['urlWhatsApp'] = site_api_url().'nota/sendWA?model%5Bid%5D='.$arr['transaksi']->id.'&tipe=2';
    $arr['urlCetak'] = site_api_url().'nota/cetakStruk?model%5Bid%5D='.$arr['transaksi']->id.'&tipe=3';
    
    $datamember = $db->select('*')->from('m_customer')->where('id', '=', $models->customer_id)->find();
    // $nomorwa = (Int) safeTelepon($datamember->no_telepon);
    $nomorwa = $datamember->no_telepon;
    $view = twigView();
    $content = $view->fetch('pdf_nota.html', [
        "data" => $arr
    ]);
    
    //DOMPDF
    /** set options */
    $options = new Dompdf\Options();
    $options->setIsRemoteEnabled(true);

    $contxt = stream_context_create([
        'ssl' => [
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true,
        ],
    ]);

    $filename = 'INVOICE'.$arr['transaksi']->kode_nota.'.pdf';
    $url = 'assets/filepdf/'.$filename;
    $link = config('SITE_API_URL').$url;
    /** set dompdf */
    $dompdf = new Dompdf\Dompdf();
    $dompdf->setOptions($options);
    $dompdf->setHttpContext($contxt);
    $dompdf->loadHtml($content);
    $dompdf->setPaper('A5', 'potrait');
    $dompdf->render();
    $output = $dompdf->output();
    
    // CEK FILE SUDAH ADA ATAU BELUM
    if($models->nama_file == null){
        // Buat Folder baru
        $filePath = 'assets/filepdf';
        if (!file_exists($filePath)) {
            mkdir($filePath, 0777);
        }

        file_put_contents($url, $output);
        if(file_put_contents($url, $output)){
            $updatenamefile = $db->update('t_penjualan', ['nama_file'=>$link], ['id' => $models->id]);
            if($updatenamefile){
                // $linkwhatsapp = "https://api.whatsapp.com/send?phone=".$nomorwa."&text= Assalamualaikum Warohmatullah Wabarokatu ".$datamember->nama." berikut kami lampirkan E-Invoice pelayanan anda di Santa Monica 17th. Terimakasih atas kepercayaan anda memilih layanan kami %0a%0a". $updatenamefile->nama_file;
                $linkwhatsapp = "https://api.whatsapp.com/send?phone=".$nomorwa."&text= Assalamualaikum Warohmatullah Wabarokatuh ".$datamember->nama." berikut kami lampirkan E Invoice pelayanan Santa Monica 17th. Terima Kasih atas kepercayaan anda memilih layanan kami %0a%0a". $updatenamefile->nama_file;
                
                echo "<script>window.open('$linkwhatsapp');</script>";
            }
        }
    }else {
        $linkwhatsapp = "https://api.whatsapp.com/send?phone=".$nomorwa."&text= Assalamualaikum Warohmatullah Wabarokatuh ".$datamember->nama." berikut kami lampirkan E Invoice pelayanan Santa Monica 17th. Terima Kasih atas kepercayaan anda memilih layanan kami %0a%0a". $models->nama_file;
        echo "<script>window.open('$linkwhatsapp');</script>";
    
        return true;
    }
});

$app->get('/nota/cetakStruk', function ($request, $response) {
    $data = $request->getParams();
    $id = $data['model']['id'];
    $db = Db::db();
    $layanan = new Kasir();
    // GET TRANSAKSI
    $models = $db->select('*')->from('t_penjualan')->where('id', '=', $id)->find();

    // GET DETAIL LAYANAN
    $orderTransaksi = $layanan->getTransaksiRecord($id);
    $orderLayanan = $layanan->getLayananRecord($id);
    $orderBarang = $layanan->getBarangRecord($id);

    $arr = [];
    $arr['transaksi'] = $orderTransaksi['data'];
    $arr['layanan'] = $orderLayanan['data'];
    $arr['barang'] = $orderBarang['data'];

    
    // KALKULASI
    $subtotallayanan = 0;
    foreach ($arr['layanan'] as $key => $value) {
        $subtotallayanan += $value->subtotal;
        $arr['subtotal_layanan'] = $subtotallayanan;
    }

    
    $subtotalbarang = 0;
    $arr['subtotal_barang'] = 0;
    foreach ($arr['barang'] as $key => $value) {
        $subtotalbarang += $value->subtotal;
        $arr['subtotal_barang'] = $subtotalbarang;
    }
    $arr['subtotal_servicelayanan'] = (Int) $arr['transaksi']->service_layanan;
    $arr['subtotal_diskon'] = (Int) $arr['transaksi']->diskon;
    $arr['total_amount'] = $arr['subtotal_layanan'] + $arr['subtotal_barang'];
    $arr['total_all'] = ($arr['total_amount'] + $arr['subtotal_servicelayanan']) - $arr['subtotal_diskon'];

    $arr['urlDownload'] = site_api_url().'nota/printPDF?model%5Bid%5D='.$arr['transaksi']->id.'&tipe=1';
    $arr['urlWhatsApp'] = site_api_url().'nota/sendWA?model%5Bid%5D='.$arr['transaksi']->id.'&tipe=2';
    $arr['urlCetak'] = site_api_url().'nota/cetakStruk?model%5Bid%5D='.$arr['transaksi']->id.'&tipe=3';
    
    $view = twigView();
    $content = $view->fetch('print_struk.html', [
        "data" => $arr
    ]);
    echo $content;
    die;
    
    // //DOMPDF
    // /** set options */
    // $options = new Dompdf\Options();
    // $options->setIsRemoteEnabled(true);

    // $contxt = stream_context_create([
    //     'ssl' => [
    //         'verify_peer' => false,
    //         'verify_peer_name' => false,
    //         'allow_self_signed' => true,
    //     ],
    // ]);

    // $filename = 'INVOICE'.$arr['transaksi']->kode_nota.'.pdf';
    // $url = 'assets/filepdf/'.$filename;
    // $link = config('SITE_API_URL').$url;
    // /** set dompdf */
    // $dompdf = new Dompdf\Dompdf();
    // $dompdf->setOptions($options);
    // $dompdf->setHttpContext($contxt);
    // $dompdf->loadHtml($content);
    // $dompdf->setPaper('A5', 'potrait');
    // $dompdf->render();
    // $output = $dompdf->output();
    
    // $dompdf->stream($filename, ['Attachment' => false]);
    // exit;
    // return $dompdf;
});

?>
