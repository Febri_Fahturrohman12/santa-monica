<?php

use Service\Db;
use Model\Customer;
use Service\Landa;

$app->get('/m_customer/index', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $customer = new Customer();

    $getData = $customer->getAll($params);

    return successResponse($response, [
        'list' => $getData['data'],
        'totalItems' => $getData['totalItem']
    ]);
});

$app->post("/m_customer/save", function ($request, $response) {
    $params = $request->getParams();
    $landa = new Landa();
    $customer = new Customer();

    $validasi = $customer->validasi($params);

    if (true === $validasi) {

        $save = $customer->save($params);

        if ($save['status']) {
            return successResponse($response, $save['data']);
        }

        return unprocessResponse($response, [$save['error']]);
    }

    return unprocessResponse($response, $validasi);

});

$app->post("/m_customer/delete", function ($request, $response) {
    $params = $request->getParams();
    $customer = new Customer();

    $model = $customer->delete($params);

    if ($model['status']) {
        return successResponse($response, $model['data']);
    }

    return unprocessResponse($response, [$model['error']]);
});

$app->post("/m_customer/restore", function ($request, $response) {
    $params = $request->getParams();
    $customer = new Customer();

    $model = $customer->restore($params);

    if ($model['status']) {
        return successResponse($response, $model['data']);
    }

    return unprocessResponse($response, [$model['error']]);
});
?>