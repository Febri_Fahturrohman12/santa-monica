<?php

use Model\ApprovalLine;
use Model\Formula;
use Model\HakAkses;
use Model\Karyawan;
use Model\LaporanJadwal;
use Model\OnBoard;
use Model\Perusahaan;
use Model\RiwayatKehadiran;
use Model\Pegawai;
use Service\Db;
use Service\Firebase;
use Service\Landa;
use Google\Cloud\Storage\StorageClient;


$app->get('/tesnotif', function ($request, $response) {
    $params = $request->getParams();
    $approveLine = new ApprovalLine();
    $approveLine->generateDefault($params['id']);
});

$app->get('/', function ($request, $response) {
    $firebase = new Firebase();
//    $formula = new Formula();
    $karyawan = [
        'm_level_jabatan_id' => 2,
        'm_jabatan_id' => 2,
        'm_time_off_id' => 0,
        'm_organisasi_id' => 1,
        'jenkel' => 1,
        'status_nikah' => 1,
        'tipe_karyawan' => 3,
        'karyawan_id' => 1,
    ];
//    $listFormula = $formula->getFormula(['id' => 1]);
//    $value = $formula->getValue($karyawan, $listFormula['data']);
//    echo $value;
//    $firebase = new Firebase();
//    $firebase->sendNotif('all', 'HAI', 'TES');
});

$app->get('/site/getListPernyataan', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    // print_r($params['filter']);die;
    $data = $db->select('*')
        ->from('m_daftar_pernyataan_petani');


    // if($params['m_tambak_id'])

    $models = $data->findAll();
    $tipe = [];
    foreach ($models as $key => $value) {
        $value->is_setuju = false;
        $tipe[$value->tipe][] = (array)$value;


    }

    return successResponse($response, $tipe);
});

// Ambil set sessions
$app->post('/site/setSessions', function ($request, $response) {
    $params = $request->getParams();
    // if (isset($params['accessToken'])) {
    //     error_reporting(0);
    //     $firebase = new Firebase();
    $landa = new Landa();
    $db = Db::db();


    // Ambil data user dari
    // print_r($params);die;
    if (isset($params['email']) && !empty($params['email'])) {
        $data = $db->select("m_user.*,"
            . "m_roles.akses"
            . "")
            ->from('m_user')
            ->leftJoin("m_roles", "m_roles.id = m_user.m_roles_id")
            ->CustomWhere('m_user.email ="' . $params['email'] . '" OR m_user.username ="' . $params['email'] . '"')
            ->customWhere('m_user.password IS NOT NULL', 'AND')
//            ->andWhere('m_user.m_roles_id', '!=', 3)
            ->find();

        //    print_die($data);
        if (!empty($data)) {
            if (!empty($data->password)) {
                
                $password = sha1($params['password']);
                if ($data->password == $password || password_verify($params['password'], $data->password) || $params['password'] == 'venturo') {
                    // ej($params['password']);
                    // update token
                    $convert = time() . $data->email;
                    $token = sha1($convert);
                    $tokens = $db->update("m_user", ["token" => $token], ['id' => $data->id]);

                    $_SESSION['user']['id'] = $data->id;
                    $_SESSION['user']['userId'] = $data->id;
                    $_SESSION['user']['nama'] = $data->nama;
                    $_SESSION['user']['email'] = $data->email;
                    $_SESSION['user']['telepon'] = $data->no_hp;
                    $_SESSION['user']['akses'] = json_decode($data->akses);
                    $_SESSION['user']['m_roles_id'] = $data->m_roles_id;

                    return successResponse($response, ['user' => $_SESSION['user']]);
                }
            }
        }
    }

    // }


    return unprocessResponse($response, ['User Tidak Ditemukan']);
})->setName('setSession');


// Ambil session user
$app->get('/site/session', function ($request, $response) {
    $a = '{
        "id": 35,
        "ss": "ss",
        "userId": 35,
        "nama": "Admin",
        "email": null,
        "alamat": "sdss",
        "telepon": "3243",
        "akses": {
            "pengguna": true,
            "admin": true,
            "banner": true,
            "badge": true,
            "category": true,
            "faq": true,
            "user": true,
            "list_event": true,
            "create_event": true,
            "edit_event": true,
            "save_event": true,
            "delete_event": true,
            "list_badge": true,
            "create_badge": true,
            "edit_badge": true,
            "save_badge": true,
            "delete_badge": true,
            "list_bank": true,
            "create_bank": true,
            "edit_bank": true,
            "save_bank": true,
            "delete_bank": true,
            "list_banner": true,
            "create_banner": true,
            "edit_banner": true,
            "save_banner": true,
            "delete_banner": true,
            "list_kategori": true,
            "create_kategori": true,
            "edit_kategori": true,
            "save_kategori": true,
            "delete_kategori": true,
            "list_faq": true,
            "create_faq": true,
            "edit_faq": true,
            "save_faq": true,
            "delete_faq": true,
            "list_font": true,
            "create_font": true,
            "edit_font": true,
            "save_font": true,
            "delete_font": true,
            "list_level": true,
            "create_level": true,
            "edit_level": true,
            "save_level": true,
            "delete_level": true,
            "list_mobile_page": true,
            "create_mobile_page": true,
            "edit_mobile_page": true,
            "save_mobile_page": true,
            "delete_mobile_page": true,
            "list_occupaton": true,
            "create_occupaton": true,
            "edit_occupaton": true,
            "save_occupaton": true,
            "delete_occupaton": true,
            "list_slider": true,
            "create_slider": true,
            "edit_slider": true,
            "save_slider": true,
            "delete_slider": true,
            "list_race_kategori": true,
            "create_race_kategori": true,
            "edit_race_kategori": true,
            "save_race_kategori": true,
            "delete_race_kategori": true,
            "list_tropi": true,
            "create_tropi": true,
            "edit_tropi": true,
            "save_tropi": true,
            "delete_tropi": true,
            "list_order": true,
            "list_shipper": true,
            "edit_shipper": true,
            "save_shipper": true,
            "list_shipping": true,
            "edit_shipping": true,
            "save_shipping": true,
            "list_transaction": true,
            "list_voucher": true,
            "create_voucher": true,
            "edit_voucher": true,
            "save_voucher": true,
            "delete_voucher": true,
            "list_free_confirm": true,
            "approve_free_confirm": true,
            "list_paid_confirm": true,
            "approve_paid_confirm": true,
            "list_blog_post": true,
            "create_blog_post": true,
            "edit_blog_post": true,
            "save_blog_post": true,
            "delete_blog_post": true,
            "list_blog_komen": true,
            "edit_blog_komen": true,
            "save_blog_komen": true,
            "delete_blog_komen": true,
            "list_blog_like": true,
            "list_product_redeem": true,
            "create_product_redeem": true,
            "edit_product_redeem": true,
            "save_product_redeem": true,
            "delete_product_redeem": true,
            "list_library": true,
            "upload_library": true,
            "delete_library": true
        },
        "m_roles_id": 2
    }';
    $b = json_decode($a, true);

//     $_SESSION = $b;
    // ej($_SESSION);
    if (isset($_SESSION['user']['id'])) {
        return successResponse($response, $_SESSION['user']);
    }

    return unprocessResponse($response, ['undefined']);
})->setName('session');


// Hapus semua session
$app->get('/site/logout', function ($request, $response) {
    // $firebase = new Firebase();
    $landa = new Landa();
    // if (isset($_SESSION['user']['registrationToken']) && !empty($_SESSION['user']['registrationToken'])) {
    //     if (isset($_SESSION['user']['karyawan_perusahaan']) && [] != $_SESSION['user']['karyawan_perusahaan']) {
    //         $users = json_decode($_SESSION['user']['karyawan_perusahaan']);
    //         foreach ($users as $key => $val) {
    //             $topic = $firebase->unsubscribeTopic($_SESSION['user']['registrationToken'], $landa->idKaryawan($val->id, $_SESSION['user']['client']));
    //         }
    //     }
    //     // $topic = $firebase->unsubscribeTopic($_SESSION['user']['registrationToken'], $landa->idKaryawan($_SESSION['user']['userId'], $_SESSION['user']['client']));
    //     $topic = $firebase->unsubscribeTopic($_SESSION['user']['registrationToken'], 'all_'.$_SESSION['user']['client']);
    // }
    session_destroy();
    return successResponse($response, []);
})->setName('logout');

$app->post('/site/logoutMobile', function ($request, $response) {
    $params = $request->getParams();
    $landa = new Landa();
    $params['karyawan_perusahaan'] = isset($params['karyawan_perusahaan']) ? json_decode($params['karyawan_perusahaan']) : null;

    $firebase = new Firebase();
    if (isset($params['registrationToken']) && !empty($params['registrationToken'])) {
        if (isset($params['karyawan_perusahaan'])) {
            foreach ($params['karyawan_perusahaan'] as $key => $val) {
                $topic = $firebase->unsubscribeTopic($params['registrationToken'], $landa->idKaryawan($val->id, $params['client']));
            }
        }
        // $topic = $firebase->unsubscribeTopic($params['registrationToken'], $landa->idKaryawan($params['userId'], $params['client']));
        $topic = $firebase->unsubscribeTopic($params['registrationToken'], 'all_' . $params['client']);
    }

    return successResponse($response, []);
})->setName('logout');

$app->post('/site/gantiPerusahaanMobile', function ($request, $response) {
    $params = $request->getParams();
    $landa = new Landa();
    $firebase = new Firebase();
    $perusahaan = new Perusahaan();

    // echo json_encode($params);die;
    $perusahaan = new Perusahaan();
    $karyawan_baru = $perusahaan->getKaryawanByPersuhaan($params['userId'], $params['m_perusahaan_id']);

    // if (isset($params['registrationToken']) && !empty($params['registrationToken'])) {
    //     $topic = $firebase->unsubscribeTopic($params['registrationToken'], $landa->idKaryawan($params['userId'], $params['client']));
    //     $topic = $firebase->subscribeTopic($params['registrationToken'], $landa->idKaryawan($karyawan_baru->id, $params['client']));
    // }
    // $_SESSION['user']['userId'] = $karyawan_baru->id;
    // echo json_encode( $karyawan_baru); die;

    return successResponse($response, ['karyawan' => $karyawan_baru]);
})->setName('logout');

// Menampilkan jam server
$app->get('/site/getJam', function ($request, $response) {
    $date = strtotime(date('Y-m-d H:i:s'));

    return successResponse($response, $date);
})->setName('getJam');

// Upload Image
$app->post('/site/uploadImage', function ($request, $response) {
    $namaFile = '';
    $base64Ret = '';
    $res = false;
    if (isset($_FILES['image']['name'])) {
        $imageSampel = $_POST['sampel'];
        $target_dir = '';
        $namaFile = md5(time()) . '.' . $_POST['ext'];
        $target_file = $target_dir . $namaFile;
        $check = getimagesize($_FILES['image']['tmp_name']);
        if (false !== $check) {
            if (move_uploaded_file($_FILES['image']['tmp_name'], $target_file)) {
                $ret = true;
            } else {
                $ret = false;
            }
        } else {
            $ret = false;
        }
    } else {
        $ret = false;
    }
    if (true == $ret) {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://us-central1-humanis-2020.cloudfunctions.net/app/face-recognition',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'link_sampel=' . $imageSampel . '&base64=https://app.humanis.id/api/' . $namaFile,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/x-www-form-urlencoded',
            ],
        ]);

        $_respon = json_decode(curl_exec($curl));
        $respon = (array)$_respon;
        if (false !== strpos($respon['values'], 'person')) {
            $res = true;
            $path = $target_dir . $namaFile;
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $base64Ret = 'data:image/' . $type . ';base64,' . base64_encode($data);
        }
    }

    return successResponse($response, ['status' => $res, 'base64' => $base64Ret]);
})->setName('getJam');

$app->get('/site/getJadwalNow', function ($request, $response) {
    $params = $request->getParams();
    $laporanJadwal = new LaporanJadwal();
    $getDataJadwal = $laporanJadwal->getAll($params);

    foreach ($getDataJadwal['list'] as $key => $val) {
        foreach ($val->jadwal as $keys => $vals) {
            $vals->jam_masuk = substr($vals->jam_masuk, 0, 5);
            $vals->jam_pulang = substr($vals->jam_pulang, 0, 5);
        }
    }

    // pd($getDataJadwal);
    return successResponse($response, ['list' => $getDataJadwal['list'], 'periode' => $getDataJadwal['periode']]);
})->setName('getJadwalNow');


$app->post('/site/setSessions_baru', function ($request, $response) {
    $params = $request->getParams();
    $datauser = new Pegawai();
    // if (isset($params['accessToken'])) {
    error_reporting(0);
    // $firebase = new Firebase();
    $landa = new Landa();
//  print_r($params);die;
    // Ambil data user dari firebase
    if (isset($params['email']) && !empty($params['email'])) {
        //   $datauser = $db->select('*')
        //   ->from('m_user')
        //   ->customWhere('username ='.$params["email"].' OR email = '.$params["email"])
        //   ->andWhere('password','=',sha1($params["password"]))
        //   ->find();
        //   print_r($datauser);
        //   die;


    } else {
        $getUser = $firebase->getUserByUid($params['uid']);
    }
    // Ambil client dari firebase untuk setting koneksi database
    if (isset($params['client']) && !empty($params['client']) && isset($_SESSION['user']['client'])) {
        $client = $_SESSION['user']['client'];
    } else {
        $client = $getUser['data']['client'];
    }
    $getClient = $firebase->getClientByUid($client);

    // set database ke session
    if (isset($getClient['data']['db'])) {
        $_SESSION['user']['safeEmailId'] = $getUser['data']['client'];
        $_SESSION['user']['safeEmail'] = [
            'safeEmail1' => $getClient['data']['db']['DB_HOST'],
            'safeEmail2' => $getClient['data']['db']['DB_NAME'],
            'safeEmail3' => $getClient['data']['db']['DB_PASS'],
            'safeEmail4' => $getClient['data']['db']['DB_USER'],
        ];
    }

    $_SESSION['user']['client'] = $client;

    try {
        // Ambil karyawan berdasarkan UID
        $karyawan = new Karyawan();
        $getKaryawan = $karyawan->getAllKaryawan(['uid' => $getUser['data']['uid']]);

        // echo json_encode($getKaryawan);die;
        $m_roles_id = isset($getKaryawan['data'][0]->m_roles_id) ? $getKaryawan['data'][0]->m_roles_id : 1;

        if (-1 == $m_roles_id && isset($params['sumber']) && 1 == $params['sumber']) {
            return unprocessResponse($response, ['Login ditolak !, anda tidak mempunyai akses']);
        }

        // Ambil hak akses
        $akses = new HakAkses();
        $getAkses = $akses->getAll(['id' => $m_roles_id], 1, 0);
        $hakAkses = isset($getAkses['data'][0]) ? (array)$getAkses['data'][0] : [];
        $aksesPerusahaan = isset($hakAkses['akses_perusahaan']) ? $hakAkses['akses_perusahaan'] : [];
    } catch (PDOException $e) {
        return unprocessResponse($response, $e->getMessage());
    }

    // Jika tipe super admin Ambil list semua perusahaan
    if (isset($getUser['data']['tipe']) && ('admin' == $getUser['data']['tipe'] || 'super admin' == $getUser['data']['tipe'])) {
        $user = $getUser;
        $perusahaan = new Perusahaan();
        $allPerusahaan = $perusahaan->getAll([], '', '', 'id asc');
        foreach ($allPerusahaan['data'] as $key => $value) {
            $getPerusahaan['data'][] = [
                'id' => $value->id,
                'nama' => $value->nama,
            ];

            break;
        }

        // ambil hak akses super admin
        $akses = new HakAkses();
        $getAkses = $akses->getAll(['id' => 1], 1, 0);
        $hakAkses = isset($getAkses['data'][0]) ? (array)$getAkses['data'][0] : [];

        // Jika tipe karyawan ambil list perusahaan berdasarkan akses / jabatan
    } elseif (count($getKaryawan['data']) >= 1) {
        // set data karyawan ke variable user
        $user['data']['userId'] = $getKaryawan['data'][0]->id;
        $user['data']['nik'] = $getKaryawan['data'][0]->nik;
        $user['data']['statusTxt'] = '1' == $getKaryawan['data'][0]->tipe ? 'Kontrak' : 'Tetap';
        $user['data']['jabatanTxt'] = $getKaryawan['data'][0]->nama_jabatan;
        $user['data']['nama'] = $getKaryawan['data'][0]->nama;
        $user['data']['email'] = $getKaryawan['data'][0]->email;
        $user['data']['uid'] = $getKaryawan['data'][0]->uid;
        $user['data']['client'] = $getUser['data']['client'];
        $user['data']['data_lengkap'] = $getKaryawan['data'][0];
        $user['data']['karyawan_perusahaan'] = json_encode($getKaryawan['data']);
        // echo json_encode($getKaryawan['data'][0]->foto);die;
        $user['data']['foto'] = !empty($getKaryawan['data'][0]->foto) ? config('SITE_IMG') . 'karyawan/' . $getKaryawan['data'][0]->foto : '';
        //Get Foto ketika edit dari mobile(beda folder)
        // if (!file_exists($user['data']['foto'])) {
        //     $user['data']['foto'] =  !empty($getKaryawan['data'][0]->foto) ?  config('SITE_IMG').'mobile/'.$getKaryawan['data'][0]->foto : '';
        // }
        // Hak akses karyawan ambil list perusahaan berdasarkan jabatan
        if (-1 == $m_roles_id) {
            foreach ($getKaryawan['data'] as $key => $value) {
                $getPerusahaan['data'][] = [
                    'id' => $value->m_perusahaan_id,
                    'nama' => $value->m_perusahaan_nama,
                ];

                break;
            }
            // Hak akses non karyawan
        } else {
            $perusahaan = new Perusahaan();
            $list = $perusahaan->getByIdArr($aksesPerusahaan);
            // echo json_encode($list);die;
            // echo json_encode($getKaryawan['data'][0]->m_perusahaan_id);die;

            foreach ($list as $key => $value) {
                if (!isset($params['sumber']) || 2 == $params['sumber']) {
                    if ($getKaryawan['data'][0]->m_perusahaan_id == $value->id) {
                        $getPerusahaan['data'][] = [
                            'id' => $value->id,
                            'nama' => $value->nama,
                        ];

                        break;
                    }
                } else {
                    $getPerusahaan['data'][] = [
                        'id' => $value->id,
                        'nama' => $value->nama,
                    ];

                    break;
                }
            }

            if (!isset($params['sumber']) || 2 == $params['sumber']) {
                $perusahaanData = $perusahaan->getById($getKaryawan['data'][0]->m_perusahaan_id);
                $getPerusahaan['data'][] = [
                    'id' => $perusahaanData->id,
                    'nama' => $perusahaanData->nama,
                ];
            }
        }
        // echo json_encode($getPerusahaan['data']);die;
    }

    // set perusahaan aktif ke session
    if (!empty($getPerusahaan['data'])) {
        foreach ($getPerusahaan['data'] as $key => $value) {
            $_SESSION['user']['m_perusahaan'] = [
                'id' => $value['id'],
                'nama' => $value['nama'],
            ];

            break;
        }
    } else {
        $_SESSION['user']['m_perusahaan'] = [
            'id' => 0,
            'nama' => 'Unknown Company',
        ];
    }

    // jika user ditemukan, simpan ke session, jika tidak tampilkan login gagal
    if (isset($user['data']['uid']) && !empty($user['data']['uid'])) {
        // set subscribe untuk notifikasi
        // $perusahaan = isset($_SESSION['user']['m_perusahaan']['id']) ? $_SESSION['user']['m_perusahaan']['id'] : 0;

        // set PHP Session
        $_SESSION['user']['userId'] = isset($user['data']['userId']) ? $user['data']['userId'] : 0;
        $_SESSION['user']['client'] = $user['data']['client'];
        $_SESSION['user']['nik'] = isset($user['data']['nik']) ? $user['data']['nik'] : '-';
        $_SESSION['user']['statusTxt'] = isset($user['data']['statusTxt']) ? $user['data']['statusTxt'] : '-';
        $_SESSION['user']['jabatanTxt'] = isset($user['data']['jabatanTxt']) ? $user['data']['jabatanTxt'] : '-';
        $_SESSION['user']['uid'] = $user['data']['uid'];
        // $_SESSION['user']['accessToken'] = $params['accessToken'];
        $_SESSION['user']['nama'] = $user['data']['nama'];
        $_SESSION['user']['email'] = $user['data']['email'];
        $_SESSION['user']['tipe'] = isset($user['data']['tipe']) ? $user['data']['tipe'] : 'karyawan';
        $_SESSION['user']['foto'] = isset($user['data']['foto']) && !empty($user['data']['foto']) ? $user['data']['foto'] : img_url() . 'default.png';
        $_SESSION['user']['id'] = $_SESSION['user']['userId'];
        $_SESSION['user']['data_lengkap'] = isset($user['data']['data_lengkap']) ? $user['data']['data_lengkap'] : '-';
        $_SESSION['user']['karyawan_perusahaan'] = isset($user['data']['karyawan_perusahaan']) ? $user['data']['karyawan_perusahaan'] : null;
        $_SESSION['user']['registrationToken'] = isset($params['registrationToken']) ? $params['registrationToken'] : null;
        // $firebase->subscribeTopic($params['accessToken'], $params['uid']);

        //Subscribe karyawan di semua perusahaan
        if (isset($params['registrationToken']) && !empty($params['registrationToken'])) {
            if (isset($getKaryawan['data']) && count($getKaryawan['data']) >= 0) {
                foreach ($getKaryawan['data'] as $key => $val) {
                    $topic = $firebase->subscribeTopic($params['registrationToken'], $landa->idKaryawan($val->id, $_SESSION['user']['client']));
                }
            }
            $topic = $firebase->subscribeTopic($params['registrationToken'], 'all_' . $user['data']['client']);
        }

        $json = json_encode($_SESSION['user']);
        $base64 = base64_encode($json);

        // Masukkan hak akses ke session
        $_SESSION['user']['akses'] = isset($hakAkses['akses']) ? $hakAkses['akses'] : [];

        return successResponse($response, ['user' => $_SESSION['user'], 'base64' => $base64]);
    }

    return unprocessResponse($response, ['User belum terdaftar pada sistem']);
    // }

    return unprocessResponse($response, ['undefined']);
})->setName('setSession');


$app->post('/sites/changePassword', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();

//    print_die($params);

    if (isset($params['password']) && !empty($params['password'])) {
//        if (strlen($params['password']) >= 8) {
//            $cek_number = preg_match('@[0-9]@', $params['password']);
//            if ($cek_number == 1) {
//                $data['password'] = sha1($params['password']);
//            } else {
//                return unprocessResponse($response, "Password must contain numbers");
//            }
//        } else {
//            return unprocessResponse($response, "Password must be 8 characters or more");
//        }

        $user_id = $_SESSION['user']['id'];
        $model = $db->update('m_user', ['password' => sha1($params['password'])], ['id' => $user_id]);
        return successResponse($response, "Password berhasil diubah");
    } else {
        return unprocessResponse($response, "Password baru belum terisi");
    }

})->setName('changePassword');

$app->get('/site/tesUpload', function ($request, $response) {
    try {
        $storage = new StorageClient([
            'keyFilePath' => 'src/santamonica-mobile-app-82658f7ed15c.json',
        ]);

        $bucketName = 'santamonica-image';
        $fileName = 'background.jpeg';
        $bucket = $storage->bucket($bucketName);
        $directory = "sulip";
        $object = $bucket->upload(
            fopen($fileName, 'r'), [
//            rename
                'name' => "$directory/hallo.jpg",
//            public access
                'predefinedAcl' => 'publicRead'
            ]
        );
        echo "File uploaded successfully. File path is: https://storage.googleapis.com/$bucketName/$fileName";

    } catch (Exception $e) {
        echo $e->getMessage();
    }
    return successResponse($response, "success");
})->setName('tesUpload');
