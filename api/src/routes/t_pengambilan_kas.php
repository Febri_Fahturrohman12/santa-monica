<?php

use Service\Db;
use Model\PengambilanKas;
use Service\Landa;

$app->get('/t_pengambilan_kas/index', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $pengambilankas = new PengambilanKas();

    $getData = $pengambilankas->getAll($params);

    return successResponse($response, [
        'list' => $getData['data'],
        'totalItems' => $getData['totalItem']
    ]);
});

$app->post("/t_pengambilan_kas/save", function ($request, $response) {
    $params = $request->getParams();
    $landa = new Landa();
    $pengambilankas = new PengambilanKas();

    $validasi = $pengambilankas->validasi($params);

    if (true === $validasi) {
        
        $save = $pengambilankas->save($params);

        if ($save['status']) {
            return successResponse($response, $save['data']);
        }

        return unprocessResponse($response, [$save['error']]);
    }

    return unprocessResponse($response, $validasi);

});

$app->post("/t_pengambilan_kas/delete", function ($request, $response) {
    $params = $request->getParams();
    $pengambilankas = new PengambilanKas();

    $model = $pengambilankas->delete($params);

    if ($model['status']) {
        return successResponse($response, $model['data']);
    }

    return unprocessResponse($response, [$model['error']]);
});

$app->post("/t_pengambilan_kas/restore", function ($request, $response) {
    $params = $request->getParams();
    $pengambilankas = new PengambilanKas();

    $model = $pengambilankas->restore($params);

    if ($model['status']) {
        return successResponse($response, $model['data']);
    }

    return unprocessResponse($response, [$model['error']]);
});


$app->get("/t_pengambilan_kas/getHistori", function ($request, $response) {
    $data = $request->getParams();
    $db = Db::db();

    $val = getHistori('m_barang_id', 0, 'nama', $data['m_barang_id']);
    $model = $val['data'];
    return successResponse($response, $model);
});
$app->get('/t_pengambilan_kas/getKaryawan', function ($request, $response) {
    $params = $request->getParams();

    $pengambilankas = new PengambilanKas();
    $data = $pengambilankas->getDataKaryawan();

    return successResponse($response, ['list' => $data['data'], 'totalItems' => $data['totalItem']]);
});

?>