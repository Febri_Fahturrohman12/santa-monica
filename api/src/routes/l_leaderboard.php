<?php

use Model\Leaderboard;

$app->get('/l_leaderboard/getAll', function ($request, $response) {
    $params = $request->getParams();
    $leaderboard = new Leaderboard();

    $getData = $leaderboard->getAll($params);

    if (isset($params['export']) && $params['export'] == 1) {
        ob_start();
        $xls = PHPExcel_IOFactory::load("assets/formatExcel/leaderboard-community.xlsx");
        $objPHPExcel = $xls->getSheet(0);

            $objPHPExcel->setCellValue('A1', "CLUB");
            if ($params['tipe'] == 'km_terbanyak' || $params['tipe'] == 'km_rata') {
                $objPHPExcel->setCellValue('B1', "KILOMETER");
            } elseif ($params['tipe'] == 'pace_rata') {
                $objPHPExcel->setCellValue('B1', "PACE");
            }
            $objPHPExcel->getStyle("A1:B1")->getFont()->setBold(true);
        foreach ($getData['data'] as $key => $value) {

            $no = $key + 2;
            $objPHPExcel->setCellValue('A' . $no, strtoupper($value['nama_club']));
            if ($params['tipe'] == 'km_terbanyak' || $params['tipe'] == 'km_rata') {
                $objPHPExcel->setCellValue('B' . $no, $value['kilometer'] . ' km');
            } elseif ($params['tipe'] == 'pace_rata') {
                $objPHPExcel->setCellValue('B' . $no, $value['pace']);
            }

            $objPHPExcel->getStyle("A" . 1 . ":B" . $no)->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                        )
                    )
                )
            );
        }

        date_default_timezone_set("Asia/Jakarta");
        $date = date("dmY");
        $hour = date("Hi");

        $writer = new PHPExcel_Writer_Excel2007($xls);
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;Filename=\"Leaderboard_Community_".$date.'_'.$hour.".xlsx\"");

        ob_end_clean();
        $writer->save('php://output');
        exit;
    } else {
        return successResponse($response, ['list' => $getData['data']]);
    }
});