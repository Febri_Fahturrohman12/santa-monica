<?php

use Service\Db;
use Service\Firebase;
use Service\Landa;


function validasi($data, $custom = array())
{
    $validasi = array(
        "nama_aplikasi" => "required",
        "no_hp" => "required",
        "email" => "required"
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}


$app->get('/m_settingAplikasi/index', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $data = $db->select('*')
        ->from('m_setting');

    $models = $data->find();
    ej($models);
    $model = !empty($models) ? $models : [];

    return successResponse($response, ['list' => $model]);
});

$app->get('/m_settingAplikasi/getDetail', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();


    $model = !empty($models) ? $models : [];
    //  echo json_encode($model);die;
    return successResponse($response, $model);
});

$app->post("/m_settingAplikasi/save", function ($request, $response) {
    $data = $request->getParams();
    $db = Db::db();
    $landa = new Landa();

    $validasi = validasi($data["model"]);
    if ($validasi === true) {
        try {
            // Logo Login  
            if (isset($data["model"]['logo_login']['base64']) && !empty($data["model"]['logo_login']['base64'])) {
                $path = 'setting_aplikasi/';

                /**
                 * upload image
                 */
                //        penamaan file = module-user-md5(userid)-strtotime
                $fileName = "logo-login-" . strtotime(date('Y-m-d H:i:s')); //string
                $foto = $landa->base64_to_jpeg($data["model"]['logo_login']['base64'], $fileName); //file
//                $upload = $landa->uploadImage($foto, "setting_aplikasi", $foto);
                $data["model"]['logo_login'] = $foto;
                /**
                 * end upload image
                 */

            } else {
                unset($data["model"]['logo_login']);
            }

            // Logo Menu
            if (isset($data["model"]['logo_menu']['base64']) && !empty($data["model"]['logo_menu']['base64'])) {
                $path = 'setting_aplikasi/';

                /**
                 * upload image
                 */
                //        penamaan file = module-user-md5(userid)-strtotime
                $fileName = "logo-menu-" . strtotime(date('Y-m-d H:i:s')); //string
                $foto = $landa->base64_to_jpeg($data["model"]['logo_menu']['base64'], $fileName); //file
//                $upload = $landa->uploadImage($foto, "setting_aplikasi", $foto);
                $data["model"]['logo_menu'] = $foto;
                /**
                 * end upload image
                 */

            } else {
                unset($data["model"]['logo_menu']);
            }

            // Logo Laporan
            if (isset($data["model"]['logo_laporan']['base64']) && !empty($data["model"]['logo_laporan']['base64'])) {
                $path = 'setting_aplikasi/';

                /**
                 * upload image
                 */
                //        penamaan file = module-user-md5(userid)-strtotime
                $fileName = "logo-laporan-" . strtotime(date('Y-m-d H:i:s')); //string
                $foto = $landa->base64_to_jpeg($data["model"]['logo_laporan']['base64'], $fileName); //file
//                $upload = $landa->uploadImage($foto, "setting_aplikasi", $foto);
                $data["model"]['logo_laporan'] = $foto;
                /**
                 * end upload image
                 */

            } else {
                unset($data["model"]['logo_laporan']);
            }

            if (isset($data["model"]["id"])) {
                $model = $db->update("m_setting", $data["model"], ["id" => $data["model"]["id"]]);
            } else {
                $model = $db->insert("m_setting", $data["model"]);
            }

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, $e->getMessage());
        }
    }
    return unprocessResponse($response, $validasi);
});


$app->post("/m_settingAplikasi/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = Db::db();
    $model = $db->delete("m_setting", ['id' => $data['id']]);
    if (isset($model)) {
        return successResponse($response, [$model]);
    }

    return unprocessResponse($response, ['terjadi masalah pada server']);
});


?>