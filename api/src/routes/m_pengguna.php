<?php

use Service\Db;
use Service\Firebase;
use Service\Landa;


function validasi($data, $custom = array())
{
    $validasi = array(
        "nama" => "required",
        "username" => 'required',

    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get('/m_pengguna/index', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
//     print_r($params['filter']);die();

    $data = $db->select('m_pengguna.*, m_pengguna.no_hp as telepon')
        ->from('m_pengguna');

    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $data->where('m_pengguna.nama', "LIKE", $val);
            } elseif ($key == "alamat") {
                $data->where('m_pengguna.alamat', "LIKE", $val);
            } elseif ($key == "is_deleted") {
                $data->where('m_pengguna.is_deleted', "LIKE", $val);
            }
        }
    }
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $data->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $data->offset($params["offset"]);
    }

    $models = $data->findAll();
    $totalItem = $data->count();

    return successResponse($response, [
        'list' => $models,
        'totalItems' => $totalItem

    ]);
});

$app->post("/m_pengguna/save", function ($request, $response) {
    $data = $request->getParams();
    $db = Db::db();
//    ej($data);

    if (isset($data['id'])) {

        if (!empty($data['password'])) {
            $data["password"] = sha1($data["password"]);
        } else {
            unset($data["password"]);
        }
        $validasi = validasi($data);

    } else {
        $data["password"] = isset($data["password"]) ? sha1($data["password"]) : "";
        $validasi = validasi($data, ["password" => "required"]);
    }
    if ($validasi === true) {
        try {
            $data["no_hp"] = isset($data["telepon"]) ? $data["telepon"] : null;

            if (isset($data["id"])) {
                $model = $db->update("m_pengguna", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("m_pengguna", $data);
            }

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->post("/m_pengguna/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = Db::db();
    $model = $db->update("m_pengguna", ['is_deleted' => 1], ['id' => $data['id']]);
    if (isset($model)) {
        return successResponse($response, [$model]);
    }

    return unprocessResponse($response, ['terjadi masalah pada server']);
});


?>