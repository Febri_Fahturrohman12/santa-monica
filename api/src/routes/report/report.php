<?php

use Service\Db;

function communityFun($params)
{
    $db = Db::db();
    if ($params['tipe'] == 'km_terbanyak') {
        $db->select('SUM(m_event_data.jarak) as jarak, m_club.nama')
            ->from('m_event_data')
            ->leftJoin('m_club_member', 'm_club_member.m_user_id = m_event_data.m_user_id')
            ->leftJoin('m_club', 'm_club.id = m_club_member.m_club_id')
            ->where('m_event_data.m_event_id', '=', $params['m_event_id'])
            ->orderBy('jarak DESC')
            ->groupBy('m_club.id');
            
        if (isset($params['limit'])) {
            $db->limit($params['limit']);
        }

        $model = $db->findAll();
        $arr = [];

        if (!empty($model)) {
            foreach ($model as $key => $val) {
                $arr[$key] = (array) $val;
                $arr[$key]['jarak'] =  round($val->jarak, 3);

            }
        }
    } elseif ($params['tipe'] == 'km_rata') {
        $db->select('AVG(m_event_data.jarak) as jarak, m_club.nama')
            ->from('m_event_data')
            ->leftJoin('m_club_member', 'm_club_member.m_user_id = m_event_data.m_user_id')
            ->leftJoin('m_club', 'm_club.id = m_club_member.m_club_id')
            ->where('m_event_data.m_event_id', '=', $params['m_event_id'])
            ->orderBy('jarak DESC')
            ->groupBy('m_club.id')
            ->findAll();

        if (isset($params['limit'])) {
            $db->limit($params['limit']);
        }
            
        $model = $db->findAll();
        $arr = [];

        if (!empty($model)) {
            foreach ($model as $key => $val) {
                $arr[$key] = (array) $val;
                $arr[$key]['jarak'] =  round($val->jarak, 3);

            }
        }
    } else {
        $models = $db->select('m_event_data.jarak as jarak, m_event_data.waktu, m_club.id as m_club_id, m_club.nama')
            ->from('m_event_data')
            ->leftJoin('m_club_member', 'm_club_member.m_user_id = m_event_data.m_user_id')
            ->leftJoin('m_club', 'm_club.id = m_club_member.m_club_id')
            ->where('m_event_data.m_event_id', '=', $params['m_event_id'])
            ->findAll();
        
        $arrClub = $arrTemp = [];
        foreach ($models as $key => $val) {
            if ($val->jarak > 0) {
                $str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $val->waktu);
                sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
                $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;
                
                $arrTemp[$val->m_club_id][$key] = $val;
                $arrTemp[$val->m_club_id][$key]->second = $time_seconds / $val->jarak;
                
                $arrClub[$val->m_club_id] = array_values($arrTemp[$val->m_club_id]);
            }
        }
        
        $arrClub = array_values($arrClub);

        $arr = [];
        foreach ($arrClub as $key => $val) {
            foreach ($val as $keys => $vals) {
                $arr[$key]['nama_club'] = $vals->nama_club;
                $arr[$key]['second'] = !isset($arr[$key]['second']) ? $vals->second : ($arr[$key]['second'] + $vals->second);
            }

            $arr[$key]['second'] = $arr[$key]['second'] / count($arrClub[$key]);
            $arr[$key]['pace'] = date('i:s', $arr[$key]['second']);
        }

        $pace = [];
        foreach ($arr as $key => $val) {
            $pace[$key] = $val['pace'];
        }

        array_multisort($pace, SORT_ASC, $arr);
        
        if (isset($params['limit'])) {
            $arr = array_slice($arr, 0, 10);
        }
    }

    return $arr;
}

$app->get('/report/index', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $event = [
        'run' => 'RUNNING',
        'cyc' => 'CYCLING'
    ];
    $race_kategori = $db->select('m_event_race.id, m_race_kategori.id kategori_id, m_race_kategori.nama race_name')
    ->from('m_event_race')
    ->leftJoin('m_race_kategori', 'm_event_race.m_race_kategori_id = m_race_kategori.id')
    ->customwhere('m_event_race.m_event_id IN (89,91)', "AND")
    ->findAll();

    $arrKategori = [];
    if (!empty($race_kategori)) {
        foreach ($race_kategori as $k => $v) {
            if (!empty($v->race_name)) {
                $arrKategori[$v->kategori_id]['id'] = $v->kategori_id;
                $arrKategori[$v->kategori_id]['nama'] = $v->race_name;
                $arrKategori[$v->kategori_id]['alias'] = str_replace(' ', '_', $v->race_name);
                $arrKategori[$v->kategori_id]['detail'] = [];
                $arrKategori[$v->kategori_id]['leng'] = 0;
            }
        }
    }

    $db->select('
    (select IFNULL(SUM(m_event_data.jarak), 0) jarak
    from m_event_data
    where m_event_data.m_event_id = t_order.m_event_id AND
    m_event_data.m_user_id = t_order.m_user_id AND
    m_event_data.status = "approve"
    ) as jarak,
    (select IFNULL(SUM(m_event_data.calories), 0) calories
    from m_event_data
    where m_event_data.m_event_id = t_order.m_event_id AND
    m_event_data.m_user_id = t_order.m_user_id AND
    m_event_data.status = "approve"
    ) as cal,
    t_order.id,
    t_order.m_event_id,
    t_order.m_user_id,
    t_order.team_name,
    m_user.nama as nama_user,
    m_user.no_hp,
    t_order.ebib_no,
    m_user.email,
    m_event.nama as nama_event,
    m_event.is_team,
    m_event_race.m_race_kategori_id kategori_id,
    m_club.nama nama_club
    ')
        ->from('t_order')
        ->innerJoin('m_user', 't_order.m_user_id = m_user.id')
        ->leftJoin('m_club_member', 'm_club_member.m_user_id = m_user.id')
        ->leftJoin('m_club', 'm_club.id = m_club_member.m_club_id')
        ->innerJoin('m_event', 't_order.m_event_id = m_event.id')
        ->leftJoin('t_confirm_order', 't_confirm_order.t_order_id = t_order.id')
        ->leftJoin('m_event_race', 'm_event_race.id = t_order.m_event_race_id')
        ->where("t_order.m_event_id", "=", 89)
        ->where("t_confirm_order.status", "=", "Approved");

    $db->orderBy('jarak DESC');
    $db->groupBy('kategori_id, m_user.id');
    $running = $db->findAll();

    $arrRunning = [];
    if (!empty($running)) {
        foreach ($running as $key => $val) {
            $event['run'] = $val->nama_event;
            $val->jarak = round($val->jarak, 2);
            $val->cal = round($val->cal, 2);

            if (isset($arrKategori[$val->kategori_id]['nama'])) {
                $arrRunning[$val->kategori_id]['id'] = $val->kategori_id;
                $arrRunning[$val->kategori_id]['nama'] = $arrKategori[$val->kategori_id]['nama'];
                $arrRunning[$val->kategori_id]['alias'] = $arrKategori[$val->kategori_id]['alias'];
                $arrRunning[$val->kategori_id]['is_user'] = 1;
                $arrRunning[$val->kategori_id]['detail'][] = $val;
                $arrRunning[$val->kategori_id]['leng'] = count($arrRunning[$val->kategori_id]['detail']);
            }
        }
    }

    $params = ['m_event_id'=> 89, 'tipe' => 'km_terbanyak'];
    $communityRunning = communityFun($params);

    $arrComRunning = [];
    if (!empty($communityRunning)) {
        $arrComRunning[0]['id'] = 0;
        $arrComRunning[0]['nama'] = 'Community';
        $arrComRunning[0]['alias'] = 'Community';
        $arrComRunning[0]['is_user'] = 0;
        $arrComRunning[0]['detail'] = $communityRunning;
        $arrComRunning[0]['leng'] = 1;
    }

    usort($arrRunning, function ($a, $b) {
        return $b['id'] - $a['id'];
    });

    if (!empty($arrRunning)) {
        $arrRunning = array_merge($arrRunning, $arrComRunning);
    }

    $db->select('
    (select IFNULL(SUM(m_event_data.jarak), 0) jarak
    from m_event_data
    where m_event_data.m_event_id = t_order.m_event_id AND
    m_event_data.m_user_id = t_order.m_user_id AND
    m_event_data.status = "approve"
    ) as jarak,
    (select IFNULL(SUM(m_event_data.calories), 0) calories
    from m_event_data
    where m_event_data.m_event_id = t_order.m_event_id AND
    m_event_data.m_user_id = t_order.m_user_id AND
    m_event_data.status = "approve"
    ) as cal,
    t_order.id,
    t_order.m_event_id,
    t_order.m_user_id,
    t_order.team_name,
    m_user.nama as nama_user,
    m_user.no_hp,
    t_order.ebib_no,
    m_user.email,
    m_event.nama as nama_event,
    m_event.is_team,
    m_event_race.m_race_kategori_id kategori_id,
    m_club.nama nama_club
    ')
        ->from('t_order')
        ->innerJoin('m_user', 't_order.m_user_id = m_user.id')
        ->leftJoin('m_club_member', 'm_club_member.m_user_id = m_user.id')
        ->leftJoin('m_club', 'm_club.id = m_club_member.m_club_id')
        ->innerJoin('m_event', 't_order.m_event_id = m_event.id')
        ->leftJoin('t_confirm_order', 't_confirm_order.t_order_id = t_order.id')
        ->leftJoin('m_event_race', 'm_event_race.id = t_order.m_event_race_id')
        ->where("t_order.m_event_id", "=", 91)
        ->where("t_confirm_order.status", "=", "Approved");

    $db->orderBy('jarak DESC');
    $db->groupBy('kategori_id, m_user.id');
    $cycling = $db->findAll();

    $arrCycling = [];
    if (!empty($cycling)) {
        foreach ($cycling as $key => $val) {
            $event['cyc'] = $val->nama_event;
            $val->jarak = round($val->jarak, 2);
            $val->cal = round($val->cal, 2);

            if (isset($arrKategori[$val->kategori_id]['nama'])) {
                $arrCycling[$val->kategori_id]['id'] = $val->kategori_id;
                $arrCycling[$val->kategori_id]['nama'] = $arrKategori[$val->kategori_id]['nama'];
                $arrCycling[$val->kategori_id]['alias'] = $arrKategori[$val->kategori_id]['alias'];
                $arrCycling[$val->kategori_id]['is_user'] = 1;
                $arrCycling[$val->kategori_id]['detail'][] = $val;
                $arrCycling[$val->kategori_id]['leng'] = count($arrCycling[$val->kategori_id]['detail']);
            }
        }
    }

    $params = ['m_event_id'=> 91, 'tipe' => 'km_terbanyak'];
    $communityCycling = communityFun($params);

    usort($arrCycling, function ($a, $b) {
        return $b['id'] - $a['id'];
    });

    $arrComCycling = [];
    if (!empty($communityCycling)) {
        $arrComCycling[0]['id'] = 0;
        $arrComCycling[0]['nama'] = 'Community';
        $arrComCycling[0]['alias'] = 'Community';
        $arrComCycling[0]['is_user'] = 0;
        $arrComCycling[0]['detail'] = $communityCycling;
        $arrComCycling[0]['leng'] = 1;
    }
    
    if (!empty($arrCycling)) {
        $arrCycling = array_merge($arrCycling, $arrComCycling);
    }

    if (empty($arrRunning)) {
        $arrRunning = $arrKategori;
    }
    if (empty($arrCycling)) {
        $arrCycling = $arrKategori;
    }
    
    return successResponse($response, ['listRunning' => array_values($arrRunning), 'listCycling' => array_values($arrCycling), 'is_team' => 0, 'kategori_race' => array_values($arrKategori), 'event' => $event]);
});
$app->get('/report/leaderboard_kekl', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $time = microtime();
    $time = explode(' ', $time);
    $time = $time[1] + $time[0];
    $start = $time;
    $event = [
        'run' => 'RUNNING',
        'cyc' => 'CYCLING'
    ];
    $race_kategori = $db->select('m_event_race.id, m_race_kategori.id kategori_id, m_race_kategori.nama race_name')
    ->from('m_event_race')
    ->leftJoin('m_race_kategori', 'm_event_race.m_race_kategori_id = m_race_kategori.id')
    ->customwhere('m_event_race.m_event_id IN (77,75)', "AND")
    ->findAll();

    $arrKategori = [];
    if (!empty($race_kategori)) {
        foreach ($race_kategori as $k => $v) {
            if (!empty($v->race_name)) {
                $arrKategori[$v->kategori_id]['id'] = $v->kategori_id;
                $arrKategori[$v->kategori_id]['nama'] = $v->race_name;
                $arrKategori[$v->kategori_id]['alias'] = str_replace(' ', '_', $v->race_name);
                $arrKategori[$v->kategori_id]['detail'] = [];
                $arrKategori[$v->kategori_id]['leng'] = 0;
            }
        }
    }

    $db->select('
    (select IFNULL(SUM(m_event_data.jarak), 0) jarak
    from m_event_data
    where m_event_data.m_event_id = t_order.m_event_id AND
    m_event_data.m_user_id = t_order.m_user_id AND
    m_event_data.status = "approve"
    ) as jarak,
    (select IFNULL(SUM(m_event_data.calories), 0) calories
    from m_event_data
    where m_event_data.m_event_id = t_order.m_event_id AND
    m_event_data.m_user_id = t_order.m_user_id AND
    m_event_data.status = "approve"
    ) as cal,
    t_order.id,
    t_order.m_event_id,
    t_order.m_user_id,
    t_order.team_name,
    m_user.nama as nama_user,
    m_user.no_hp,
    t_order.ebib_no,
    m_user.email,
    m_event.nama as nama_event,
    m_event.is_team,
    m_event_race.m_race_kategori_id kategori_id,
    m_club.nama nama_club
    ')
        ->from('t_order')
        ->innerJoin('m_user', 't_order.m_user_id = m_user.id')
        ->leftJoin('m_club_member', 'm_club_member.m_user_id = m_user.id')
        ->leftJoin('m_club', 'm_club.id = m_club_member.m_club_id')
        ->innerJoin('m_event', 't_order.m_event_id = m_event.id')
        ->leftJoin('t_confirm_order', 't_confirm_order.t_order_id = t_order.id')
        ->leftJoin('m_event_race', 'm_event_race.id = t_order.m_event_race_id')
        ->where("t_order.m_event_id", "=", 77)
        ->where("t_confirm_order.status", "=", "Approved");

    $db->orderBy('jarak DESC');
    $db->groupBy('kategori_id, m_user.id');
    // $db->limit(100);
    $running = $db->findAll();

    $arrRunning = [];
    if (!empty($running)) {
        foreach ($running as $key => $val) {
            $event['run'] = $val->nama_event;
            $val->jarak = round($val->jarak, 2);
            $val->cal = round($val->cal, 2);

            if (isset($arrKategori[$val->kategori_id]['nama'])) {
                $arrRunning[$val->kategori_id]['id'] = $val->kategori_id;
                $arrRunning[$val->kategori_id]['nama'] = $arrKategori[$val->kategori_id]['nama'];
                $arrRunning[$val->kategori_id]['alias'] = $arrKategori[$val->kategori_id]['alias'];
                $arrRunning[$val->kategori_id]['is_user'] = 1;
                $arrRunning[$val->kategori_id]['detail'][] = $val;
                $arrRunning[$val->kategori_id]['leng'] = count($arrRunning[$val->kategori_id]['detail']);
            }
        }
    }

    $params = ['m_event_id'=> 77, 'tipe' => 'km_terbanyak'];
    $communityRunning = communityFun($params);

    $arrComRunning = [];
    if (!empty($communityRunning)) {
        $arrComRunning[0]['id'] = 0;
        $arrComRunning[0]['nama'] = 'Community';
        $arrComRunning[0]['alias'] = 'Community';
        $arrComRunning[0]['is_user'] = 0;
        $arrComRunning[0]['detail'] = $communityRunning;
        $arrComRunning[0]['leng'] = 1;
    }

    usort($arrRunning, function ($a, $b) {
        return $b['id'] - $a['id'];
    });

    if (!empty($arrRunning)) {
        $arrRunning = array_merge($arrRunning, $arrComRunning);
    }

    $db->select('
    (select IFNULL(SUM(m_event_data.jarak), 0) jarak
    from m_event_data
    where m_event_data.m_event_id = t_order.m_event_id AND
    m_event_data.m_user_id = t_order.m_user_id AND
    m_event_data.status = "approve"
    ) as jarak,
    (select IFNULL(SUM(m_event_data.calories), 0) calories
    from m_event_data
    where m_event_data.m_event_id = t_order.m_event_id AND
    m_event_data.m_user_id = t_order.m_user_id AND
    m_event_data.status = "approve"
    ) as cal,
    t_order.id,
    t_order.m_event_id,
    t_order.m_user_id,
    t_order.team_name,
    m_user.nama as nama_user,
    m_user.no_hp,
    t_order.ebib_no,
    m_user.email,
    m_event.nama as nama_event,
    m_event.is_team,
    m_event_race.m_race_kategori_id kategori_id,
    m_club.nama nama_club
    ')
        ->from('t_order')
        ->innerJoin('m_user', 't_order.m_user_id = m_user.id')
        ->leftJoin('m_club_member', 'm_club_member.m_user_id = m_user.id')
        ->leftJoin('m_club', 'm_club.id = m_club_member.m_club_id')
        ->innerJoin('m_event', 't_order.m_event_id = m_event.id')
        ->leftJoin('t_confirm_order', 't_confirm_order.t_order_id = t_order.id')
        ->leftJoin('m_event_race', 'm_event_race.id = t_order.m_event_race_id')
        ->where("t_order.m_event_id", "=", 75)
        ->where("t_confirm_order.status", "=", "Approved");

    $db->orderBy('jarak DESC');
    $db->groupBy('kategori_id, m_user.id');
    // $db->limit(100);
    $cycling = $db->findAll();

    $arrCycling = [];
    if (!empty($cycling)) {
        foreach ($cycling as $key => $val) {
            $event['cyc'] = $val->nama_event;
            $val->jarak = round($val->jarak, 2);
            $val->cal = round($val->cal, 2);

            if (isset($arrKategori[$val->kategori_id]['nama'])) {
                $arrCycling[$val->kategori_id]['id'] = $val->kategori_id;
                $arrCycling[$val->kategori_id]['nama'] = $arrKategori[$val->kategori_id]['nama'];
                $arrCycling[$val->kategori_id]['alias'] = $arrKategori[$val->kategori_id]['alias'];
                $arrCycling[$val->kategori_id]['is_user'] = 1;
                $arrCycling[$val->kategori_id]['detail'][] = $val;
                $arrCycling[$val->kategori_id]['leng'] = count($arrCycling[$val->kategori_id]['detail']);
            }
        }
    }

    $params = ['m_event_id'=> 75, 'tipe' => 'km_terbanyak'];
    $communityCycling = communityFun($params);

    usort($arrCycling, function ($a, $b) {
        return $b['id'] - $a['id'];
    });

    $arrComCycling = [];
    if (!empty($communityCycling)) {
        $arrComCycling[0]['id'] = 0;
        $arrComCycling[0]['nama'] = 'Community';
        $arrComCycling[0]['alias'] = 'Community';
        $arrComCycling[0]['is_user'] = 0;
        $arrComCycling[0]['detail'] = $communityCycling;
        $arrComCycling[0]['leng'] = 1;
    }
    
    if (!empty($arrCycling)) {
        $arrCycling = array_merge($arrCycling, $arrComCycling);
    }

    if (empty($arrRunning)) {
        $arrRunning = $arrKategori;
    }
    if (empty($arrCycling)) {
        $arrCycling = $arrKategori;
    }
    $time = microtime();
    $time = explode(' ', $time);
    $time = $time[1] + $time[0];
    $finish = $time;
    $total_time = round(($finish - $start), 4);
    return successResponse($response, ['listRunning' => array_values($arrRunning), 'listCycling' => array_values($arrCycling), 'is_team' => 0, 'kategori_race' => array_values($arrKategori), 'event' => $event, 'time' => $total_time]);
});
$app->get('/report/leaderboard_kekl2', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $time = microtime();
    $time = explode(' ', $time);
    $time = $time[1] + $time[0];
    $start = $time;
    $event = [
        'run' => 'RUNNING',
        'cyc' => 'CYCLING'
    ];
    $race_kategori = $db->select('m_event_race.id, m_race_kategori.id kategori_id, m_race_kategori.nama race_name')
    ->from('m_event_race')
    ->leftJoin('m_race_kategori', 'm_event_race.m_race_kategori_id = m_race_kategori.id')
    ->customwhere('m_event_race.m_event_id IN (77,75)', "AND")
    ->findAll();

    $arrKategori = [];
    if (!empty($race_kategori)) {
        foreach ($race_kategori as $k => $v) {
            if (!empty($v->race_name)) {
                $arrKategori[$v->kategori_id]['id'] = $v->kategori_id;
                $arrKategori[$v->kategori_id]['nama'] = $v->race_name;
                $arrKategori[$v->kategori_id]['alias'] = str_replace(' ', '_', $v->race_name);
                $arrKategori[$v->kategori_id]['detail'] = [];
                $arrKategori[$v->kategori_id]['leng'] = 0;
            }
        }
    }

    $db->select('
    (select IFNULL(SUM(m_event_data.jarak), 0) jarak
    from m_event_data
    where m_event_data.m_event_id = t_order.m_event_id AND
    m_event_data.m_user_id = t_order.m_user_id AND
    m_event_data.status = "approve"
    ) as jarak,
    (select IFNULL(SUM(m_event_data.calories), 0) calories
    from m_event_data
    where m_event_data.m_event_id = t_order.m_event_id AND
    m_event_data.m_user_id = t_order.m_user_id AND
    m_event_data.status = "approve"
    ) as cal,
    t_order.id,
    t_order.m_event_id,
    t_order.m_user_id,
    t_order.team_name,
    m_user.nama as nama_user,
    m_user.no_hp,
    t_order.ebib_no,
    m_user.email,
    m_event.nama as nama_event,
    m_event.is_team,
    m_event_race.m_race_kategori_id kategori_id,
    m_club.nama nama_club
    ')
        ->from('t_order')
        ->innerJoin('m_user', 't_order.m_user_id = m_user.id')
        ->leftJoin('m_club_member', 'm_club_member.m_user_id = m_user.id')
        ->leftJoin('m_club', 'm_club.id = m_club_member.m_club_id')
        ->innerJoin('m_event', 't_order.m_event_id = m_event.id')
        ->leftJoin('t_confirm_order', 't_confirm_order.t_order_id = t_order.id')
        ->leftJoin('m_event_race', 'm_event_race.id = t_order.m_event_race_id')
        ->customwhere('m_event_race.m_event_id IN (77,75)', "AND")
        ->where("t_confirm_order.status", "=", "Approved");

    $db->orderBy('jarak DESC');
    $db->groupBy('kategori_id, m_user.id');
    $data = $db->findAll();
  
    $arrRunning = $arrCycling = [];
    if (!empty($data)) {
        foreach ($data as $key => $val) {
            $val->jarak = round($val->jarak, 2);
            $val->cal = round($val->cal, 2);

            if (isset($arrKategori[$val->kategori_id]['nama'])) {
                if ($val->m_event_id == 77) {
                    $event['run'] = $val->nama_event;
                    $arrRunning[$val->kategori_id]['id'] = $val->kategori_id;
                    $arrRunning[$val->kategori_id]['nama'] = $arrKategori[$val->kategori_id]['nama'];
                    $arrRunning[$val->kategori_id]['alias'] = $arrKategori[$val->kategori_id]['alias'];
                    $arrRunning[$val->kategori_id]['is_user'] = 1;
                    $arrRunning[$val->kategori_id]['detail'][] = $val;
                    $arrRunning[$val->kategori_id]['leng'] = count($arrRunning[$val->kategori_id]['detail']);
                } else{
                    $event['cyc'] = $val->nama_event;
                    $arrCycling[$val->kategori_id]['id'] = $val->kategori_id;
                    $arrCycling[$val->kategori_id]['nama'] = $arrKategori[$val->kategori_id]['nama'];
                    $arrCycling[$val->kategori_id]['alias'] = $arrKategori[$val->kategori_id]['alias'];
                    $arrCycling[$val->kategori_id]['is_user'] = 1;
                    $arrCycling[$val->kategori_id]['detail'][] = $val;
                    $arrCycling[$val->kategori_id]['leng'] = count($arrCycling[$val->kategori_id]['detail']);
                }
            }
        }
    }

    $params = ['m_event_id'=> 77, 'tipe' => 'km_terbanyak'];
    $communityRunning = communityFun($params);

    $arrComRunning = [];
    if (!empty($communityRunning)) {
        $arrComRunning[0]['id'] = 0;
        $arrComRunning[0]['nama'] = 'Community';
        $arrComRunning[0]['alias'] = 'Community';
        $arrComRunning[0]['is_user'] = 0;
        $arrComRunning[0]['detail'] = $communityRunning;
        $arrComRunning[0]['leng'] = 1;
    }

    usort($arrRunning, function ($a, $b) {
        return $b['id'] - $a['id'];
    });

    if (!empty($arrRunning)) {
        $arrRunning = array_merge($arrRunning, $arrComRunning);
    }

    $params = ['m_event_id'=> 75, 'tipe' => 'km_terbanyak'];
    $communityCycling = communityFun($params);

    usort($arrCycling, function ($a, $b) {
        return $b['id'] - $a['id'];
    });

    $arrComCycling = [];
    if (!empty($communityCycling)) {
        $arrComCycling[0]['id'] = 0;
        $arrComCycling[0]['nama'] = 'Community';
        $arrComCycling[0]['alias'] = 'Community';
        $arrComCycling[0]['is_user'] = 0;
        $arrComCycling[0]['detail'] = $communityCycling;
        $arrComCycling[0]['leng'] = 1;
    }
    
    if (!empty($arrCycling)) {
        $arrCycling = array_merge($arrCycling, $arrComCycling);
    }

    if (empty($arrRunning)) {
        $arrRunning = $arrKategori;
    }
    if (empty($arrCycling)) {
        $arrCycling = $arrKategori;
    }
    $time = microtime();
    $time = explode(' ', $time);
    $time = $time[1] + $time[0];
    $finish = $time;
    $total_time = round(($finish - $start), 4);
    return successResponse($response, ['listRunning' => array_values($arrRunning), 'listCycling' => array_values($arrCycling), 'is_team' => 0, 'kategori_race' => array_values($arrKategori), 'event' => $event, 'time' => $total_time]);
});