<?php 
use Service\Db;
use Service\Firebase;
use Service\Landa;

$app->get('/report_global/event', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $data = $db->select('id, nama')
        ->from('m_event')
        ->where("is_deleted", "=", 0);

        if (isset($params['status'])) {
            $db->andwhere("status", "=", $params['status']);
        } else{
            $db->customWhere("status IN ('active', 'finished')", "AND");
        }

    if (isset($_SESSION['user']['m_roles_id']) && !empty($_SESSION['user']['m_roles_id']) && $_SESSION['user']['m_roles_id'] > 3 && isset($_SESSION['user']['m_roles_event'])) {
        $db->customWhere("m_event.id IN (" . $_SESSION['user']['m_roles_event'] . ")", "AND");
    }

    if (isset($params['m_event_id']) && !empty($params['m_event_id'])) {
        $db->where("id", "=", $params['m_event_id']);
    }

    $db->orderBy('id DESC');
    $models = $data->findAll();
    return successResponse($response, $models);
});
?>