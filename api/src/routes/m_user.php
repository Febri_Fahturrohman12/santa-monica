<?php

use Service\Db;
use Model\User;


$app->get('/m_user/index', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $user = new User();

    $getData = $user->getAll($params);

    return successResponse($response, [
        'list' => $getData['data'],
        'totalItems' => $getData['totalItem']
    ]);
});

$app->post("/m_user/save", function ($request, $response) {
    $params = $request->getParams();
    $user = new User();

    $validasi = $user->validasi($params);

    if (true === $validasi) {
        $save = $user->save($params);

        if ($save['status']) {
            return successResponse($response, $save['data']);
        }

        return unprocessResponse($response, [$save['error']]);
    }

    return unprocessResponse($response, $validasi);

});

$app->post("/m_user/delete", function ($request, $response) {
    $params = $request->getParams();
    $user = new User();

    $model = $user->delete($params);

    if ($model['status']) {
        return successResponse($response, $model['data']);
    }

    return unprocessResponse($response, [$model['error']]);
});

$app->post("/m_user/restore", function ($request, $response) {
    $params = $request->getParams();
    $user = new User();

    $model = $user->restore($params);

    if ($model['status']) {
        return successResponse($response, $model['data']);
    }

    return unprocessResponse($response, [$model['error']]);
});


$app->get("/m_user/getHistori", function ($request, $response) {
    $data = $request->getParams();
    $db = Db::db();

    $val = getHistori('m_user_id', 0, 'nama', $data['m_user_id']);
    $model = $val['data'];
    return successResponse($response, $model);
});

?>