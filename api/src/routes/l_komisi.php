<?php

use Service\Db;
use Model\LaporanKomisi;

$app->get('/l_komisi/index', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $komisi = new LaporanKomisi();

    $getData = $komisi->getAll($params);
    if (!isset($params['is_export']) || empty($params['is_export'])) {
        return successResponse($response, $getData);
    }
});

$app->post('/l_komisi/hapus', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $komisi = new LaporanKomisi();

    $getData = $komisi->hapus($params);

    return successResponse($response, $getData);
});


?>
