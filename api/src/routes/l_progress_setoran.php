<?php

use Service\Db;
use Model\LaporanProgress;

$app->get('/l_progress_setoran/getAnggota', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $laporanprogress = new LaporanProgress();
    $getData = $laporanprogress->getAnggota();

    return successResponse($response, [
        'list' => $getData['data']
    ]);
});

$app->get('/l_progress_setoran/getAll', function ($request, $response) {
    $params = $request->getParams();
    $laporanprogress = new LaporanProgress();
    $mulai = $params['tahun'].'-'.$params['bulan'].'-1';
    $selesai = date('Y-m-t', strtotime($params['tahun'].'-'.$params['bulan'].'-1'));
    
    $params['tanggal_mulai'] = date('Y-m-d', strtotime($mulai));
    $params['tanggal_selesai'] = $selesai;
    $params['bulan'] = isset($params['bulan']) ? $params['bulan'] : '';
    $params['tahun'] = isset($params['tahun']) ? $params['tahun'] : '';
    
    $getData = $laporanprogress->getAll($params);
    
    if (isset($params['is_export']) && 1 == $params['is_export']) {
        $view = twigView();
        $content = $view->fetch('phpexcelformat/jadwal.html', [
            'data' => $getData['list'],
            'periode' => $getData['periode'],
            'listJudul' => $getData['listJudul'],
        ]);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        if (null !== $getData['periode']) {
            header('Content-Disposition: attachment;Filename="Laporan Jadwal ('.$getData['periode'].')-'.date('Y-m-d H:i:s').'.xls"');
        } else {
            header('Content-Disposition: attachment;Filename="Laporan Jadwal (Semua Periode)-'.date('Y-m-d H:i:s').'.xls"');
        }
        echo $content;
    } else {
        return successResponse($response, ['list' => $getData['list'], 'periode' => $getData['periode'], 'listJudul' => $getData['listJudul']]);
    }
});

?>
