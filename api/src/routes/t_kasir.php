<?php

use Service\Db;
use Model\Kasir;

$app->get('/t_kasir/getTransaksi', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $kasir = new Kasir();

    $getData = $kasir->getTransaksi($params);

    return successResponse($response, $getData);
});

$app->get('/t_kasir/getLayanan', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $kasir = new Kasir();

    $getData = $kasir->getLayanan($params);

    return successResponse($response, [
        'list' => $getData['data']
    ]);
});

$app->get('/t_kasir/getBarang', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $kasir = new Kasir();

    $getData = $kasir->getBarang();

    return successResponse($response, [
        'list' => $getData['data']
    ]);
});

$app->get('/t_kasir/getCaraPembayaran', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $kasir = new Kasir();

    $getData = $kasir->getCaraPembayaran();

    return successResponse($response, [
        'list' => $getData['data']
    ]);
});

$app->get('/t_kasir/getDiskon', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $kasir = new Kasir();

    $getData = $kasir->getDiskon();

    return successResponse($response, [
        'list' => $getData['data']
    ]);
});

/**
 * CHECKOUT
 */

$app->post('/t_kasir/checkout', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $kasir = new Kasir();
//print_die($params);
    /**
     * VALIDATE
     */

    $validasi = $kasir->validasi($params["checkout"]);

    if ($validasi === true) {
        try {
            /**
             * CEK STOK
             */

            if (!empty($params['listBarang'])) {
                $cekStok = $kasir->cekStok($params['listBarang']);

                if (isset($cekStok['status']) && $cekStok['status'] === true) {
                    return unprocessResponse($response, $cekStok['erroMessage']);
                }
            }

            /**
             * CREATE CUSTOMER JIKA CUSTOMER BARU
             */


            /**
             * CREATE PENJUALAN
             */

            $createPenjualan = $kasir->createPenjualan($params['checkout']);
            if (isset($createPenjualan['error'])) {
                return unprocessResponse($response, [$createPenjualan['error']]);
            }

            /**
             * CREATE DETAIL PENJUALAN
             */

            $createDetailPenjualan = $kasir->createDetailPenjualan($params['listLayanan'], $params['listBarang'], $createPenjualan['data']->id);

            if (isset($createDetailPenjualan['error'])) {
                return unprocessResponse($response, [$createDetailPenjualan['error']]);
            }

            /**
             * CREATE CARA PEMBAYARAN
             */

            $createCaraPembayaran = $kasir->createCaraPembayaran($params['checkout'], $params['cara_pembayaran'], $createPenjualan['data']->id);

            if (isset($createCaraPembayaran['error'])) {
                return unprocessResponse($response, [$createCaraPembayaran['error']]);
            }

            /**
             * CREATE KARTU STOK
             */

            if (isset($params['listBarang']) && !empty($params['listBarang'])) {
                $createKartuStok = $kasir->createKartuStok($params['checkout'], $params['listBarang'], $createPenjualan['data']);
                if (isset($createKartuStok['error'])) {
                    return unprocessResponse($response, [$createKartuStok['error']]);
                }
            }


            return successResponse($response, $createPenjualan['data']);

        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server" . $e]);
        }
    };
});


?>
