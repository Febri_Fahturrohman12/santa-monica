<?php

use Service\Db;
use Model\Penerimaan;
use Service\LandaInv;
use Service\Landa;


$app->get('/t_penerimaan/index', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $user = new Penerimaan();

    $getData = $user->getAll($params);

    return successResponse($response, [
        'list' => $getData['data'],
        'totalItems' => $getData['totalItem']
    ]);
});

$app->get('/t_penerimaan/getBarang', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $peneriman = new Penerimaan();

    $getData = $peneriman->getBarang();

    return successResponse($response, [
        'list' => $getData['data']
    ]);
});

/**
 * SAVE PENERIMAAN
 */

$app->post("/t_penerimaan/save", function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $landa = new Landa();
    $peneriman = new Penerimaan();
    $inv = new LandaInv();
    /**
     * SIAPKAN DATA
     */
    $params["data"]["status"] = isset($params["status"]) ? $params["status"] : "draft";
    $params["data"]["tanggal"] = $landa->arrayToDate($params["data"]["tanggal"]);
    $params["data"]["no_invoice"] = isset($params["data"]["no_invoice"]) ? $params["data"]["no_invoice"] : "";


    /**
     * VALIDASI DATA
     */
    $validasi = $peneriman->validasi($params["data"]);

    if ($validasi === true) {
        try {
            /**
             * CEK NO INVOICE
             */
            $cekNoInvoice = $peneriman->cekNoInvoice($params['data']);

            if (isset($cekNoInvoice['error'])) {
                return unprocessResponse($response, $cekNoInvoice['error']);
            }

            $validasi_detail = $peneriman->validasi_detail($params['detail']);

            if ($validasi_detail === true) {


                $save = $peneriman->save($params);

                if (isset($save['error'])){
                    return unprocessResponse($response, [$save['error']]);
                }

                /**
                 * SIMPAN KE KARTU STOK
                 */

                if ($params['status'] == "selesai" && isset($save['item']) && !empty($save['item'])) {
                    $db->update("inv_penerimaan", [
                        "approved_by" => $_SESSION["user"]["id"],
                        "approved_at" => strtotime("now"),
                    ], ["id" => $save["data"]->id]);

                    $inv->insertKartuStok($save['item'], "inv_penerimaan_id", $save['data']->id, $save['data']->kode, "Penerimaan Item", "masuk", $params["data"]["tanggal"]);
                }

                return successResponse($response, $save['data']);
            } else {
                return unprocessResponse($response, ["Lengkapi detail"]);
            }
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server" . $e]);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->post("/t_penerimaan/saveStatus", function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $landa = new Landa();
    $peneriman = new Penerimaan();
    $inv = new LandaInv();

    try {

        /**
         * Cek apakah stok sudah digunakan
         */
        $cek = $peneriman->checkAvailabelUnpostIn("inv_penerimaan_id", $params);
        if (!$cek) {
            return unprocessResponse($response, ["Stok sudah dikeluarkan pada transaksi yang lain"]);
        } else {
            $hapus["status"] = "batal";
            $hapus["deleted_at"] = strtotime("now");
            $hapus["deleted_by"] = $_SESSION["user"]["id"];
            $hapus["deleted_note"] = $params["alasan"];
            $model = $db->update("inv_penerimaan", $hapus, ["id" => $params["id"]]);

            /**
             * Unpost dengan mengeluarkan item
             */
            $inv->unPost("keluar", "inv_penerimaan_id", $model->id, "Unpost Penerimaan");
        }

        return successResponse($response, []);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi kesalahan pada server"]);
    }
    return unprocessResponse($response, ["Terjadi kesalahan pada server"]);
});




?>
