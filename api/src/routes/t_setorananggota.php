<?php

use Service\Db;
use Model\SetoranAnggota;
use Service\LandaInv;
use Service\Landa;


$app->get('/t_setorananggota/index', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $setorananggota = new SetoranAnggota();

    $getData = $setorananggota->getAll($params);

    return successResponse($response, [
        'list' => $getData['data'],
        'totalItems' => $getData['totalItem']
    ]);
});

$app->get('/t_setorananggota/getBarang', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $peneriman = new SetoranAnggota();

    $getData = $peneriman->getBarang();

    return successResponse($response, [
        'list' => $getData['data']
    ]);
});
$app->get('/t_setorananggota/getDetail', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $peneriman = new SetoranAnggota();
    $getData = $peneriman->getDetail($params['id']);

    return successResponse($response, [
        'list' => $getData['data']
    ]);
});
$app->get('/t_setorananggota/getSetoran', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $peneriman = new SetoranAnggota();
    $getData = $peneriman->getSetoran();

    return successResponse($response, [
        'list' => $getData['data']
    ]);
});
$app->get('/t_setorananggota/getPJ', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $peneriman = new SetoranAnggota();
    $getData = $peneriman->getPJ();

    return successResponse($response, [
        'list' => $getData['data']
    ]);
});

/**
 * SAVE Setoran
 */

$app->post("/t_setorananggota/save", function ($request, $response) {
    $params = $request->getParams();
    $landa = new Landa();
    $peneriman = new SetoranAnggota();
    
    
    /**
     * SIAPKAN DATA
     */
    $params["data"]['m_user_id'] = $_SESSION['user']['id'];
    $params["data"]["tanggal"] = $landa->arrayToDate($params["data"]["tanggal"]);
    $params["data"]['status'] = 0;
    
    /**
     * VALIDASI DATA
     */
    $validasi = $peneriman->validasi($params["data"]);

    if ($validasi === true) {
        try {
            $save = $peneriman->save($params);

            if (isset($save['error'])){
                return unprocessResponse($response, [$save['error']]);
            }
            return successResponse($response, $save['data']);
            
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server" . $e]);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->post("/t_setorananggota/delete", function ($request, $response) {
    $params = $request->getParams();
    $setoran = new SetoranAnggota();
    
    $model = $setoran->delete($params);

    if ($model['status']) {
        return successResponse($response, $model['data']);
    }

    return unprocessResponse($response, [$model['error']]);
});

$app->post("/t_setorananggota/restore", function ($request, $response) {
    $params = $request->getParams();
    $setoran = new SetoranAnggota();

    $model = $setoran->restore($params);

    if ($model['status']) {
        return successResponse($response, $model['data']);
    }

    return unprocessResponse($response, [$model['error']]);
});
$app->post("/t_setorananggota/approve", function ($request, $response) {
    $params = $request->getParams();
    $setoran = new SetoranAnggota();

    $model = $setoran->approve($params);

    if ($model['status']) {
        return successResponse($response, $model['data']);
    }

    return unprocessResponse($response, [$model['error']]);
});
?>
