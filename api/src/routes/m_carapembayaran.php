<?php

use Service\Db;
use Model\CaraPembayaran;
use Service\Landa;

$app->get('/m_carapembayaran/index', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $carapembayaran = new CaraPembayaran();

    $getData = $carapembayaran->getAll($params);

    return successResponse($response, [
        'list' => $getData['data'],
        'totalItems' => $getData['totalItem']
    ]);
});

$app->post("/m_carapembayaran/save", function ($request, $response) {
    $params = $request->getParams();
    $landa = new Landa();
    $carapembayaran = new CaraPembayaran();

    $validasi = $carapembayaran->validasi($params);

    if (true === $validasi) {

        $save = $carapembayaran->save($params);

        if ($save['status']) {
            return successResponse($response, $save['data']);
        }

        return unprocessResponse($response, [$save['error']]);
    }

    return unprocessResponse($response, $validasi);

});

$app->post("/m_carapembayaran/delete", function ($request, $response) {
    $params = $request->getParams();
    $carapembayaran = new CaraPembayaran();

    $model = $carapembayaran->delete($params);

    if ($model['status']) {
        return successResponse($response, $model['data']);
    }

    return unprocessResponse($response, [$model['error']]);
});

$app->post("/m_carapembayaran/restore", function ($request, $response) {
    $params = $request->getParams();
    $carapembayaran = new CaraPembayaran();

    $model = $carapembayaran->restore($params);

    if ($model['status']) {
        return successResponse($response, $model['data']);
    }

    return unprocessResponse($response, [$model['error']]);
});


$app->get("/m_carapembayaran/getHistori", function ($request, $response) {
    $data = $request->getParams();
    $db = Db::db();

    $val = getHistori('m_barang_id', 0, 'nama', $data['m_barang_id']);
    $model = $val['data'];
    return successResponse($response, $model);
});

?>