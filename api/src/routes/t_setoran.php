<?php

use Service\Db;
use Model\Setoran;
use Service\LandaInv;
use Service\Landa;


$app->get('/t_setoran/index', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $user = new Setoran();

    $getData = $user->getAll($params);

    return successResponse($response, [
        'list' => $getData['data'],
        'totalItems' => $getData['totalItem']
    ]);
});

$app->get('/t_setoran/getBarang', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $peneriman = new Setoran();

    $getData = $peneriman->getBarang();

    return successResponse($response, [
        'list' => $getData['data']
    ]);
});
$app->get('/t_setoran/getDetail', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $peneriman = new Setoran();
    $getData = $peneriman->getDetail($params['id']);

    return successResponse($response, [
        'list' => $getData['data']
    ]);
});

/**
 * SAVE Setoran
 */

$app->post("/t_setoran/save", function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $landa = new Landa();
    $peneriman = new Setoran();
    $inv = new LandaInv();
    /**
     * SIAPKAN DATA
     */
    $params["data"]["tanggal"] = $landa->arrayToDate($params["data"]["tanggal"]);

    /**
     * VALIDASI DATA
     */
    $validasi = $peneriman->validasi($params["data"]);

    if ($validasi === true) {
        try {
            $validasi_detail = $peneriman->validasi_detail($params['detail']);

            if ($validasi_detail === true) {
                $save = $peneriman->save($params);

                if (isset($save['error'])){
                    return unprocessResponse($response, [$save['error']]);
                }
                return successResponse($response, $save['data']);
            } else {
                return unprocessResponse($response, ["Lengkapi detail"]);
            }
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server" . $e]);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->post("/t_setoran/delete", function ($request, $response) {
    $params = $request->getParams();
    $setoran = new Setoran();

    $model = $setoran->delete($params);

    if ($model['status']) {
        return successResponse($response, $model['data']);
    }

    return unprocessResponse($response, [$model['error']]);
});

$app->post("/t_setoran/restore", function ($request, $response) {
    $params = $request->getParams();
    $setoran = new Setoran();

    $model = $setoran->restore($params);

    if ($model['status']) {
        return successResponse($response, $model['data']);
    }

    return unprocessResponse($response, [$model['error']]);
});
?>
