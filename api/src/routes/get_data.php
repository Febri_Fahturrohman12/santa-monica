<?php

use Service\Db;
use Service\Landa;


$app->get('/get_data/hak_akses', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $data = $db->select('*')
        ->from('m_roles')
        ->where("is_deleted", "=", 0);
    if (isset($params['id'])) {
        $data->andwhere('id', '=', $params['id']);
    }
    $models = $data->findAll();
    return successResponse($response, $models);
});

$app->get('/get_data/provinsi', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $data = $db->select('*')
        ->from('w_provinsi');
    $db->orderBy('provinsi');
    $models = $data->findAll();
    return successResponse($response, $models);
});

$app->get('/get_data/kota', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $data = $db->select('*')
        ->from('w_kota');
    $db->orderBy('kota');
    if (isset($params['provinsi_id']) && !empty($params['provinsi_id'])) {
        $db->where("provinsi_id", "=", $params['provinsi_id']);
    }

    $models = $data->findAll();
    $models = json_decode(json_encode($models), true);

    return successResponse($response, $models);
});

$app->get('/get_data/kecamatan', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $data = $db->select('*')
        ->from('w_kecamatan');
    $db->orderBy('kecamatan');
    if (isset($params['kota_id']) && !empty($params['kota_id'])) {
        $db->where("kota_id", "=", $params['kota_id']);
    }

    $models = $data->findAll();
    return successResponse($response, $models);
});

$app->get('/get_data/desa', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();

    $data = $db->select('*')
        ->from('w_desa');
    $db->orderBy('desa');

    if (isset($params['kecamatan_id']) && !empty($params['kecamatan_id'])) {
        $db->where("kecamatan_id", "=", $params['kecamatan_id']);
    }
    $models = $data->findAll();

    return successResponse($response, $models);
});


$app->get('/get_data/user', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $data = $db->select('*')
        ->from('m_user')
        ->where("is_deleted", "=", 0);

    if (isset($params['m_user_id']) && !empty($params['m_user_id'])) {
        $db->where("id", "=", $params['m_user_id']);
    }

    $models = $data->findAll();
    return successResponse($response, $models);
});

$app->get('/get_data/setting', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $model = $db->select('*')
        ->from('m_setting')
        ->find();

    return successResponse($response, $model);
})->setName('setting');

$app->get('/get_data/getCustomerSearch', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $db->select('id,nama,no_telepon, nama as nama_view,tipe')
        ->from('m_customer')
        ->where("m_customer.is_deleted", "=", 0);

    if (isset($params['nama']) && !empty($params['nama'])){
        $db->where("m_customer.nama","like", $params['nama']);
    }


    $model = $db->findAll();

    return successResponse($response, $model);
})->setName('customer');
