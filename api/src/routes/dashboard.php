<?php

use Service\Db;
use Service\Firebase;
use Service\Landa;
use Model\Dashboard;

$app->get('/dashboard/index', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();


    $datenow['day'] = date('l');
    $datenow['date'] = date('d');
    $datenow['month'] = date('F');
    $datenow['year'] = date('Y');

    $imagedefault = site_api_url() . 'assets/img/default.png';

    $datauser = $db->select('m_user.*, m_roles.nama as nama_roles')
        ->from('m_user')
        ->leftJoin("m_roles", "m_roles.id = m_user.m_roles_id")
        ->where('m_user.id', '=', $_SESSION['user']['id'])
        ->find();

    return successResponse($response, [
        'datenow' => $datenow,
        'imagedefault' => $imagedefault,
        'datauser' => $datauser
    ]);
});

$app->get('/dashboard/getAnggota', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();

    $db->select('*')
        ->from('m_user');
    
    $filter = (array)json_decode($params["filter"]);
    if (isset($params) && !empty($params)) {
        foreach ($filter as $key => $val) {
            if ('namaanggota' == $key) {
                $db->where('nama', 'like', $val);
            } 
        }
    }
    
    $db->orderBy('nama ASC');
    $dataanggota = $db->findAll();
    $totalanggota = $db->count();

    return successResponse($response, [
        'list' => $dataanggota,
        'total' => $totalanggota
    ]);
});

$app->get('/dashboard/getBarang', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();

    $db->select('*')
        ->from('m_barang');
    
    $filter = (array)json_decode($params["filter"]);
    if (isset($params) && !empty($params)) {
        foreach ($filter as $key => $val) {
            if ('namabarang' == $key) {
                $db->where('nama', 'like', $val);
            } 
        }
    }
    $db->orderBy('nama ASC');
    $dataanggota = $db->findAll();
    $totalanggota = $db->count();

    return successResponse($response, [
        'list' => $dataanggota,
        'total' => $totalanggota
    ]);
});

$app->get('/dashboard/cekSetoran', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();

    $db->select('t_setor_anggota.*,pj.nama as nm_pj')
        ->from('t_setor_anggota')
        ->leftJoin("m_user as pj", "pj.id = t_setor_anggota.m_pj_id")
        ->where('t_setor_anggota.m_user_id', '=', $_SESSION['user']['id'])
        ->andWhere('t_setor_anggota.is_deleted', '=', 0);
    
    $filter = (array)json_decode($params["filter"]);
    if (isset($params) && !empty($params)) {
        foreach ($filter as $key => $val) {
            if ('t_setoran_id' == $key) {
                $db->where('t_setor_anggota.t_setoran_id', 'like', $val);
            } 
        }
    }
    $datasetor = $db->find();
    
    if($datasetor){
        $listbarang = $db->select('t_setoran_detail.*, m_barang.nama as nm_barang')
                    ->from('t_setoran_detail')
                    ->leftJoin("m_barang", "m_barang.id = t_setoran_detail.m_barang_id")
                    ->where('t_setoran_detail.t_setoran_id', '=', $datasetor->t_setoran_id)
                    ->findAll();
        if($datasetor->status == 0){
            return successResponse($response, [
                'list' => $datasetor,
                'available' => 1,
                'keterangan' => "Mantap brader udah disetor, Tapi belum di acc tuh PJ nya tanyain si ".$datasetor->nm_pj,
                'barangsetoran' => $listbarang
            ]);
        }else {
            
            return successResponse($response, [
                'list' => $datasetor,
                'available' => 1,
                'keterangan' => "Mantap brader udah disetor dan di acc ".$datasetor->nm_pj,
                'barangsetoran' => $listbarang
            ]);
        }
    }else {
        $filter = (array)json_decode($params["filter"]);
        $listbarang = $db->select('t_setoran_detail.*, m_barang.nama as nm_barang')
                    ->from('t_setoran_detail')
                    ->leftJoin("m_barang", "m_barang.id = t_setoran_detail.m_barang_id")
                    ->where('t_setoran_detail.t_setoran_id', '=', $filter['t_setoran_id'])
                    ->findAll();

        return successResponse($response, [
            'list' => $datasetor,
            'available' => 0,
            'keterangan' => "Yaelahh... Hari gini cuma mau inpo perang doang lu ???",
            'barangsetoran' => $listbarang
        ]);
    }
});

