<?php

use Service\Db;
use Model\Diskon;
use Service\Landa;

$app->get('/m_diskon/index', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $diskon = new Diskon();

    $getData = $diskon->getAll($params);

    return successResponse($response, [
        'list' => $getData['data'],
        'totalItems' => $getData['totalItem']
    ]);
});

$app->post("/m_diskon/save", function ($request, $response) {
    $params = $request->getParams();
    $landa = new Landa();
    $diskon = new Diskon();

    $validasi = $diskon->validasi($params);

    if (true === $validasi) {
        // Upload foto diskon
        if (isset($params['foto']) && !empty($params['foto'])) {
            $path = 'assets/img/diskon/';
            $uploadFile = $landa->base64ToImage($path, $params['foto']);
            if ($uploadFile['status']) {
                $params['foto'] = $uploadFile['data'];
            } else {
                return unprocessResponse($response, [$uploadFile['error']]);
            }
        }

        $save = $diskon->save($params);

        if ($save['status']) {
            return successResponse($response, $save['data']);
        }

        return unprocessResponse($response, [$save['error']]);
    }

    return unprocessResponse($response, $validasi);

});

$app->post("/m_diskon/delete", function ($request, $response) {
    $params = $request->getParams();
    $diskon = new Diskon();

    $model = $diskon->delete($params);

    if ($model['status']) {
        return successResponse($response, $model['data']);
    }

    return unprocessResponse($response, [$model['error']]);
});

$app->post("/m_diskon/restore", function ($request, $response) {
    $params = $request->getParams();
    $diskon = new Diskon();

    $model = $diskon->restore($params);

    if ($model['status']) {
        return successResponse($response, $model['data']);
    }

    return unprocessResponse($response, [$model['error']]);
});


$app->get("/m_diskon/getHistori", function ($request, $response) {
    $data = $request->getParams();
    $db = Db::db();

    $val = getHistori('m_diskon_id', 0, 'nama', $data['m_diskon_id']);
    $model = $val['data'];
    return successResponse($response, $model);
});

?>