<?php

namespace Service;

use Google\Cloud\Storage\StorageClient;

/**
 * Class landa digunakan untuk menyimpan method global.
 */
class Landa
{
    /**
     * Upload gambar.
     *
     * @param mixed $path
     * @param mixed $base64
     *
     * @return string nama file
     */
    public function base64ToImage($path, $base64)
    {
        try {
            $image_parts = explode(';base64,', $base64);
            $image_type_aux = explode('image/', $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $namaFile = uniqid() . '.' . $image_type;
            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            $file = $path . $namaFile;
            file_put_contents($file, $image_base64);

            return [
                'status' => true,
                'data' => $namaFile,
            ];
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }

    /**
     * buildTree.
     *
     * @param array $elements array yang akan dijadikan struktur tree
     * @param int $parentId id induk
     *
     * @return array
     */
    public function buildTree(array $elements, $parentId = 0)
    {
        $branch = [];
        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                } else {
                    $element['children'] = [];
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

    /**
     * Simpan base 64 ke file PDF.
     *
     * @param array $base64
     * @param string $path
     * @param string $custom_name
     *
     * @return array
     */
    public function saveBase64TMP($base64, $path, $custom_name = null)
    {
        try {
            if (isset($base64['base64'])) {
                $extension = substr($base64['filename'], strrpos($base64['filename'], ',') + 1);

                if (!empty($custom_name)) {
                    $nama = $custom_name;
                } else {
                    $nama = $base64['filename'];
                }
                $file = base64_decode($base64['base64']);
                file_put_contents($path . '/' . $nama, $file);

                return [
                    'status' => true,
                    'fileName' => $nama,
                    'filePath' => $path . '/' . $nama,
                ];
            }
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }

    /**
     * base64ToFile.
     *
     * @param mixed $data
     *
     * @return array file detail
     */
    public function base64ToFile($data)
    {
        try {
            if (isset($data['base64']) && !empty($data['base64'])) {
                $batas = strpos($data['base64'], 'base64,');
                $batas_potong = $batas + 7;
                $data['base64'] = substr($data['base64'], $batas_potong);

                $folder = 'assets/excel';

                if (!is_dir($folder)) {
                    mkdir($folder, 0777);
                }

                $save = $this->saveBase64TMP($data, $folder);

                if ($save['status']) {
                    return [
                        'status' => true,
                        'data' => [
                            'fileName' => $save['fileName'],
                            'filePath' => $save['filePath'],
                        ],
                    ];
                }

                return [
                    'status' => false,
                    'error' => $save['error'],
                ];
            }
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }

    public function base64ToFilePath($data, $path, $kode)
    {
        try {
            if (isset($data['base64']) && !empty($data['base64'])) {
                // $batas = strpos($data['base64'], 'base64,');
                // $batas_potong = $batas + 7;
                // $data['base64'] = substr($data['base64'], $batas_potong);

                $folder = $path;

                if (!is_dir($folder)) {
                    mkdir($folder, 0777);
                }
                $nama_custom = $kode . '_' . $data['filename'];

                $save = $this->saveBase64TMP($data, $folder, $nama_custom);

                if ($save['status']) {
                    return [
                        'status' => true,
                        'data' => [
                            'fileName' => $save['fileName'],
                            'filePath' => $save['filePath'],
                        ],
                    ];
                }

                return [
                    'status' => false,
                    'error' => $save['error'],
                ];
            }
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }

    /**
     * Konversi array ke tanggal.
     *
     * @param mixed $tglArr
     */
    public function arrayToDate($tglArr)
    {
        // ej($tglArr);
        if (!empty($tglArr)) {
            $tahun = isset($tglArr['year']) ? $tglArr['year'] : date('Y');
            $bulan = isset($tglArr['month']) ? $tglArr['month'] : date('m');
            $hari = isset($tglArr['day']) ? $tglArr['day'] : date('d');

            return $tahun . '-' . $bulan . '-' . $hari;
        }

        return null;
    }

    public function arrayToTime($timeArr)
    {
        if (!empty($timeArr)) {
            $h = isset($timeArr['hour']) ? $timeArr['hour'] : date('H');
            $i = isset($timeArr['minute']) ? $timeArr['minute'] : date('i');
            $s = isset($timeArr['second']) ? $timeArr['second'] : date('s');

            return $h . ':' . $i . ':' . $s;
        }

        return null;
    }

    public function arrayToDateCustom($tglArr)
    {
//         ej($tglArr);
        if (!empty($tglArr)) {
            $tahun = isset($tglArr['year']) ? $tglArr['year'] : date('Y');
            if (($tglArr['month'] != 10 || $tglArr['month'] != 11 || $tglArr['month'] != 12)) {
                $bulan = isset($tglArr['month']) ? '0' . $tglArr['month'] : date('m');
            } else {
                $bulan = isset($tglArr['month']) ? $tglArr['month'] : date('m');
            }
            $hari = isset($tglArr['day']) ? $tglArr['day'] : date('d');

            return $tahun . '-' . $bulan . '-' . $hari;

        }

        return null;
    }

    public function arrayToDateCustom2($tglArr)
    {
        if (!empty($tglArr)) {
            $tahun = isset($tglArr['year']) ? $tglArr['year'] : date('Y');
            if (($tglArr['month'] != 10 || $tglArr['month'] != 11 || $tglArr['month'] != 12)) {
                $bulan = isset($tglArr['month']) ? '0' . $tglArr['month'] : date('m');
            } else {
                $bulan = isset($tglArr['month']) ? $tglArr['month'] : date('m');
            }
            $hari = isset($tglArr['day']) ? $tglArr['day'] : date('d');

            $arr = $tahun . '-' . $bulan . '-' . $hari;
            return date("Y-m-d", strtotime($arr));
        }

        return null;
    }

    public function objectToDate($tglArr)
    {
        if (!empty($tglArr)) {
            $tahun = isset($tglArr->year) ? $tglArr->year : date('Y');
            $bulan = isset($tglArr->month) ? $tglArr->month : date('m');
            $hari = isset($tglArr->day) ? $tglArr->day : date('d');

            return $tahun . '-' . $bulan . '-' . $hari;
        }

        return null;
    }

    /**
     * Enkripsi string.
     *
     * @param string $string
     *
     * @return string
     */
    public function safeString($string)
    {
        $iv = substr(hash('sha256', 'humanis2020*'), 0, 16);

        return openssl_encrypt($string, 'AES-256-CBC', 'encryptionhash', 0, $iv);
    }

    /**
     * Decript string.
     *
     * @param string $encryptedText
     *
     * @return string
     */
    public function unsafeString($encryptedText)
    {
        $iv = substr(hash('sha256', 'humanis2020*'), 0, 16);

        return openssl_decrypt($encryptedText, 'AES-256-CBC', 'encryptionhash', 0, $iv);
    }

    /**
     * id Karyawan.
     *
     * @param null|mixed $userId
     * @param null|mixed $clientId
     *
     * @return string
     */
    public function idKaryawan($userId = null, $clientId = null)
    {
        $user = empty($userId) ? $_SESSION['user']['id'] : $userId;
        $client = empty($clientId) ? $_SESSION['user']['client'] : $clientId;

        return $user . '_' . $client;
    }

    public function indonesian_date($timestamp = '', $date_format = 'j F Y', $suffix = '')
    {
        if ('' == trim($timestamp)) {
            $timestamp = time();
        } elseif (!ctype_digit($timestamp)) {
            $timestamp = strtotime($timestamp);
        }
        // remove S (st,nd,rd,th) there are no such things in indonesia :p
        $date_format = preg_replace('/S/', '', $date_format);
        $pattern = [
            '/Mon[^day]/', '/Tue[^sday]/', '/Wed[^nesday]/', '/Thu[^rsday]/',
            '/Fri[^day]/', '/Sat[^urday]/', '/Sun[^day]/', '/Monday/', '/Tuesday/',
            '/Wednesday/', '/Thursday/', '/Friday/', '/Saturday/', '/Sunday/',
            '/Jan[^uary]/', '/Feb[^ruary]/', '/Mar[^ch]/', '/Apr[^il]/', '/May/',
            '/Jun[^e]/', '/Jul[^y]/', '/Aug[^ust]/', '/Sep[^tember]/', '/Oct[^ober]/',
            '/Nov[^ember]/', '/Dec[^ember]/', '/January/', '/February/', '/March/',
            '/April/', '/June/', '/July/', '/August/', '/September/', '/October/',
            '/November/', '/December/',
        ];
        $replace = ['Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab', 'Min',
            'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu',
            'Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des',
            'Januari', 'Februari', 'Maret', 'April', 'Juni', 'Juli', 'Agustus', 'Sepember',
            'Oktober', 'November', 'Desember',
        ];
        $date = date($date_format, $timestamp);
        $date = preg_replace($pattern, $replace, $date);

        return "{$date} {$suffix}";
    }

    public function base64_to_jpeg($base64_string, $output_file)
    {
        // $tmp_dir = ini_get('upload_tmp_dir') ? ini_get('upload_tmp_dir') : sys_get_temp_dir();
//        get extension
        $image_info = getimagesize($base64_string);
        $extension = (isset($image_info["mime"]) ? explode('/', $image_info["mime"])[1] : "");
        // $extension = explode('/', mime_content_type($base64_string))[1]; // ini bug

        $fileName = $output_file . '.' . $extension;
//        $fileName = "sulip.jpg";
        // open the output file for writing
        $ifp = fopen($fileName, 'wb');
//        print_die($ifp);

        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode(',', $base64_string);

        // we could add validation here with ensuring count( $data ) > 1
        fwrite($ifp, base64_decode($data[1]));

        // clean up the file resource
        fclose($ifp);

        return $fileName;
    }

    public function uploadImage($file, $directory = "sulip", $fileName)
    {
        try {
            $storage = new StorageClient([
                'keyFilePath' => 'src/santamonica-mobile-app-82658f7ed15c.json',
            ]);

            $host = $_SERVER['SERVER_NAME'];
            $arrHost = (explode(".", $host));
            if ($arrHost[0] == "localhost") {
                $bucketName = 'santamonica-image-test';
            } else if ($arrHost[0] == "test") {
                $bucketName = 'santamonica-image-test';
            } else if ($arrHost[0] == "api") {
                $bucketName = 'santamonica-image';
            } else {
                $bucketName = 'santamonica-image';
            }

            $bucket = $storage->bucket($bucketName);
            $object = $bucket->upload(
                fopen($fileName, 'r'), [
//            rename
                    'name' => "$directory/$fileName",
//            public access
                    'predefinedAcl' => 'publicRead'
                ]
            );

            // unlink this file after upload on google storage
            unlink($file);

//            echo "File uploaded successfully. File path is: https://storage.googleapis.com/$bucketName/$fileName";
            return $fileName;
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function uploadFile($file, $directory = "sulip", $fileName)
    {
        try {
            $storage = new StorageClient([
                'keyFilePath' => 'src/santamonica-mobile-app-82658f7ed15c.json',
            ]);

            $host = $_SERVER['SERVER_NAME'];
            $arrHost = (explode(".", $host));
            if ($arrHost[0] == "localhost") {
                $bucketName = 'santamonica-image-test';
            } else if ($arrHost[0] == "test") {
                $bucketName = 'santamonica-image-test';
            } else if ($arrHost[0] == "api") {
                $bucketName = 'santamonica-image';
            } else {
                $bucketName = 'santamonica-image';
            }
            $bucket = $storage->bucket($bucketName);
            $object = $bucket->upload(
                fopen($file, 'r'), [
//            rename
                    'name' => "$directory/$fileName",
//            public access
                    'predefinedAcl' => 'publicRead'
                ]
            );
//            echo "File uploaded successfully. File path is: https://storage.googleapis.com/$bucketName/$fileName";
            return $fileName;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }


    public function deleteImage($file)
    {
        try {
            $storage = new StorageClient([
                'keyFilePath' => 'src/santamonica-mobile-app-82658f7ed15c.json',
            ]);

            $host = $_SERVER['SERVER_NAME'];
            $arrHost = (explode(".", $host));
            if ($arrHost[0] == "localhost") {
                $bucketName = 'santamonica-image-test';
            } else if ($arrHost[0] == "test") {
                $bucketName = 'santamonica-image-test';
            } else if ($arrHost[0] == "api") {
                $bucketName = 'santamonica-image';
            } else {
                $bucketName = 'santamonica-image';
            }
            $bucket = $storage->bucket($bucketName);
            $object = $bucket->object($file);

            $object->delete();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

    }

    public function cekImage($file)
    {
        try {
            $storage = new StorageClient([
                'keyFilePath' => 'src/santamonica-mobile-app-82658f7ed15c.json',
            ]);

//            print_die($storage);
            $host = $_SERVER['SERVER_NAME'];
            $arrHost = (explode(".", $host));
            if ($arrHost[0] == "localhost") {
                $bucketName = 'santamonica-image-test';
            } else if ($arrHost[0] == "test") {
                $bucketName = 'santamonica-image-test';
            } else if ($arrHost[0] == "api") {
                $bucketName = 'santamonica-image';
            } else {
                $bucketName = 'santamonica-image';
            }
            $bucket = $storage->bucket($bucketName);
            $object = $bucket->object($file);

            if ($object->exists()) {
                return true;
            } else {
                return false;
            }

        } catch (\Exception $e) {
            echo $e->getMessage();
        }

    }
}
