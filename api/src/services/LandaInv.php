<?php

namespace Service;

use Service\Db;

/**
 * Library Stok
 * author : Wahyu Agung Tribawono
 * email : wahyuagun26@gmail.com
 * versi : beta
 */
class LandaInv extends \Cahkampung\Landadb
{
    public function __construct()
    {
        $db_setting = Db::db();
        @parent::__construct($db_setting->dbSetting);
    }

    /**
     * [multiInsert memasukkan banyak data dengan 1 query]
     * @param  [string] $tabel [nama tabel]
     * @param  [array] $data [data yang akan diinput]
     */
    public function multiInsert($tabel, $data)
    {
        /**
         * Prepare data
         */
        foreach ($data as $k => $v) {
            $columName = $dataInsert = [];
            foreach ($v as $key => $val) {
                $columName[] = $key;
                $dataInsert[] = $this->escape($val);
            }
            $columValue[] = "(" . implode(",", $dataInsert) . ")";
        }
        /**
         * Insert data
         */
        if ((isset($columName) && !empty($columName)) && (isset($columValue) && !empty($columName))) {
            $this->run("INSERT INTO " . $tabel . " (" . implode(",", $columName) . ") VALUES " . implode(",", $columValue) . ";");
        }
    }

    /**
     * [unPost mengembalikan stok]
     * @param  [string] $jenis_unpost [tentukan stok baru masuk / keluar]
     * @param  [string] $reff_type [transaksi yang diunpost]
     * @param  [string] $reff_id [id transaksi yang diunpost]
     * @param  [string] $catatan [catatan unpost]
     */
    public function unPost($jenis_unpost = "masuk", $reff_type, $reff_id, $catatan)
    {
        /**
         * Ambil detail transaksi asli dari inv_kartu_stok
         */
        $this->select("inv_kartu_stok.*")
            ->from("inv_kartu_stok")
            ->leftJoin("m_barang", "m_barang.id = inv_kartu_stok.m_barang_id")
            ->where("reff_type", "=", $reff_type)
            ->andWhere("reff_id", "=", $reff_id)
            ->andWhere("is_unpost", "=", 0);
        /**
         * Tentukan jenis transaksi
         */
        if ($jenis_unpost == "masuk") {
            $this->andWhere("jumlah_keluar", ">", 0);
        } else {
            $this->andWhere("jumlah_masuk", ">", 0);
        }
        /**
         * Proses unpost detail transaksi
         */
        $kartuStok = $this->findAll();
        foreach ($kartuStok as $key => $value) {
            $newHpp = 0;
            if ($jenis_unpost == "masuk") {
                $data[] = [
                    "kode" => $value->kode,
                    "m_barang_id" => $value->m_barang_id,
                    "m_gudang_id" => $value->m_gudang_id,
                    "catatan" => $catatan,
                    "jenis_stok" => $jenis_unpost,
                    "tanggal" => date("Y-m-d"),
                    "jumlah_masuk" => $value->jumlah_keluar,
                    "harga_masuk" => $value->harga_keluar,
                    "hpp" => ($newHpp > 0) ? $newHpp : $value->harga_keluar,
                    "reff_type" => $reff_type,
                    "reff_id" => $reff_id,
                    "stok" => $value->jumlah_keluar,
                    "tanggal_expired" => $value->tanggal_expired,
                    "is_unpost" => 1,
                    "created_at" => strtotime("now"),
                    "created_by" => (isset($_SESSION["user"]["id"]) ? $_SESSION["user"]["id"] : 0),
                    "modified_at" => strtotime("now"),
                    "modified_by" => (isset($_SESSION["user"]["id"]) ? $_SESSION["user"]["id"] : 0),
                ];
            } else {
                $data[] = [
                    "kode" => $value->kode,
                    "m_barang_id" => $value->m_barang_id,
                    "m_gudang_id" => $value->m_gudang_id,
                    "catatan" => $catatan,
                    "jenis_stok" => $jenis_unpost,
                    "tanggal" => date("Y-m-d"),
                    "jumlah_keluar" => $value->jumlah_masuk,
                    "harga_keluar" => $value->harga_masuk,
                    "reff_type" => $reff_type,
                    "reff_id" => $reff_id,
                    "tanggal_expired" => $value->tanggal_expired,
                    "inv_kartu_stok_id" => $value->id,
                    "is_unpost" => 1,
                    "created_at" => strtotime("now"),
                    "created_by" => (isset($_SESSION["user"]["id"]) ? $_SESSION["user"]["id"] : 0),
                    "modified_at" => strtotime("now"),
                    "modified_by" => (isset($_SESSION["user"]["id"]) ? $_SESSION["user"]["id"] : 0),
                ];
                /**
                 * Hapus stok masuk
                 */
                $detMasuk["stok"] = 0;
                $detMasuk["hpp"] = 0;
                $this->update("inv_kartu_stok", $detMasuk, ["id" => $value->id]);
            }
        }
        /**
         * Simpan data ke table Kartu Stok
         */
        if (isset($data) && !empty($data)) {
            $this->multiInsert('inv_kartu_stok', $data);
        }
    }

    /**
     * [getHpp mencari hpp]
     * @param  [int]  $item_id
     * @param  [int]  $lokasi_id
     * @param  [string]  $tanggal
     * @param  [string]  $persediaan
     * @param  [double] $jumlahKeluar
     */
    public function getHpp($item_id, $tanggal = null, $persediaan = 'FIFO', $jumlahKeluar = 0)
    {
        /**
         * Ambil sisa stok yang tersedia
         */
        $this->select("
            induk.m_barang_id, 
            induk.id, 
            induk.kode, 
            induk.jumlah_masuk, 
            sum(inv_kartu_stok.jumlah_keluar) as keluar, 
            induk.harga_masuk,
            induk.hpp
        ")
            ->from("inv_kartu_stok as induk")
            ->leftJoin("inv_kartu_stok", "inv_kartu_stok.inv_kartu_stok_id = induk.id and inv_kartu_stok.tanggal <= '" . date("Y-m-d", strtotime($tanggal)) . "'")
            ->leftJoin("m_barang", "m_barang.id = inv_kartu_stok.m_barang_id")
            ->where("induk.m_barang_id", "=", $item_id)
            ->andWhere("induk.jumlah_masuk", "!=", 0)
            ->andWhere("induk.is_habis", "=", 0)
            ->groupBy("induk.id")
            ->having("jumlah_masuk - keluar > 0 or keluar is null");

        if (!empty($tanggal)) {
            $this->customWhere("induk.tanggal <= '" . date("Y-m-d", strtotime($tanggal)) . "'", "AND");
        }
        /**
         * Tentukan Tipe Persediaan
         */
        if ($persediaan == 'FIFO') {
            $this->orderBy("induk.tanggal ASC, induk.id ASC");
            $data = $this->findAll();
        }

        /**
         * Proses mencari HPP
         */
        /**
         * Mencari HPP untuk FIFO
         */

        $hpp = [];
        foreach ($data as $key => $value) {
            $value->stok = $value->jumlah_masuk - $value->keluar;
            $hpp_ = [
                'hpp' => $value->hpp,
                'jumlah' => $jumlahKeluar > $value->stok ? $value->stok : $jumlahKeluar,
                'inv_kartu_stok_id' => $value->id,
                'kode' => $value->kode,
            ];

            array_push($hpp, $hpp_);
            if ($jumlahKeluar <= $value->stok) {

                /**
                 * Update Pembelian is_habis
                 */

                /**
                 * ketika get HPP SO tidak update
                 */
                if (($value->stok - $jumlahKeluar) == 0) {
                    // Ketika pembelian habis dikeluarkan
                    $this->update("inv_kartu_stok", ["is_habis" => 1], ["id" => $value->id]);
                } else if (($value->stok - $jumlahKeluar) < 0) {
                    // Ketika pembelian minus
                    $this->update("inv_kartu_stok", ["is_habis" => 2], ["id" => $value->id]);
                } else {
                    $this->update("inv_kartu_stok", ["is_habis" => 0], ["id" => $value->id]);
                    // Ketika pembelian masih
                }

                break;
            }


            $jumlahKeluar -= $value->stok;
            $this->update("inv_kartu_stok", ["is_habis" => 1], ["id" => $value->id]);
        }
        /** END */
        if (!empty($persediaan)) {
            return $hpp;
        } else {
            return [];
        }
    }

    /**
     * [insertKartuStok memasukkan mutasi stok ke tabel inv_kartu_stok]
     * @param  [array] $detail [detail item]
     * @param  [string] $reff_type [asal transaksi]
     * @param  [string] $reff_id [id asal transaksi]
     * @param  [string] $kode [kode asal transaksi]
     * @param  [string] $catatan [catatan]
     * @param  [int] $lokasi_id [lokasi gudang]
     * @param  [string] $jenis_stok [jenis stok masuk / keluar]
     * @param  [string] $tanggal [tanggal]
     */

    public function insertKartuStok($detail, $reff_type, $reff_id, $kode, $catatan, $jenis_stok = "masuk", $tanggal = null, $created_by = null)
    {
        $data = [];
        $created_at = !empty($tanggal) ? $tanggal : date("Y-m-d");
        $tanggal = empty($tanggal) ? date("Y-m-d") : date("Y-m-d", strtotime($tanggal));

        $detailGroupByItem = [];
        foreach ($detail as $key => $val) {
            /**
             * SELAIN PENERIMAAN
             */
            if ($jenis_stok !== 'masuk') {
                if (isset($detailGroupByItem[$val['m_barang_id']])) {
                    $detailGroupByItem[$val['m_barang_id']]["jumlah"] += $val["jumlah"];
                } else {
                    $detailGroupByItem[$val['m_barang_id']] = $val;
                }
            }else{
                $detailGroupByItem[] = $val;
            }
        }

        foreach ($detailGroupByItem as $key => $val) {
            $jumlah = isset($val["jumlah"]) ? $val["jumlah"] : 0;
            $harga = isset($val["harga"]) ? $val["harga"] : 0;
            $barang_id = isset($val["m_barang_id"]) ? $val["m_barang_id"] : null;
            $created_by = isset($val["created_by"]) ? $val["created_by"] : (isset($_SESSION["user"]["id"]) ? $_SESSION["user"]["id"] : (isset($created_by) && !empty($created_by) ? $created_by : 0));
            if ($jenis_stok == "masuk") {
                /**
                 * Insert Stok Masuk
                 */
                $data[] = [
                    "kode" => $kode,
                    "m_barang_id" => $barang_id,
                    "catatan" => $catatan,
                    "jenis_stok" => $jenis_stok,
                    "tanggal" => $tanggal,
                    "jumlah_masuk" => $jumlah,
                    "harga_masuk" => $harga,
                    "hpp" => $harga,
                    "reff_type" => $reff_type,
                    "reff_id" => $reff_id,
                    "is_habis" => 0,
                    "created_at" => !empty($created_at) ? strtotime($created_at) : strtotime("now"),
                    "created_by" => $created_by,
                    "modified_at" => strtotime("now"),
                    "modified_by" => $created_by,
                ];
            } else {

                /**
                 * Insert Stok Keluar
                 */
                $hpp = $this->getHpp($barang_id, $tanggal, $val["tipe_persediaan"], $jumlah);
                foreach ($hpp as $k => $v) {
                    $data[] = [
                        "kode" => $kode,
                        "m_barang_id" => $barang_id,
                        "catatan" => $catatan,
                        "jenis_stok" => $jenis_stok,
                        "tanggal" => $tanggal,
                        "jumlah_keluar" => $v["jumlah"],
                        "harga_keluar" => (!empty($harga)) ? $harga : $v["hpp"],
                        "hpp" => $v["hpp"],
                        "reff_type" => $reff_type,
                        "reff_id" => $reff_id,
                        "inv_kartu_stok_id" => $v["inv_kartu_stok_id"],
                        "created_at" => !empty($tanggal) ? strtotime($tanggal) : strtotime("now"),
                        "created_by" => $created_by,
                        "modified_at" => !empty($tanggal) ? strtotime($tanggal) : strtotime("now"),
                        "modified_by" => $created_by,
                    ];
                }
            }
        }
        /**
         * Simpan data ke table Kartu Stok
         */
        $this->multiInsert('inv_kartu_stok', $data);
    }

}
