<?php

namespace Model;

use Exception;
use Service\Db;
use Service\Landa;

class PengambilanKas
{
    /**
     * simpan class Landa DB ke variabel #db.
     */
    private $db;

    /**
     * variabel untuk menyimpan nama tabel.
     */
    private $table;

    /**
     * konstruktor memanggil library landa Db.
     */
    public function __construct()
    {
        $this->db = Db::db();
        $this->landa = new Landa();
        $this->table = 't_pengambilan_kas';
    }

    /**
     * Ambil semua data perusahaan.
     *
     * @param array $params
     * @param int $limit
     * @param int $offset
     * @param string $order
     */
    public function getAll($params = [], $limit = 0, $offset = 0, $order = '')
    {
        $this->db->select('t_pengambilan_kas.*, m_user.nama as nama_karyawan, pembuat.nama as nm_pembuat')
        ->from('t_pengambilan_kas')
        ->leftJoin("m_user", "t_pengambilan_kas.user_id = m_user.id")
        ->leftJoin('m_user as pembuat', 'pembuat.id = t_pengambilan_kas.created_by');
        $filter = (array)json_decode($params["filter"]);
        if (isset($params) && !empty($params)) {
            $periode_mulai = '';
            $periode_selesai = '';
            foreach ($filter as $key => $val) {
                if ('id' == $key) {
                    $this->db->where('id', '=', $val);
                } else if ('nama' == $key) {
                    $this->db->where('t_pengambilan_kas.nama', 'like', $val);
                } else if ('status' == $key) {
                    $this->db->where('t_pengambilan_kas.is_deleted', '=', $val);
                } else if ($key == "periode_mulai") {
                    $periode_mulai = $val;
                } else if ($key == "periode_selesai") {
                    $periode_selesai = $val;
                } else {
                    $this->db->where($key, 'like', $val);
                }
            }
        }
        // Set limit
        if (isset($params['limit']) && !empty($params['limit'])) {
            $this->db->limit($params['limit']);
        }
        // Set offset
        if (isset($params['offset']) && !empty($params['offset'])) {
            $this->db->offset($params['offset']);
        }
        if (!empty($periode_mulai) && !empty($periode_selesai)) {
            $this->db->customWhere('DATE(t_pengambilan_kas.tanggal) >= "' . $periode_mulai . '" AND DATE(t_pengambilan_kas.tanggal) <= "' . $periode_selesai . '"', "AND");
        }
        $this->db->orderBy("t_pengambilan_kas.id DESC");
        $models = $this->db->findAll();
        $totalItem = $this->db->count();

        foreach ($models as $key => $value) {
            $value->tanggal_pengambilan = date("d F Y", strtotime($value->tanggal));
        }
        return [
            'data' => $models,
            'totalItem' => $totalItem,
        ];
    }

    public function generateKode()
    {
        $cekKode = $this->db->select('kode')
            ->from($this->table)
            ->orderBy('kode DESC')
            ->find()
        ;

        try {
            if ($cekKode) {
                $kode_terakhir = $cekKode->kode;
            } else {
                $kode_terakhir = 0;
            }

            $tipe = 'BRG';
            $kode_item = (substr($kode_terakhir, -4) + 1);
            $kode = substr('0000'.$kode_item, strlen($kode_item));
            $tanggal = date('y').date('m');
            $kode = $tipe.$tanggal.$kode;

            return [
                'status' => true,
                'data' => $kode,
            ];
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => 'Gagal Generate Kode',
            ];
        }
    }

    /**
     * Method untuk menyimpan data user.
     *
     * @param array $data
     * @param array $customParams
     * @param mixed $params
     */
    public function save($params, $customParams = [])
    {
        try {
            if (isset($params['tanggal'])) {
                $params['tanggal'] = $this->landa->arrayToDate($params['tanggal']);
            }

            if (isset($params["id"])) {
                $model = $this->db->update("t_pengambilan_kas", $params, ["id" => $params["id"]]);
            } else {
                $model = $this->db->insert("t_pengambilan_kas", $params);
            }

            return [
                'status' => true,
                'data' => $model,
            ];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }

    /**
     * Hapus hak akses.
     *
     * @param array $params
     *
     * @return array
     */
    public function restore($params)
    {
        try {
            $model = $this->db->update($this->table, ['is_deleted' => 0], ['id' => $params['id']]);

            return [
                'status' => true,
                'data' => $model,
            ];
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }

    /**
     * Hapus hak akses.
     *
     * @param array $params
     *
     * @return array
     */
    public function delete($params)
    {
        try {
            $model = $this->db->update($this->table, ['is_deleted' => 1], ['id' => $params['id']]);

            return [
                'status' => true,
                'data' => $model,
            ];
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }

    public function getDataKaryawan()
    {
        $this->db->select('*')
            ->from('m_user')->where('is_deleted', '=', '0')->andWhere('id', "!=", 1);
        $models = $this->db->findAll();
        $totalItem = $this->db->count();

        return [
            'data' => $models,
            'totalItem' => $totalItem,
        ];
    }

    /**
     * Validasi data yang dikirim.
     *
     * @param array $data
     * @param array $custom
     */
    public function validasi($data, $custom = [])
    {
        $validasi = [
            'nama' => 'required'
        ];

        return validate($data, $validasi, $custom);
    }
}
