<?php

namespace Model;

use Service\Db;
use Service\Landa;
use Service\LandaInv;

class Kasir
{
    /**
     * simpan class Landa DB ke variabel #db.
     */
    private $db;

    /**
     * variabel untuk menyimpan nama tabel.
     */
    private $table;

    /**
     * konstruktor memanggil library landa Db.
     */
    public function __construct()
    {
        $this->db = Db::db();
        $this->landa = new Landa();
        $this->landaInv = new LandaInv();
        $this->table_pelayanan = 'm_layanan';
        $this->table_barang = 'm_barang';
        $this->table_caraPembayaran = 'm_cara_pembayaran';
        $this->table_diskon = 'm_diskon';
    }

    public function getTransaksi($params = [])
    {
        /**
         * DEKLARASI ARRAY TOTAL
         */
        $total = [
            "layanan" => [
                "harga" => 0,
                "jumlah" => 0,
                "subtotal" => 0,
            ],
            "barang" => [
                "harga" => 0,
                "jumlah" => 0,
                "subtotal" => 0,
            ],
            "checkout" => [
                "total_tindakan_subtotal" => 0,
            ],
            "pembayaran" => 0,
            "kembalian" => 0
        ];

        $this->db->select('t_penjualan.*')
            ->from("t_penjualan");

        $this->db->where("t_penjualan.id", "=", $params['id']);

        $penjualan = $this->db->find();

        if (!empty($penjualan)) {
            $penjualan->m_customer_id = $this->db->select("m_customer.*, m_customer.nama as nama_view")
                ->from("m_customer")
                ->where("id", "=", $penjualan->customer_id)
                ->find();

//            $penjualan->diskon_nominal = $penjualan->diskon;
//            $penjualan->diskon = $penjualan->nama_diskon;

            $penjualan->total_diskon_layanan = 0;
            $penjualan->total_diskon_barang = 0;
        }


        $this->db->select('
        t_penjualan_detail.*,
        IFNULL(t_penjualan_detail.diskon,0) as diskon,
        m_barang.nama as nama_barang,
        m_layanan.nama as nama_layanan
        ')
            ->from("t_penjualan_detail")
            ->leftJoin("t_penjualan", "t_penjualan.id = t_penjualan_detail.penjualan_id")
            ->leftJoin("m_barang", "m_barang.id = t_penjualan_detail.barang_id AND t_penjualan_detail.tipe = 'barang'")
            ->leftJoin("m_layanan", "m_layanan.id = t_penjualan_detail.barang_id AND t_penjualan_detail.tipe = 'layanan'");

        $this->db->where("t_penjualan_detail.penjualan_id", "=", $params['id']);

        $models = $this->db->findAll();

        $listLayanan = $listBarang = [];
        if (!empty($models)) {
            foreach ($models as $key => $value) {
                $value->subtotal = $value->jumlah * $value->harga;

                if ($value->tipe == 'layanan') {
                    $value->nama = $value->nama_layanan;
                    $listLayanan[] = $value;

                    $total['layanan']['harga'] += ($value->jumlah * $value->harga);
                    $total['layanan']['jumlah'] += $value->jumlah;
                    $total['layanan']['subtotal'] += ($value->jumlah * $value->harga);

                    /**
                     * TOTAL DISKON LAYANAN
                     */

                    $penjualan->total_diskon_layanan += (int)$value->diskon;

                } else {
                    $value->nama = $value->nama_barang;
                    $listBarang[] = $value;

                    $total['barang']['harga'] += ($value->jumlah * $value->harga);
                    $total['barang']['jumlah'] += $value->jumlah;
                    $total['barang']['subtotal'] += ($value->jumlah * $value->harga);

                    /**
                     * TOTAL DISKON BARANG
                     */

                    $penjualan->total_diskon_barang += (int)$value->diskon;

                }
            }
        }

        $this->db->select("t_pembayaran.*, m_cara_pembayaran.nama")
            ->from("t_pembayaran")
            ->leftJoin("m_cara_pembayaran", "m_cara_pembayaran.id = t_pembayaran.cara_pembayaran_id");

        $this->db->where("t_pembayaran.penjualan_id", "=", $params['id']);

        $caraPembayaran = $this->db->findAll();

        if (!empty($caraPembayaran)) {
            foreach ($caraPembayaran as $key => $value) {
                $total['pembayaran'] += $value->jumlah_bayar;
                $total['kembalian'] += $value->kembalian;
            }
        }


        return [
            'checkout' => $penjualan,
            'listLayanan' => $listLayanan,
            'listBarang' => $listBarang,
            'caraPembayaran' => $caraPembayaran,
            'total' => $total
        ];
    }

    public function getLayanan($params = [])
    {
        $this->db->select('*')
            ->from($this->table_pelayanan)
            ->where($this->table_pelayanan . ".is_deleted", "=", 0);

        if (isset($params['tipe']) && !empty($params['tipe'])) {
            $this->db->where("tipe", "=", $params['tipe']);
        }

        $models = $this->db->findAll();

        return [
            'data' => $models
        ];
    }

    public function getBarang($params = [])
    {
        $this->db->select('*')
            ->from($this->table_barang)
            ->where($this->table_barang . ".is_deleted", "=", 0);

        $models = $this->db->findAll();

        return [
            'data' => $models
        ];
    }

    public function getCaraPembayaran($params = [])
    {
        $this->db->select('*')
            ->from($this->table_caraPembayaran)
            ->where($this->table_caraPembayaran . ".is_deleted", "=", 0);

        $models = $this->db->findAll();

        return [
            'data' => $models
        ];
    }

    public function getDiskon($params = [])
    {
        $this->db->select('*')
            ->from($this->table_diskon)
            ->where($this->table_diskon . ".is_deleted", "=", 0);

        $models = $this->db->findAll();

        return [
            'data' => $models
        ];
    }

    /**
     * ================================================================ CHECKOUT ================================================================
     */

    /**
     * Validasi data yang dikirim.
     *
     * @param array $data
     * @param array $custom
     */
    public function validasi($data, $custom = [])
    {
        $validasi = [
            'm_customer_id' => 'required',
        ];

        return validate($data, $validasi, $custom);
    }


    /**
     * CEK STOK
     */

    public function cekStok($detail = [])
    {
        $detailGrubByItem = [];
        $jumlahItem = [];
        foreach ($detail as $key => $val) {
            if (empty($jumlahItem[$val['id']])) {
                $jumlahItem[$val['id']] = 0;
            }
            $jumlahItem[$val['id']] += $val['jumlah'];
            $detailGrubByItem[$val['id']] = $val;
            $detailGrubByItem[$val['id']]['jumlah'] = $jumlahItem[$val['id']];
        }

        $cekStok = checkStokKasir($detailGrubByItem);

        return $cekStok;
    }


    /**
     * CREATE PENJUALAN
     */

    public function createPenjualan($param)
    {
        try {
            $data_penjualan = [
                "customer_id" => @$param['m_customer_id']['id'],
                "terapis_id" => @$param['terapis_id'],
//                "diskon" => @$param['diskon']['nominal'],
//                "nama_diskon" => @$param['diskon']['nama'],
                "diskon" => @$param['total_diskon_layanan'] + @$param['total_diskon_barang'],
                "kode_nota" => generateNota(),
                "service_layanan" => @$param['service_layanan'],
                "total" => @$param['jumlah_bayar'],
                "created_at" => strtotime($this->landa->arrayToDate($param["tanggal"]) . ' ' . date("H:i:s")),
                "waktu" => isset($param["waktu"]) && !empty($param["waktu"]) ? $this->landa->arrayToTime($param["waktu"]) : null,
                'status' => 'selesai',
                "keterangan" => isset($param['keterangan']) ? $param['keterangan'] : '',
                "fee_perusahaan" => 0,
                "fee_terapis" => 0,
                "fee_kasir" => 0,
                "device" => $_SERVER["HTTP_USER_AGENT"]
            ];

            $penjualan = $this->db->insert("t_penjualan", $data_penjualan);
            return [
                'status' => true,
                'data' => $penjualan,
            ];
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }

    /**
     * CREATE DETAIL PENJUALAN
     */

    public function createDetailPenjualan($layanan, $barang, $penjualan_id)
    {
        try {

            if (isset($layanan) && !empty($layanan)) {
                $arrLayanan = [];
                foreach ($layanan as $key => $value) {
                    $arrLayanan['penjualan_id'] = $penjualan_id;
                    $arrLayanan['tipe'] = 'layanan';
                    $arrLayanan['barang_id'] = $value['id'];
                    $arrLayanan['jumlah'] = $value['jumlah'];
                    $arrLayanan['harga'] = $value['harga'];
                    $arrLayanan['m_diskon_id'] = isset($value['jenis_diskon']) && !empty($value['jenis_diskon']) ? $value['jenis_diskon']['id'] : null;
                    if (isset($value['jenis_diskon']) && !empty($value['jenis_diskon']) && !empty($value['jenis_diskon']['id'])) {
                        $arrLayanan['jenis_diskon'] = $value['jenis_diskon']['nama'];
                    } else {
                        $arrLayanan['jenis_diskon'] = null;
                    }
                    $arrLayanan['diskon'] = @$value['diskon'];
                    $arrLayanan['terapis_id'] = null;
                    $arrLayanan['kasir_id'] = $_SESSION['user']['id'];
                    $arrLayanan['k_perusahaan'] = $value['k_perusahaan'];
                    $arrLayanan['k_marketing'] = $value['k_marketing'];
                    $arrLayanan['k_manager'] = $value['k_manager'];
                    $arrLayanan['k_therapist'] = $value['k_therapist'];
                    $this->db->insert("t_penjualan_detail", $arrLayanan);
                }
            }

            if (isset($barang) && !empty($barang)) {
                $arrBarang = [];
                foreach ($barang as $key => $value) {
                    $arrBarang['penjualan_id'] = $penjualan_id;
                    $arrBarang['tipe'] = 'barang';
                    $arrBarang['barang_id'] = $value['id'];
                    $arrBarang['jumlah'] = $value['jumlah'];
                    $arrBarang['harga'] = $value['harga'];
                    $arrBarang['m_diskon_id'] = isset($value['jenis_diskon']) && !empty($value['jenis_diskon']) ? $value['jenis_diskon']['id'] : null;
                    if (isset($value['jenis_diskon']) && !empty($value['jenis_diskon']) && !empty($value['jenis_diskon']['id'])) {
                        $arrBarang['jenis_diskon'] = $value['jenis_diskon']['nama'];
                    } else {
                        $arrBarang['jenis_diskon'] = null;
                    }
                    $arrBarang['diskon'] = @$value['diskon'];
                    $arrBarang['terapis_id'] = null;
                    $arrBarang['kasir_id'] = $_SESSION['user']['id'];
                    $this->db->insert("t_penjualan_detail", $arrBarang);
                }
            }

            return [
                'status' => true,
                'data' => true,
            ];
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }


    /**
     * CREATE CARA PEMBAYARAN
     */

    public function createCaraPembayaran($checkout, $param, $penjualan_id)
    {
        try {

            if (isset($param) && !empty($param)) {
                $arrCaraPembayaran = [];

                $kembalian = (int)$checkout['cara_pembayaran'] - ((int)$checkout['jumlah_bayar']);

                if ($kembalian <= 0) {
                    $kembalian = 0;
                }

                foreach ($param as $key => $value) {
                    if (!empty($value['jumlah_bayar'])) {
                        if ($value['tipe'] == 'tunai') {
                            $arrCaraPembayaran['kembalian'] = $kembalian;
                        } else {
                            $arrCaraPembayaran['kembalian'] = 0;
                        }

                        $arrCaraPembayaran['penjualan_id'] = $penjualan_id;
                        $arrCaraPembayaran['cara_pembayaran_id'] = $value['id'];
                        $arrCaraPembayaran['jumlah_bayar'] = $value['jumlah_bayar'];

                        $this->db->insert("t_pembayaran", $arrCaraPembayaran);
                    }
                }
            }

            return [
                'status' => true,
                'data' => true,
            ];
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }


    /**
     * CREATE CARA PEMBAYARAN
     */

    public function createKartuStok($checkout, $barang, $penjualan)
    {
        try {
            if (!empty($barang)) {

                $tanggal_stok = isset($checkout['tanggal']) && !empty($checkout['tanggal']) ? $this->landa->arrayToDateCustom2($checkout["tanggal"]) : null;
                $tglStok = date('Y-m-d') !== $tanggal_stok ? ' (Tanggal ' . $tanggal_stok . ')' : '';

                $item = [];
                foreach ($barang as $key => $value) {
                    $item[] = [
                        "m_barang_id" => $value['id'],
                        "tipe_persediaan" => 'FIFO',
                        "jumlah" => $value["jumlah"],
                        "tipe_item" => 'barang'
                    ];
                }

                $this->landaInv->insertKartuStok($item, "t_penjualan_id", $penjualan->id, $penjualan->kode_nota, "Penjualan Nota " . $penjualan->kode_nota . $tglStok, "keluar");

                return [
                    'status' => true,
                    'data' => true,
                ];
            }

            return [
                'status' => false,
                'error' => ['Terjadi masalah pada server'],
            ];
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }

    // INVOICE
    public function getTransaksiRecord($id)
    {

        date_default_timezone_set("Asia/Jakarta");

        $this->db->select('t_penjualan.*, m_user.nama as nm_therapis, m_customer.nama as nm_customer, m_customer.no_telepon, m_customer.tipe as tipe_member, m_customer.w_kota_id, w_kota.kota as nm_kota')
            ->from('t_penjualan')
            ->leftJoin("m_user", "m_user.id = t_penjualan.terapis_id")
            ->leftJoin("m_customer", "m_customer.id = t_penjualan.customer_id")
            ->leftJoin("w_kota", "w_kota.id = m_customer.w_kota_id")
            ->where('t_penjualan.id', "=", $id);

        $models = $this->db->find();

        if ($models) {
            if ($models->tipe_member == 0) {
                $models->tipe_member = "Non Member";
            } else {
                $models->tipe_member = "Member";
            }
            $models->dibuat_pada = date('d F Y', $models->created_at);

            $models->dibuat_oleh = $this->db->select('nama')->from('m_user')->where('id', '=', $models->created_by)->find()->nama;

            return [
                'data' => $models
            ];
        } else {
            return [
                'status' => false,
                'error' => ['Data tidak ditemukan'],
            ];
        }
    }

    public function getLayananRecord($id)
    {
        $this->db->select('t_penjualan_detail.*, m_layanan.nama as nm_service, m_layanan.isi_paket')
            ->from('t_penjualan_detail')
            ->leftJoin("m_layanan", "m_layanan.id = t_penjualan_detail.barang_id")
            ->where('penjualan_id', "=", $id)
            ->andWhere('t_penjualan_detail.tipe', '=', 'layanan');

        $models = $this->db->findAll();

        foreach ($models as $key => $value) {
            if ($value->jumlah !== null || $value->jumlah !== "") {
                $value->subtotal = $value->jumlah * $value->harga;
            }
        }
        return [
            'data' => $models
        ];
    }

    public function getBarangRecord($id)
    {
        $this->db->select('t_penjualan_detail.*, m_barang.nama as nm_service')
            ->from('t_penjualan_detail')
            ->leftJoin("m_barang", "m_barang.id = t_penjualan_detail.barang_id")
            ->where('penjualan_id', "=", $id)
            ->andWhere('t_penjualan_detail.tipe', '=', 'barang');

        $models = $this->db->findAll();

        foreach ($models as $key => $value) {
            if ($value->jumlah !== null || $value->jumlah !== "") {
                $value->subtotal = $value->jumlah * $value->harga;
            }
        }

        return [
            'data' => $models
        ];
    }
}
