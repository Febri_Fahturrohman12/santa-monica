<?php

namespace Model;

use Service\Db;
use Service\Landa;

class LaporanProgress
{
    /**
     * simpan class Landa DB ke variabel #db.
     */
    private $db;

    /**
     * variabel untuk menyimpan nama tabel.
     */
    private $table;

    /**
     * konstruktor memanggil library landa Db.
     */
    public function __construct()
    {
        $this->db = Db::db();
    }

    public function getAnggota()
    {
        $this->db->select('*')
            ->from("m_user");

        $models = $this->db->findAll();

        return [
            'data' => $models
        ];
    }

    function getAll($params)
    {   
        if (isset($params['bulan'])) {
            if (1 == $params['bulan']) {
                $nama_bulan = 'Januari';
            } elseif (2 == $params['bulan']) {
                $nama_bulan = 'Februari';
            } elseif (3 == $params['bulan']) {
                $nama_bulan = 'Maret';
            } elseif (4 == $params['bulan']) {
                $nama_bulan = 'April';
            } elseif (5 == $params['bulan']) {
                $nama_bulan = 'Mei';
            } elseif (6 == $params['bulan']) {
                $nama_bulan = 'Juni';
            } elseif (7 == $params['bulan']) {
                $nama_bulan = 'Juli';
            } elseif (8 == $params['bulan']) {
                $nama_bulan = 'Agustus';
            } elseif (9 == $params['bulan']) {
                $nama_bulan = 'September';
            } elseif (10 == $params['bulan']) {
                $nama_bulan = 'Oktober';
            } elseif (11 == $params['bulan']) {
                $nama_bulan = 'November';
            } elseif (12 == $params['bulan']) {
                $nama_bulan = 'Desember';
            }
        }
        
        $arrJudul = [];
        $arrFinal = [];

        $arrIdSetoran = [];
        // JUDUL
        $listJudul = $this->db->select('id as kebutuhan_id, nama,tanggal,keterangan')->from('t_setoran')->where('tanggal', '>=', $params['tanggal_mulai'])->andWhere('tanggal', '<=', $params['tanggal_selesai'])->orderBy('tanggal ASC')->findAll();
        foreach ($listJudul as $key => $value) {
            $value->label = date('d M', strtotime($value->tanggal));
            $arrIdSetoran[] = $value->kebutuhan_id;
        }
        if($listJudul){
            $arrJudul = $listJudul;

            // DATA ANGGOTA
            $this->db->select('id, nama, steam')
                ->from('m_user');
            
            if (isset($params['m_user_id']) && !empty($params['m_user_id']) && $params['m_user_id'] != 'null' && $params['m_user_id'] > 0) {
                // parameter id karyawan
                $this->db->andWhere('m_user.id', '=', $params['m_user_id']);
            }
            $this->db->orderBy('nama ASC');
            $models = $this->db->findAll();

            // DATA SETORAN
            $arr_data = [];
            foreach ($models as $key => $value) {
                $arr_data[$key] = $value;
                $arr_data[$key]->detail = $arrJudul;
            }


            // DATA PROGRESS
            $this->db->select('t_setor_anggota.id,t_setor_anggota.m_user_id,t_setor_anggota.t_setoran_id,t_setor_anggota.m_pj_id,t_setor_anggota.tanggal,t_setor_anggota.status, t_setoran.tanggal as tgl_kebutuhan,m_user.nama as nama_pj')
                ->from('t_setor_anggota')
                ->join('left join', 't_setoran', 't_setoran.id = t_setor_anggota.t_setoran_id')
                ->join('left join', 'm_user', 'm_user.id = t_setor_anggota.m_pj_id')
                ->where('t_setor_anggota.is_deleted', '=', 0)
                ->customWhere('t_setor_anggota.t_setoran_id IN ('.implode(',',$arrIdSetoran).')')
                ->orderBy('t_setoran.tanggal ASC');
            $modelsprogress = $this->db->findAll();
            $arr_progress = [];
            foreach ($modelsprogress as $key => $value) {
                $arr_progress[$value->m_user_id][$value->t_setoran_id] = $value;
            }

            $arr_kelompok = [];
            foreach ($arr_data as $key => $value) {
                $arr_kelompok[$key]['id'] = $value->id;
                $arr_kelompok[$key]['nama'] = $value->nama;
                $arr_kelompok[$key]['steam'] = $value->steam;

                foreach ($value->detail as $keys => $vals) {
                    $arr_kelompok[$key]['detail'][$keys]['kebutuhan_id'] = $vals->kebutuhan_id;
                    $arr_kelompok[$key]['detail'][$keys]['nama'] = $vals->nama;
                    $arr_kelompok[$key]['detail'][$keys]['tanggal'] = $vals->tanggal;
                    $arr_kelompok[$key]['detail'][$keys]['keterangan'] = $vals->keterangan;
                    $arr_kelompok[$key]['detail'][$keys]['label'] = $vals->label;

                    // STATUS 0 PENDING KUNING, 1 APPROVE HIJAU, 3 BELUM SETOR MERAH
                    if(isset($arr_progress[$value->id][$vals->kebutuhan_id])){
                        $arr_kelompok[$key]['detail'][$keys]['status_setor'] = $arr_progress[$value->id][$vals->kebutuhan_id]->status;
                        $arr_kelompok[$key]['detail'][$keys]['nama_pj'] = $arr_progress[$value->id][$vals->kebutuhan_id]->nama_pj;
                        $arr_kelompok[$key]['detail'][$keys]['dets'] = $arr_progress[$value->id][$vals->kebutuhan_id];
                    }else {
                        $arr_kelompok[$key]['detail'][$keys]['status_setor'] = 3;
                        $arr_kelompok[$key]['detail'][$keys]['nama_pj'] = "-";
                        $arr_kelompok[$key]['detail'][$keys]['dets'] = [];
                    }
                }
            }
            
            return [
                'status' => true,
                'list' => $arr_kelompok,
                'periode' => $nama_bulan.' '.$params['tahun'],
                'listJudul' => $arrJudul,
            ];
        }else {
            return [
                'status' => false,
                'list' => [],
                'periode' => $nama_bulan.' '.$params['tahun'],
                'listJudul' => $arrJudul,
            ];
        }
    }
}
