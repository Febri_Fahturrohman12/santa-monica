<?php

namespace Model;

use Service\Db;

class Slider {
    /**
     * simpan class Landa DB ke variabel #db.
     */
    private $db;

    /**
     * variabel untuk menyimpan nama tabel.
     */
    private $table;

    /**
     * konstruktor memanggil library landa Db.
     */
    public function __construct() {
        $this->db   = Db::db();
        $this->table= 'm_slider';
    }

    /**
     * Ambil semua data perusahaan.
     *
     * @param array  $params
     * @param int    $limit
     * @param int    $offset
     * @param string $order
     */
    public function getAll($params = [], $limit = 0, $offset = 0, $order = '') {
        $this->db->select('*')
            ->from($this->table)
            ->where($this->table.'.is_deleted', '=', 0);

        // Filter
        if (isset($params['filter']) && !is_array($params['filter'])) {
            // jika parameter dalam bentuk json
            $filter = (isset($params['filter'])) ? (array) json_decode($params['filter']) : [];
        } elseif (isset($params['filter']) && is_array($params['filter'])) {
            $filter = $params['filter'];
        }

        if (isset($filter) && !empty($filter)) {
            foreach ($filter as $key => $val) {
                if (!empty($val)) {
                    if ('judul' == $key) {
                        $this->db->where('judul', 'like', $val);
                    }
                }
            }
        }

        // Set limit
        if (isset($params['limit']) && !empty($params['limit'])) {
            $this->db->limit($params['limit']);
        }
        
        // Set offset
        if (isset($params['offset']) && !empty($params['offset'])) {
            $this->db->offset($params['offset']);
        }

        $this->db->orderBy($this->table .'.id DESC');
        $models = $this->db->findAll();
        $totalItem = $this->db->count();

        return [
            'data'      => $models,
            'totalItem' => $totalItem,
        ];
    }

    public function save($data, $customParams = []) {
        try {
            if (!isset($data['id'])) {
                $data['is_deleted'] = 0;

                $model = $this->db->insert($this->table, $data);
            } elseif (empty($customParams)) {
                $model = $this->db->update($this->table, $data, ['id' => $data['id']]);
            } else {
                $model = $this->db->update($this->table, $data, $customParams);
            }

            // Hapus data redis
            $redis = redisConnect();
            $redis->del("getSliderBeforeLogin");

            return [
                'status'=> true,
                'data'  => $model
            ];
        } catch (\Exception $e) {
            return [
                'status'=> false,
                'error' => $e->getMessage()
            ];
        }
    }

    public function delete($data) {
        $redis      = redisConnect();
        try {
            if (null != $data['id']) {
                $model = (object) $this->db->update($this->table, ['is_deleted' => $data['is_deleted']], ['id' => $data['id']]);

                if(!empty($resultRedis)){
                    $resultRedis = json_decode($resultRedis);

                    foreach($resultRedis as $key => $val):
                        if($model->id == $val->id){
                            $resultRedis[$key] = $model;
                            break;
                        }
                    endforeach;

                    // Perbarui data redis
                    $redis->del("getSliderBeforeLogin");
                }
            } 

            return [
                'status'=> true,
                'data'  => $model
            ];
        } catch (\Exception $e) {
            return [
                'status'=> false,
                'error' => $e->getMessage()
            ];
        }
    }

    /**
     * Validasi.
     *
     * @param array $data   [data yang akan divalidasi]
     * @param array $custom [custom validasi]
     *
     * @return boolean
     */
    public function validasi($data, $custom = []) {
        $validasi = [
            'deskripsi' => 'required',
            'judul'     => 'required',
            'status'    => 'required'
        ];

        return validate($data, $validasi, $custom);
    }
}