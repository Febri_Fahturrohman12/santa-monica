<?php

namespace Model;

use Exception;
use Service\Db;
use Service\Landa;

class Barang
{
    /**
     * simpan class Landa DB ke variabel #db.
     */
    private $db;

    /**
     * variabel untuk menyimpan nama tabel.
     */
    private $table;

    /**
     * konstruktor memanggil library landa Db.
     */
    public function __construct()
    {
        $this->db = Db::db();
        $this->landa = new Landa();
        $this->table = 'm_barang';
    }

    /**
     * Ambil semua data perusahaan.
     *
     * @param array $params
     * @param int $limit
     * @param int $offset
     * @param string $order
     */
    public function getAll($params = [], $limit = 0, $offset = 0, $order = '')
    {
        $this->db->select('*')
            ->from('m_barang');

        $filter = (array)json_decode($params["filter"]);
        if (isset($params) && !empty($params)) {
            foreach ($filter as $key => $val) {
                if ('id' == $key) {
                    $this->db->where('id', '=', $val);
                } elseif ('kode' == $key) {
                    $this->db->where('m_barang.kode', 'like', $val);
                } elseif ('nama' == $key) {
                    $this->db->where('m_barang.nama', 'like', $val);
                } elseif ('status' == $key) {
                    $this->db->where('m_barang.is_deleted', '=', $val);
                } else {
                    $this->db->where($key, 'like', $val);
                }
            }
        }
        // Set limit
        if (isset($params['limit']) && !empty($params['limit'])) {
            $this->db->limit($params['limit']);
        }
        // Set offset
        if (isset($params['offset']) && !empty($params['offset'])) {
            $this->db->offset($params['offset']);
        }
        $this->db->orderBy("m_barang.nama ASC");
        $models = $this->db->findAll();
        $totalItem = $this->db->count();

        return [
            'data' => $models,
            'totalItem' => $totalItem,
        ];
    }

    public function generateKode()
    {
        $cekKode = $this->db->select('kode')
            ->from($this->table)
            ->orderBy('kode DESC')
            ->find()
        ;

        try {
            if ($cekKode) {
                $kode_terakhir = $cekKode->kode;
            } else {
                $kode_terakhir = 0;
            }

            $tipe = 'BRG';
            $kode_item = (substr($kode_terakhir, -4) + 1);
            $kode = substr('0000'.$kode_item, strlen($kode_item));
            $tanggal = date('y').date('m');
            $kode = $tipe.$tanggal.$kode;

            return [
                'status' => true,
                'data' => $kode,
            ];
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => 'Gagal Generate Kode',
            ];
        }
    }

    /**
     * Method untuk menyimpan data user.
     *
     * @param array $data
     * @param array $customParams
     * @param mixed $params
     */
    public function save($params, $customParams = [])
    {
        try {
            
            if (isset($params["id"])) {
                $model = $this->db->update("m_barang", $params, ["id" => $params["id"]]);
            } else {
                $params['kode'] = $this->generateKode()['data'];
                

                $model = $this->db->insert("m_barang", $params);
            }

            return [
                'status' => true,
                'data' => $model,
            ];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }

    /**
     * Hapus hak akses.
     *
     * @param array $params
     *
     * @return array
     */
    public function restore($params)
    {
        try {
            $model = $this->db->update($this->table, ['is_deleted' => 0], ['id' => $params['id']]);

            return [
                'status' => true,
                'data' => $model,
            ];
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }

    /**
     * Hapus hak akses.
     *
     * @param array $params
     *
     * @return array
     */
    public function delete($params)
    {
        try {
            $model = $this->db->update($this->table, ['is_deleted' => 1], ['id' => $params['id']]);

            return [
                'status' => true,
                'data' => $model,
            ];
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }

    /**
     * Validasi data yang dikirim.
     *
     * @param array $data
     * @param array $custom
     */
    public function validasi($data, $custom = [])
    {
        $validasi = [
            'nama' => 'required',
        ];

        return validate($data, $validasi, $custom);
    }
}
