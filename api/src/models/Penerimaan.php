<?php

namespace Model;

use Service\Db;
use Service\Landa;

class Penerimaan
{
    /**
     * simpan class Landa DB ke variabel #db.
     */
    private $db;

    /**
     * variabel untuk menyimpan nama tabel.
     */
    private $table;

    /**
     * konstruktor memanggil library landa Db.
     */
    public function __construct()
    {
        $this->db = Db::db();
        $this->landa = new Landa();
        $this->table_penerimaan = 't_penerimaan';
    }

    /**
     * Ambil semua data penerimaan.
     *
     * @param array $params
     * @param int $limit
     * @param int $offset
     * @param string $order
     */

    public function getAll($params = [], $limit = 0, $offset = 0, $order = '')
    {
        $this->db->select('inv_penerimaan.*')
            ->from('inv_penerimaan');

        $filter = (array)json_decode($params["filter"]);
        if (isset($params) && !empty($params)) {
            foreach ($filter as $key => $val) {
                if ('id' == $key) {
                    $this->db->where('id', '=', $val);
                } elseif ('kode' == $key) {
                    $this->db->where('inv_penerimaan.kode', 'like', $val);
                }  else {
                    $this->db->where($key, 'like', $val);
                }
            }
        }
        // Set limit
        if (isset($params['limit']) && !empty($params['limit'])) {
            $this->db->limit($params['limit']);
        }
        // Set offset
        if (isset($params['offset']) && !empty($params['offset'])) {
            $this->db->offset($params['offset']);
        }
        $this->db->orderBy("id DESC");
        $models = $this->db->findAll();
        $totalItem = $this->db->count();

        return [
            'data' => $models,
            'totalItem' => $totalItem,
        ];
    }



    public function getBarang($params = [])
    {
        $this->db->select('*')
            ->from($this->table_barang)
            ->where($this->table_barang . ".is_deleted", "=", 0);

        $models = $this->db->findAll();

        return [
            'data' => $models
        ];
    }

    public function cekNoInvoice($params)
    {
        $this->db->select("no_invoice")
            ->from("inv_penerimaan")
            ->where("no_invoice", "=", $params["no_invoice"]);

        if (isset($params["id"]) && !empty($params["id"])) {
            $this->db->andWhere("id", "!=", $params["id"]);
        }
        $cek = $this->db->find();
        if (isset($cek->no_invoice)) {
            return [
                'status' => false,
                'error' => "No Invoice " . $cek->no_invoice . " sudah terdaftar",
            ];
        }
    }

    /**
     * SIMPAN DATA DAN DETAIL
     */

    public function save($params)
    {

        try {


            if (isset($params["data"]["id"])) {
                $model = $this->db->update("inv_penerimaan", $params["data"], ["id" => $params["data"]["id"]]);
                $this->db->delete("inv_penerimaan_det", ["inv_penerimaan_id" => $params["data"]["id"]]);
            } else {
                $params["data"]["kode"] = generateKodePenerimaan();
                $model = $this->db->insert("inv_penerimaan", $params["data"]);
            }


            /**
             * Simpan detail
             */

            $total = 0;
            $item = [];
            if (isset($params["detail"]) && !empty($params["detail"])) {
                foreach ($params["detail"] as $key => $val) {
                    $total++;
                    $detail["m_barang_id"] = isset($val["m_barang_id"]) ? $val["m_barang_id"] : '';
                    $detail["jumlah"] = isset($val["jumlah"]) ? $val["jumlah"] : '';
                    $detail["harga"] = isset($val["harga"]) ? $val["harga"] : '';
                    $detail["inv_penerimaan_id"] = $model->id;

                    if ($detail['m_barang_id'] > 0) {
                        $modelDetail = $this->db->insert("inv_penerimaan_det", $detail);
                        /**
                         * Simpan ke array kartu stok
                         */
                        if ($model->status == 'selesai') {
                            $item[] = [
                                "m_barang_id" => $modelDetail->m_barang_id,
                                "tipe_persediaan" => 'FIFO',
                                "jumlah" => $modelDetail->jumlah,
                                "harga" => $modelDetail->harga,
                            ];
                        }
                    }
                }
            }

            $model->total_item = $total;
            return [
                'status' => true,
                'data' => $model,
                'item' => $item,
            ];
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }

    public function saveStatus($params, $custom = array())
    {
        $cek = true;

        foreach ($detail as $key => $value) {
            if (empty($value['m_barang_id'])) {
                $cek = false;
            }

            if (empty($value['jumlah'])) {
                $cek = false;
            }

            if (empty($value['harga'])) {
                $cek = false;
            }
        }

        return $cek;
    }

    /**
     * CEK STOK TERSEDIA
     */

    public function checkAvailabelUnpostIn($reff_type, $params)
    {
        /**
         * Ambil id kartu stok masuk
         */
        $model = $this->db->select("*")
            ->from("inv_kartu_stok")
            ->where("reff_type", "=", $reff_type)
            ->andWhere("reff_id", "=", $params['id'])
            ->andWhere("jumlah_masuk", ">", 0)
            ->findAll();
        $idKartuStok = [];
        foreach ($model as $key => $value) {
            $idKartuStok[] = $value->id;
        }
        /**
         * Cek jika stok sudah dikeluarkan
         */
        if (!empty($idKartuStok)) {
            $cek = $this->db->find("select count(*) as total from inv_kartu_stok where inv_kartu_stok_id IN (" . implode(",", $idKartuStok) . ")");
        }

        if (isset($cek->total) && $cek->total > 0) {
            return false;
        } else {
            return true;
        }
    }


    /**
     * Validasi data yang dikirim.
     *
     * @param array $data
     * @param array $custom
     */
    public function validasi($data, $custom = [])
    {
        $validasi = [
            'no_invoice' => 'required',
            'tanggal' => 'required',
        ];

        return validate($data, $validasi, $custom);
    }

    public function validasi_detail($detail, $custom = array())
    {
        $cek = true;

        foreach ($detail as $key => $value) {
            if (empty($value['m_barang_id'])) {
                $cek = false;
            }

            if (empty($value['jumlah'])) {
                $cek = false;
            }

            if (empty($value['harga'])) {
                $cek = false;
            }
        }

        return $cek;
    }

}
