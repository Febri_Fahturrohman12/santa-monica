<?php

namespace Model;

use Service\Db;
use Service\Landa;
use Service\LandaInv;

class LaporanPengambilanKas
{
    /**
     * simpan class Landa DB ke variabel #db.
     */
    private $db;

    /**
     * variabel untuk menyimpan nama tabel.
     */
    private $table;

    /**
     * konstruktor memanggil library landa Db.
     */
    public function __construct()
    {
        $this->db = Db::db();
        $this->landa = new Landa();
        $this->table = 't_penjualan';
    }

    public function getAll($params = [], $limit = 0, $offset = 0, $order = '')
    {   
        
        $this->db->select('
        t_pengambilan_kas.*,
        m_user.nama as nama_karyawan
        ')
        ->from('t_pengambilan_kas')
        ->leftJoin('m_user', 'm_user.id = t_pengambilan_kas.user_id');

        if (isset($params['periode_mulai']) && !empty($params['periode_mulai']) && isset($params['periode_selesai']) && !empty($params['periode_selesai'])) {
            $this->db->customWhere("t_pengambilan_kas.tanggal >= '" . $params['periode_mulai'] . "' and t_pengambilan_kas.tanggal <= '" . $params['periode_selesai'] . "'", "AND");
        }
        if(isset($params['user_id']) && !empty($params['user_id']) && $params['user_id'] != "null" && $params['user_id'] != "undefined" && $params['user_id'] != null){
            $this->db->where("t_pengambilan_kas.user_id", "=", $params['user_id']);
        }

        $this->db->orderBy("t_pengambilan_kas.tanggal DESC");

        $models = $this->db->findAll();
        $totalItem = $this->db->count();

        $arr = [];
        $total = [
            'total_nominal' => 0,
        ];
        if (!empty($models)) {
            foreach ($models as $key => $value) {
                $arr[$value->user_id]['id'] = $value->id;
                $arr[$value->user_id]['nama'] = $value->nama;
                $arr[$value->user_id]['nama_karyawan'] = $value->nama_karyawan;
                $arr[$value->user_id]['tanggal'] = date('d/m/y', strtotime($value->tanggal));
                $arr[$value->user_id]['user_id'] = $value->user_id;
                $arr[$value->user_id]['nominal'] = $value->nominal;
                $arr[$value->user_id]['keterangan'] = $value->keterangan;
                $arr[$value->user_id]['created_at'] = $value->created_at;
                $arr[$value->user_id]['created_by'] = $value->created_by;
                $arr[$value->user_id]['modified_at'] = $value->modified_at;
                $arr[$value->user_id]['modified_by'] = $value->modified_by;

                if (!isset($arr[$value->user_id]['total_nominal'])) {
                    $arr[$value->user_id]['total_nominal'] = 0;
                }
                
                $arr[$value->user_id]['total_nominal'] += $value->nominal;
                $arr[$value->user_id]['detail'][] = $value;

                $arr[$value->user_id]['rowspan'] = count($arr[$value->user_id]['detail']);
                $arr[$value->user_id]['detail_length'] = count($arr[$value->user_id]['detail']);

                $total['total_nominal'] += $value->nominal;
            }
        }

        // EXPORT EXCEL
        if (isset($params['is_export']) && $params['is_export'] == 1) {
            $params['periode_mulai'] = getDateIndo($params['periode_mulai']);
            $params['periode_selesai'] = getDateIndo($params['periode_selesai']);
            $view = twigView();

            $content = $view->fetch('laporan/pengambilankas.html', [
                'data' => array_values($arr),
                'totalData' => $total,
                'filter' => $params
            ]);
            
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;Filename="Laporan Pengambilan Kas.xls"');
            echo $content;
        } else {
            return [
                'data' => array_values($arr),
                'total' => $total,
                'totalItem' => $totalItem,
            ];
        }
    }

    public function hapus($params = [])
    {
        try {
            $db = $this->db;
            $penjualan = $db->find("SELECT * FROM t_penjualan where id = '" . $params['id'] . "' ");
            $penjualan->riwayat_nota = $penjualan->kode_nota;
            $penjualan->penjualan_id = $penjualan->id;
            $penjualan->riwayat_id = $penjualan->id;
            $penjualan->alasan = $params['alasan'];
            $penjualan->modified_by = $_SESSION['user']['id'];
            unset($penjualan->modified_at);

            /**
             * insert pelayanan history
             */
            $penjualanJson = json_decode(json_encode($penjualan), TRUE);
            unset($penjualanJson['id']);

            $db->insert("t_penjualan_hapus_history", $penjualanJson);

            /**
             * Ambil kartu stok
             */
            $db->select("
                    inv_kartu_stok.id,
                    inv_kartu_stok.m_barang_id,
                    inv_kartu_stok.jumlah_keluar,
                    inv_kartu_stok.inv_kartu_stok_id
                ")->from("inv_kartu_stok")
                ->where("reff_type", "=", "t_penjualan_id")
                ->andWhere("reff_id", "=", $penjualan->id);
            $model = $db->findAll();

            foreach ($model as $key => $value) {
                /**
                 * Kembalikan stok
                 */
                $db->run("update inv_kartu_stok set is_habis = 0 where id = '" . $value->inv_kartu_stok_id . "'");
                /**
                 * Hapus stok penjualan
                 */
                $db->delete("inv_kartu_stok", ["id" => $value->id]);
            }


            $detail = $db->findAll("select * from t_penjualan_detail where penjualan_id = '" . $penjualan->id . "'");
            foreach ($detail as $key => $value) {
                $value->alasan = $params['alasan'];
                $value->modified_by = $_SESSION['user']['id'];
                $value->riwayat_id = $value->id;
                unset($value->modified_at);
                $valueJson = json_decode(json_encode($value), TRUE);
                unset($valueJson['id']);
                $db->insert("t_penjualan_detail_hapus_history", $valueJson);
            }

            $deletePembayaran = $db->delete("t_pembayaran", ["penjualan_id" => $penjualan->id]);
            $deletePenjualan = $db->delete("t_penjualan", ["id" => $penjualan->id]);

            return [
                'status' => true,
                'data' => 'berhasil',
            ];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }
}
