<?php

namespace Model;

use Service\Db;
use Service\Landa;
use Service\LandaInv;

class LaporanPenjualan
{
    /**
     * simpan class Landa DB ke variabel #db.
     */
    private $db;

    /**
     * variabel untuk menyimpan nama tabel.
     */
    private $table;

    /**
     * konstruktor memanggil library landa Db.
     */
    public function __construct()
    {
        $this->db = Db::db();
        $this->landa = new Landa();
        $this->table = 't_penjualan';
    }

    public function getAll($params = [], $limit = 0, $offset = 0, $order = '')
    {
        $this->db->select('
        t_penjualan.*,
        t_penjualan_detail.id as t_penjualan_detail_id,
        t_penjualan_detail.penjualan_id,
        t_penjualan_detail.tipe,
        t_penjualan_detail.barang_id,
        t_penjualan_detail.jumlah,
        t_penjualan_detail.harga,
        t_penjualan_detail.harga,
        t_penjualan_detail.jenis_diskon,
        IFNULL(t_penjualan_detail.diskon,0) as diskon_detail,
        t_penjualan_detail.terapis_id,
        t_penjualan_detail.kasir_id,
        t_penjualan_detail.k_perusahaan,
        t_penjualan_detail.k_therapist,
        t_penjualan_detail.k_manager,
        t_penjualan_detail.k_marketing,
        m_customer.nama as nama_customer,
        m_customer.no_telepon as no_telepon,
        m_layanan.nama as nama_layanan,
        m_barang.nama as nama_barang,
        m_user.nama as nama_terapis
        ')
            ->from('t_penjualan_detail')
            ->innerJoin("t_penjualan", "t_penjualan.id = t_penjualan_detail.penjualan_id")
            ->leftJoin("m_layanan", "m_layanan.id = t_penjualan_detail.barang_id AND t_penjualan_detail.tipe = 'layanan'")
            ->leftJoin("m_barang", "m_barang.id = t_penjualan_detail.barang_id AND t_penjualan_detail.tipe = 'barang'")
            ->leftJoin("m_customer", "m_customer.id = t_penjualan.customer_id")
            ->leftJoin("m_user", "m_user.id = t_penjualan.terapis_id");

        if (isset($params['periode_mulai']) && !empty($params['periode_mulai']) && isset($params['periode_selesai']) && !empty($params['periode_selesai'])) {
            $this->db->customWhere("FROM_UNIXTIME(t_penjualan.created_at,'%Y-%m-%d') >= '" . $params['periode_mulai'] . "' and FROM_UNIXTIME(t_penjualan.created_at,'%Y-%m-%d') <= '" . $params['periode_selesai'] . "'", "AND");
        }

        $this->db->orderBy("t_penjualan.created_at DESC");
        $models = $this->db->findAll();
        $totalItem = $this->db->count();

        $arr = [];
        $total = [
            'jumlah' => 0,
            'harga' => 0,
            'diskon_detail' => 0,
            'service_layanan' => 0,
            'sub_total' => 0,
            'total_all' => 0,
            'k_terapis' => 0,
            'k_manager' => 0,
            'k_marketing' => 0,
            'k_perusahaan' => 0
        ];


        $temp = [];
        if (!empty($models)) {
            foreach ($models as $key => $value) {


                $value->sub_total = ($value->harga * $value->jumlah) - $value->diskon_detail;
                $value->komisi_terapis = !empty($value->k_therapist) ? ($value->harga * $value->k_therapist) / 100 : 0;
                $value->komisi_manager = !empty($value->k_manager) ? (($value->sub_total - $value->komisi_terapis) * $value->k_manager) / 100 : 0;
                $value->komisi_marketing = !empty($value->k_marketing) ? ($value->sub_total * $value->k_marketing) / 100 : 0;
//                $value->komisi_perusahaan = !empty($value->k_perusahaan) ? ($value->harga * $value->k_perusahaan) / 100 : 0;
                $value->komisi_perusahaan = $value->sub_total - $value->komisi_terapis - $value->komisi_manager - $value->komisi_marketing;

//                $value->diskon= !empty($value->diskon) ? $value->diskon : 0;
                $arr[$value->penjualan_id]['id'] = $value->id;
                $arr[$value->penjualan_id]['nama_customer'] = $value->nama_customer;
                $arr[$value->penjualan_id]['nama_terapis'] = $value->nama_terapis;
                $arr[$value->penjualan_id]['no_telepon_export'] = telp62($value->no_telepon);
                $arr[$value->penjualan_id]['no_telepon'] = !empty($value->no_telepon) ? telp62($value->no_telepon) : '-';
                $arr[$value->penjualan_id]['waktu'] = date('H:i', strtotime($value->waktu));
                $arr[$value->penjualan_id]['kode_nota'] = $value->kode_nota;
                $arr[$value->penjualan_id]['service_layanan'] = $value->service_layanan;
                $arr[$value->penjualan_id]['aaa '] = $value->service_layanan;
                $arr[$value->penjualan_id]['customer_id'] = $value->customer_id;
                $arr[$value->penjualan_id]['diskon'] = $value->diskon;
                $arr[$value->penjualan_id]['keterangan'] = $value->keterangan;
                $arr[$value->penjualan_id]['terapis_id'] = $value->terapis_id;
                $arr[$value->penjualan_id]['status'] = $value->status;
                $arr[$value->penjualan_id]['is_deleted'] = $value->is_deleted;
                $arr[$value->penjualan_id]['tanggal'] = getDateIndo(date('Y-m-d', $value->created_at));
                $arr[$value->penjualan_id]['created_at'] = $value->created_at;
                $arr[$value->penjualan_id]['created_by'] = $value->created_by;
                $arr[$value->penjualan_id]['modified_at'] = $value->modified_at;
                $arr[$value->penjualan_id]['modified_by'] = $value->modified_by;

                if (!isset($arr[$value->penjualan_id]['total'])) {
                    $arr[$value->penjualan_id]['total'] = 0;
                }

                $arr[$value->penjualan_id]['total'] += $value->sub_total;
                $arr[$value->penjualan_id]['detail'][] = $value;

                $arr[$value->penjualan_id]['rowspan'] = count($arr[$value->penjualan_id]['detail']);
                $arr[$value->penjualan_id]['detail_length'] = count($arr[$value->penjualan_id]['detail']);

                $total['jumlah'] += $value->jumlah;
                $total['harga'] += $value->harga;
                $total['diskon_detail'] += $value->diskon_detail;

                if (!in_array($value->id, $temp)) {
                    $total['service_layanan'] += $arr[$value->penjualan_id]['service_layanan'];
                    $temp[] = $value->id;
                }

                $total['sub_total'] += $value->sub_total;
                $total['total_all'] += ($value->harga * $value->jumlah) - $value->diskon_detail;
                $total['k_terapis'] += $value->komisi_terapis;
                $total['k_manager'] += $value->komisi_manager;
                $total['k_marketing'] += $value->komisi_marketing;
                $total['k_perusahaan'] += $value->komisi_perusahaan;
            }
        }
//print_die($arr);

        if (isset($params['is_export']) && $params['is_export'] == 1) {
            $params['periode_mulai'] = getDateIndo($params['periode_mulai']);
            $params['periode_selesai'] = getDateIndo($params['periode_selesai']);
            $view = twigView();
//print_die(array_values($arr));
            $content = $view->fetch('laporan/penjualan.html', [
                'data' => array_values($arr),
                'totalData' => $total,
                'filter' => $params
            ]);

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;Filename="Laporan Penjualan.xls"');
            echo $content;
        } else {
            return [
                'data' => array_values($arr),
                'total' => $total,
                'totalItem' => $totalItem,
            ];
        }
    }

    public function hapus($params = [])
    {
        try {
            $db = $this->db;
            $penjualan = $db->find("SELECT * FROM t_penjualan where id = '" . $params['id'] . "' ");
            $penjualan->riwayat_nota = $penjualan->kode_nota;
            $penjualan->penjualan_id = $penjualan->id;
            $penjualan->riwayat_id = $penjualan->id;
            $penjualan->alasan = $params['alasan'];
            $penjualan->modified_by = $_SESSION['user']['id'];
            unset($penjualan->modified_at);

            /**
             * insert pelayanan history
             */
            $penjualanJson = json_decode(json_encode($penjualan), TRUE);
            unset($penjualanJson['id']);

            $db->insert("t_penjualan_hapus_history", $penjualanJson);

            /**
             * Ambil kartu stok
             */
            $db->select("
                    inv_kartu_stok.id,
                    inv_kartu_stok.m_barang_id,
                    inv_kartu_stok.jumlah_keluar,
                    inv_kartu_stok.inv_kartu_stok_id
                ")->from("inv_kartu_stok")
                ->where("reff_type", "=", "t_penjualan_id")
                ->andWhere("reff_id", "=", $penjualan->id);
            $model = $db->findAll();

            foreach ($model as $key => $value) {
                /**
                 * Kembalikan stok
                 */
                $db->run("update inv_kartu_stok set is_habis = 0 where id = '" . $value->inv_kartu_stok_id . "'");
                /**
                 * Hapus stok penjualan
                 */
                $db->delete("inv_kartu_stok", ["id" => $value->id]);
            }


            $detail = $db->findAll("select * from t_penjualan_detail where penjualan_id = '" . $penjualan->id . "'");
            foreach ($detail as $key => $value) {
                $value->alasan = $params['alasan'];
                $value->modified_by = $_SESSION['user']['id'];
                $value->riwayat_id = $value->id;
                unset($value->modified_at);
                $valueJson = json_decode(json_encode($value), TRUE);
                unset($valueJson['id']);
                $db->insert("t_penjualan_detail_hapus_history", $valueJson);
            }

            $deletePembayaran = $db->delete("t_pembayaran", ["penjualan_id" => $penjualan->id]);
            $deletePenjualan = $db->delete("t_penjualan", ["id" => $penjualan->id]);

            return [
                'status' => true,
                'data' => 'berhasil',
            ];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }
}
