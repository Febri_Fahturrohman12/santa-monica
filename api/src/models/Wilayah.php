<?php

namespace Model;

use Exception;
use Service\Db;
use Service\Landa;

class Wilayah
{
    /**
     * simpan class Landa DB ke variabel #db.
     */
    private $db;

    /**
     * variabel untuk menyimpan nama tabel.
     */
    private $table;

    /**
     * konstruktor memanggil library landa Db.
     */
    public function __construct()
    {
        $this->db = Db::db();
        $this->landa = new Landa();
        $this->table = '';
    }

    
    /**
     * Ambil list provinsi.
     *
     * @return [array] [list data provinsi]
     */
    public function getDataProvinsi()
    {
        $this->db->select('*')
            ->from('w_provinsi')->where('is_deleted', '=', '0');
        $models = $this->db->findAll();
        $totalItem = $this->db->count();

        return [
            'data' => $models,
            'totalItem' => $totalItem,
        ];
    }
    public function getDataKota($id)
    {
        $this->db->select('*, kota as text')
            ->from('w_kota')->where('is_deleted', '=', '0');

        if ('0' != $id) {
            $this->db->andWhere('provinsi_id', '=', $id);
        }

        $models = $this->db->findAll();
        $totalItem = $this->db->count();

        return [
            'data' => $models,
            'totalItem' => $totalItem,
        ];
    }

    public function getAllKota()
    {
        
        $this->db
            ->select('w_kota.id, w_kota.kota, w_provinsi.provinsi')
            ->from('w_kota w_kota')
            ->leftJoin('w_provinsi w_provinsi', 'w_provinsi.id = w_kota.provinsi_id')
            ->orderBy('w_kota.id');
        $models = $this->db->findAll();
        $totalItem = $this->db->count();
        foreach($models as $val):
            $arr[] = [
                "id" => $val->id,
                "kota" => $val->kota . ", " . $val->provinsi,
            ];
        endforeach;


        return [
            'data' => $arr,
            'totalItem' => $totalItem,
        ];
    }

    public function getDataKecamatan($id)
    {
        
        $this->db->select('*')
            ->from('w_kecamatan')->where('is_deleted', '=', '0');

        if ('0' != $id) {
            $this->db->andWhere('kota_id', '=', $id);
        }

        $models = $this->db->findAll();
        $totalItem = $this->db->count();

        return [
            'data' => $models,
            'totalItem' => $totalItem,
        ];
    }

    public function getDataDesa($id)
    {
        
        $this->db->select('*')
            ->from('w_desa')->where('is_deleted', '=', '0');

        if ('0' != $id) {
            $this->db->andWhere('kecamatan_id', '=', $id);
        }

        $models = $this->db->findAll();
        $totalItem = $this->db->count();

        return [
            'data' => $models,
            'totalItem' => $totalItem,
        ];
    }
    /**
     * Validasi data yang dikirim.
     *
     * @param array $data
     * @param array $custom
     */
    public function validasi($data, $custom = [])
    {
        $validasi = [
            'nama' => 'required',
            'kode' => 'required'
        ];

        return validate($data, $validasi, $custom);
    }
}
