<?php

namespace Model;

use Service\Db;
use Service\Landa;
use Service\LandaInv;

class LaporanKartuStok
{
    /**
     * simpan class Landa DB ke variabel #db.
     */
    private $db;

    /**
     * variabel untuk menyimpan nama tabel.
     */
    private $table;

    /**
     * konstruktor memanggil library landa Db.
     */
    public function __construct()
    {
        $this->db = Db::db();
        $this->landa = new Landa();
        $this->table = 'inv_kartu_stok';
    }

    public function getAll($params = [], $limit = 0, $offset = 0, $order = '')
    {
        $db = $this->db;
        $startDate = getTanggal($params["periode_mulai"]);
        $endDate = getTanggal($params["periode_selesai"]);
        $barangId = $params["m_barang_id"];
        /**
         * Ambil data item
         */
        $barang = $db->select('m_barang.*')
            ->from('m_barang')
            ->where('m_barang.id', '=', $barangId)
            ->find();

        /**
         * Ambil Saldo Awal
         */
        $db->select("
            induk.id,
            induk.harga_masuk,
            induk.hpp,
            induk.jumlah_masuk,
            sum(inv_kartu_stok.jumlah_keluar) as jumlah_keluar
        ")
            ->from("inv_kartu_stok as induk")
            ->leftJoin("inv_kartu_stok", "induk.id = inv_kartu_stok.inv_kartu_stok_id and inv_kartu_stok.tanggal < '" . $startDate . "'")
            ->where("induk.m_barang_id", "=", $barang->id)
            ->customWhere("induk.tanggal < '" . $startDate . "'", "AND")
            ->andWhere("induk.jenis_stok", "=", "masuk")
            ->orderBy("induk.tanggal ASC, induk.id ASC")
            ->groupBy("induk.id");

        $modelSaldoAwal = $db->findAll();

        $saldoAwal = [];
        foreach ($modelSaldoAwal as $key => $value) {
            $sisa = $value->jumlah_masuk - $value->jumlah_keluar;
            if ($sisa > 0) {
                $saldoAwal[$value->id] = [
                    "stok" => (int)$sisa,
                    "harga_masuk" => (int)$value->harga_masuk,
                    "total" => (int)$sisa * $value->harga_masuk,
                ];
            }
        }

        $arrSaldoAwal = [];
        /**
         * Inialisasi saldo awal
         */
        $data[1]['jumlah_saldo'][0] = ['nilai' => 0];
        $data[1]['harga_saldo'][0] = ['nilai' => 0];
        $data[1]['total_saldo'][0] = ['nilai' => 0];
        $saldo = [
            'jumlah' => 0,
            'saldo' => 0,
            'harga' => 0,
        ];

        $hargaTmp[] = [
            'jumlah' => 0,
            'harga' => 0,
            'saldo' => 0,
        ];
        foreach ($saldoAwal as $key => $value) {
            /**
             * Set saldo awal berdasarkan harga masuk
             */
            if ($value["stok"] > 0) {
                $arrSaldoAwal[$value['harga_masuk']]["jumlah"] = (isset($arrSaldoAwal[$value['harga_masuk']]["jumlah"]) ? $arrSaldoAwal[$value['harga_masuk']]["jumlah"] : 0) + $value['stok'];
                $arrSaldoAwal[$value['harga_masuk']]["harga"] = $value['harga_masuk'];
                $arrSaldoAwal[$value['harga_masuk']]["saldo"] = $value['harga_masuk'] * $arrSaldoAwal[$value['harga_masuk']]["jumlah"];
            }
        }

        /**
         * Set detail saldo awal
         */
        $data[1]['kode'] = '';
        $data[1]['catatan'] = 'Saldo Awal';
        $data[1]['tanggal'] = $startDate;
        $data[1]['saldo'] = $arrSaldoAwal;
        if (!empty($arrSaldoAwal)) {
            foreach ($arrSaldoAwal as $key => $value) {
                $hargaTmp[] = [
                    'jumlah' => $value['jumlah'],
                    'harga' => $value['harga'],
                    'saldo' => $value['saldo'],
                ];
            }
        }

        /**
         * Siapkan histori kartu stok
         */
        $db->select("inv_kartu_stok.*, m_user.nama as petugas")
            ->from("inv_kartu_stok")
            ->leftJoin("m_barang", "m_barang.id = inv_kartu_stok.m_barang_id")
            ->leftJoin("m_user", "m_user.id = inv_kartu_stok.created_by")
            ->where("m_barang_id", "=", $barang->id)
            ->customWhere("inv_kartu_stok.tanggal >= '" . $startDate . "'", "AND")
            ->customWhere("inv_kartu_stok.tanggal <= '" . $endDate . "'", "AND")
            ->orderBy("inv_kartu_stok.tanggal ASC, inv_kartu_stok.id ASC");

        $models = $db->findAll();

        $harga_masuk_terakhir = 0;
        foreach ($models as $key => $value) {
            if ($value->jenis_stok == 'masuk') {
                /**
                 * Saldo Akhir Selain average
                 */
                $hargaTmp[] = [
                    'jumlah' => $value->jumlah_masuk,
                    'harga' => $value->harga_masuk,
                    'saldo' => $value->harga_masuk * $value->jumlah_masuk,
                ];

                $tot_masuk = $value->harga_masuk * $value->jumlah_masuk;
                $data[$value->kode . $value->id]['jumlah_keluar'] = [];
                $data[$value->kode . $value->id]['hargas_keluar'] = [];
                $data[$value->kode . $value->id]['total_keluar'] = [];
                $data[$value->kode . $value->id]['jumlah_masuk'][] = ['nilai' => $value->jumlah_masuk];
                $data[$value->kode . $value->id]['harga_masuk'][] = ['nilai' => $value->harga_masuk];
                $data[$value->kode . $value->id]['total_masuk'][] = ['nilai' => $tot_masuk];
                $data[$value->kode . $value->id]['saldo'] = $hargaTmp;

            } else if ($value->jenis_stok == 'keluar') {
                if ($value->catatan == 'Unpost Penerimaan') {
                    $value->hpp = $value->harga_keluar;
                }

                $total = $value->hpp * $value->jumlah_keluar;
                $stop = false;
                $arrTmp = $hargaTmp;

                /**
                 * Pengurangan Stok selain Average
                 */
                $sisaStok = round($value->jumlah_keluar);
                $valHpp = $value->hpp;
                foreach ($hargaTmp as $k => $v) {
                    if ($v['harga'] == $valHpp) {
                        $sisa = $hargaTmp[$k]['jumlah'] - $sisaStok;
                        if ($sisa < 0) {
                            $hargaTmp[$k]['jumlah'] = 0;
                            $sisaStok -= $v['jumlah'];
                        } else {
                            $hargaTmp[$k]['jumlah'] = $sisa;
                            $sisaStok = 0;
                        }
                        $hargaTmp[$k]['saldo'] = $hargaTmp[$k]['jumlah'] * $hargaTmp[$k]['saldo'];
                    }
                    if ($hargaTmp[$k]['jumlah'] == 0) {
                        unset($hargaTmp[$k]);
                        if (empty($hargaTmp[$k])) {
                            unset($hargaTmp[$k]);
                        }
                    }
                    if ($v['harga'] == $valHpp && $sisaStok == 0) {
                        $stop = true;
                        break;
                    }
                }

                $data[$value->kode . $value->id]['jumlah_masuk'] = [];
                $data[$value->kode . $value->id]['harga_masuk'] = [];
                $data[$value->kode . $value->id]['total_masuk'] = [];
                $data[$value->kode . $value->id]['jumlah_keluar'][] = ['nilai' => $value->jumlah_keluar];
                $data[$value->kode . $value->id]['hargas_keluar'][] = ['nilai' => $value->hpp];
                $data[$value->kode . $value->id]['total_keluar'][] = ['nilai' => $total];
                $data[$value->kode . $value->id]['saldo'] = array_values($hargaTmp);

            } else {
                unset($models[$key]);
            }
            $data[$value->kode . $value->id]['kode'] = $value->kode;
            $data[$value->kode . $value->id]['reff_id'] = $value->reff_id;
            $data[$value->kode . $value->id]['reff_type'] = $value->reff_type;
            $data[$value->kode . $value->id]['catatan'] = $value->catatan;
            $data[$value->kode . $value->id]['tanggal'] = $value->tanggal;
            $data[$value->kode . $value->id]['petugas'] = $value->petugas;
        }
//print_die($data);
        /**
         * Atur saldo akhir
         */

        foreach ($data as $key => $val) {
            if ($key != '1') {
                $data[$key]['jumlah_saldo'][] = ['nilai' => 0];
                $data[$key]['harga_saldo'][] = ['nilai' => 0];
                $data[$key]['total_saldo'][] = ['nilai' => 0];
            }

            if (isset($val['saldo'])) {
                foreach ($val['saldo'] as $k => $v2) {
                    if (isset($data[$key]['total_saldo'][0]) && $data[$key]['total_saldo'][0]['nilai'] == 0) {
                        unset($data[$key]['jumlah_saldo'][0]);
                        unset($data[$key]['harga_saldo'][0]);
                        unset($data[$key]['total_saldo'][0]);
                    }
                    $data[$key]['jumlah_saldo'][] = ['nilai' => $v2['jumlah']];
                    $data[$key]['harga_saldo'][] = ['nilai' => $v2['harga']];
                    $data[$key]['total_saldo'][] = ['nilai' => $v2['jumlah'] * $v2['harga']];

                    $data[$key]['jumlah_saldo'] = array_values($data[$key]['jumlah_saldo']);
                    $data[$key]['harga_saldo'] = array_values($data[$key]['harga_saldo']);
                    $data[$key]['total_saldo'] = array_values($data[$key]['total_saldo']);
                }
            }
            unset($data[$key]['saldo']);
        }

        $barang->detail = array_values($data);

        return [
            'data' => $barang
        ];
    }
}
