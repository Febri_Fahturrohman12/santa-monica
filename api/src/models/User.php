<?php

namespace Model;

use Service\Db;
use Service\Landa;

class User
{
    /**
     * simpan class Landa DB ke variabel #db.
     */
    private $db;

    /**
     * variabel untuk menyimpan nama tabel.
     */
    private $table;

    /**
     * konstruktor memanggil library landa Db.
     */
    public function __construct()
    {
        $this->db = Db::db();
        $this->landa = new Landa();
        $this->table = 'm_user';
    }

    /**
     * Ambil semua data perusahaan.
     *
     * @param array $params
     * @param int $limit
     * @param int $offset
     * @param string $order
     */
    public function getAll($params = [], $limit = 0, $offset = 0, $order = '')
    {
        $this->db->select('m_user.*, 
        m_user.no_hp as telepon,
         date(m_user.tgl_lahir) as tgl_lahir,
         m_roles.nama nama_roles,
         m_user.w_provinsi_id as provinsi_id,
         m_user.w_kota_id as kota_id,
         m_user.w_kecamatan_id as kecamatan_id,
         m_user.w_desa_id as desa_id
         ')
            ->from('m_user')
            ->leftJoin("m_roles", "m_roles.id = m_user.m_roles_id");

        $filter = (array)json_decode($params["filter"]);
        if (isset($params) && !empty($params)) {
            foreach ($filter as $key => $val) {
                if ('id' == $key) {
                    $this->db->where('id', '=', $val);
                } elseif ('nama' == $key) {
                    $this->db->where('m_user.nama', 'like', $val);
                } elseif ('status' == $key) {
                    $this->db->where('m_user.is_deleted', '=', $val);
                } else {
                    $this->db->where($key, 'like', $val);
                }
            }
        }
        // Set limit
        if (isset($params['limit']) && !empty($params['limit'])) {
            $this->db->limit($params['limit']);
        }
        // Set offset
        if (isset($params['offset']) && !empty($params['offset'])) {
            $this->db->offset($params['offset']);
        }
        $this->db->orderBy("m_user.m_roles_id ASC, m_user.id DESC");
        $models = $this->db->findAll();
        $totalItem = $this->db->count();

        return [
            'data' => $models,
            'totalItem' => $totalItem,
        ];
    }

    /**
     * Method untuk menyimpan data user.
     *
     * @param array $data
     * @param array $customParams
     * @param mixed $params
     */
    public function save($params, $customParams = [])
    {
        try {

            /**
             * CEK USERNAME SAMA
             */

            if (isset($params['username']) && !empty($params['username'])){
                $this->db->select("id,nama")
                    ->from("m_user")
                    ->where("username","=", $params['username'])
                    ->where("is_deleted","=",0);

                if (isset($params['id'])){
                    $this->db->where("id","!=",$params['id']);
                }

                $cekUser = $this->db->find();

                if (!empty($cekUser)){
                    return [
                        'status' => false,
                        'error' => ['Mohon maaf, username sudah terdaftar'],
                    ];
                }
            }

            if (isset($params['id'])) {

                if (!empty($params['password'])) {
                    $params["password"] = sha1($params["password"]);
                } else {
                    unset($params["password"]);
                }
            } else {
                $params["password"] = isset($params["password"]) ? sha1($params["password"]) : "";
            }

            if (isset($params['tgl_lahir'])) {
                $params['tgl_lahir'] = $this->landa->arrayToDate($params['tgl_lahir']);
            }

            $params["w_provinsi_id"] = isset($params["provinsi_id"]) ? $params["provinsi_id"] : null;
            $params["w_kota_id"] = isset($params["kota_id"]) ? $params["kota_id"] : null;
            $params["w_kecamatan_id"] = isset($params["kecamatan_id"]) ? $params["kecamatan_id"] : null;
            $params["w_desa_id"] = isset($params["desa_id"]) ? $params["desa_id"] : null;
            $params["m_roles_id"] = isset($params["m_roles_id"]) ? $params["m_roles_id"] : null;

            if (isset($params["id"])) {
                $model = $this->db->update("m_user", $params, ["id" => $params["id"]]);

                setHistori('m_user_id', $model->id, 'Updated');
            } else {
                $model = $this->db->insert("m_user", $params);

                setHistori('m_user_id', $model->id, 'Created');
            }

            return [
                'status' => true,
                'data' => $model,
            ];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }

    /**
     * Hapus hak akses.
     *
     * @param array $params
     *
     * @return array
     */
    public function restore($params)
    {
        try {
            $model = $this->db->update($this->table, ['is_deleted' => 0], ['id' => $params['id']]);
            setHistori('m_user_id', $params['id'], 'Restore');

            return [
                'status' => true,
                'data' => $model,
            ];
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }

    /**
     * Hapus hak akses.
     *
     * @param array $params
     *
     * @return array
     */
    public function delete($params)
    {
        try {
            $model = $this->db->update($this->table, ['is_deleted' => 1], ['id' => $params['id']]);
            setHistori('m_user_id', $params['id'], 'Deleted');

            return [
                'status' => true,
                'data' => $model,
            ];
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }

    /**
     * Validasi data yang dikirim.
     *
     * @param array $data
     * @param array $custom
     */
    public function validasi($data, $custom = [])
    {
        $validasi = [
            'nama' => 'required',
            'm_roles_id' => 'required'
        ];

        return validate($data, $validasi, $custom);
    }
}
