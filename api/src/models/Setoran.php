<?php

namespace Model;

use Service\Db;
use Service\Landa;

class Setoran
{
    /**
     * simpan class Landa DB ke variabel #db.
     */
    private $db;

    /**
     * variabel untuk menyimpan nama tabel.
     */
    private $table;

    /**
     * konstruktor memanggil library landa Db.
     */
    public function __construct()
    {
        $this->db = Db::db();
        $this->landa = new Landa();
        $this->table_penerimaan = 't_setoran';
        $this->table_barang = 'm_barang';
    }

    /**
     * Ambil semua data penerimaan.
     *
     * @param array $params
     * @param int $limit
     * @param int $offset
     * @param string $order
     */

    public function getAll($params = [], $limit = 0, $offset = 0, $order = '')
    {
        $this->db->select('t_setoran.*, m_user.nama as dibuat_oleh')
            ->from('t_setoran')
            ->join('left join', 'm_user', 'm_user.id = t_setoran.created_by');

        $filter = (array)json_decode($params["filter"]);
        if (isset($params) && !empty($params)) {
            foreach ($filter as $key => $val) {
                if ('id' == $key) {
                    $this->db->where('id', '=', $val);
                } else if ('is_deleted' == $key) {
                    $this->db->where('t_setoran.is_deleted', '=', $val);
                }  else {
                    $this->db->where($key, 'like', $val);
                }
            }
        }
        // Set limit
        if (isset($params['limit']) && !empty($params['limit'])) {
            $this->db->limit($params['limit']);
        }
        // Set offset
        if (isset($params['offset']) && !empty($params['offset'])) {
            $this->db->offset($params['offset']);
        }
        $this->db->orderBy("t_setoran.id DESC");
        $models = $this->db->findAll();
        $totalItem = $this->db->count();
        foreach ($models as $key => $value) {
            $value->dibuat_pada = date('d M Y', $value->created_at);
        }

        return [
            'data' => $models,
            'totalItem' => $totalItem,
        ];
    }



    public function getBarang($params = [])
    {
        $this->db->select('*')
            ->from($this->table_barang)
            ->where($this->table_barang . ".is_deleted", "=", 0);

        $models = $this->db->findAll();
        return [
            'data' => $models
        ];
    }
    public function getDetail($id)
    {
        $this->db->select('*')
            ->from("t_setoran_detail")
            ->where("t_setoran_detail.t_setoran_id", "=", $id);

        $models = $this->db->findAll();
        return [
            'data' => $models
        ];
    }

    public function cekNoInvoice($params)
    {
        $this->db->select("no_invoice")
            ->from("inv_penerimaan")
            ->where("no_invoice", "=", $params["no_invoice"]);

        if (isset($params["id"]) && !empty($params["id"])) {
            $this->db->andWhere("id", "!=", $params["id"]);
        }
        $cek = $this->db->find();
        if (isset($cek->no_invoice)) {
            return [
                'status' => false,
                'error' => "No Invoice " . $cek->no_invoice . " sudah terdaftar",
            ];
        }
    }

    /**
     * SIMPAN DATA DAN DETAIL
     */

    public function save($params)
    {
        try {
            if (isset($params["data"]["id"])) {
                $model = $this->db->update("t_setoran", $params["data"], ["id" => $params["data"]["id"]]);
                $this->db->delete("t_setoran_detail", ["t_setoran_id" => $params["data"]["id"]]);
            } else {
                $model = $this->db->insert("t_setoran", $params["data"]);
            }

            /**
             * Simpan detail
             */
            $total = 0;
            if (isset($params["detail"]) && !empty($params["detail"])) {
                foreach ($params["detail"] as $key => $val) {
                    $total++;
                    $detail["t_setoran_id"] = $model->id;
                    $detail["m_barang_id"] = isset($val["m_barang_id"]) ? $val["m_barang_id"] : '';
                    $detail["jumlah"] = isset($val["jumlah"]) ? $val["jumlah"] : '';

                    if ($detail['m_barang_id'] > 0) {
                        $modelDetail = $this->db->insert("t_setoran_detail", $detail);
                    }
                }
            }

            $model->total_item = $total;
            return [
                'status' => true,
                'data' => $model
            ];
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }

    /**
     * Hapus kebutuhan setoran.
     *
     * @param array $params
     *
     * @return array
     */
    public function restore($params)
    {
        try {
            $model = $this->db->update("t_setoran", ['is_deleted' => 0], ['id' => $params['id']]);

            return [
                'status' => true,
                'data' => $model,
            ];
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }

    /**
     * Hapus kebutuhan setoran.
     *
     * @param array $params
     *
     * @return array
     */
    public function delete($params)
    {
        try {
            $model = $this->db->update("t_setoran", ['is_deleted' => 1], ['id' => $params['id']]);

            return [
                'status' => true,
                'data' => $model,
            ];
        } catch (Exception $e) {
            return [
                'status' => false,
                'error' => $e->getMessage(),
            ];
        }
    }


    public function saveStatus($params, $custom = array())
    {
        $cek = true;

        foreach ($detail as $key => $value) {
            if (empty($value['m_barang_id'])) {
                $cek = false;
            }

            if (empty($value['jumlah'])) {
                $cek = false;
            }

            if (empty($value['harga'])) {
                $cek = false;
            }
        }

        return $cek;
    }

    /**
     * CEK STOK TERSEDIA
     */

    public function checkAvailabelUnpostIn($reff_type, $params)
    {
        /**
         * Ambil id kartu stok masuk
         */
        $model = $this->db->select("*")
            ->from("inv_kartu_stok")
            ->where("reff_type", "=", $reff_type)
            ->andWhere("reff_id", "=", $params['id'])
            ->andWhere("jumlah_masuk", ">", 0)
            ->findAll();
        $idKartuStok = [];
        foreach ($model as $key => $value) {
            $idKartuStok[] = $value->id;
        }
        /**
         * Cek jika stok sudah dikeluarkan
         */
        if (!empty($idKartuStok)) {
            $cek = $this->db->find("select count(*) as total from inv_kartu_stok where inv_kartu_stok_id IN (" . implode(",", $idKartuStok) . ")");
        }

        if (isset($cek->total) && $cek->total > 0) {
            return false;
        } else {
            return true;
        }
    }


    /**
     * Validasi data yang dikirim.
     *
     * @param array $data
     * @param array $custom
     */
    public function validasi($data, $custom = [])
    {
        $validasi = [
            'nama' => 'required',
            'tanggal' => 'required',
        ];

        return validate($data, $validasi, $custom);
    }

    public function validasi_detail($detail, $custom = array())
    {
        $cek = true;

        foreach ($detail as $key => $value) {
            if (empty($value['m_barang_id'])) {
                $cek = false;
            }

            if (empty($value['jumlah'])) {
                $cek = false;
            }
        }

        return $cek;
    }

}
