<?php

function validate($data, $validasi, $custom = [])
{
    if (!empty($custom)) {
        $validasiData = array_merge($validasi, $custom);
    } else {
        $validasiData = $validasi;
    }

    $validate = GUMP::is_valid($data, $validasiData);

    if ($validate === true) {
        return true;
    } else {
        return $validate;
    }
}


function print_die($params)
{
    echo json_encode($params);

    exit;
}


function m_to_km($number)
{
    $data = ($number / 1000);

    return $data;
}