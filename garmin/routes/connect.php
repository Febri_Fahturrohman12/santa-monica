<?php
function getConfig()
{
    $config = array(
        'identifier' => getenv('GARMIN_KEY'),
        'secret' => getenv('GARMIN_SECRET'),
        'callback_uri' => getenv('GARMIN_CALLBACK_URI')
    );

    return $config;
}
$app->get('/connect/test', function ($request, $response) {
    ej("Testing");
});

$app->get('/connect/auth', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    // echo json_encode($params);exit();

    try {

        $config = getConfig();
        $server = new Stoufa\GarminApi\GarminApi($config);

        // Retreive temporary credentials from server 
        $temporaryCredentials = $server->getTemporaryCredentials();
        // Save temporary crendentials in session to use later to retreive authorization token
        $_SESSION['temporaryCredentials'] = $temporaryCredentials;

        $garmin_identifier = $temporaryCredentials->getIdentifier();
        $garmin_secret = $temporaryCredentials->getSecret();

        $db->update('m_user', ['garmin_identifier' => $garmin_identifier, 'garmin_secret' => $garmin_secret], ['id' => $params['m_user_id']]);
        // Get authorization link 
        $link = $server->getAuthorizationUrl($temporaryCredentials);
        $link .= '?m_user_id=' . $params['m_user_id'];
        return successResponse($response, ['link' => $link]);
    } catch (\Throwable $th) {
        // catch your exception here
        $error = $th->getMessage();
        return unprocessResponse($response, $error);
    }

})->setName("auth");
$app->get('/connect/callback', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    try {
        $user = $db->select('m_user.*')->from('m_user')->where('id', '=', $params['m_user_id'])->find();
        $config = getConfig();
        $server = new Stoufa\GarminApi\GarminApi($config);
        // echo json_encode($user);die();
        $identifier = $user->garmin_identifier;
        $secret = $user->garmin_secret;
        // Retreive temporary credentials from server 
        $temporaryCredentials = $server->getTemporaryCredentials();
        $temporaryCredentials->setIdentifier($identifier);
        $temporaryCredentials->setSecret($secret);

        // We will now retrieve token credentials from the server.
        $tokenCredentials = $server->getTokenCredentials($temporaryCredentials, $params['oauth_token'], $params['oauth_verifier']);
        // save token identifier in session
        $identifier = $tokenCredentials->getIdentifier();
        $secret = $tokenCredentials->getSecret();
        $data = [
            'garmin_oauth_token' => $params['oauth_token'],
            'garmin_oauth_verifier' => $params['oauth_verifier'],
            'garmin_identifier' => $identifier,
            'garmin_secret' => $secret,
            'is_callback' => 1,
            'is_garmin' => 1
        ];

        $model = $db->update('m_user', $data, ['id' => $params['m_user_id']]);
        return $response->withRedirect(site_url() . 'connect/success?status_code=200');
    } catch (\Throwable $th) {
        // catch your exception here
        $error = $th->getMessage();
        $data = [
            'garmin_oauth_token' => null,
            'garmin_oauth_verifier' => null,
            'garmin_identifier' => null,
            'garmin_secret' => null,
            'is_callback' => 0,
            'is_garmin' => 0
        ];

        $model = $db->update('m_user', $data, ['id' => $params['m_user_id']]);
        return $response->withRedirect(site_url() . 'connect/error?status_code=422');
    }
})->setName("callback");

$app->get('/connect/summary_activity', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    try {
        $user = $db->select('m_user.*')
            ->from('m_user')
            ->where('id', '=', $params['m_user_id'])
            ->find();

        $induk = $db->select("m_kategori.m_kategori_garmin_id, m_kategori.nama, m_kategori_garmin.nama as induk_garmin ")
            ->from("m_event")
            ->leftJoin("m_kategori", "m_kategori.id = m_event.m_kategori_id")
            ->leftJoin("m_kategori_garmin", "m_kategori_garmin.id = m_kategori.m_kategori_garmin_id")
            ->where("m_event.id", "=", $params['m_event_id'])
            ->find();

        $parent = $db->select("m_kategori_garmin.*")
            ->from("m_kategori_garmin")
            ->where("m_kategori_garmin.parent", "=", $induk->induk_garmin)
            ->findAll();

        $arrCek = [];
        if (!empty($induk)) {
            foreach ($parent as $k => $v) {
                $arrCek[] = str_replace(" ", "_", $v->nama);
            }
            array_unshift($arrCek, $induk->induk_garmin);
        }

        // ej($user);
//        $cek = arrayToString($arrCek);
// print_die($cek);
        $config = getConfig();
        $server = new Stoufa\GarminApi\GarminApi($config);
        $tokenCredentials = new League\OAuth1\Client\Credentials\TokenCredentials();
        $identifier = $user->garmin_identifier;
        $secret = $user->garmin_secret;
        $tokenCredentials->setIdentifier($identifier);
        $tokenCredentials->setSecret($secret);

//        $uploadStartTimeInSeconds = isset($params['startDate']) && !empty($params['startDate']) ? strtotime(date('Y-m-d H:i', strtotime($params['startDate']))) : (new DateTime())->modify('-1 day')->getTimestamp(); // start time in seconds
        $uploadStartTimeInSeconds = strtotime($params['endDate'] . "-1 days");
//        (new DateTime())->modify('-1 day')->getTimestamp(); // start time in seconds
        $uploadEndTimeInSeconds = isset($params['endDate']) && !empty($params['endDate']) ? strtotime(date('Y-m-d H:i', strtotime($params['endDate']))) : (new DateTime())->getTimestamp(); // end time in seconds

        $paramsDate = [
            'uploadStartTimeInSeconds' => $uploadStartTimeInSeconds, // time in seconds utc
            'uploadEndTimeInSeconds' => $uploadEndTimeInSeconds // time in seconds utc
        ];

        // $server->backfillActivitySummary($tokenCredentials, $params);
        $summary = $server->getActivitySummary($tokenCredentials, $paramsDate);
        $data = json_decode($summary, true);
//print_die($data);
        $cek = $db->select("m_event.*,m_kategori_strava.nama as type_strava ")
            ->from("m_event")
            ->leftJoin("m_kategori", "m_kategori.id = m_event.m_kategori_id")
            ->leftJoin("m_kategori_strava", "m_kategori_strava.id = m_kategori.m_kategori_strava_id")
            ->where("m_event.id", "=", $params['m_event_id'])
            ->find();

        $cek_data = $db->select("id_garmin")
            ->from("m_event_data")
            ->where("m_event_data.m_event_id", "=", $params['m_event_id'])
            ->findAll();
        $cek_data = stringToArray(arrayToString(json_decode(json_encode($cek_data), true), 'id_garmin'));

        $kategori = $db->select("m_kategori.*, m_kategori_strava.nama as kategori_strava")
            ->from("m_kategori")
            ->leftJoin("m_kategori_strava","m_kategori_strava.id = m_kategori.m_kategori_strava_id")
            ->where("is_parent", "=", 1)
            ->customWhere("m_kategori.id IN (" . $cek->m_kategori_id . ")", "AND")
            ->findAll();


        $arr = [];
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                if (!isset($value['manual'])) {
                    $convert = date("Y-m-d", ($value['startTimeInSeconds'] + $value['durationInSeconds']));
                    if ($cek->tgl_race_mulai <= $convert && $cek->tgl_race_selesai >= $convert) {
                        if (!in_array($value['activityId'], $cek_data)) {
//                        if ($value['manual'] === false) {
                            if (in_array($value['activityType'], $arrCek)) {
                                $arr[$key]['id_garmin'] = $value['activityId'];
                                $arr[$key]['type'] = $value['activityType'];
                                $arr[$key]['calories'] = isset($value['activeKilocalories']) ? $value['activeKilocalories'] : 0;
                                $arr[$key]['m_user_id'] = $user->id;
                                $arr[$key]['m_event_id'] = $params['m_event_id'];
                                $arr[$key]['jarak'] = isset($value['distanceInMeters']) ? round(m_to_km($value['distanceInMeters']), 2) : 0;
                                $arr[$key]['waktu'] = isset($value['durationInSeconds']) ? gmdate("H:i:s", $value['durationInSeconds']) : null;
                                $arr[$key]['tgl'] = date("d M Y H:i:s", $value['startTimeInSeconds']);

                                if (isset($value['startTimeInSeconds']) && isset($value['durationInSeconds'])) {
                                    $arr[$key]['tgl_end_activity'] = date("d M Y H:i:s", ($value['startTimeInSeconds'] + $value['durationInSeconds']));
                                }
                                $arr[$key]['tgl_start'] = date("Y-m-d H:i:s", $value['startTimeInSeconds']);
                                $arr[$key]['status'] = 'approve';
                                $arr[$key]['is_garmin'] = 1;
                            }
                        }
                    }
                }
            }
        }

//        if (!empty($data)) {
//            foreach ($data as $key => $value) {
//                // if ($value['manual'] === false) {
//                if (in_array($value['activityType'], $arrCek)) {
//                    $arr[$key]['id_garmin'] = $value['activityId'];
//                    $arr[$key]['type'] = $value['activityType'];
//                    $arr[$key]['calories'] = isset($value['activeKilocalories']) ? $value['activeKilocalories'] : 0;
//                    $arr[$key]['m_user_id'] = $user->id;
//                    $arr[$key]['m_event_id'] = $params['m_event_id'];
//                    $arr[$key]['jarak'] = isset($value['distanceInMeters']) ? round(m_to_km($value['distanceInMeters']), 2) : 0;
//                    $arr[$key]['waktu'] = isset($value['durationInSeconds']) ? gmdate("H:i:s", $value['durationInSeconds']) : null;
//                    $arr[$key]['tgl'] = date("d M Y H:i:s", $value['startTimeInSeconds']);
//                    $arr[$key]['tgl_start'] = date("Y-m-d H:i:s", $value['startTimeInSeconds']);
//                    $arr[$key]['status'] = 'approve';
//                    $arr[$key]['is_garmin'] = 1;
//                }
//                // }
//            }
//        }
//        print_die($arr);
        return successResponse($response, array_values($arr));
    } catch (\Throwable $th) {
        // catch your exception here
        $error = $th->getMessage();
        return unprocessResponse($response, $error);
    }
})->setName("callback");


$app->get('/connect/summary_activity_free_run', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    try {
        $params['m_kategori_garmin_id'] = isset($params['m_kategori_garmin_id']) && !empty($params['m_kategori_garmin_id']) ? $params['m_kategori_garmin_id'] : 0;

        $user = $db->select('m_user.*')
            ->from('m_user')
            ->where('id', '=', $params['m_user_id'])
            ->find();

        $induk = $db->select("m_kategori_garmin.id, m_kategori_garmin.nama induk_garmin ")
            ->from("m_kategori_garmin")
            ->where("m_kategori_garmin.id", "=", $params['m_kategori_garmin_id'])
            ->find();
        $parent = $db->select("m_kategori_garmin.*")
            ->from("m_kategori_garmin")
            ->where("m_kategori_garmin.parent", "=", $induk->induk_garmin)
            ->findAll();

        // ej($parent);
        $arrCek = [];
        if (!empty($induk)) {
            foreach ($parent as $k => $v) {
                $v->nama = strtoupper(strtolower($v->nama));
                $arrCek[] = str_replace(" ", "_", $v->nama);
            }
            array_unshift($arrCek, $induk->induk_garmin);
        }

        $config = getConfig();
        $server = new Stoufa\GarminApi\GarminApi($config);
        $tokenCredentials = new League\OAuth1\Client\Credentials\TokenCredentials();
        $identifier = $user->garmin_identifier;
        $secret = $user->garmin_secret;
        $tokenCredentials->setIdentifier($identifier);
        $tokenCredentials->setSecret($secret);

//        $uploadStartTimeInSeconds = isset($params['startDate']) && !empty($params['startDate']) ? strtotime(date('Y-m-d H:i', strtotime($params['startDate']))) : (new DateTime())->modify('-1 day')->getTimestamp(); // start time in seconds
        $uploadStartTimeInSeconds = strtotime($params['endDate'] . "-1 days");
//        (new DateTime())->modify('-1 day')->getTimestamp(); // start time in seconds
        $uploadEndTimeInSeconds = isset($params['endDate']) && !empty($params['endDate']) ? strtotime(date('Y-m-d H:i', strtotime($params['endDate']))) : (new DateTime())->getTimestamp(); // end time in seconds

        $paramsDate = [
            'uploadStartTimeInSeconds' => $uploadStartTimeInSeconds, // time in seconds utc
            'uploadEndTimeInSeconds' => $uploadEndTimeInSeconds // time in seconds utc
        ];

        // $server->backfillActivitySummary($tokenCredentials, $params);
        $summary = $server->getActivitySummary($tokenCredentials, $paramsDate);
//        print_die($summary);
        $data = json_decode($summary, true);
//        print_die($data);
        $arr = [];
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                if (in_array($value['activityType'], $arrCek)) {
                    $arr[$key]['id_garmin'] = $value['activityId'];
                    $arr[$key]['type'] = $value['activityType'];
                    $arr[$key]['calories'] = isset($value['activeKilocalories']) ? $value['activeKilocalories'] : 0;
                    $arr[$key]['m_user_id'] = $user->id;
                    $arr[$key]['jarak'] = isset($value['distanceInMeters']) ? round(m_to_km($value['distanceInMeters']), 2) : 0;
                    $arr[$key]['waktu'] = isset($value['durationInSeconds']) ? gmdate("H:i:s", $value['durationInSeconds']) : null;
                    $arr[$key]['tgl'] = date("d M Y H:i:s", $value['startTimeInSeconds']);
                    $arr[$key]['status'] = 'approve';
                    $arr[$key]['is_garmin'] = 1;
                }
            }
        }
//        print_die($arr);
        return successResponse($response, array_values($arr));
    } catch (\Throwable $th) {
        // catch your exception here
        $error = $th->getMessage();
        return unprocessResponse($response, $error);
    }
})->setName("summary_activity_free_run");

$app->get('/connect/summary_detail', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    try {
        $user = $db->select('m_user.*')->from('m_user')->where('id', '=', $params['m_user_id'])->find();
        $config = getConfig();
        $server = new Stoufa\GarminApi\GarminApi($config);
        $tokenCredentials = new League\OAuth1\Client\Credentials\TokenCredentials();
        $identifier = $user->garmin_identifier;
        $secret = $user->garmin_secret;
        $tokenCredentials->setIdentifier($identifier);
        $tokenCredentials->setSecret($secret);

        $uploadStartTimeInSeconds = isset($params['startDate']) && !empty($params['startDate']) ? strtotime(date('Y-m-d H:i', strtotime($params['startDate']))) : (new DateTime())->modify('-1 day')->getTimestamp(); // start time in seconds 
        $uploadEndTimeInSeconds = isset($params['endDate']) && !empty($params['endDate']) ? strtotime(date('Y-m-d H:i', strtotime($params['endDate']))) : (new DateTime())->getTimestamp(); // end time in seconds

        $params = [
            'uploadStartTimeInSeconds' => $uploadStartTimeInSeconds, // time in seconds utc
            'uploadEndTimeInSeconds' => $uploadEndTimeInSeconds // time in seconds utc
        ];
        $summary = $server->getActivityDetailsSummary($tokenCredentials, $params);

        $data = json_decode($summary, true);
        // print_r($params);exit;

        return successResponse($response, $data);
    } catch (\Throwable $th) {
        // catch your exception here
        $error = $th->getMessage();
        return unprocessResponse($response, $error);
    }
})->setName("callback");


$app->post('/connect/saveActivity', function ($request, $response) {
    $params = $request->getParams();
//    $landa = new Landa();
    $db = $this->db;

    $cek_data = $db->select("id_garmin")
        ->from("m_event_data")
        ->findAll();
    $cek_data = stringToArray(arrayToString(json_decode(json_encode($cek_data), true), 'id_garmin'));

    $data = json_decode($params['list']);
    $detail = [];
    foreach ($data as $key => $value) {
        if (!in_array($value->id_garmin, $cek_data)) {
            $detail['id_garmin'] = $value->id_garmin;
            $detail['m_user_id'] = $params['m_user_id'];
            $detail['m_event_id'] = $params['m_event_id'];
            $detail['is_garmin'] = 1;
            $detail['calories'] = isset($value->calories) ? $value->calories : 0;
            $detail['type'] = isset($value->type) ? $value->type : null;
            $detail['jarak'] = isset($value->jarak) ? $value->jarak : 0;
            $detail['waktu'] = isset($value->waktu) ? $value->waktu : null;
            $detail['tgl'] = isset($value->tgl_start) ? date("Y-m-d H:i:s", strtotime($value->tgl_start)) : date("Y-m-d H:i:s");

            if (isset($value->startTimeInSeconds) && isset($value->durationInSeconds)) {
                $detail['tgl_end_activity'] = date("d M Y H:i:s", ($value->startTimeInSeconds + $value->durationInSeconds));
            }

            $detail['status'] = 'approve';
            $detail['device'] = isset($params['device']) ? $params['device'] : null;

            $model = $db->insert("m_event_data", $detail);
        }
    }

    return successResponse($response, []);

})->setName('saveActivity');


$app->post('/connect/saveActivityFreeRun', function ($request, $response) {
    $params = $request->getParams();
//    $landa = new Landa();
    $db = $this->db;

    $cek_data = $db->select("id_garmin")
        ->from("m_user_data")
        ->findAll();
    $cek_data = stringToArray(arrayToString(json_decode(json_encode($cek_data), true), 'id_garmin'));

    $data = json_decode($params['list']);
    $detail = [];
    foreach ($data as $key => $value) {
        if (!in_array($value->id_garmin, $cek_data)) {
            //    print_die($value);
            $detail['tipe'] = 'garmin';
            $detail['id_garmin'] = $value->id_garmin;
            $detail['m_user_id'] = $params['m_user_id'];
            $detail['calories'] = isset($value->calories) ? $value->calories : 0;
            $detail['type'] = isset($value->type) ? $value->type : null;
            $detail['jarak'] = isset($value->jarak) ? $value->jarak : 0;
            $detail['waktu'] = isset($value->waktu) ? $value->waktu : null;
            $detail['tgl'] = isset($value->tgl) ? date("Y-m-d H:i:s", strtotime($value->tgl)) : date("Y-m-d H:i:s");
            $detail['status'] = 'approve';
            $detail['m_kategori_id'] = isset($value->m_kategori_id) ? $value->m_kategori_id : null;
            $detail['device'] = isset($params['device']) ? $params['device'] : null;

            $model = $db->insert("m_user_data", $detail);
            // ej($model);
        }
    }
    return successResponse($response, $detail);

})->setName('saveActivityFreeRun');

$app->get('/connect/error', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;


    // return unprocessResponse($response, ['error']);

})->setName("error");
$app->get('/connect/success', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    // return unprocessResponse($response, ['success']);

})->setName("success");

$app->post('/connect/removeGarmin', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $model = $db->update("m_user", [
        'garmin_oauth_token' => null,
        'garmin_oauth_verifier' => null,
        'garmin_identifier' => null,
        'garmin_secret' => null,
        'is_callback' => 0,
        'is_garmin' => 0
    ], ["id" => $params['m_user_id']]);

    return successResponse($response, 'logout success');
})->setName("removeGarmin");

$app->get('/connect/getUser', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $user = $db->select('m_user.*')->from('m_user')->where('id', '=', $params['m_user_id'])->find();
    $config = getConfig();
    $server = new Stoufa\GarminApi\GarminApi($config);
    $tokenCredentials = new League\OAuth1\Client\Credentials\TokenCredentials();
    $identifier = $user->garmin_identifier;
    $secret = $user->garmin_secret;
    $tokenCredentials->setIdentifier($identifier);
    $tokenCredentials->setSecret($secret);

    // $data = [];
    $userId = $server->getUserUid($tokenCredentials);
    // $data['userId'] = $userId;
    $data2 = $server->getUserDetails($tokenCredentials);

    ej($data2);
    // return unprocessResponse($response, ['success']);

})->setName("user");

$app->get('/connect/tes_summary_activity', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
        $user = $db->select('m_user.*')
            ->from('m_user')
            ->where('id', '=', $params['m_user_id'])
            ->find();

        $induk = $db->select("m_kategori.m_kategori_garmin_id, m_kategori.nama, m_kategori_garmin.nama as induk_garmin ")
            ->from("m_event")
            ->leftJoin("m_kategori", "m_kategori.id = m_event.m_kategori_id")
            ->leftJoin("m_kategori_garmin", "m_kategori_garmin.id = m_kategori.m_kategori_garmin_id")
            ->where("m_event.id", "=", $params['m_event_id'])
            ->find();

        $parent = $db->select("m_kategori_garmin.*")
            ->from("m_kategori_garmin")
            ->where("m_kategori_garmin.parent", "=", $induk->induk_garmin)
            ->findAll();

        $arrCek = [];
        if (!empty($induk)) {
            foreach ($parent as $k => $v) {
                $arrCek[] = str_replace(" ", "_", $v->nama);
            }
            array_unshift($arrCek, $induk->induk_garmin);
        }

        // ej($user);
//        $cek = arrayToString($arrCek);
// print_die($cek);
        $config = getConfig();
        $server = new Stoufa\GarminApi\GarminApi($config);
        $tokenCredentials = new League\OAuth1\Client\Credentials\TokenCredentials();
        $identifier = $user->garmin_identifier;
        $secret = $user->garmin_secret;
        $tokenCredentials->setIdentifier($identifier);
        $tokenCredentials->setSecret($secret);

//        $uploadStartTimeInSeconds = isset($params['startDate']) && !empty($params['startDate']) ? strtotime(date('Y-m-d H:i', strtotime($params['startDate']))) : (new DateTime())->modify('-1 day')->getTimestamp(); // start time in seconds
        $uploadStartTimeInSeconds = strtotime($params['endDate'] . "-1 days");
//        (new DateTime())->modify('-1 day')->getTimestamp(); // start time in seconds
        $uploadEndTimeInSeconds = isset($params['endDate']) && !empty($params['endDate']) ? strtotime(date('Y-m-d H:i', strtotime($params['endDate']))) : (new DateTime())->getTimestamp(); // end time in seconds

        $paramsDate = [
            'uploadStartTimeInSeconds' => $uploadStartTimeInSeconds, // time in seconds utc
            'uploadEndTimeInSeconds' => $uploadEndTimeInSeconds // time in seconds utc
        ];

        // $server->backfillActivitySummary($tokenCredentials, $params);
        $summary = $server->getActivitySummary($tokenCredentials, $paramsDate);
//        print_die($summary);
        $data = json_decode($summary, true);
//        print_die($data);


        $cek = $db->select("m_event.*,m_kategori_strava.nama as type_strava ")
            ->from("m_event")
            ->leftJoin("m_kategori", "m_kategori.id = m_event.m_kategori_id")
            ->leftJoin("m_kategori_strava", "m_kategori_strava.id = m_kategori.m_kategori_strava_id")
            ->where("m_event.id", "=", $params['m_event_id'])
            ->find();

        $cek_data = $db->select("id_garmin")
            ->from("m_event_data")
            ->where("m_event_data.m_event_id", "=", $params['m_event_id'])
            ->findAll();
        $cek_data = stringToArray(arrayToString(json_decode(json_encode($cek_data), true), 'id_garmin'));


        $arr = [];
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $convert = date("Y-m-d", ($value['startTimeInSeconds'] + $value['durationInSeconds']));
                if ($cek->tgl_race_mulai <= $convert && $cek->tgl_race_selesai >= $convert) {
                    if (!in_array($value['activityId'], $cek_data)) {
//                        if ($value['manual'] === false) {
                        if (in_array($value['activityType'], $arrCek)) {
                            $arr[$key]['id_garmin'] = $value['activityId'];
                            $arr[$key]['type'] = $value['activityType'];
                            $arr[$key]['calories'] = isset($value['activeKilocalories']) ? $value['activeKilocalories'] : 0;
                            $arr[$key]['m_user_id'] = $user->id;
                            $arr[$key]['m_event_id'] = $params['m_event_id'];
                            $arr[$key]['jarak'] = isset($value['distanceInMeters']) ? round(m_to_km($value['distanceInMeters']), 2) : 0;
                            $arr[$key]['waktu'] = isset($value['durationInSeconds']) ? gmdate("H:i:s", $value['durationInSeconds']) : null;
                            $arr[$key]['tgl'] = date("d M Y H:i:s", $value['startTimeInSeconds']);
                            $arr[$key]['tgl_end_activity'] = date("d M Y H:i:s", ($value['startTimeInSeconds'] + $value['durationInSeconds']));
                            $arr[$key]['tgl_start'] = date("Y-m-d H:i:s", $value['startTimeInSeconds']);
                            $arr[$key]['status'] = 'approve';
                            $arr[$key]['is_garmin'] = 1;
                        }
//                        }
                    }
                }
            }
        }
//        print_die($arr);
        return successResponse($response, array_values($arr));
    } catch (\Throwable $th) {
        // catch your exception here
        $error = $th->getMessage();
        return unprocessResponse($response, $error);
    }
})->setName("callback");