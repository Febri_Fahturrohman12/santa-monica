<?php
$app->get('/', function ($request, $responsep) {

});

$app->get('/site/session', function ($request, $response) {
    if (isset($_SESSION['user']['id'])) {
        return successResponse($response, $_SESSION);
    }
    return unprocessResponse($response, ['undefined']);
})->setName("session");
$app->get('/site/coba', function ($request, $response) {
    
    return successResponse($response, ['coba']);
})->setName("coba");
$app->get('/site/auth', function ($request, $response) {
    // try
    // {

        $config = array(
            'identifier'     => getenv('GARMIN_KEY'),
            'secret'         => getenv('GARMIN_SECRET'),
            'callback_uri'   => getenv('GARMIN_CALLBACK_URI') 
        );

        $server = new Stoufa\GarminApi\GarminApi($config);

        // Retreive temporary credentials from server 
        $temporaryCredentials = $server->getTemporaryCredentials();

        // Save temporary crendentials in session to use later to retreive authorization token
        $_SESSION['temporaryCredentials'] = $temporaryCredentials;

        // Get authorization link 
        $link = $server->getAuthorizationUrl($temporaryCredentials);
    // }
    // catch (\Throwable $th)
    // {
    //     // catch your exception here
    //     $error = $th->getMessage();
    // }

    return successResponse($response, [$link]);
})->setName("coba");
$app->post('/site/login', function ($request, $response) {
    $params = $request->getParams();
    $sql    = $this->db;

    $username = isset($params['username']) ? $params['username'] : '';
    $password = isset($params['password']) ? $params['password'] : '';

    $model = $sql->select("m_pengguna.*,m_pengguna_akses.akses")
        ->from("m_pengguna")
        ->where("username", "=", $username)
        ->andWhere("password", "=", sha1($password))
        ->leftJoin("m_pengguna_akses", "m_pengguna_akses.id = m_pengguna.m_pengguna_akses_id")
        ->find();

    if (!empty($model)) {
        $_SESSION['user']['id']                  = $model->id;
        $_SESSION['user']['username']            = $model->username;
        $_SESSION['user']['nama']                = $model->nama;
        $_SESSION['user']['m_pengguna_akses_id'] = $model->m_pengguna_akses_id;
        $_SESSION['user']['akses']               = json_decode($model->akses);

        return successResponse($response, $_SESSION);
    }
    return unprocessResponse($response, ['Authentication Systems gagal, username atau password Anda salah.']);
})->setName("login");

$app->get('/site/logout', function () {
    session_destroy();
})->setName("logout");
